
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->



    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Rplus Demand Sense</title>
        <link rel="icon" type="image/ico" href="resources/assets/images/favicon.ico" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">




        <!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
        <!-- vendor css files -->
        <link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"	rel="stylesheet">
        <link rel="stylesheet" href="resources/assets/css/vendor/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="resources/assets/css/vendor/animate.css">
        <link rel="stylesheet" href="resources/assets/css/vendor/font-awesome.min.css">
        <link rel="stylesheet" href="resources/assets/js/vendor/animsition/css/animsition.min.css">
		 <link rel="stylesheet" href="resources/assets/js/vendor/file-upload/css/jquery.fileupload.css">
        <link rel="stylesheet" href="resources/assets/js/vendor/file-upload/css/jquery.fileupload-ui.css">
        <!-- CSS adjustments for browsers with JavaScript disabled -->
        <noscript><link rel="stylesheet" href="resources/assets/js/vendor/file-upload/css/jquery.fileupload-noscript.css"></noscript>
        <noscript><link rel="stylesheet" href="resources/assets/js/vendor/file-upload/css/jquery.fileupload-ui-noscript.css"></noscript>
		
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/jquery.dataTables.min.css">
		
        <!-- project main css files -->
        <link rel="stylesheet" href="resources/assets/css/main.css">
        <!--/ stylesheets -->



        <!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
        <script src="resources/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <!--/ modernizr -->


	<script type="text/javascript">
	
	var myVar, btn;    
    function prog(){
    	
		$.ajax({
			   url: "/DemandSense/fileUpload/progressBarValues",
			   
			   error: function(xhr, status, error) {
			        console.log(status);
			        console.log(error);
			    },
			   success: function(data) {
				   
				   if($("#Upload_Exception").val() == "Exception"){
					   $(".prog").children().remove();
					   clearInterval(myVar); // stop the timer
				        $(".table  .load-btn").each(function(){
				        	if($(this).attr("id") == btn){
								$(this).text("Load Data");
							}
				        	$(this).prop('disabled', false);
						});
				        $(".fileError").remove();
			        	$( "<p class='fileError' style='color:red;'>Oops!!! Some Exception occured.</p>" ).insertAfter("#"+btn+"Prog");
				   }else{
					   var count = 0;
					   if(data.totalRowCount == 0){
						   count = 0;
					   }else{
						   count = 100 /data.totalRowCount;
					   }
						var progress = Math.round(data.currentRowNumber * count );
						console.log(data.currentRowNumber+"----"+data.totalRowCount);
						$("#progresslabel").css({width:progress*3+"px", background:'green',"position":"absolute", "text-align":"center","color":"#fff"}).text(progress+"%");
						if(progress == 100){
						 stopFunction();
						 $(".prog").children().remove();
						 $(".table input[type=file]").val('');
						 $('<span id="sucess">Sucessfully Uploaded!</span>').clone().appendTo("#"+btn+"Prog").css({color:'green'});
						}
				   }
				   
				    
			   },
			   type: 'POST'
			});
		
		
    }
    function stopFunction(){
        clearInterval(myVar); // stop the timer
        $(".table  .load-btn").each(function(){
        	if($(this).attr("id") == btn){
				$(this).text("Completed");
			}
        	$(this).prop('disabled', false);
		});
    }
    
    
//"/DemandSense/viewdata/product"

function UploadFile2DB(fldObj)
{
	
	$(".table").find(".fileError").remove();
	$(".table").find("#sucess").remove();

	var XHR = new XMLHttpRequest();
	var buttonName = btn = fldObj.name;
	
	
	if(buttonName == "prodMaster"){ // "bfileInput1"| Product Master
		StrURL = "/DemandSense/fileUpload/loadProductMasterFileToDB";
	}
	 else if(buttonName == "locationMaster"){ // "bfileInput2"| Location Master
		StrURL = "/DemandSense/fileUpload/loadLocationMasterFileToDB";
	} 
	else if(buttonName == "customerMaster"){ // "bfileInput3"| Customer Master
		StrURL = "/DemandSense/fileUpload/loadCustomerMasterFileToDB";
	} 
	else if(buttonName == "socialMaster"){// bfileInput4"| Social Master
		StrURL = "/DemandSense/fileUpload/loadFactSocialMasterFileToDB";
	}
	else if(buttonName == "timeMaster"){ // "bfileInput5"| Time Master
		StrURL = "/DemandSense/fileUpload/loadTimeMasterFileToDB";
	}
	else if(buttonName == "eventData"){ // "bfileInput6"| Event data
		StrURL = "/DemandSense/fileUpload/loadEventDataFileToDB";
	}
	else if(buttonName == "inventoryData"){ // "bfileInput7"| Inventory  data
		StrURL = "/DemandSense/loadInventoryDataFileToDB";
	}
	else if(buttonName == "externalData"){ // "bfileInput7"| External  Data
		StrURL = "/DemandSense/fileUpload/loadExternalDataFileToDB";
	}
	else if(buttonName == "priceData"){ //"bfileInput8"| Price  data
		StrURL = "/DemandSense/fileUpload/loadPriceDataFileToDB";
	}
	else if(buttonName == "promotionData") {// "bfileInput9"| Promotion data
		StrURL = "/DemandSense/fileUpload/loadPromotionDataFileToDB";
	} 
	else if(buttonName == "salesData") {// "bfileInput10"| Sales data
		StrURL = "/DemandSense/fileUpload/loadSalesDataFileToDB";
	}
	else if(buttonName == "socialData"){ // "bfileInput11"| Social data
		StrURL = "/DemandSense/fileUpload/loadDimSocialDataFileToDB";
	}
	var Fil_Obj = document.getElementById(buttonName);
	var fil = Fil_Obj.files;
	var formData = new FormData();
	
	if(fil.length != 0){
		$(".table .load-btn").each(function(){
			if($(this).attr("id") == buttonName){
				$(this).text("Loading...");
				$(this).prop('disabled', true);
			}else{
				$(this).attr("disabled", "disabled");
			}
		});
		
		
	    
		$(".prog").children().remove();
		$('<br><div id="progressbar" class="progressbar"><div id="progresslabel" class="progressbarlabel"></div></div>').clone().appendTo("#"+fldObj.id+"Prog");
		
	
		
		formData.append(Fil_Obj.name, fil[0], fil[0].name);
		
		XHR.open("POST", StrURL, true);
		XHR.onreadystatechange=function()
		{
			//alert(StrURL + ": outside if condition");
		    if(XHR.readyState==4 && XHR.status==200)
		    {
		    	//stopFunction();
		    }
		}
		XHR.send(formData); 
		    

	        myVar = setInterval("prog()", 400);
	     
			
	}else{
		$(".fileError").remove();
		$("#sucess").remove();
		$( "<p class='fileError' style='color:red;'>Please Select file</p>" ).insertAfter("#"+buttonName);
	}

	
	return false;
}



</script>


    </head>





    <body id="minovate" class="appWrapper sidebar-sm-forced">
    
    
    <div style="display:block"><%-- <%=session.getAttribute("uploadError") %> --%></div>
	<%-- <h2><%=session.getAttribute("uploadError") %></h2> --%>
    
  <%--   <%
//allow access only if session exists
String user = null;
if(session.getAttribute("userName") == null){
	session.setAttribute("notLogin", "please login first");
	%>
	<jsp:forward page="login.jsp"></jsp:forward>
	<%
}else
	user = (String) session.getAttribute("userName");
	session.setMaxInactiveInterval(30*60);
%>
 --%>

	


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->












        <!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
        <div id="wrap" class="animsition">

			<input type="hidden" id="Upload_Exception" value="<%=session.getAttribute("uploadError") %>">


            <!-- ===============================================
            ================= HEADER Content ===================
            ================================================ -->
            <section id="header" class="scheme-light">
                <header class="clearfix">

                    <!-- Branding -->
                    <div class="branding scheme-light">
                        <a class="brand" href="index.html">
						
                            <span><strong>Demand</strong>Sense</span>
                        </a>
                        <a href="#" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
                    </div>
                    <!-- Branding end -->



                    <!-- Left-side navigation -->
                    <ul class="nav-left pull-left list-unstyled list-inline">
                        <li class="sidebar-collapse divided-right">
                            <a href="#" class="collapse-sidebar">
                                <i class="fa fa-outdent"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- Left-side navigation end -->




                    <!-- Search -->
                  <!--   <div class="search" id="main-search">
                        <input type="text" class="form-control underline-input" placeholder="Search...">
                    </div> -->
                    <!-- Search end -->




                   <img src="resources/assets/images/paper_boat_logo.png" alt="Image" class="img-circle size-50x50" style="margin-left: 30%;">
				<!-- Right-side navigation -->
				<ul class="nav-right pull-right list-inline">

					<li class="dropdown nav-profile"><a
						class="dropdown-toggle" data-toggle="dropdown"> <!-- <img
							src="resources/assets/images/Default_logo.jpg" alt="Image"
							class="img-circle size-30x30"> --> 
							<span>Welcome <%=session.getAttribute("loggedUserName") %><i class="fa fa-angle-down"></i></span>
					</a>


						<ul class="dropdown-menu animated littleFadeInRight" role="menu">

							
							<li><a href="profile"> <i class="fa fa-cog"></i>Profile
							</a></li>
							<li><a href="logs"> <i class="fa fa-list-alt"></i>Usage Log
							</a></li>
							<li class="divider"></li>
							<li><a href="/DemandSense/user/logout"> <i class="fa fa-sign-out"></i>Logout
							</a></li>

						</ul></li>
				</ul>
                    <!-- Right-side navigation end -->



                </header>

            </section>
            <!--/ HEADER Content  -->





            <!-- =================================================
            ================= CONTROLS Content ===================
            ================================================== -->
            <div id="controls">





                <!-- ================================================
                ================= SIDEBAR Content ===================
                ================================================= -->
                <aside id="sidebar">


                    <div id="sidebar-wrap">

                        <div class="panel-group slim-scroll" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#sidebarNav">
                                            Navigation <i class="fa fa-angle-up"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">


                                        <!-- ===================================================
                                        ================= NAVIGATION Content ===================
                                        ==================================================== -->
                                       <ul id="navigation">
                                            <li><a href="index"><i class="fa fa-dashboard"></i> <span>Forecast Analysis</span></a></li>
                                            <li><a href="DataExplorer"><i class="fa fa-table"></i> <span>Data Explorer</span></a></li>
                                             <!-- <li><a href="DataAudit"><i class="fa fa-search"></i> <span>Data Audit</span></a></li>
                                             -->
                                            <% if(session.getAttribute("userType").equals("user_admin") || session.getAttribute("userType").equals("super_admin")){ %>
                                            <li class="active open"><a href="ExtractionApp"><i class="fa fa-upload"></i> <span>Data Upload</span></a></li>
											<%} %>
											<li><a href="report"><i class="fa fa-desktop"></i> <span>Report</span><span class="label label-success">new</span></a></li>
											<li><a href="support"><i class="fa fa-question-circle"></i> <span>Support</span><span class="label label-success">new</span></a></li>
											<!-- <li><a href="NewProductLaunch"><i class="fa fa-desktop"></i> <span>New Product Launch</span><span class="label label-success">new</span></a></li>
                                              <li><a href="#"><i class="fa fa-shopping-cart"></i> <span>Internal Experts</span> <span class="label label-success">new</span></a>                                          </li>
                                            <li><a href="#"><i class="fa fa-desktop"></i> <span>Prescriptive Analysis</span><span class="label label-success">new</span></a></li>
                                          -->
										   </ul>
                                        <!--/ NAVIGATION Content -->
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </aside>
                <!--/ SIDEBAR Content -->





                <!-- =================================================
                ================= RIGHTBAR Content ===================
                ================================================== -->
                
                <!--/ RIGHTBAR Content -->




            </div>
            <!--/ CONTROLS Content -->




            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
            <section id="content">

                <div class="page page-sidebar-sm-layout">

                    <div class="pageheader">

                       <div class="page-bar">

                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html"><i class="fa fa-home"></i> Rplus</a>
                                </li>
                                <li>
                                    <a href="#">Extraction App</a>
                                </li>
                            </ul>
                            
                        </div>

                    </div>

                         <!-- col -->
                <div class="col-md-12">

                            <!-- tile -->
                            <section class="tile">

                                <!-- tile header -->
                                <div class="tile-header dvd dvd-btm">
                                <div class="tile-toggle" style="cursor: pointer;">
                                    <h1><strong>Flat File Upload</strong></h1> </div>
                                    <ul class="controls">
									<li> <a role="button" tabindex="0" class="fileUploadRefresh">
                                                        <i class="fa fa-refresh"></i> Refresh
                                                    </a>
                                    </li>
									 <li> <a role="button" tabindex="0" class="tile-toggle">
                                                        <span class="minimize"><i class="fa fa-angle-down"></i></span>
                                                        <span class="expand"><i class="fa fa-angle-up"></i></span>
                                                    </a>
                                    </li>
									 </ul>
									
                                </div>
                                <!-- /tile header -->

                                <!-- tile body -->
							  <div class="tile-body">
					  <div class="tile-body p-0">
								<div class="table-responsive">
								<!-- <form action="/DemandSense" enctype="multipart/form-data"> -->
                                    <table class="table">
                                    <tbody>
                                        <thead>
                                        <tr>
                                        <td>Product Master:</td>
        								<td><input class="input-file uniform_on" id="prodMaster" name="prodMaster" type="file" >
        								<div class="prog" id="prodMasterProg"></div>
        								</td>
                                        <td>  <button type="button" class="btn btn-primary load-btn" id="prodMaster" name="prodMaster"  onclick="UploadFile2DB(this)">Load Data</button></td>
                                        <td>  <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="viewData('product')">View Data</button></td>
                                        </tr>
                                        </thead>
                                   
                                        <tr>
                                         <!-- <form action="/DemandSense" enctype="multipart/form-data">  -->
                                        <div>
                                        
                                            <td>Location Master:</td>
                                            <td><input class="input-file uniform_on" id="locationMaster" name="locationMaster" type="file" >
                                            <div class="prog" id="locationMasterProg"></div>
                                            </td>
                                            <td> <button type="submit" class="btn btn-primary load-btn"  id="locationMaster" name="locationMaster" onclick="UploadFile2DB(this)">Load Data</button></td>
                                            <td> <button type="submit" class="btn btn-primary"  data-toggle="modal" data-target="#myModal" onclick="viewData('location')">View Data</button></td>
                                        
                                            </div>
                                            <!--  </form>  -->
                                        </tr>
                                        
                                        <!-- <form action="/DemandSense" enctype="multipart/form-data">-->
                                        <!-- <tr>
                                            <td>Customer Master:</td>
                                            <td><input class="input-file uniform_on" id="customerMaster" name="customerMaster" type="file" >
                                            <div class="prog" id="customerMasterProg"></div>
                                            </td>
                                            <td> <button type="submit" class="btn btn-primary load-btn" id="customerMaster" name="customerMaster"  onclick="UploadFile2DB(this)">Load Data</button></td>
                                            <td> <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="viewData('customer')">View Data</button></td>
                                        </tr>
                                        </form> 
                                        <tr>
                                           <td>Social Master:</td>
                                            <td><input class="input-file uniform_on" id="socialMaster" name="socialMaster" type="file" >
                                            <div class="prog" id="socialMasterProg"></div>
                                            </td>
                                            <td> <button type="submit" class="btn btn-primary load-btn" id="socialMaster" name="socialMaster"  onclick="UploadFile2DB(this)">Load Data</button></td>
                                            <td> <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="viewData('dimSocial')">View Data</button></td>
                                        </tr>
                                        <tr>
                                            <td>Time Master:</td>
                                            <td><input class="input-file uniform_on" id="timeMaster" name="timeMaster" type="file" >
                                            <div class="prog" id="timeMasterProg"></div>
                                            </td>
                                            <td> <button type="submit" class="btn btn-primary load-btn" id="timeMaster" name="timeMaster"  onclick="UploadFile2DB(this)">Load Data</button></td>
                                            <td> <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="viewData('time')">View Data</button></td>
                                        </tr>
                                        <tr>
                                            <td>Event Data:</td>
                                            <td><input class="input-file uniform_on" id="eventData" name="eventData" type="file" >
                                            <div class="prog" id="eventDataProg"></div>
                                            </td>
                                            <td> <button type="submit" class="btn btn-primary load-btn" id="eventData" name="eventData"  onclick="UploadFile2DB(this)">Load Data</button></td>
                                            <td> <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="viewData('event')">View Data</button></td>
                                        </tr>
                                        <tr>
                                            <td>Inventory Data:</td>
                                            <td><input class="input-file uniform_on" id="inventoryData" name="inventoryData" type="file" >
                                            <div class="prog" id="inventoryDataProg"></div>
                                            </td>
                                            <td> <button type="submit" class="btn btn-primary load-btn"  id="inventoryData"  name="inventoryData"onclick="UploadFile2DB(this)">Load Data</button></td>
                                            <td> <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="viewData('inventory')">View Data</button></td>
                                        </tr>
                                        <tr>
                                            <td>External Data:</td>
                                            <td><input class="input-file uniform_on" id="externalData" name="externalData"  type="file" >
                                            <div class="prog" id="externalDataProg"></div>
                                            </td>
                                            <td> <button type="submit" class="btn btn-primary load-btn" id="externalData"  name="externalData" onclick="UploadFile2DB(this)">Load Data</button></td>
                                            <td> <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="viewData('external')">View Data</button></td>
                                        </tr>
                                        <tr>
                                            <td>Price Data:</td>
                                            <td><input class="input-file uniform_on" id="priceData" name="priceData"  type="file" >
                                            <div class="prog" id="priceDataProg"></div>
                                            </td>
                                            <td> <button type="submit" class="btn btn-primary load-btn"  id="priceData" name="priceData" onclick="UploadFile2DB(this)">Load Data</button></td>
                                            <td> <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="viewData('price')">View Data</button></td>
                                        </tr>
                                        <tr>
                                            <td>Promotion Data:</td>
                                            <td><input class="input-file uniform_on" id="promotionData" name="promotionData"  type="file" >
                                            <div class="prog" id="promotionDataProg"></div>
                                            </td>
                                            <td> <button type="submit" class="btn btn-primary load-btn" id="promotionData" name="promotionData"  onclick="UploadFile2DB(this)">Load Data</button></td>
                                            <td> <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="viewData('promotion')">View Data</button></td>
                                        </tr> -->
                                        <tr>
                                            <td>Sales Data:</td>
                                            <td><input class="input-file uniform_on" id="salesData" name="salesData" type="file" >
                                            <div class="prog" id="salesDataProg"></div>
                                            </td>
                                            <td> <button type="submit" class="btn btn-primary load-btn" id="salesData"  name="salesData" onclick="UploadFile2DB(this)">Load Data</button></td>
                                            <td> <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="viewData('sales')">View Data</button></td>
                                        </tr>
                                        <!-- <tr>
                                            <td>Social Data:</td>
                                            <td><input class="input-file uniform_on" id="socialData" name="socialData"  type="file" >
                                            <div class="prog" id="socialDataProg"></div>
                                            </td>
                                            <td> <button type="submit" class="btn btn-primary load-btn" id="socialData" name="socialData"  onclick="UploadFile2DB(this)">Load Data</button></td>
                                            <td> <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="viewData('factSocial')">View Data</button></td>
                                        </tr> -->
                                        </tbody>
                                    </table>
                                 <!-- </form> -->
								</div>
								</div>	
                    
                                <!-- /tile body -->

                            </section>
                            <!-- /tile -->

                        </div>
                        <!-- /selection end --> 

    <!-- Forecast -->
                <div class="col-md-12">

                            <!-- tile -->
                            <section class="tile">

                                <!-- tile header -->
                                <div class="tile-header dvd dvd-btm">
                                <div class="tile-toggle" style="cursor: pointer;">
                                    <h1><strong>API's</strong></h1></div>
                                    <ul class="controls">
									<li> <a role="button" tabindex="0" class="tile-refresh">
                                                        <i class="fa fa-refresh"></i> Refresh
                                                    </a>
                                    </li>
									 <li> <a role="button" tabindex="0" class="tile-toggle">
                                                        <span class="minimize"><i class="fa fa-angle-down"></i></span>
                                                        <span class="expand"><i class="fa fa-angle-up"></i></span>
                                                    </a>
                                    </li>
									 </ul>
									
                                </div>
                                <!-- /tile header -->

                                <!-- tile body -->
                                <div class="tile-body">
					  

								</div>	
                                <!-- /tile body -->

                            </section>
                            <!-- /tile -->

                        </div>
                        <!-- /Forecast -->  
						
				<!-- Data Monitor -->
                <div class="col-md-12">

                            <!-- tile -->
                            <section class="tile">

                                <!-- tile header -->
                                <div class="tile-header dvd dvd-btm">
                                <div class="tile-toggle" style="cursor: pointer;">
                                    <h1><strong>Data Moniter</strong></h1></div>
                                    <ul class="controls">
									<li> <a role="button" tabindex="0" class="tile-refresh">
                                                        <i class="fa fa-refresh"></i> Refresh
                                                    </a>
                                    </li>
									 <li> <a role="button" tabindex="0" class="tile-toggle">
                                                        <span class="minimize"><i class="fa fa-angle-down"></i></span>
                                                        <span class="expand"><i class="fa fa-angle-up"></i></span>
                                                    </a>
                                    </li>
									 </ul>
									
                                </div>
                                <!-- /tile header -->

                                <!-- tile body -->
                                <div class="tile-body">
					    <div class="tile-body">
                                    <div class="table-responsive">
                                        
                                    </div>
                                </div>

								</div>	
                                <!-- /tile body -->

                            </section>
                            <!-- /tile -->

                        </div>
                        <!-- /Data Monitor -->		
				
                </div>
                
            </section>
            <!--/ CONTENT -->






        </div>
        <!--/ Application Content -->






  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">

                <table id="example10" cellspacing="0" width="100%" class="table-responsive display">
			        <thead>
			            <tr id="tableHead">
			            </tr>
			        </thead>
			    </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>












        <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
      
        <script src="resources/assets/js/jquery-ui.min.js"></script>
      
      	<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
      	
        <script src="resources/assets/js/vendor/file-upload/js/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="resources/assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')</script>
		
        <script src="resources/assets/js/vendor/bootstrap/bootstrap.min.js"></script>

        <script src="resources/assets/js/vendor/jRespond/jRespond.min.js"></script>

        <script src="resources/assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>

        <script src="resources/assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>

        <script src="resources/assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>
		
		<script src="resources/assets/js/vendor/daterangepicker/moment.min.js"></script>
		
		<script src="resources/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
   
         <script src="resources/assets/js/vendor/screenfull/screenfull.min.js"></script>

        <script src="resources/assets/js/vendor/countTo/jquery.countTo.js"></script>
		<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
        <script src="resources/assets/js/vendor/file-upload/js/vendor/jquery.ui.widget.js"></script>
        <!-- The Templates plugin is included to render the upload/download listings -->
        <script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
        <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
        <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
        <!-- The Canvas to Blob plugin is included for image resizing functionality -->
        <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
        <!-- blueimp Gallery script -->
        <script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
        <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
        <script src="resources/assets/js/vendor/file-upload/js/jquery.iframe-transport.js"></script>
        <!-- The basic File Upload plugin -->
        <script src="resources/assets/js/vendor/file-upload/js/jquery.fileupload.js"></script>
        <!-- The File Upload processing plugin -->
        <script src="resources/assets/js/vendor/file-upload/js/jquery.fileupload-process.js"></script>
        <!-- The File Upload image preview & resize plugin -->
        <script src="resources/assets/js/vendor/file-upload/js/jquery.fileupload-image.js"></script>
        <!-- The File Upload audio preview plugin -->
        <script src="resources/assets/js/vendor/file-upload/js/jquery.fileupload-audio.js"></script>
        <!-- The File Upload video preview plugin -->
        <script src="resources/assets/js/vendor/file-upload/js/jquery.fileupload-video.js"></script>
        <!-- The File Upload validation plugin -->
        <script src="resources/assets/js/vendor/file-upload/js/jquery.fileupload-validate.js"></script>
        <!-- The File Upload user interface plugin -->
        <script src="resources/assets/js/vendor/file-upload/js/jquery.fileupload-ui.js"></script>

      


        <!--/ vendor javascripts -->




        <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
        <script src="resources/assets/js/main.js"></script>
        <!--/ custom javascripts -->

		<!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
        <style>
 .progressbar {
  width:300px;
  height:21px;
}
.progressbarlabel {
  width:300px;
  height:21px;
  position:absolute;
  text-align:center;
  font-size:small;
}
  </style>
	<script>
	$(document).ready(function(){
		
		// refresh code
		$(".fileUploadRefresh").click(function(){
			$(".table .load-btn").each(function(){
				$(this).text("Load Data");
				$(this).prop('disabled', false);
			});
			$('.table input[type=file]').each(function(){
		        $(this).after($(this).clone(true)).remove();
		    });
			$(".fileError").remove();
			$(".prog").children().remove();
			clearInterval(myVar);
		}) 
		
		
		$(".table input[type=file]").on('change',function() {
			$(".fileError").remove();
			$("#sucess").remove();
			var loadBtn = $(this).attr('id');
	        if ($(this).val().split('.').pop().toLowerCase() == 'xls'|| $(this).val().split('.').pop().toLowerCase() == 'csv' ) {
	        	 
	 		    $(".table .load-btn").each(function(){
	 				if($(this).attr("id") == loadBtn){
	 					$(this).text("Load");
	 				}
	 			});
	        }else {
	        	$(".fileError").remove();
	        	$("#sucess").remove();
	        	$( "<p class='fileError' style='color:red;'>Format is not supported</p>" ).insertAfter("#"+loadBtn+"Prog");
	        	$(this).val('');
	        }
		   
		});
	})

	function viewData(th){
		// Progress Bar
		
		
		// View Data
		$(".modal-title").text("View "+th+"(s)");
		$(".modal-body").children().remove();
	    $.ajax(
	         {
	            url: "/DemandSense/viewdata/"+th,
	            type: "GET",
	                contentType: "application/json",
	                dataType: "json",
	                
	                error: function(msg) {
				        console.log(msg);
				        $(".modal-body").html("<p>"+msg+"</p>");
				    },
	               success: function (data) 
	               {   
	            	   var row_head = [];
	            	   if(data[0]==null){
	            		   $(".modal-body").html("<p>No Data available</p>");
	            	   }else{
		            	   $('<table id="example10" class="display" cellspacing="0" width="100%" class="table-responsive"> <thead><tr id="tableHead"></tr></thead></table>').clone().appendTo(".modal-body");
		            	   for(key in data[0]) {
		            		   row_head.push(key);
		            		   $("<td>"+key+"</td>").clone().appendTo("#tableHead");
		            		 }
		                     var oTable =  $('#example10').dataTable(
		                    {
		                       "fnDrawCallback": function ( oSettings )
		                         {
		                         if ( oSettings.bSorted || oSettings.bFiltered )
		                         {
		                             for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
		                                 { }
		                         }
		                     }, 
		                     "scrollY": 400,
		                     "pageLength": 10,
		                     "scrollX": true,  
		                     "bDestroy": true,
		                     });
		                     
		                   var row = [];             
		                   $.each(data, function(i, UserReport)    
		                       {
		                	   row[i] =  [];
		                	   var jj = 0;
	
		                	   $.each(UserReport, function(j, val){
		                		   row[i][jj] = val;
		                		   jj++;
		                	   });
		                	   
	                       }); 
	                           oTable.fnClearTable();
	                           oTable.fnAddData(row);
	            	   }
	               }
	               });
	}
            $(window).load(function(){


                /*
                 * jQuery File Upload Plugin JS Example 8.9.1
                 * https://github.com/blueimp/jQuery-File-Upload
                 *
                 * Copyright 2010, Sebastian Tschan
                 * https://blueimp.net
                 *
                 * Licensed under the MIT license:
                 * http://www.opensource.org/licenses/MIT
                 */

                /* global $, window */

                $(function () {
                    'use strict';

                    // Initialize the jQuery File Upload widget:
                    $('#fileupload').fileupload({
                        // Uncomment the following to send cross-domain cookies:
                        //xhrFields: {withCredentials: true},
                        url: 'resources/assets/js/vendor/file-upload/server/'
                    });

                    // Enable iframe cross-domain access via redirect option:
                    $('#fileupload').fileupload(
                            'option',
                            'redirect',
                            window.location.href.replace(
                                    /\/[^\/]*$/,
                                    'resources/assets/js/vendor/file-upload/cors/result.html?%s'
                            )
                    );

                    if (window.location.hostname === 'blueimp.github.io') {
                        // Demo settings:
                        $('#fileupload').fileupload('option', {
                            url: '//jquery-file-upload.appspot.com/',
                            // Enable image resizing, except for Android and Opera,
                            // which actually support image resizing, but fail to
                            // send Blob objects via XHR requests:
                            disableImageResize: /Android(?!.*Chrome)|Opera/
                                    .test(window.navigator.userAgent),
                            maxFileSize: 5000000,
                            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
                        });
                        // Upload server status check for browsers with CORS support:
                        if ($.support.cors) {
                            $.ajax({
                                url: '//jquery-file-upload.appspot.com/',
                                type: 'HEAD'
                            }).fail(function () {
                                $('<div class="alert alert-danger"/>')
                                        .text('Upload server currently unavailable - ' +
                                        new Date())
                                        .appendTo('#fileupload');
                            });
                        }
                    } else {
                        // Load existing files:
                        $('#fileupload').addClass('fileupload-processing');
                        $.ajax({
                            // Uncomment the following to send cross-domain cookies:
                            //xhrFields: {withCredentials: true},
                            url: $('#fileupload').fileupload('option', 'url'),
                            dataType: 'json',
                            context: $('#fileupload')[0]
                        }).always(function () {
                            $(this).removeClass('fileupload-processing');
                        }).done(function (result) {
                            $(this).fileupload('option', 'done')
                                    .call(this, $.Event('done'), {result: result});
                        });
                    }

                });



            });
        </script>			
        
        <!--/ Page Specific Scripts -->
<style>

.modal-dialog {
				width: 90%;
			}
.modal-title {
				    font-size: 15px;
			}
.modal-header {
			    background-color: #7BAE15;
			    color: #6F6F77;
				}
.modal-body {
    padding: 8px 6px 4px;
			}		
.modal-footer	{padding: 4px 7px 4px;
				 background-color: #7BAE15;
				 }
.btn-file {
	position: relative;
	overflow: hidden;
}

.btn-file input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	min-width: 100%;
	min-height: 100%;
	font-size: 100px;
	text-align: right;
	filter: alpha(opacity = 0);
	opacity: 0;
	outline: none;
	background: white;
	cursor: inherit;
	display: block;
}
.tile .tile-body { padding:0px;}
</style>



        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

    </body>
</html>
