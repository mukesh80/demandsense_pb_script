

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->



    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Rplus Demand Sense</title>
        <link rel="icon" type="image/ico" href="resources/assets/images/favicon.ico" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">




        <!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
        <!-- vendor css files -->
        <link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
        <link rel="stylesheet" href="resources/assets/css/vendor/bootstrap.min.css">
        <link rel="stylesheet" href="resources/assets/css/vendor/animate.css">
        <link rel="stylesheet" href="resources/assets/css/vendor/font-awesome.min.css">
        <link rel="stylesheet" href="resources/assets/js/vendor/animsition/css/animsition.min.css">
		<link rel="stylesheet" href="resources/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css">
		 <link rel="stylesheet" href="resources/assets/js/vendor/chosen/chosen.css">
        <link rel="stylesheet" href="resources/assets/js/vendor/summernote/summernote.css">


        <!-- project main css files -->
        <link rel="stylesheet" href="resources/assets/css/main.css">
        <!--/ stylesheets -->



        <!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
        <script type="text/javascript" src="http://gc.kis.scr.kaspersky-labs.com/1B74BD89-2A22-4B93-B451-1C9E1052A0EC/main.js" charset="UTF-8"></script><script src="resources/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <!--/ modernizr -->
<script type="text/javascript">

	function doValidation()	{
		var val = 0;
		var str1 = "", str2 = "";
		var flg = false;
		str1 = rJavaFrm.threadCount.value;
		if(str1.trim().length>0) {
			val = parseInt(str1, 10);
			if(val <= 0) {
				alert("Please Enter Thread Count")
				return flg;
			}
		}
		
		str1 = rJavaFrm.existRFiles.value;
		str2 = rJavaFrm.rFile.value;
		if(str1 == "select" && str2.trim().length > 0) {
			// Fine // New R file
		}
		else if(str1.trim().length > 0 && str2.trim().length == 0) {
			// Fine // Exist R file
		}
		else {
			alert("Check both Existing and New R File\r\n\"It Should be either one\"");
			return flg;
		}
		
		str1 = rJavaFrm.dbTableName.value;
		str2 = rJavaFrm.dbSchemaName.value;
		if(str1.trim().length > 0 && str2.trim().length > 0) {
			// Fine // Schema and Table present
		}
		else {
			alert("Check for both \"Table\" and \"Schema\" name");
			return flg;
		}
		flg = true;
		return flg;
	}
	
	function doSave() {
		alert("doSave");
		// rJavaFrm.R_File.value;
		var XHR = new XMLHttpRequest();
		var formData = new FormData();
		//var StrURL = "http://localhost:8080/DemandSense/rAdmin/rJavaMultiThreadSave";
		var StrURL = "/DemandSense/rAdmin/rJavaMultiThreadSave";
		
		formData.append("Thread_Cnt",rJavaFrm.Thread_Cnt.value);
		formData.append("Exist_R_Files",rJavaFrm.Exist_R_Files.value);
		formData.append("R_File",rJavaFrm.R_File.value);
		formData.append("Tbl_name",rJavaFrm.Tbl_name.value);
		formData.append("Schema_name",rJavaFrm.Schema_name.value);
		formData.append("JVM_Ram_size",rJavaFrm.JVM_Ram_size.value);
		formData.append("JVM_Heap_size",rJavaFrm.JVM_Heap_size.value);
		alert("Data income completed");
		XHR.open("POST", StrURL, true);
		XHR.onreadystatechange=function(){
		    if(XHR.readyState==4 && XHR.status==200) {
		    	//alert("REST Return: " + XHR.responseText);
		    	var res = XHR.responseText;
		    	if(res == "true")
		    		doExecuteR();
		    	else
		    		alert("Thread settings saved Successfully");
		    }
		}
		XHR.send(formData);
	}
	
	function doExecute() {
		//alert("doExecute");
		var XHR = new XMLHttpRequest();
		var formData = new FormData();
		var fileObj = document.getElementById("rFile");
		var fil = fileObj.files;
		copyFile2Hid(fileObj);
		//var StrURL = "http://localhost:8080/DemandSense/rAdmin/rJavaMultiThreadProc";
		var StrURL = "/DemandSense/rAdmin/rJavaMultiThreadProc";
		//alert("doExecute: " + rJavaFrm.hR_File.value);
		formData.append(fileObj.name, fil[0], fil[0].name);
		formData.append("hrFile", rJavaFrm.hrFile.value);
		formData.append("threadCount",rJavaFrm.threadCount.value);
		formData.append("dbTableName",rJavaFrm.dbTableName.value);
		formData.append("dbSchemaName",rJavaFrm.dbSchemaName.value);
		formData.append("jvmRamSize",rJavaFrm.jvmRamSize.value);
		formData.append("jvmHeapSize",rJavaFrm.jvmHeapSize.value);
		XHR.open("POST", StrURL, true);
		XHR.onreadystatechange=function(){
		    if(XHR.readyState==4 && XHR.status==200) {
		    	//alert("REST Return: " + XHR.responseText);
		    	var res = XHR.responseText;
		    	if(res == "true")
		    		doExecuteR();
		    	else
		    		alert("R file upload failed");
		    }
		    else if(XHR.readyState==1) {
		    	//alert(StrURL + ": Server connection established");
	    	}
		    else if(XHR.readyState==2) {
		    	//alert(StrURL + ": Request received");
	    	}
		    else if(XHR.readyState==3) {
		    	//alert(StrURL + ": Processing request ");
	    	}
		}
		XHR.send(formData);
		return false;
	}
	
	function doExecuteR() {
		alert("R Process Completed");
		//var XHR = new XMLHttpRequest();
	}
	
	function doStopProc() {
		alert("doStopProc");
		
		var XHR = new XMLHttpRequest();
		var StrURL = "/DemandSense/rAdmin/rJavaThreadStop";
		XHR.open("POST", StrURL, true);
		XHR.onreadystatechange=function(){
		    if(XHR.readyState==4 && XHR.status==200) {
		    	//alert("REST Return: " + XHR.responseText);
		    	var res = XHR.responseText;
		    	if(res == "true")
		    		alert("R Java Thread process stopped");
		    	else
		    		alert("No thread process in back ground");
		    }
		    else if(XHR.readyState==1) {
		    	//alert(StrURL + ": Server connection established");
	    	}
		    else if(XHR.readyState==2) {
		    	//alert(StrURL + ": Request received");
	    	}
		    else if(XHR.readyState==3) {
		    	//alert(StrURL + ": Processing request ");
	    	}
		}
		XHR.send();
		return false;
	}
	
	function doShowLog() {
		//alert("doShowLog");
	}
   
	function copyFile2Hid(fileFld) {
		var fldName = fileFld.name;
		var fldValue = fileFld.value;
		if(fldName.length > 0 && fldValue.length > 0 ) {
			var hFldStr = "h" + fldName;
			var hFldObj = document.getElementById(hFldStr);
			hFldObj.value = fldValue;
			//alert("copyFileName2Hid: " + fldValue);
			//alert("copyFileName2Hid: " + rJavaFrm.hrFile.value);
		}
	}
	
	function copyFileName2Hid() {
		var fldName = fileFld.name;
		var fldValue = fileFld.value;
		//alert(fldName + " : " + fldValue);
		if(fldName.length > 0 && fldValue.length > 0 ){
			var hFldStr = "h" + fldName;
			var hFldObj = document.getElementById(hFldStr);
			hFldObj.value = fldValue;
			//alert("copyFileName2Hid: " + fldValue);
			//alert("copyFileName2Hid: " + rJavaFrm.hrFile.value);
		}
	}
</script>



    </head>





    <body id="minovate" class="appWrapper sidebar-sm-forced">






        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->












        <!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
        <div id="wrap" class="animsition">






            <!-- ===============================================
            ================= HEADER Content ===================
            ================================================ -->
            <section id="header" class="scheme-light">
                <header class="clearfix">

                    <!-- Branding -->
                    <div class="branding scheme-light">
                        <a class="brand" href="index.html">
						
                            <span><strong>Demand</strong>Sense</span>
                        </a>
                        <a href="#" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
                    </div>
                    <!-- Branding end -->



                    <!-- Left-side navigation -->
                    <ul class="nav-left pull-left list-unstyled list-inline">
                        <li class="sidebar-collapse divided-right">
                            <a href="#" class="collapse-sidebar">
                                <i class="fa fa-outdent"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- Left-side navigation end -->




                    <!-- Search -->
                    <div class="search" id="main-search">
                        <input type="text" class="form-control underline-input" placeholder="Search...">
                    </div>
                    <!-- Search end -->




                    <!-- Right-side navigation -->
                    <ul class="nav-right pull-right list-inline">

					<li class="dropdown nav-profile"><a
						class="dropdown-toggle" data-toggle="dropdown"> <!-- <img
							src="resources/assets/images/Default_logo.jpg" alt="Image"
							class="img-circle size-30x30"> --> 
							<span>Welcome <%=session.getAttribute("loggedUserName") %><i class="fa fa-angle-down"></i></span>
					</a>

						<ul class="dropdown-menu animated littleFadeInRight" role="menu">

							
							<li><a href="Setting"> <i class="fa fa-cog"></i>Settings
							</a></li>
							<li><a href="logs"> <i class="fa fa-list-alt"></i>Usage Log
							</a></li>
							<li class="divider"></li>
							<li><a href="/DemandSense/user/logout"> <i class="fa fa-sign-out"></i>Logout
							</a></li>

						</ul></li>
				</ul>
                    <!-- Right-side navigation end -->



                </header>

            </section>
            <!--/ HEADER Content  -->





            <!-- =================================================
            ================= CONTROLS Content ===================
            ================================================== -->
            <div id="controls">





                <!-- ================================================
                ================= SIDEBAR Content ===================
                ================================================= -->
                <aside id="sidebar">


                    <div id="sidebar-wrap">

                        <div class="panel-group slim-scroll" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#sidebarNav">
                                            Navigation <i class="fa fa-angle-up"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">


                                        <!-- ===================================================
                                        ================= NAVIGATION Content ===================
                                        ==================================================== -->
                                        <ul id="navigation">
                                            <li><a href="index"><i class="fa fa-dashboard"></i> <span>Predictive Analysis</span></a></li>
                                            <!-- <li><a href="DataAudit"><i class="fa fa-pencil"></i> <span>Data Audit</span></a></li> -->
                                            <li><a href="DataExplorer"><i class="fa fa-list"></i> <span>Data Explorer</span></a></li>
                                            <% if(session.getAttribute("userType").equals("user_admin") || session.getAttribute("userType").equals("super_admin")){ %>
                                            <li><a href="ExtractionApp"><i class="fa fa-upload"></i> <span>Data Upload</span></a></li>
											<%} %>
											<li><a href="report"><i class="fa fa-desktop"></i> <span>Report</span><span class="label label-success">new</span></a></li>
											<li><a href="support"><i class="fa fa-question-circle"></i> <span>Support</span><span class="label label-success">new</span></a></li>
											<!-- <li><a href="NewProductLaunch"><i class="fa fa-desktop"></i> <span>New Product Launch</span><span class="label label-success">new</span></a></li>
                                            <li><a href="#"><i class="fa fa-shopping-cart"></i> <span>Internal Experts</span> <span class="label label-success">new</span></a>                                       
                                                 </li>
                                            <li><a href="#"><i class="fa fa-desktop"></i> <span>Prescriptive Analysis</span><span class="label label-success">new</span></a></li>
                                          -->
										   </ul>
                                        <!--/ NAVIGATION Content -->
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </aside>
                <!--/ SIDEBAR Content -->





                <!-- =================================================
                ================= RIGHTBAR Content ===================
                ================================================== -->
                
                <!--/ RIGHTBAR Content -->




            </div>
            <!--/ CONTROLS Content -->




            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
            <section id="content">

                <div class="page page-sidebar-sm-layout">

                    <div class="pageheader">

                       <div class="page-bar">

                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html"><i class="fa fa-home"></i> Rplus</a>
                                </li>
                                <li>
                                    <a href="#">Settings</a>
                                </li>
                            </ul>
                            
                        </div>

                    </div>
					
					
                         <!-- col -->
                <div class="col-md-12">

                            <!-- tile -->
                            <section class="tile">

                                <!-- tile header -->
                                <div class="tile-header dvd dvd-btm">
                                    <h1><strong>Global Settings</strong></h1>
                                    <ul class="controls">
									<li> <a role="button" tabindex="0" class="tile-refresh">
                                                        <i class="fa fa-refresh"></i> Refresh
                                                    </a>
                                    </li>
									 <li> <a role="button" tabindex="0" class="tile-toggle">
                                                        <span class="minimize"><i class="fa fa-angle-down"></i></span>
                                                        <span class="expand"><i class="fa fa-angle-up"></i></span>
                                                    </a>
                                    </li>
									 </ul>
									
                                </div>
                                <!-- /tile header -->

                                <!-- tile body -->
                                <div class="tile-body">
									
                                    <form class="form-horizontal" role="form">
									<div class="form-group">
										<label class="col-sm-2 control-label">Current Date :</label>
										<div class="col-sm-6">
                                         <div class='input-group datepicker w-360'>
                                                    <input type='text' class="form-control" />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
											</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Start Date :</label>
										 <div class="col-sm-6">
                                         <div class='input-group datepicker w-360'>
                                                    <input type='text' class="form-control" />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
										</div>
									</div>
										
										 <div class="form-group">
                                            <label for="input01" class="col-sm-2 control-label">Delta Reload Horizon : </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="input01">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="input01" class="col-sm-2 control-label">Forecast Horizon : </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="input01">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="input01" class="col-sm-2 control-label">Active Horizon : </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="input01">
                                            </div>
                                        </div>
										
								</form>
									
								</div>	
                                <!-- /tile body -->

                            </section>
                            <!-- /tile -->

                        </div>
                        <!-- /Global Settings end --> 

    <!-- User & Authorization -->
                <div class="col-md-12">

                            <!-- tile -->
                            <section class="tile">

                                <!-- tile header -->
                                <div class="tile-header dvd dvd-btm">
                                    <h1><strong>User & Authorization</strong></h1>
                                    <ul class="controls">
									<li> <a role="button" tabindex="0" class="tile-refresh">
                                                        <i class="fa fa-refresh"></i> Refresh
                                                    </a>
                                    </li>
									 <li> <a role="button" tabindex="0" class="tile-toggle">
                                                        <span class="minimize"><i class="fa fa-angle-down"></i></span>
                                                        <span class="expand"><i class="fa fa-angle-up"></i></span>
                                                    </a>
                                    </li>
									 </ul>
									
                                </div>
                                <!-- /tile header -->

                                <!-- tile body -->
                                <div class="tile-body">
									 <form class="form-inline" role="form" style="padding-bottom: 20px;">
                                        <div class="form-group">
										
											<label for="input01" class="col-sm-3 control-label"><b>Search Name:</b></label>
											<div class="col-sm-5">
                                                <input type="text" class="form-control" id="input07" placeholder="Search Name" >
                                            </div>
                                           
                                        </div>
                                        <div class="form-group" >
											<label for="input01" class="col-sm-3 control-label"><b>Description:</b></label>
											<div class="col-sm-5">
                                                <input type="text" class="form-control" id="input07" placeholder="Description" >
                                            </div>
                                              </div>
										<div class="form-group" >
										  <label for="input01" class="col-sm-3 control-label"><b>Publish:</b></label>
                                         <select class="form-control mb-6">
                                                    <option>Private to System</option>
                                         </select>
										</div>
                                    </form>
									<form class="form-inline" role="form" >
									<label class="col-sm-3 control-label"><b>Ignore Case:</b></label>
                                            <input type="checkbox" value=""><i></i>  
									</form></br>
									<form class="form-inline" role="form" style="padding-bottom: 20px;">
									
										  <label  class="col-sm-3 control-label"><b>Primary Table:</b></label>
                                         <select class="form-control mb-6">
                                                    <option>Item</option>
                                         </select>
										
									</form>
									<form class="form-inline" role="form" style="padding-bottom: 20px;">
									
										  <label  class="col-sm-3 control-label"><b>Criteria  Type:</b></label>
										  <label class="radio-inline">
                                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option1"> Column Filter
                                           </label>
										   <label class="radio-inline">
                                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"> Hierachy
                                           </label>
										   <label class="radio-inline">
                                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option3"> Interactive Hierachy
                                           </label>
										   <label class="radio-inline">
                                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option4"> Cluster
                                           </label>
									</form>
									<form class="form-inline" role="form" style="padding-bottom: 20px;">
									
										  <div class="form-group" >
										  <label  class="col-sm-3 control-label"><b>Table:</b></label>
                                         <select class="form-control mb-10">
                                                    <option></option>
                                         </select>
										</div>
										<div class="form-group" >
										  <label  class="col-sm-3 control-label"><b>Column</b></label>
                                         <select class="form-control mb-10">
                                                    <option></option>
                                         </select>
										</div>
										<div class="form-group" >
										  <label  class="col-sm-3 control-label"><b>Operator</b></label>
                                         <select class="form-control mb-10">
                                                    <option>=</option>
													<option><</option>
													<option><=</option>
													<option>>=</option>
													<option>></option>
                                         </select>
										</div></form>
										<form class="form-inline" role="form" style="padding-bottom: 20px;">
										<div class="form-group" style="width:100%;">
										  <label class="col-sm-1 control-label"><b>Values:</b></label>
										   <label class="col-sm-3 radio-inline">
                                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option1"> Column Filter
                                           </label>
										 <label class="col-sm-3 radio-inline">
                                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"> Prompted Entry field
                                           </label>
										    <label class="col-sm-3 radio-inline">
                                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option3"> Prompted Entry list
											 </label>
											 <button type="button" class="btn btn-primary btn-sm mb-10">Apply</button>
										</div>
									</form>
									
									<div class="form-group" style="padding-bottom: 40px;">
                                            <label  class="col-sm-3 control-label">Search Criteria:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="input03">
                                                  </div>
                                        </div>
									<form class="form-inline" role="form" >
									<div class="form-group" style="padding-bottom: 20px;">
                                            <label  class="col-sm-5 control-label">Equation:</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="input03">
                                                  </div>
                                        </div>
										
									</form>
									<div class="form-group" style="padding-bottom: 20px;">
                                            <label  class="col-sm-2 control-label"> <button type="button" class="btn btn-default btn-sm mb-10">Hide SQL</button></label>
                                            <div class="col-sm-2">
                                                cxv,cjnhvkxn,lkj
                                                  </div>
                                        </div>
									
								</div>	
                                <!-- /tile body -->

                            </section>
                            <!-- /tile -->

                        </div>
                        <!-- /User & Authorization -->  
						
				<!-- R-Java Jobs -->
                <div class="col-md-12">

                            <!-- tile -->
                            <section class="tile">

                                <!-- tile header -->
                                <div class="tile-header dvd dvd-btm">
                                    <h1><strong>Application Jobs</strong></h1>
                                    <ul class="controls">
									<li> <a role="button" tabindex="0" class="tile-refresh">
                                                        <i class="fa fa-refresh"></i> Refresh
                                                    </a>
                                    </li>
									 <li> <a role="button" tabindex="0" class="tile-toggle">
                                                        <span class="minimize"><i class="fa fa-angle-down"></i></span>
                                                        <span class="expand"><i class="fa fa-angle-up"></i></span>
                                                    </a>
                                    </li>
									 </ul>
									
                                </div>
                                <!-- /tile header -->

                                <!-- tile body -->
                                <div class="tile-body" style="height:1150px;">
								                    <!-- page content -->
										 <div class="col-md-6">			
									 <div class="panel panel-default ">
                                        <div class="panel-heading">
                                            <h3 class="panel-title ">DB Input Job</h3>
                                        </div>
                                        <div class="panel-body">
										   <form class="form-horizontal" role="form">

										<div class="form-group">
                                            <label class="col-sm-4 control-label">Existing DB Input:</label>
                                            <div class="col-sm-6">

                                                <select class="form-control mb-10">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
											</div>
										</div>
										
										
										<div class="form-group">
                                            <label class="col-sm-4 control-label">DB Input File (New):</label>
                                            <div class="col-sm-6" >
                                                <input type="file" class="filestyle" data-buttonText="Find file" data-iconName="fa fa-inbox">
                                            </div>
                                        </div>

                                     
										 <div class="form-group">
                                            <label for="input01" class="col-sm-4 control-label">Schema  name: </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="input01">
                                            </div>
                                        </div>
										 <div class="form-group">
                                            <label for="input01" class="col-sm-4 control-label">DB  name: </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="input01">
                                            </div>
                                        </div>
										 
										<div class="form-group">
                                            <label  class="col-sm-4 control-label"> </label>
                                            <div class="col-sm-6">
                                                <textarea class="form-control rows="4" style="width: 100%;" placeholder="To show the Thread process updates"></textarea>
                                            </div> 
                                        </div>
										
										 <form class="form-inline" role="form">
                                        <div class="form-group">
										 <label  class="col-sm-2 control-label">  </label>
										  <div class="col-sm-2">
                                                <button type="submit" class="btn btn-default">Save</button> 
											</div>
											<div class="col-sm-2">
                                                <button type="submit" class="btn btn-default">Execute</button> 
											</div>
											<div class="col-sm-2">
                                                <button type="submit" class="btn btn-default">Stop Prog</button> 
											</div>
											<div class="col-sm-2">
                                                <button type="submit" class="btn btn-default">Show Log</button> 
											</div>
										
                                            
                                        </div>
                                        
                                        
                                    </form>
										
										
								</form>
										  
										  
										  
                                        </div>
                                    </div>
									</div>
									<!-- --------  dinput jobs end------>
									
								 
								  <div class="col-md-6">			
									 <div class="panel panel-default ">
                                        <div class="panel-heading">
                                            <h3 class="panel-title ">DB Output Job</h3>
                                        </div>
                                        <div class="panel-body">
										   <form class="form-horizontal" role="form">

										
										<div class="form-group">
                                            <label class="col-sm-4 control-label">Existing DB Output:</label>
                                            <div class="col-sm-6">

                                                <select class="form-control mb-10">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
											</div>
										</div>
										
										<div class="form-group">
                                            <label class="col-sm-4 control-label">DB Output File (New):</label>
                                            <div class="col-sm-6" >
                                                <input type="file" class="filestyle" data-buttonText="Find file" data-iconName="fa fa-inbox">
                                            </div>
                                        </div>

                                     
										 <div class="form-group">
                                            <label for="input01" class="col-sm-4 control-label">Schema  name: </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="input01">
                                            </div>
                                        </div>
										  <div class="form-group">
                                            <label for="input01" class="col-sm-4 control-label">DB  name: </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="input01">
                                            </div>
                                        </div>
										 
										<div class="form-group">
                                            <label  class="col-sm-4 control-label"> </label>
                                            <div class="col-sm-6">
                                                <textarea class="form-control rows="4" style="width: 100%;" placeholder="To show the Thread process updates"></textarea>
                                            </div> 
                                        </div>
										
										 <form class="form-inline" role="form">
                                        <div class="form-group">
										 <label  class="col-sm-2 control-label">  </label>
										  <div class="col-sm-2">
                                                <button type="submit" class="btn btn-default">Save</button> 
											</div>
											<div class="col-sm-2">
                                                <button type="submit" class="btn btn-default">Execute</button> 
											</div>
											<div class="col-sm-2">
                                                <button type="submit" class="btn btn-default">Stop Prog</button> 
											</div>
											<div class="col-sm-2">
                                                <button type="submit" class="btn btn-default">Show Log</button> 
											</div>
										
                                            
                                        </div>
                                        
                                        
                                    </form>
										
										
								</form>
										  
										  
										  
                                        </div>
                                    </div>
									</div>
									
									<!--------------DB Output Job----------->
									
									
									<div class="col-md-6">			
									 <div class="panel panel-default ">
                                        <div class="panel-heading">
                                            <h3 class="panel-title ">Java Jobs</h3>
                                        </div>
                                        <div class="panel-body">
										   <form class="form-horizontal" role="form" name="rJavaFrm">

                                        <div class="form-group">
                                            <label for="input01" class="col-sm-4 control-label">Thread Count</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="threadCount" name="threadCount" value="">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="col-sm-4 control-label">Existing R Files:</label>
                                            <div class="col-sm-6">
                                                <select class="form-control mb-10" name="existRFiles" id="existRFiles">
                                                    
                                                </select>
											</div>
										</div>
										
										<div class="form-group">
                                            <label class="col-sm-4 control-label">R File (New):</label>
                                            <div class="col-sm-6" >
                                                <input type="file" class="filestyle" data-buttonText="Find file" data-iconName="fa fa-inbox" name="rFile" id="rFile" onblur="copyFile2Hid(this);">
                                            </div>
                                        </div>

                                     
										
										 <div class="form-group">
                                            <label for="input01" class="col-sm-4 control-label">Table name: </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" name="dbTableName" id="dbTableName" size="20" value="" />
                                            </div>
                                        </div>
										 <div class="form-group">
                                            <label for="input01" class="col-sm-4 control-label">Schema  name: </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" name="dbSchemaName" id="dbSchemaName" value="" />
                                            </div>
                                        </div>
										 <div class="form-group">
                                            <label for="input01" class="col-sm-4 control-label">JVM Ram size:  </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" name="jvmRamSize" id="jvmRamSize" value="" />
                                            </div> MB
                                        </div>
										 <div class="form-group">
                                            <label for="input01" class="col-sm-4 control-label">JVM Heap size:  </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" name="jvmHeapSize" id="jvmHeapSize" value="" />
                                            </div> MB
                                            <input type="hidden" name="hrFile" id="hrFile" />
                                        </div>
										<div class="form-group">
                                            <label  class="col-sm-4 control-label"> </label>
                                            <div class="col-sm-6">
                                                <textarea class="form-control rows="4" style="width: 100%;" placeholder="To show the Thread process updates"></textarea>
                                            </div> 
                                        </div>
										
										 <form class="form-inline" role="form">
                                        <div class="form-group">
										 <label  class="col-sm-2 control-label">  </label>
										  <div class="col-sm-2">
                                                <button type="submit" class="btn btn-default" onclick="doSave()">Save</button> 
											</div>
											<div class="col-sm-2">
                                                <button type="submit" class="btn btn-default" onclick="return doExecute()">Execute</button> 
											</div>
											<div class="col-sm-2">
                                                <button type="submit" class="btn btn-default" onclick="doStopProc()">Stop Prog</button> 
											</div>
											<div class="col-sm-2">
                                                <button type="submit" class="btn btn-default" onclick="doShowLog()">Show Log</button> 
											</div>
                                        </div>
                                        
                                        
                                    </form>
										
										
								</form>
										  
										  
										  
                                        </div>
                                    </div>
									</div>
								 <!----------R jobs End--------->
									 <div class="col-md-6">			
									 <div class="panel panel-default ">
                                        <div class="panel-heading">
                                            <h3 class="panel-title ">Application Log </h3>
                                        </div>
                                        <div class="panel-body">
										  
										  
                                        </div>
                                    </div>
									</div>
									
									<!----------PArameters end------------------>

								</div>	
                                <!-- /tile body -->

                            </section>
                            <!-- /tile -->

                        </div>
                        <!-- /R-Java Jobs -->		
						
						  <!-- Db Jobs -->
                
                        <!-- /Db Jobs -->  
						
						<!-- R-Java Jobs -->
                		
							
                </div>
                
            </section>
            <!--/ CONTENT -->






        </div>
        <!--/ Application Content -->














        <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="resources/assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="resources/assets/js/vendor/bootstrap/bootstrap.min.js"></script>

        <script src="resources/assets/js/vendor/jRespond/jRespond.min.js"></script>

        <script src="resources/assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>

        <script src="resources/assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>

        <script src="resources/assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>
		
		<script src="resources/assets/js/vendor/daterangepicker/moment.min.js"></script>
		
		<script src="resources/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
   
         <script src="resources/assets/js/vendor/screenfull/screenfull.min.js"></script>
		 
		 <script src="resources/assets/js/vendor/chosen/chosen.jquery.min.js"></script>
		 
		   <script src="resources/assets/js/vendor/filestyle/bootstrap-filestyle.min.js"></script>

        <script src="resources/assets/js/vendor/countTo/jquery.countTo.js"></script>
		
		
        <script src="resources/assets/js/vendor/parsley/parsley.min.js"></script>

		
		 <script src="resources/assets/js/vendor/form-wizard/jquery.bootstrap.wizard.min.js"></script>
      


        <!--/ vendor javascripts -->




        <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
        <script src="resources/assets/js/main.js"></script>
        <!--/ custom javascripts -->

		<!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
	 <script>
            $(window).load(function(){

                $('#rootwizard').bootstrapWizard({
                    onTabShow: function(tab, navigation, index) {
                        var $total = navigation.find('li').length;
                        var $current = index+1;

                        // If it's the last tab then hide the last button and show the finish instead
                        if($current >= $total) {
                            $('#rootwizard').find('.pager .next').hide();
                            $('#rootwizard').find('.pager .finish').show();
                            $('#rootwizard').find('.pager .finish').removeClass('disabled');
                        } else {
                            $('#rootwizard').find('.pager .next').show();
                            $('#rootwizard').find('.pager .finish').hide();
                        }

                    },

                    onNext: function(tab, navigation, index) {

                        var form = $('form[name="step'+ index +'"]');

                        form.parsley().validate();

                        if (!form.parsley().isValid()) {
                            return false;
                        }

                    },

                    onTabClick: function(tab, navigation, index) {

                        var form = $('form[name="step'+ (index+1) +'"]');
                        form.parsley().validate();

                        if (!form.parsley().isValid()) {
                            return false;
                        }

                    }

                });

            });
        </script>

			
        
        <!--/ Page Specific Scripts -->




        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

    </body>
</html>
