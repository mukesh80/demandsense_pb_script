
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->



<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Rplus Demand Sense</title>
<link rel="icon" type="image/ico"
	href="resources/assets/images/favicon.ico" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">




<!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
<!-- vendor css files -->
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="resources/assets/css/vendor/bootstrap.min.css">
<link rel="stylesheet" href="resources/assets/css/vendor/animate.css">

<link rel="stylesheet"
	href="resources/assets/js/vendor/animsition/css/animsition.min.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/morris/morris.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/rickshaw/rickshaw.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="resources/css/smartPaginator.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="resources/css/jqueryTag.css" />
<link href="resources/css/datepicker.css" type="text/css"
	rel="stylesheet">

<link rel="stylesheet" href="resources/assets/css/classic.css">
<link rel="stylesheet" href="resources/assets/js/vendor/datatables/datatables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/jquery.dataTables.min.css">

<link rel="stylesheet" href="resources/assets/js/vendor/datatables/extensions/Responsive/css/dataTables.responsive.css">




<!-- project main css files -->
<link rel="stylesheet" href="resources/assets/css/main.css">
<!--/ stylesheets -->



<!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
<script
	src="resources/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<!--/ modernizr -->

<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>


</head>





<body id="minovate" class="appWrapper sidebar-sm-forced">

	





	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->












	<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
	<div id="wrap" class="animsition">






		<!-- ===============================================
            ================= HEADER Content ===================
            ================================================ -->
		<section id="header" class="scheme-light">
			<header class="clearfix">

				<!-- Branding -->
				<div class="branding scheme-light">
					<a class="brand" href="index.html"> <span><strong>Demand</strong>Sense</span>
					</a> <a href="#" class="offcanvas-toggle visible-xs-inline"><i
						class="fa fa-bars"></i></a>
				</div>
				<!-- Branding end -->



				<!-- Left-side navigation -->
				<ul class="nav-left pull-left list-unstyled list-inline">
					<li class="sidebar-collapse divided-right"><a href="#"
						class="collapse-sidebar"> <i class="fa fa-outdent"></i>
					</a></li>
				</ul>
				<!-- Left-side navigation end -->




				<!-- Search -->
				<!-- <div class="search" id="main-search">
					<input type="text" class="form-control underline-input"
						placeholder="Search...">
				</div> -->
				<!-- Search end -->




				<img src="resources/assets/images/paper_boat_logo.png" alt="Image" class="img-circle size-50x50" style="margin-left: 30%;">
				<!-- Right-side navigation -->
				<ul class="nav-right pull-right list-inline">

					<li class="dropdown nav-profile"><a
						class="dropdown-toggle" data-toggle="dropdown"> <!-- <img
							src="resources/assets/images/Default_logo.jpg" alt="Image"
							class="img-circle size-30x30"> --> 
							<span>Welcome <%=session.getAttribute("loggedUserName") %><i class="fa fa-angle-down"></i></span>
					</a>


						<ul class="dropdown-menu animated littleFadeInRight" role="menu">

							
							<li><a href="profile"> <i class="fa fa-cog"></i>Profile
							</a></li>
							<li><a href="logs"> <i class="fa fa-list-alt"></i>Usage Log
							</a></li>
							<li class="divider"></li>
							<li><a href="/DemandSense/user/logout"> <i class="fa fa-sign-out"></i>Logout
							</a></li>

						</ul></li>
				</ul>
				<!-- Right-side navigation end -->



			</header>

		</section>
		<!--/ HEADER Content  -->





		<!-- =================================================
            ================= CONTROLS Content ===================
            ================================================== -->
		<div id="controls">





			<!-- ================================================
                ================= SIDEBAR Content ===================
                ================================================= -->
			<aside id="sidebar">


				<div id="sidebar-wrap">

					<div class="panel-group slim-scroll" role="tablist">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab">
								<h4 class="panel-title">
									<a data-toggle="collapse" href="#sidebarNav"> Navigation <i
										class="fa fa-angle-up"></i>
									</a>
								</h4>
							</div>
							<div id="sidebarNav" class="panel-collapse collapse in"
								role="tabpanel">
								<div class="panel-body">


									<!-- ===================================================
                                        ================= NAVIGATION Content ===================
                                        ==================================================== -->
									  <ul id="navigation">
                                            <li><a href="index"><i class="fa fa-dashboard"></i> <span>Forecast Analysis</span></a></li>
                                            <li class="active open"><a href="DataExplorer"><i class="fa fa-table"></i> <span>Data Explorer</span></a></li>
                                             <!-- <li><a href="DataAudit"><i class="fa fa-search"></i> <span>Data Audit</span></a></li> -->
                                            <% if(session.getAttribute("userType").equals("user_admin") || session.getAttribute("userType").equals("super_admin")){ %>
                                            <li><a href="ExtractionApp"><i class="fa fa-upload"></i> <span>Data Upload</span></a></li>
											<%} %>
											<li><a href="report"><i class="fa fa-desktop"></i> <span>Report</span><span class="label label-success">new</span></a></li>
											<li><a href="support"><i class="fa fa-question-circle"></i> <span>Support</span><span class="label label-success">new</span></a></li>
											<!-- <li><a href="NewProductLaunch"><i class="fa fa-desktop"></i> <span>New Product Launch</span><span class="label label-success">new</span></a></li>
                                            <li><a href="#"><i class="fa fa-shopping-cart"></i> <span>Internal Experts</span> <span class="label label-success">new</span></a>                                          </li>
                                            <li><a href="#"><i class="fa fa-desktop"></i> <span>Prescriptive Analysis</span><span class="label label-success">new</span></a></li>
                                          -->
										   </ul>
									<!--/ NAVIGATION Content -->


								</div>
							</div>
						</div>
					</div>

				</div>


			</aside>
			<!--/ SIDEBAR Content -->





			<!-- =================================================
                ================= RIGHTBAR Content ===================
                ================================================== -->

			<!--/ RIGHTBAR Content -->




		</div>
		<!--/ CONTROLS Content -->




		<!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
		<section id="content">

			<div class="page page-sidebar-sm-layout">

				<div class="pageheader">

					<div class="page-bar">

						<ul class="page-breadcrumb">
							<li><a href="index.html"><i class="fa fa-home"></i>
									Rplus</a></li>
							<li><a href="index.html">Data Explorer</a></li>
						</ul>

					</div>

				</div>
<input type="hidden" id="selected-criteria" value="<%=session.getAttribute("forcastData") %>">
				<!-- col -->
				<div class="col-md-12">

					<!-- tile -->
					<section class="tile">

						<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
						<div class="tile-toggle" style="cursor: pointer;">
							<h1>
								<strong>Selection</strong>
							</h1> </div>
							<ul class="controls">
								<li><a role="button" tabindex="0" class="tile-refresh">
										<i class="fa fa-refresh"></i> Refresh
								</a></li>
								<li><a role="button" tabindex="0" class="tile-toggle">
										<span class="minimize"><i class="fa fa-angle-down"></i></span>
										<span class="expand"><i class="fa fa-angle-up"></i></span>
								</a></li>
							</ul>

						</div>
						<!-- /tile header -->

						<!-- tile body -->
						<div class="tile-body" id="Select-section">
							<form class="form-inline input-week" role="form" style="margin-bottom: 15px; margin-left: 30px;" >
							<div class="form-group">
							
							 <label class="col-sm-3 control-label"> <i class="fa fa-calendar fa-2x"></i> </label>
							 <label class="col-sm-3 control-label"> 	 From Week </label>
						
							 <select class="form-control dte"  id="ondat" onchange="changeFunc1();">
								<script language="javascript" type="text/javascript"> 
								for(var d=01;d<=53;d++)
								{
									if(d < 10){
										document.write("<option value=0"+d+">0"+d+"</option>");
									}else{
										document.write("<option value="+d+">"+d+"</option>");
									}
								}
							
								
								</script>
							</select>
				
							  <select class="form-control dte" id="onyar" onchange="changeFunc();">
								<script language="javascript" type="text/javascript"> 
							
							for(var d=2006;d<=2020;d++)
							{
							    document.write("<option value="+d+">"+d+"</option>");
							}
							
								</script>
							</select>
							</div>
										
						
							 
							 <div class="form-group">
							 <label class="col-sm-3 control-label" style="float: left;">To Week</label>
							 <select class="form-control dte" id="ondat1" onchange="changeFunc3();">
								<script language="javascript" type="text/javascript"> 
							
							for(var d=1;d<=53;d++)
							{
								if(d < 10){
									document.write("<option value=0"+d+">0"+d+"</option>");
								}else{
									document.write("<option value="+d+">"+d+"</option>");
								}
							}
							
								</script>
							</select>
				
							  <select class="form-control dte" id="onyar1" onchange="changeFunc4();">
								<script language="javascript" type="text/javascript"> 
							
							for(var d=2006;d<=2020;d++)
							{
								document.write("<option value="+d+">"+d+"</option>");
							}
							
							
								</script>
							</select>
							</div>
							<div class="form-group" style="font-weight: bold;margin-left: 1%;">
								<span>  </span>
								<span id="fron-date-display"></span>
								<span> - </span>
								<span id="to-date-display"></span>
							</div>
						</form>
					
							<form class="form-horizontal">


								<div class="form-group">
									<label class="col-sm-1 control-label"> <i
										class="fa fa-cubes fa-2x"></i></label>
									<div class="col-sm-10 productInput">
										<input class="form-control input-sm" id="productInput"
											type="text" placeholder="Select Products"> <span
											id="launchProduct" data-toggle="modal"
											data-target="#launchProductModal"
											data-options="splash-2 splash-ef-11"><i
											class="fa fa-bars "
											style="margin-left: 15px; cursor: pointer;"></i></span>

									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-1 control-label"> <i
										class="fa fa-map-marker fa-2x"></i></label>
									<div class="col-sm-10 locationInput">
										<input class="form-control input-sm" id="locationInput"
											type="text" placeholder="Select Products"> <span
											id="location" data-toggle="modal"
											data-target="#launchLocationModal"><i
											class="fa fa-bars "
											style="margin-left: 15px; cursor: pointer;"></i></span>

									</div>
								</div>
								<div class="form-group" id="myForm">
									<label class="col-sm-1 control-label"></label>
									<div class="col-sm-2">
										<!-- <label class="checkbox checkbox-custom"> <input
											name="customRadio" type="radio" id="ag" value="aggregation"><i></i>
											Aggregation
										</label> -->
									</div>
									<div class="col-sm-2">
										<!-- <label class="checkbox checkbox-custom"> <input
											name="customRadio" type="radio" value="non-aggregation"><i></i>
											Non Aggregation
										</label> -->
									</div>
									<div class="col-md-2"></div>
									<div class=".col-xs-6 .col-md-4">
										<button onclick="export_data()"  type="button" class="btn btn-default btn-sm mb-10 export-dis" disabled>Export Data</button>
										<button type="button" onclick="reset_btn()" class="btn btn-default btn-sm mb-10">Reset</button>
										<button type="button" class="btn btn-primary btn-sm mb-10"
											id="submit1">Show</button>
									</div>

								</div>

								<div id="slider" style="margin-top: 4%; margin-bottom: 1%"></div>

								<div id="slider1" style="margin-top: 4%; margin-bottom: 1%"></div>

							</form>
							<form class="form-inline" role="form">
								<div class="col-sm-3" id="event-start" style="padding-top: 5px;"></div>
								<div class="col-sm-3" id="event-end"
									style="float: right; padding-top: 5px;"></div>
							</form>
						</div>
						<!-- /tile body -->

					</section>
					<!-- /tile -->

				</div>
				<!-- /selection end -->

				

				
				
				
								<!-- Social -->
				<div class="col-md-12">

					<!-- tile -->
					<section class="tile">

						<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
							<div class="tile-toggle" style="cursor: pointer;">
							<h1>
								<strong>Significant Table</strong>
							</h1>
							</div>
							<ul class="controls">
								<li><a role="button" tabindex="0" class="tile-refresh">
										<i class="fa fa-refresh"></i> Refresh
								</a></li>
								<li><a role="button" tabindex="0" class="tile-toggle">
										<span class="minimize"><i class="fa fa-angle-down"></i></span>
										<span class="expand"><i class="fa fa-angle-up"></i></span>
								</a></li>
							</ul>

						</div>
						<!-- /tile header -->

						<!-- tile body -->
						 <div class="tile-body" style=" overflow: overlay;display: none;">
                                   <!--  <table id="example" class="display" width="100%"></table>-->
                                   
                                   <div class="row">
                                        <div class="col-md-6"><div id="tableTools"></div></div>
                                        <div class="col-md-6"><div id="colVis"></div></div>
                                    </div>
			                                   <table id="example1" class="display" cellspacing="0" width="100%" class="table-responsive">
										        <thead>
										            <tr>
										                <th>Category</th>
										                <th>Sub Category</th>
										                <th>Product</th>
										                <th>Region</th>
										                <th>State</th>
										                <th>Location</th>
										                <th>UID</th>
										                <th>Significant Factors</th>
										                <th>Correlation</th>
										                
										            </tr>
										        </thead>
										 
										    </table>
                                </div>
						<!-- /tile body -->
						</section>
					</div>	
						<!-- /col -->
						
						<!-- Social -->
				<div class="col-md-12">

					<!-- tile -->
					<section class="tile">

						<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
							<div class="tile-toggle" style="cursor: pointer;">
							<h1>
								<strong>Accuracy Table</strong>
							</h1>
							</div>
							<ul class="controls">
								<li><a role="button" tabindex="0" class="tile-refresh">
										<i class="fa fa-refresh"></i> Refresh
								</a></li>
								<li><a role="button" tabindex="0" class="tile-toggle">
										<span class="minimize"><i class="fa fa-angle-down"></i></span>
										<span class="expand"><i class="fa fa-angle-up"></i></span>
								</a></li>
							</ul>

						</div>
						<!-- /tile header -->

						<!-- tile body -->
						 <div class="tile-body" style=" overflow: overlay;display: none; ">
                                   <!--  <table id="example" class="display" width="100%"></table>-->
                                   
			                                   <table id="example2" class="display" cellspacing="0" width="100%"  class="table-responsive">
										        <thead>
										            <tr>
										                
										                <th>Category</th>
										                <th>Sub Category</th>
										                <th>Product</th>
										                <th>Country</th>
										                <th>Location</th>
										                <th>UID</th>
										                <th>RMSE</th>
										                <th>MAE</th>
										                <th>MPE</th>
										                <th>MAPE</th>
										                <th>MASE</th>
										                <!-- <th>calDay</th> -->
										            </tr>
										        </thead>
										 
										    </table>
                                </div>
						<!-- /tile body -->
								
					</section>
					<!-- /tile -->

				</div>
				<!-- /Social -->
			<!-- Social -->
			<div class="col-md-12">

		<!-- 			tile -->
					<section class="tile">

						<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
							<div class="tile-toggle" style="cursor: pointer;">
							<h1>
								<strong>Forecast Table</strong>
							</h1>
							</div>
							<ul class="controls">
								<li><a role="button" tabindex="0" class="tile-refresh">
										<i class="fa fa-refresh"></i> Refresh
								</a></li>
								<li><a role="button" tabindex="0" class="tile-toggle">
										<span class="minimize"><i class="fa fa-angle-down"></i></span>
										<span class="expand"><i class="fa fa-angle-up"></i></span>
								</a></li>
							</ul>

						</div>
						<!-- /tile header -->

						<!-- tile body -->
						 <div class="tile-body" style=" overflow: overlay;display: none;">
<!--                                     <table id="example" class="display" width="100%"></table> -->
                                   
			                                   <table id="example" class="display" cellspacing="0" width="100%" class="table-responsive">
										        <thead>
										            <tr>
										                <th>Category</th>
										                <th>Sub Category</th>
										                <th>Product</th>
										                <th>Location</th>
										                <th>Actual Qty</th> <!-- Actual Quantity -->
										                <th>Override Qty</th>
										                <th>Forecast Qty</th>
										                <th>Data Type</th>
										                <th>Week</th>
										            </tr>
										        </thead>
										 
										    </table>
                                </div>
						<!-- /tile body -->
						</section>
					</div>	 
								
			</div>

		</section>
		<!--/ CONTENT -->





	<!--/ Application Content -->


	<div class="modal fade" id="launchProductModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h5 class="modal-title">Select Product/Category</h5>
				</div>
				<div class="modal-body">
					<div>

						<!-- Nav tabs -->
						<ul class="nav nav-tabs" id="productTabs" role="tablist">
							<li role="presentation" class="active"><a id="categoryLink"
								href="#categoriesTab" aria-controls="categoriesTab" role="tab"
								data-toggle="tab"> Categories(<span id="categoryCount"></span>)
							</a></li>
							<li role="presentation"><a id="subCategoryLink"
								href="#subCategoryTab" aria-controls="subCategoryTab" role="tab"
								data-toggle="tab">Sub Categories(<span id="subCategoryCount"></span>)
							</a></li>
							<li role="presentation"><a id="productLink" href="#products"
								aria-controls="products" role="tab" data-toggle="tab">Products(<span
									id="productCount"></span>)
							</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content" id="productTabsContent">
							<div role="tabpanel" class="tab-pane active" id="categoriesTab">
							<table style="width: 100%;">
							<thead>
								<tr>
									<td style="width: 28.6%; font-weight: bold;" class="cattd">Category Id</td>
									<td style="font-weight: bold;">Category Name</td>
								</tr>
							</thead>
							</table>
								<table id="categoryTable" style="width: 100%;">
									
								</table>
								<div id="category-paginator"></div>
							</div>
							<div role="tabpanel" class="tab-pane" id="subCategoryTab">
							<table style="width: 100%;">

								<thead>
									<tr>
										<td style="width: 28.6%; font-weight: bold;">Sub Category Id</td>
										<td style="font-weight: bold;">Sub Category Name</td>
									</tr>
								</thead>
							</table>
								<table id="subCategoryTable" style="width: 100%;">
									
								</table>
								<div id="subcategory-paginator"></div>
							</div>
							<div role="tabpanel" class="tab-pane" id="productTab">
							<table style="width: 100%;">
							<thead>
									<tr>
										<td style="width: 28.6%; font-weight: bold;">Product Id</td>
										<td style="font-weight: bold;">Product Name</td>
									</tr>
								</thead>
							</table>
								<table id="productTable" style="width: 100%;">
									
								</table>
								<div id="product-paginator"></div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	
	
	<div class="modal fade" id="launchLocationModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h5 class="modal-title">Select Location</h5>
				</div>
				<div class="modal-body">
					<div>

						<!-- Nav tabs -->
						<ul class="nav nav-tabs" id="locationTabs" role="tablist">
							<li role="presentation" class="active"><a id="countriesLink"
								href="#countriesTab" aria-controls="countriesTab" role="tab"
								data-toggle="tab"> Countries(<span id="countryCount"></span>)
							</a></li>
							<li role="presentation"><a id="regionTab" href="#regionTab"
								aria-controls="regionTab" role="tab" data-toggle="tab">Regions(<span
									id="regionCount"></span>)
							</a></li>
							<li role="presentation"><a id="stateTab" href="#stateTab"
								aria-controls="stateTab" role="tab" data-toggle="tab">States(<span
									id="stateCount"></span>)
							</a></li>
							<li role="presentation"><a id="districtTab"
								href="#districtTab" aria-controls="districtTab" role="tab"
								data-toggle="tab">City(<span id="districtCount"></span>)
							</a></li>
							<li role="presentation"><a id="cityTab" href="#cityTab"
								aria-controls="cityTab" role="tab" data-toggle="tab">Warehouse(<span
									id="cityCount"></span>)
							</a></li>
							<!-- <li role="presentation"><a id="locationTab"
								href="#locationTab" aria-controls="locationTab" role="tab"
								data-toggle="tab">Location Name(<span id="locationCount"></span>)
							</a></li> -->
						</ul>

						<!-- Tab panes -->
						<div class="tab-content" id="locationTabsContent">
							<div role="tabpanel" class="tab-pane active" id="countriesTab">
							<table style="width: 100%;">
							<thead>
									<tr>
										<td style="    width: 28.6%;font-weight: bold;"
											class="cattd">Country Code</td>
										<td style="font-weight: bold;">Country
											Name</td>
									</tr>
								</thead>
							</table>
								<table id="countryTable" style="width: 100%;">
									
								</table>
								<div id="country-paginator"></div>
							</div>
							<div role="tabpanel" class="tab-pane" id="regionTab">
							<table style="width: 100%;">
							<thead>
									<tr>
										<td style="width: 28.6%;font-weight: bold;">Region
											Code</td>
										<td style="font-weight: bold;">Region
											Name</td>
									</tr>
								</thead>
							</table>
								<table id="regionTable" style="width: 100%;">
									
								</table>
								<div id="region-paginator"></div>
							</div>
							<div role="tabpanel" class="tab-pane" id="stateTab">
							<table style="width: 100%;">
							<thead>
									<tr>
										<td style="width: 28.6%;font-weight: bold;">State
											Code</td>
										<td style="font-weight: bold;">State
											Name</td>
									</tr>
								</thead>
							</table>
								<table id="stateTable" style="width: 100%;">
									
								</table>
								<div id="state-paginator"></div>
							</div>
							<div role="tabpanel" class="tab-pane" id="districtTab">
							<table style="width: 100%;">
							<thead>
									<tr>
										<td style="width: 28.6%;font-weight: bold;">City Code</td>
										<td style="font-weight: bold;">City Name</td>
									</tr>
								</thead>
							</table>
								<table id="districtTable" style="width: 100%;">
									
								</table>
								<div id="district-paginator"></div>
							</div>
							<div role="tabpanel" class="tab-pane" id="cityTab">
							<table style="width: 100%;">
							<thead>
									<tr>
										<td style="width: 28.6%; font-weight: bold;">Warehouse
											Code</td>
										<td style="font-weight: bold;">Warehouse
											Name</td>
									</tr>
								</thead>
							</table>
								<table id="cityTable" style="width: 100%;">
									
								</table>
								<div id="city-paginator"></div>
							</div>
							<!-- <div role="tabpanel" class="tab-pane" id="locationTab">
							<table style="width: 100%;">
							<thead>
									<tr>
										<td style="width: 28.6%; font-weight: bold;">Location
											Code</td>
										<td style="font-weight: bold;">Location
											Name</td>
									</tr>
								</thead>
							</table>
								<table id="locationTable" style="width: 100%;">
									
								</table>
								<div id="location-paginator"></div>
							</div> -->
						</div>

					</div>

				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->








	<!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
	<script>
		window.jQuery
				|| document
						.write('<script src="resources/assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')
	</script>

	<script src="resources/assets/js/vendor/bootstrap/bootstrap.min.js"></script>
	
	<script src="http://code.highcharts.com/highcharts.js"></script>

	<script src="resources/assets/js/vendor/jRespond/jRespond.min.js"></script>

	<script
		src="resources/assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>

	<script
		src="resources/assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>

	<script
		src="resources/assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>

	<!--  <script src="resources/assets/js/vendor/daterangepicker/moment.min.js"></script>

        <script src="resources/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>-->

	<script src="resources/js/bootstrap-datepicker.js"></script>

	<script src="resources/assets/js/vendor/screenfull/screenfull.min.js"></script>
	<script src="resources/assets/js/vendor/slider/bootstrap-slider.min.js"></script>


	<script src="resources/assets/js/vendor/countTo/jquery.countTo.js"></script>
	<script src="resources/assets/js/vendor/nouislider/nouislider.js"></script>
	<script src="resources/assets/js/vendor/nouislider/nouislider.min.js"></script>
	<script src="resources/assets/js/vendor/nouislider/wNumb.js"></script>
	<script src="resources/assets/js/vendor/datatables/js/jquery.dataTables.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/dataTables.bootstrap.js"></script>
      <!--   <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>-->
      		<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>






	<!--/ vendor javascripts -->




	<!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
	<script src="resources/assets/js/jQAllRangeSliders-withRuler-min.js"></script>
	<script src="resources/assets/js/main.js"></script>
<!-- <script src="resources/assets/js/product.js"></script>  -->
	 <script src="resources/assets/js/table.js"></script>
	<script src="resources/js/jqueryPagination.js"></script>
	<script src="resources/assets/js/spin.js"></script>
	<script src="resources/js/jqueryTag.js"></script>
	<!--  <script src="resources/js/custom.js"></script>-->
	

	<!--/ custom javascripts -->

	<!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->




	<!--/ Page Specific Scripts -->


 <script>
 
        
        </script>
			
			
	<style type="text/css">
#launchProductModal {
	width: 100%;
	background: transparent;
	padding-left: 17px;
}

.modal-backdrop {
	opacity: 0.3;
}

.tagsinput {
	height: 30px !important;
	font-size: 12px;
	line-height: 0.5;
	border-radius: 3px;
	min-height: auto !important;
	width: 85% !important;
	float: left !important;
}

div.tagsinput span.tag {
	padding: 12px 2px 9px;
	font-size: 11px;
    font-weight: bold;
    background-color: #7BAE15;
    color: #FFFFFF;
}    
div.tagsinput span.tag a {
	color: #FFFFFF;
} 
#productInput_tag, #locationInput_tag {
	width:auto !important;
}

.categoryName,.subCategoryName,.productName,.countryName,.regionName,.districtName,#locationTable .locationName,.cityName,.stateName
	{
	cursor: pointer;
	/* text-transform: lowercase; */
 .dte {width: 100%; height: 24px; padding: 0px !important;vertical-align: middle; margin-left: 10px;border: 1px solid #CCC;font-size: 13px;}
}
.dataTables_scrollBody { height:auto !important;}



.ui-rangeSlider .ui-rangeSlider-innerBar{
	margin: 0 !importent;
	background: none;
}

#slider {
	
	margin-top: 70px;
    height: 22px;
    background: #DDD;

}

.ui-rangeSlider-withArrows .ui-rangeSlider-container {
    margin: 0 !important;
}

.ui-rangeSlider-arrow.ui-rangeSlider-rightArrow {

	display:none;
}

.ui-rangeSlider-arrow.ui-rangeSlider-leftArrow {

	display:none;
}
.ui-rangeSlider-ruler {
	margin-right: 1.5% !important;

}

</style>




</body>
</html>
