
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->



<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Rplus Demand Sense</title>
<link rel="icon" type="image/ico" href="resources/assets/images/favicon.ico" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">




        <!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
        <!-- vendor css files -->
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet"
	href="resources/assets/css/vendor/bootstrap.min.css">
<link rel="stylesheet" href="resources/assets/css/vendor/animate.css">
<link rel="stylesheet"
	href="resources/assets/css/vendor/font-awesome.min.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/animsition/css/animsition.min.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/morris/morris.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/rickshaw/rickshaw.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="resources/css/smartPaginator.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="resources/css/jqueryTag.css" />
<link href="resources/css/datepicker.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/waitMe.min.css" type="text/css" rel="stylesheet">
	

<link rel="stylesheet" href="resources/assets/css/classic.css">



<!-- project main css files -->
<link rel="stylesheet" href="resources/assets/css/main.css">
<!--/ stylesheets -->


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">


        <!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
        <script src="resources/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <!--/ modernizr -->

<style type="text/css">
	.tooltip.top .tooltip-arrow {
				border-top-color: red;
				color: red;
			}
			.tooltip-inner {
				    background-color: white;
				    color: red;
				    border: 2px solid red;
			}
		
</style>


    </head>





    <body id="minovate" class="appWrapper">






        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->












        <!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
        <div id="wrap" class="animsition">






            <!-- ===============================================
            ================= HEADER Content ===================
            ================================================ -->
          <section id="header" class="scheme-light">
			<header class="clearfix">

				<!-- Branding -->
				<div class="branding scheme-light">
					<a class="brand" href="index.html"> <span><strong>Demand</strong>Sense</span>
					</a> <a href="#" class="offcanvas-toggle visible-xs-inline"><i
						class="fa fa-bars"></i></a>
				</div>
				<!-- Branding end -->



				<!-- Left-side navigation -->
				<ul class="nav-left pull-left list-unstyled list-inline">
					<li class="sidebar-collapse divided-right"><a href="#"
						class="collapse-sidebar"> <i class="fa fa-outdent"></i>
					</a></li>
				</ul>
				<!-- Left-side navigation end -->




				<!-- Search -->
				<!-- <div class="search" id="main-search">
					<input type="text" class="form-control underline-input"
						placeholder="Search...">
				</div> -->
				
				<!-- Search end -->



				<img src="resources/assets/images/paper_boat_logo.png" alt="Image" class="img-circle size-50x50" style="margin-left: 30%;">
				<!-- Right-side navigation -->
				<ul class="nav-right pull-right list-inline">

					<li class="dropdown nav-profile"><a
						class="dropdown-toggle" data-toggle="dropdown"> <!-- <img
							src="resources/assets/images/Default_logo.jpg" alt="Image"
							class="img-circle size-30x30"> --> 
							<span>Welcome <%=session.getAttribute("loggedUserName") %><i class="fa fa-angle-down"></i></span>
					</a>

						<ul class="dropdown-menu animated littleFadeInRight" role="menu">

							
							<li><a href="profile"> <i class="fa fa-cog"></i>Profile
							</a></li>
							<li><a href="logs"> <i class="fa fa-list-alt"></i>Usage Log
							</a></li>
							<li class="divider"></li>
							<li><a href="/DemandSense/user/logout"> <i class="fa fa-sign-out"></i>Logout
							</a></li>

						</ul></li>
				</ul>
				<!-- Right-side navigation end -->



			</header>

		</section>
		<!--/ HEADER Content  -->





            <!-- =================================================
            ================= CONTROLS Content ===================
            ================================================== -->
            <div id="controls">





                <!-- ================================================
                ================= SIDEBAR Content ===================
                ================================================= -->
                <aside id="sidebar">


                    <div id="sidebar-wrap">

                        <div class="panel-group slim-scroll" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#sidebarNav">
                                            Navigation <i class="fa fa-angle-up"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">


                                        <!-- ===================================================
                                        ================= NAVIGATION Content ===================
                                        ==================================================== -->
                                       <ul id="navigation">
                                       <li ><a href="index"><i class="fa fa-home"></i> <span>Main Menu</span></a></li>
                                       <li class="active open"><a href="#"><i class="fa fa-desktop"></i> <span>Report</span></a></li>
                                            <li><a href="#"><i class="fa fa-shopping-cart"></i> <span>Sales</span><span class="label label-success">new</span> </a>
                                            	<ul>
                                                    <!-- <li class="sales week" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i> Sales Week </a></li> -->
                                                    <li class="sales month" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i><span> Monthly Forecast by City </span></a></li>
                                                    <li class="sales analysis" onClick="driveAnalysis(this);"><a href="#"><i class="fa fa-caret-right"></i><span> Sales Drive Analysis </span><span class="label label-success">new</span> </a></li>
                                                    
                                                </ul>
                                            </li>
                                            <!-- <li><a href="index"><i class="fa fa-globe"></i> <span>Operation</span></a>
                                            <ul>
                                                    <li class="operation week" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i> Operation Week </a></li>
                                                    <li class="operation month" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i> Operation Month </a></li>
                                                    <li class="operation day" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i> Operation Day  </a></li>
                                                    
                                                </ul>
                                              </li>-->
                                             <li><a href="#"><i class="fa fa-truck"></i> <span>Supply Chain</span><span class="label label-success">new</span> </a>
                                             	<ul>
                                                    <li class="Supply_Chain week" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i> <span>Weekly Forecast by Warehouse </span></a></li>
                                                    <li class="sales day" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i> <span>Daily Forecast by Warehouse</span> <span class="label label-success">new</span> </a></li>
                                                    <!-- <li class="Supply_Chain month" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i> Supply Chain Month </a></li>
                                                    <li class="Supply_Chain day" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i> Supply Chain Day  </a></li> -->
                                                    
                                                </ul>
                                             </li>
                                             <!-- <li><a href="#"><i class="fa fa-credit-card"></i> <span>Financial</span></a>
                                             	<ul>
                                                    <li class="financial week" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i> Financial Week </a></li>
                                                    <li class="financial month" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i> Financial Month </a></li>
                                                    <li class="financial day" onClick="details(this);"><a href="#"><i class="fa fa-caret-right"></i> Financial Day  </a></li>
                                                    
                                                </ul>
                                             </li> -->
                                            <li><a href="support"><i class="fa fa-question-circle"></i> <span>Support</span><span class="label label-success">new</span></a></li>
											
											
										   </ul>


                                    </div>
                                </div>
                            </div>
                            

                    </div>


                </aside>
                <!--/ SIDEBAR Content -->




            </div>
            <!--/ CONTROLS Content -->




            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
            <section id="content">

                <div class="page page-dashboard">

                    <div class="pageheader">

                        <div class="page-bar">
						   <div class="page-bar">
		
								<ul class="page-breadcrumb">
									<li><a href="index.html"><i class="fa fa-home"></i>
											Rplus</a></li>
									<li><a href="index.html">Report</a></li>
								</ul>
		
							</div>

                        </div>

                    </div>

                    <!-- cards row -->
                  <div class="init-div" style="text-align: center; padding: 45px 0; font-size: xx-large; opacity: 0.4;">
                  	Please select a Report from the left hand panel.
                  </div>
                  
                  <div class="main-div" style="display:none;">
                  
                  <form class="form-inline input-week" role="form" style="margin-bottom: 15px; margin-left: 30px;" >
							<div style="color:red;">
								<label id="error" style="padding-left:2%"></label>
								
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label city-text" style="padding-top:2%">Select City</label>
								
								<select class="form-control dte citys" multiple>
								
								</select>
				
							</div>
							<div class="form-group">
							
							 <label class="col-sm-3 control-label"> <i class=""></i> </label>
							 <label class="col-sm-3 control-label from-date-text" style="padding-top:2%"></label>
						
							 <select class="form-control dte from-date">
							 								
							</select>
							
							<input type="text" class="dateSelect from-date-month form-control" placeholder="Select Date">
							
							<select class="form-control dte from-year">
								
							</select>
							</div>
							
							 <div class="form-group">
							 <label class="col-sm-3 control-label to-date-text" style="float: left;padding-top:2%;"></label>
							 <select class="form-control dte to-date"disabled>
							
							 </select>
							 <input type="text" class=" to-date-month from-control" disabled>
				
							 <select class="form-control dte to-year" disabled>
								
							</select>
							</div>
							<div class="form-group col-md-10" style="margin-top: -10px; margin-bottom: 10px;">
								<button class="pull-right submit-form btn btn-primary" type="button" style="margin-left: 10px;">Submit</button>
								<button class="pull-right export-button btn btn-primary" type="button" disabled>Export</button>
							</div>
							
						</form>
						
                  		

					<div class="col-md-12">
				
					<!-- tile -->
					<section class="tile" style="float:left; width:100%;">

						<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
							<h1>
								<strong class="cat-main-text"></strong>
							</h1>
							<h1 class="pull-right">
								<strong>All Quantities in Cases (12 EA/Case)</strong>
							</h1>
							
							<ul class="controls">
								<!-- li><a role="button" tabindex="0" class="tile-refresh">
										<i class="fa fa-refresh"></i> Refresh
								</a></li-->
								<!-- <li><a role="button" tabindex="0" class="tile-toggle">
										<span class="minimize"><i class="fa fa-angle-down"></i></span>
										<span class="expand"><i class="fa fa-angle-up"></i></span>
								</a></li> -->
							</ul>

						</div>
						<!-- /tile header -->
						
						<!-- tile body -->
						<div class="tile-body" id="Select-section">
							<div class="init-body-div" style="text-align: center; padding: 45px 0; font-size: xx-large; opacity: 0.4;">
			                  	Please select Location(s) and Week / Month.
			                </div>
			                <div class="init-body loading waitMe_body" style="min-height: 366px;display:none;">
							<div class="col-md-6" style="padding:0;margin-right: -1px; width:60%">
							 <table id="example" class="display" cellspacing="0" width="100%" class="table-responsive">
						        <thead class="thead1">
						        <tr>
						        		<th colspan="4" class="thead1-th"></th>
						                
						            </tr>
						            <tr>
						                <th>Region</th>
						                <th>State</th>
						                <th class="city-text1">Warehouse</th>
						                <th>Product</th>
						                
						            </tr>
						        </thead>
						 		<tbody class="result">
						 		
						 		</tbody>
						    </table>
						    </div>
						    <div class="col-md-6" style="overflow-x: scroll;padding:0; width:40%">
						    
							<table id="example1" class="display" cellspacing="0" width="100%" class="table-responsive">
						        <thead class="thead">
						            <!-- <tr>
						                <th colspan="2">year week</th>
						                <th colspan="2">year week2</th>
						                <th colspan="2">year week3</th>
						                
						            </tr>
						            <tr>
						             	<th>Actual</th>
						                <th>Forcast</th>
						                <th>Actual</th>
						                <th>Forcast</th>
						                <th>Actual</th>
						                <th>Forcast</th>
						            </tr> -->
						        </thead>
						 		<tbody class="result1">
						 		
						 		</tbody>
						    </table>
							</div>
							</div>
						</div>
						<!-- /tile body -->

					</section>
					<!-- /tile -->

				</div>
				<!-- /selection end -->
				</div>
				<div class="temp" style="text-align: center; padding: 45px 0; font-size: xx-large; opacity: 0.4; display: none;">
                  	Comming Soon...
                </div>
				<div class="main-div-alanysis" style="display: none;">
					<form class="form-inline " role="form" style="margin-bottom: 15px; margin-left: 30px;" >
							<div style="color:red;">
								<label id="error-a" style="padding-left:2%"></label>
								
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label" style="padding-top:2%">Select Regions</label>
								
								<select class="form-control dte regions" multiple>
								
								</select>
				
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" style="padding-top:2%">Select Sub-Category</label>
								
								<select class="form-control dte subCat" multiple>
								
								</select>
				
							</div>
							<div class="form-group">
							
							 <label class="col-sm-3 control-label" style="padding-top:2%">From Year</label>
						
							 <select class="form-control dte from-year-analysis">
							 								
							</select>
							
							</div>
							
							 <div class="form-group">
							 <label class="col-sm-3 control-label" style="float: left;padding-top:2%;">To Year</label>
							 <select class="form-control dte to-year-analysis"disabled>
							
							 </select>
							</div>
							<div class="form-group col-md-10" style="margin-top: -10px; margin-bottom: 10px;">
								<button class="pull-right submit-form-analysis btn btn-primary" type="button" style="margin-left: 10px;">Submit</button>
							</div>
							
						</form>
						
				
                <section class="tile" style="float:left; width:100%;">
                	<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
							<h1>
								<strong class="swap-text-p">Sales Drive Analysis - Sub-Category comparison</strong>
							</h1>
							
							<ul class="controls swap-category catt">
								<li><a role="button" tabindex="0"  class="swap-text-f">
										Swap to Sub-Category - Region
								</a></li>
								
							</ul>

						</div>
						<!-- /tile header -->
						
						<!-- tile body -->
						<div class="tile-body" id="Select-section">
							<div class="init-analysis-div" style="text-align: center; padding: 45px 0; font-size: xx-large; opacity: 0.4;">
			                  	Please select Regions, Sub-Category and Year.
			                </div>
			                <div class="init-analysis  loading waitMe_body" style="min-height: 366px;display:none;">
								
								    <div class="col-md-6" style="overflow-x: scroll;padding:0; width:100%">
								    
									<table id="analysis" class="display" cellspacing="0" width="100%" class="table-responsive">
								        <thead class="">
								            <!-- <tr>
								                <th colspan="2">year week</th>
								                <th colspan="2">year week2</th>
								                <th colspan="2">year week3</th>
								                
								            </tr>
								            <tr>
								             	<th>Actual</th>
								                <th>Forcast</th>
								                <th>Actual</th>
								                <th>Forcast</th>
								                <th>Actual</th>
								                <th>Forcast</th>
								            </tr> -->
								        </thead>
								 		<tbody class="">
								 		
								 		</tbody>
								    </table>
								</div>
							</div>
			            </div>
                		<!-- tile body -->
                	</section>
                </div>
                </div>

                
            </section>
            <!--/ CONTENT -->






        </div>
        <!--/ Application Content -->
        
        <!-- View Model Start -->
		<div class="modal fade bs-example-modal-lg" id="myModal" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Sales Drive Alalysis</h4>
		          <div>
		          	<select class="details-select"></select>
		          </div>
		        </div>
		        <div class="modal-body" style="overflow-x: scroll;padding:0; width:100%; display:none;">
		
		                
					    <table id="analysis_details" class="display" cellspacing="0" width="100%" class="table-responsive">
					        <thead class="">
					            
					        </thead>
					 		<tbody class="">
					 		
					 		</tbody>
					    </table>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
		<!-- View Model Start -->

        <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="resources/assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="resources/assets/js/vendor/bootstrap/bootstrap.min.js"></script>

        <script src="resources/assets/js/vendor/jRespond/jRespond.min.js"></script>

        <script src="resources/assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>

        <script src="resources/assets/js/vendor/daterangepicker/moment.min.js"></script>

        <script src="resources/assets/js/vendor/screenfull/screenfull.min.js"></script>

        <script src="resources/assets/js/vendor/flot/jquery.flot.min.js"></script>
        <script src="resources/assets/js/vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>
        <script src="resources/assets/js/vendor/flot-spline/jquery.flot.spline.min.js"></script>

        <script src="resources/assets/js/vendor/easypiechart/jquery.easypiechart.min.js"></script>

       
		<script src="resources/assets/js/vendor/datatables/js/jquery.dataTables.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/dataTables.bootstrap.js"></script>
     	<!--   <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>-->
      	<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/waitMe.min.js"></script>
      	
      	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      	
        <script src="resources/assets/js/vendor/daterangepicker/daterangepicker.js"></script>
		<!--/ vendor javascripts -->




        <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
        <script src="resources/assets/js/main.js"></script>
        <script src="resources/assets/js/reports.js"></script>
        <script src="resources/assets/js/reportsDriveAnalysis.js"></script>
        <!--/ custom javascripts -->



<style type="text/css">
	thead{
		background: #ccc;
	}
	th, td {
		padding: 7px;
	}
	table{border-collapse:collapse}
tbody tr{border-bottom:thin solid}

#analysis td {
	min-width: 11em;
}


.modal-dialog {
				width: 90%;
			}
.modal-title {
				    font-size: 15px;
			}
.modal-header {
			    background-color: #7BAE15;
			    color: #6F6F77;
				}
.modal-body {
    padding: 8px 6px 4px;
			}		
.modal-footer	{padding: 4px 7px 4px;
				 background-color: #7BAE15;
				 }
.btn-file {
	position: relative;
	overflow: hidden;
}

.btn-file input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	min-width: 100%;
	min-height: 100%;
	font-size: 100px;
	text-align: right;
	filter: alpha(opacity = 0);
	opacity: 0;
	outline: none;
	background: white;
	cursor: inherit;
	display: block;
}
.tile .tile-body { padding:0px;}
	
</style>




        <!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
        <script type="text/javascript">
        
        	$(document).ready(function(){
        		$('.dateSelect').datepicker({dateFormat: 'dd-mm-yy'});
        	})
        </script>
        
       <!--  <script>
            $(window).load(function(){
                // Initialize Statistics chart
                var data = [{
                    data: [[1,15],[2,40],[3,35],[4,39],[5,42],[6,50],[7,46],[8,49],[9,59],[10,60],[11,58],[12,74]],
                    label: 'Unique Visits',
                    points: {
                        show: true,
                        radius: 4
                    },
                    splines: {
                        show: true,
                        tension: 0.45,
                        lineWidth: 4,
                        fill: 0
                    }
                }, {
                    data: [[1,50],[2,80],[3,90],[4,85],[5,99],[6,125],[7,114],[8,96],[9,130],[10,145],[11,139],[12,160]],
                    label: 'Page Views',
                    bars: {
                        show: true,
                        barWidth: 0.6,
                        lineWidth: 0,
                        fillColor: { colors: [{ opacity: 0.3 }, { opacity: 0.8}] }
                    }
                }];

                var options = {
                    colors: ['#e05d6f','#61c8b8'],
                    series: {
                        shadowSize: 0
                    },
                    legend: {
                        backgroundOpacity: 0,
                        margin: -7,
                        position: 'ne',
                        noColumns: 2
                    },
                    xaxis: {
                        tickLength: 0,
                        font: {
                            color: '#fff'
                        },
                        position: 'bottom',
                        ticks: [
                            [ 1, 'JAN' ], [ 2, 'FEB' ], [ 3, 'MAR' ], [ 4, 'APR' ], [ 5, 'MAY' ], [ 6, 'JUN' ], [ 7, 'JUL' ], [ 8, 'AUG' ], [ 9, 'SEP' ], [ 10, 'OCT' ], [ 11, 'NOV' ], [ 12, 'DEC' ]
                        ]
                    },
                    yaxis: {
                        tickLength: 0,
                        font: {
                            color: '#fff'
                        }
                    },
                    grid: {
                        borderWidth: {
                            top: 0,
                            right: 0,
                            bottom: 1,
                            left: 1
                        },
                        borderColor: 'rgba(255,255,255,.3)',
                        margin:0,
                        minBorderMargin:0,
                        labelMargin:20,
                        hoverable: true,
                        clickable: true,
                        mouseActiveRadius:6
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: '%s: %y',
                        defaultTheme: false,
                        shifts: {
                            x: 0,
                            y: 20
                        }
                    }
                };

                var plot = $.plot($("#statistics-chart"), data, options);

                $(window).resize(function() {
                    // redraw the graph in the correctly sized div
                    plot.resize();
                    plot.setupGrid();
                    plot.draw();
                });
                // * Initialize Statistics chart

                //Initialize morris chart
                Morris.Donut({
                    element: 'browser-usage',
                    data: [
                        {label: 'Chrome', value: 25, color: '#00a3d8'},
                        {label: 'Safari', value: 20, color: '#2fbbe8'},
                        {label: 'Firefox', value: 15, color: '#72cae7'},
                        {label: 'Opera', value: 5, color: '#d9544f'},
                        {label: 'Internet Explorer', value: 10, color: '#ffc100'},
                        {label: 'Other', value: 25, color: '#1693A5'}
                    ],
                    resize: true
                });
                //*Initialize morris chart


                // Initialize owl carousels
                $('#todo-carousel, #feed-carousel, #notes-carousel').owlCarousel({
                    autoPlay: 5000,
                    stopOnHover: true,
                    slideSpeed : 300,
                    paginationSpeed : 400,
                    singleItem : true,
                    responsive: true
                });

                $('#appointments-carousel').owlCarousel({
                    autoPlay: 5000,
                    stopOnHover: true,
                    slideSpeed : 300,
                    paginationSpeed : 400,
                    navigation: true,
                    navigationText : ['<i class=\'fa fa-chevron-left\'></i>','<i class=\'fa fa-chevron-right\'></i>'],
                    singleItem : true
                });
                //* Initialize owl carousels


                // Initialize rickshaw chart
                var graph;

                var seriesData = [ [], []];
                var random = new Rickshaw.Fixtures.RandomData(50);

                for (var i = 0; i < 50; i++) {
                    random.addData(seriesData);
                }

                graph = new Rickshaw.Graph( {
                    element: document.querySelector("#realtime-rickshaw"),
                    renderer: 'area',
                    height: 133,
                    series: [{
                        name: 'Series 1',
                        color: 'steelblue',
                        data: seriesData[0]
                    }, {
                        name: 'Series 2',
                        color: 'lightblue',
                        data: seriesData[1]
                    }]
                });

                var hoverDetail = new Rickshaw.Graph.HoverDetail( {
                    graph: graph,
                });

                graph.render();

                setInterval( function() {
                    random.removeData(seriesData);
                    random.addData(seriesData);
                    graph.update();

                },1000);
                //* Initialize rickshaw chart

                //Initialize mini calendar datepicker
                $('#mini-calendar').datetimepicker({
                    inline: true
                });
                //*Initialize mini calendar datepicker


                //todo's
                $('.widget-todo .todo-list li .checkbox').on('change', function() {
                    var todo = $(this).parents('li');

                    if (todo.hasClass('completed')) {
                        todo.removeClass('completed');
                    } else {
                        todo.addClass('completed');
                    }
                });
                //* todo's


                //initialize datatable
                $('#project-progress').DataTable({
                    "aoColumnDefs": [
                      { 'bSortable': false, 'aTargets': [ "no-sort" ] }
                    ],
                });
                //*initialize datatable

                //load wysiwyg editor
                $('#summernote').summernote({
                    toolbar: [
                        //['style', ['style']], // no style button
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        //['insert', ['picture', 'link']], // no insert buttons
                        //['table', ['table']], // no table button
                        //['help', ['help']] //no help button
                    ],
                    height: 143   //set editable area's height
                });
                //*load wysiwyg editor
            });
        </script> -->
        <!--/ Page Specific Scripts -->






        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        

    </body>
</html>
