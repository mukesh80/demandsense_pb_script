
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->



    <head>

        <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Rplus Demand Sense</title>
<link rel="icon" type="image/ico" href="resources/assets/images/favicon.ico" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">




        <!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
       <!-- vendor css files -->
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="resources/assets/css/vendor/bootstrap.min.css">
<link rel="stylesheet" href="resources/assets/css/vendor/animate.css">
<link rel="stylesheet"
	href="resources/assets/css/vendor/font-awesome.min.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/animsition/css/animsition.min.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/morris/morris.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/rickshaw/rickshaw.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="resources/css/smartPaginator.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="resources/css/jqueryTag.css" />
<link href="resources/css/datepicker.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/waitMe.min.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="resources/assets/js/vendor/summernote/summernote.css">
<link rel="stylesheet" href="resources/assets/js/vendor/chosen/chosen.css">


	

<link rel="stylesheet" href="resources/assets/css/classic.css">



<!-- project main css files -->
<link rel="stylesheet" href="resources/assets/css/main.css">
<!--/ stylesheets -->





        <!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
        <script src="resources/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <!--/ modernizr -->




    </head>





    <body id="minovate" class="appWrapper">






        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->












        <!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
        <div id="wrap" class="animsition">






            <!-- ===============================================
            ================= HEADER Content ===================
            ================================================ -->
          <section id="header" class="scheme-light">
			<header class="clearfix">

				<!-- Branding -->
				<div class="branding scheme-light">
					<a class="brand" href="index.html"> <span><strong>Demand</strong>Sense</span>
					</a> <a href="#" class="offcanvas-toggle visible-xs-inline"><i
						class="fa fa-bars"></i></a>
				</div>
				<!-- Branding end -->



				<!-- Left-side navigation -->
				<ul class="nav-left pull-left list-unstyled list-inline">
					<li class="sidebar-collapse divided-right"><a href="#"
						class="collapse-sidebar"> <i class="fa fa-outdent"></i>
					</a></li>
				</ul>
				<!-- Left-side navigation end -->




				<!-- Search -->
				<!-- <div class="search" id="main-search">
					<input type="text" class="form-control underline-input"
						placeholder="Search...">
				</div> -->
				
				<!-- Search end -->



				<img src="resources/assets/images/paper_boat_logo.png" alt="Image" class="img-circle size-50x50" style="margin-left: 30%;">
				<!-- Right-side navigation -->
				<ul class="nav-right pull-right list-inline">

					<li class="dropdown nav-profile"><a
						class="dropdown-toggle" data-toggle="dropdown"> <!-- <img
							src="resources/assets/images/Default_logo.jpg" alt="Image"
							class="img-circle size-30x30"> --> 
							<span>Welcome <%=session.getAttribute("loggedUserName") %><i class="fa fa-angle-down"></i></span>
					</a>

						<ul class="dropdown-menu animated littleFadeInRight" role="menu">

							
							<li><a href="profile"> <i class="fa fa-cog"></i>Profile
							</a></li>
							<li><a href="logs"> <i class="fa fa-list-alt"></i>Usage Log
							</a></li>
							<li class="divider"></li>
							<li><a href="/DemandSense/user/logout"> <i class="fa fa-sign-out"></i>Logout
							</a></li>

						</ul></li>
				</ul>
				<!-- Right-side navigation end -->



			</header>

		</section>
		<!--/ HEADER Content  -->





            <!-- =================================================
            ================= CONTROLS Content ===================
            ================================================== -->
            <div id="controls">





                <!-- ================================================
                ================= SIDEBAR Content ===================
                ================================================= -->
                <aside id="sidebar">


                    <div id="sidebar-wrap">

                        <div class="panel-group slim-scroll" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#sidebarNav">
                                            Navigation <i class="fa fa-angle-up"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">


                                        <!-- ===================================================
                                        ================= NAVIGATION Content ===================
                                        ==================================================== -->
	                                    <ul id="navigation">
                                            <li class="active open"><a href="index"><i class="fa fa-dashboard"></i> <span>Forecast Analysis</span></a></li>
                                            <li><a href="DataExplorer"><i class="fa fa-table"></i> <span>Data Explorer</span></a></li>
                                             <!-- <li><a href="DataAudit"><i class="fa fa-search"></i> <span>Data Audit</span></a></li>
                                             -->
                                            <% if(session.getAttribute("userType").equals("user_admin") || session.getAttribute("userType").equals("super_admin")){ %>
                                            <li><a href="ExtractionApp"><i class="fa fa-upload"></i> <span>Data Upload</span></a></li>
											<%} %>
											<li><a href="report"><i class="fa fa-desktop"></i> <span>Report</span><span class="label label-success">new</span></a></li>
											<li><a href="support"><i class="fa fa-question-circle"></i> <span>Support</span><span class="label label-success">new</span></a></li>
											<!-- <li><a href="NewProductLaunch"><i class="fa fa-desktop"></i> <span>New Product Launch</span><span class="label label-success">new</span></a></li>
                                              <li><a href="#"><i class="fa fa-shopping-cart"></i> <span>Internal Experts</span> <span class="label label-success">new</span></a>                                          </li>
                                            <li><a href="#"><i class="fa fa-desktop"></i> <span>Prescriptive Analysis</span><span class="label label-success">new</span></a></li>
                                          -->
										</ul>
										<!--/ NAVIGATION Content -->


                                    </div>
                                </div>
                            </div>
                            

                    </div>


                </aside>
                <!--/ SIDEBAR Content -->




            </div>
            <!--/ CONTROLS Content -->




            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
            <section id="content">

                <div class="page page-dashboard">

                    <div class="pageheader">

                        <div class="page-bar">
						   <div class="page-bar">
		
								<ul class="page-breadcrumb">
									<li><a href="index.html"><i class="fa fa-home"></i>
											Rplus</a></li>
									<li><a href="index.html">Support</a></li>
								</ul>
		
							</div>

                        </div>

                    </div>

                    <!-- cards row -->
                  
                  <div class="main-div">
                  
                  
                  		

					<div class="col-md-12">
				
					<!-- tile -->
					<section class="tile" style="float:left; width:100%;">

						<!-- tile header -->
						<div class="tile-header dvd dvd-btm" style="cursor: pointer;">
							<h1>
								<strong class="cat-main-text"></strong>
							</h1>
							
							
							<ul class="controls">
								<!-- li><a role="button" tabindex="0" class="tile-refresh">
										<i class="fa fa-refresh"></i> Refresh
								</a></li-->
								<!-- <li><a role="button" tabindex="0" class="tile-toggle">
										<span class="minimize"><i class="fa fa-angle-down"></i></span>
										<span class="expand"><i class="fa fa-angle-up"></i></span>
								</a></li> -->
							</ul>

						</div>
						<!-- /tile header -->
						
						<!-- tile body -->
						<div class="tile-body" id="Select-section">
							<label class="sucess-msg" style="color: #FFFFFF; width: auto; padding:5px 20px ; background-color: rgba(3, 128, 3, 0.44); display:none;">Request/Feedback posted.</label>
                                    <form id="newMail" class="form-horizontal mt-20">
									 <div class="form-group">
                                        <label class="col-lg-2 control-label">Name:</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="name" id="name"  placeholder="Mention your name">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Mobile:</label>
                                        <div class="col-lg-7">
                                            <input type="text" id="mobile" class="form-control" name="mobile"   placeholder="Mention your mobile number">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Subject:</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="subject" id="subject"  placeholder="Mention Issue/Problem/Feedback ">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Details:</label>
                                        <div class="col-lg-8">
                                            <!-- <div id="summernote">Content</div> -->
                                            <textarea rows="10" class="form-control" cols="90" id="mailbody" name="body" placeholder="Enter details of Issue/Problem/Feedback here..."></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-8 col-lg-offset-2">
                                            <button type="submit" class="btn btn-greensea btn-ef btn-ef-7 btn-ef-7b b-0 br-2 sendMail"><i class="fa fa-envelope"></i> Send Message</button>
                                        </div>
                                    </div>
                                </form>

							</div>
						<!-- /tile body -->

					</section>
					<!-- /tile -->

				</div>
				<!-- /selection end -->
				</div>

           
                </div>

                
            </section>
            <!--/ CONTENT -->






        </div>
        <!--/ Application Content -->



        <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="resources/assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="resources/assets/js/vendor/bootstrap/bootstrap.min.js"></script>

        <script src="resources/assets/js/vendor/jRespond/jRespond.min.js"></script>

        <script src="resources/assets/js/vendor/d3/d3.min.js"></script>
        <script src="resources/assets/js/vendor/d3/d3.layout.min.js"></script>

        

        <script src="resources/assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>

        <script src="resources/assets/js/vendor/daterangepicker/moment.min.js"></script>
        <script src="resources/assets/js/vendor/daterangepicker/daterangepicker.js"></script>

        <script src="resources/assets/js/vendor/screenfull/screenfull.min.js"></script>

        <script src="resources/assets/js/vendor/flot/jquery.flot.min.js"></script>
        <script src="resources/assets/js/vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>
        <script src="resources/assets/js/vendor/flot-spline/jquery.flot.spline.min.js"></script>

        <script src="resources/assets/js/vendor/easypiechart/jquery.easypiechart.min.js"></script>
		  <script src="resources/assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>
       
        <script src="resources/assets/js/vendor/chosen/chosen.jquery.min.js"></script>
       
		<script src="resources/assets/js/vendor/datatables/js/jquery.dataTables.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
        <script src="resources/assets/js/vendor/datatables/extensions/dataTables.bootstrap.js"></script>
     	<!--   <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>-->
      	<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
      	 <script src="resources/assets/js/vendor/summernote/summernote.min.js"></script>
      	<script src="resources/js/jquery.validate.min.js"></script>
		<!--/ vendor javascripts -->




        <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
        <script src="resources/assets/js/main.js"></script>
        <script src="resources/assets/js/reports.js"></script>
        <!--/ custom javascripts -->



<style type="text/css">
	thead{
		background: #ccc;
	}
	th, td {
		padding: 7px;
	}
	table{border-collapse:collapse}
tbody tr{border-bottom:thin solid}
	
</style>




        <!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
      
   <script>
        $(window).load(function(){

            //load wysiwyg editor
            $('#summernote').summernote({
                height: 200   //set editable area's height
            });
            //*load wysiwyg editor

        });
        
        
        $("#newMail").validate({
			rules: {
				name: "required",
				subject: "required",
				body: "required",
				mobile: {
					required: true,
					minlength: 10,
					maxlength: 10
				}
			},
			messages: {
				nameame: "Please enter your name",
				subject: "Please enter your subject",
				body: "Body content required",
				mobile: {
   					required: "Please enter mobile number",
   					minlength: "mobile number should be 10 numbers",
   					maxlength: "mobile number should be 10 numbers"
   				}
			},
			showErrors: function(errorMap, errorList) {
		          // Clean up any tooltips for valid elements
		          $.each(this.validElements(), function (index, element) {
		              var $element = $(element);
		              $element.data("title", "") // Clear the title - there is no error associated anymore
		                  .removeClass("error")
		                  .tooltip("destroy");
		          });
		          // Create new tooltips for invalid elements
		          $.each(errorList, function (index, error) {
		              var $element = $(error.element);
		              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
		                  .data("title", error.message)
		                  .addClass("error")
		                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
		          });
		      },
			submitHandler: function(form) {
				//alert($(".profile").serialize());
				var name = $("#name").val();
	        	var mobile = $("#mobile").val();
	        	var subject = $("#subject").val();
	        	var body = $("#mailbody").val();
				$(".sendMail").prop("disabled", true);
				$.ajax({
        			url: "/DemandSense/support/mail",
        			type: "GET",
        			data:{
        				name: name,
        				mobile: mobile,
        				subject: subject,
        				body: body
        				
        			},
        			
        			success: function(result){
        				if(result == "sucess"){
        					$(".sendMail").prop("disabled", false);
        					$(".sucess-msg").css({"background-color":"green"}).show();
            				$("#newMail")[0].reset();
            				setTimeout('$(".sucess-msg").fadeOut()',10000);
        				}
        				console.log(result);
        			},
        			error: function(result){
        				$(".sendMail").prop("disabled", false);
        				$(".sucess-msg").css({"background-color":"red"}).text("Oops! Our service is down Please try after some time.").show();
    					setTimeout('$(".sucess-msg").fadeOut()',10000);
        				console.log(result);
        			}
        		})
			}
		});
		
        
        /* 
        $(".sendMail").click(function(){
        	var name = $("#name").val();
        	var mobile = $("#mobile").val();
        	var subject = $("#subject").val();
        	var body = $("#mailbody").val();
        	if(mobile.length != 10){
        		alert mobile
        	}
        	console.log(name+"-----"+mobile+"-----"+subject+"----"+body+"----"+$("#mailbody").val())
        	if(name == "" || mailto == "" || subject == "" || body == ""){
        		alert("all fields are required");
        	}else{
        		
        		
        		
        	}
        }) */
    </script>
   
        <!--/ Page Specific Scripts -->






        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        

    </body>
</html>
