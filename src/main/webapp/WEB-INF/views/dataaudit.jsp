
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->



<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Rplus Demand Sense</title>
<link rel="icon" type="image/ico"
	href="resources/assets/images/favicon.ico" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">


<!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
<!-- vendor css files -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="resources/assets/css/vendor/bootstrap.min.css">
<link rel="stylesheet" href="resources/assets/css/vendor/animate.css">
<link rel="stylesheet"
	href="resources/assets/css/vendor/font-awesome.min.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/animsition/css/animsition.min.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/morris/morris.css">
<link rel="stylesheet"
	href="resources/assets/js/vendor/rickshaw/rickshaw.min.css">

<link rel="stylesheet" href="resources/assets/css/classic.css">



<!-- project main css files -->
<link rel="stylesheet" href="resources/assets/css/main.css">
<!--/ stylesheets -->



<!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
<script
	src="resources/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<!--/ modernizr -->

 		<script src="resources/assets/js/jquery-1.10.2.min.js"></script>
        <script src="resources/assets/js/jquery-ui.min.js"></script>
	
</head>

<body id="minovate" class="appWrapper sidebar-sm-forced">






	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->






<%-- <%
//allow access only if session exists
String user = null;
if(session.getAttribute("userName") == null){
	session.setAttribute("notLogin", "please login first");
	%>
	<jsp:forward page="login.jsp"></jsp:forward>
	<%
}else
	user = (String) session.getAttribute("userName");
	session.setMaxInactiveInterval(30*60);
%>
 --%>




	<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
	<div id="wrap" class="animsition">






		<!-- ===============================================
            ================= HEADER Content ===================
            ================================================ -->
		<section id="header" class="scheme-light">
			<header class="clearfix">

				<!-- Branding -->
				<div class="branding scheme-light">
					<a class="brand" href="index.html"> <span><strong>Demand</strong>Sense</span>
					</a> <a href="#" class="offcanvas-toggle visible-xs-inline"><i
						class="fa fa-bars"></i></a>
				</div>
				<!-- Branding end -->



				<!-- Left-side navigation -->
				<ul class="nav-left pull-left list-unstyled list-inline">
					<li class="sidebar-collapse divided-right"><a href="#"
						class="collapse-sidebar"> <i class="fa fa-outdent"></i>
					</a></li>
				</ul>
				<!-- Left-side navigation end -->




				<!-- Search -->
				<!-- <div class="search" id="main-search">
					<input type="text" class="form-control underline-input"
						placeholder="Search...">
				</div> -->
				<!-- Search end -->




				<img src="resources/assets/images/paper_boat_logo.png" alt="Image" class="img-circle size-50x50" style="margin-left: 30%;">
				<!-- Right-side navigation -->
				<ul class="nav-right pull-right list-inline">

					<li class="dropdown nav-profile"><a
						class="dropdown-toggle" data-toggle="dropdown"> <!-- <img
							src="resources/assets/images/Default_logo.jpg" alt="Image"
							class="img-circle size-30x30"> --> 
							<span>Welcome <%=session.getAttribute("loggedUserName") %><i class="fa fa-angle-down"></i></span>
					</a>


						<ul class="dropdown-menu animated littleFadeInRight" role="menu">

							
							<li><a href="profile"> <i class="fa fa-cog"></i>Profile
							</a></li>
							<li><a href="logs"> <i class="fa fa-list-alt"></i>Usage Log
							</a></li>
							<li class="divider"></li>
							<li><a href="/DemandSense/user/logout"> <i class="fa fa-sign-out"></i>Logout
							</a></li>

						</ul></li>
				</ul>
				<!-- Right-side navigation end -->



			</header>

		</section>
		<!--/ HEADER Content  -->





		<!-- =================================================
            ================= CONTROLS Content ===================
            ================================================== -->
		<div id="controls">





			<!-- ================================================
                ================= SIDEBAR Content ===================
                ================================================= -->
			<aside id="sidebar">


				<div id="sidebar-wrap">

					<div class="panel-group slim-scroll" role="tablist">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab">
								<h4 class="panel-title">
									<a data-toggle="collapse" href="#sidebarNav"> Navigation <i
										class="fa fa-angle-up"></i>
									</a>
								</h4>
							</div>
							<div id="sidebarNav" class="panel-collapse collapse in"
								role="tabpanel">
								<div class="panel-body">


									<!-- ===================================================
                                        ================= NAVIGATION Content ===================
                                        ==================================================== -->
									<ul id="navigation">
                                            <li><a href="index"><i class="fa fa-dashboard"></i> <span>Forecast Analysis</span></a></li>
                                            <li><a href="DataExplorer"><i class="fa fa-table"></i> <span>Data Explorer</span></a></li>
                                             <li class="active open"><a href="DataAudit"><i class="fa fa-search"></i> <span>Data Audit</span></a></li>
                                            <% if(session.getAttribute("userType").equals("user_admin") || session.getAttribute("userType").equals("super_admin")){ %>
                                            <li><a href="ExtractionApp"><i class="fa fa-upload"></i> <span>Data Upload</span></a></li>
											<%} %>
											<li><a href="report"><i class="fa fa-desktop"></i> <span>Report</span><span class="label label-success">new</span></a></li>
											<li><a href="support"><i class="fa fa-question-circle"></i> <span>Support</span><span class="label label-success">new</span></a></li>
											<!-- <li><a href="NewProductLaunch"><i class="fa fa-desktop"></i> <span>New Product Launch</span><span class="label label-success">new</span></a></li>
                                              <li><a href="#"><i class="fa fa-shopping-cart"></i> <span>Internal Experts</span> <span class="label label-success">new</span></a>                                          </li>
                                            <li><a href="#"><i class="fa fa-desktop"></i> <span>Prescriptive Analysis</span><span class="label label-success">new</span></a></li>
                                          -->
										   </ul>
									<!--/ NAVIGATION Content -->


								</div>
							</div>
						</div>
					</div>

				</div>


			</aside>
			<!--/ SIDEBAR Content -->





			<!-- =================================================
                ================= RIGHTBAR Content ===================
                ================================================== -->

			<!--/ RIGHTBAR Content -->




		</div>
		<!--/ CONTROLS Content -->




		<!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
		<section id="content">

			<div class="page page-sidebar-sm-layout">

				<div class="pageheader">

					<div class="page-bar">

						<ul class="page-breadcrumb">
							<li><a href="index.html"><i class="fa fa-home"></i>
									Rplus</a></li>
							<li><a href="index.html">Data Audit</a></li>
						</ul>

					</div>

				</div>

				

				

				<!-- External -->
				<div class="col-md-12">

					<!-- tile -->
					<section class="tile">

						<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
						<div class="tile-toggle" style="cursor: pointer;">
							<h1>
								<strong>Data Source Quality Report</strong>
							</h1> </div>
							<ul class="controls">
								<!-- <li><a role="button" tabindex="0" class="tile-refresh">
										<i class="fa fa-refresh"></i> Refresh
								</a></li> -->
								<li><a role="button" tabindex="0" class="tile-toggle">
										<span class="minimize"><i class="fa fa-angle-down"></i></span>
										<span class="expand"><i class="fa fa-angle-up"></i></span>
								</a></li>
							</ul>

						</div>
						<!-- /tile header -->

						<!-- tile body -->
						<div class="tile-body">
						    <div class="row">
						 <form class="form-inline" role="form" style="padding-top: 15px;">
						
						 <div class="form-group">
                                            <label class="col-sm-3 control-label"> Table Selection</label>
                                            <div class="col-sm-2">

                                                <select id="table_name" onchange="aud_graphs(this)" class="form-control mb-10">

                                                </select>

                                            </div>
                               </div>  
                               <div class="form-group" style="float:right;" >
                                            <label class="col-sm-3 control-label"> Field Selection</label>
                                            <div class="col-sm-2">
											
                                                 <select id="col_names" onchange="col_graphs(this)"  class="form-control mb-10">
                                                   
                                                </select>

                                            </div>
                               </div>
                                
                                    </form>
    
								<div class="col-lg-12">
								<h4  style="color: #000; margin-left: 14%;">Average Quality Score</h4>
									<div class="col-lg-5" id="qchart" style=" height: 300px;"></div>
									
									<div class="col-lg-6 col-sm-3"><h4 class="h4tag" style="color: #000; margin-left: 24%;    margin-top: -6%;">Average Quality Score</h4></div>
									<div class="col-lg-6 col-sm-3" style="overflow-y:auto; height: 240px;     margin-left:3%;">
										<div id="bar-ch" style=" height: 400px; width:500px;"></div> 
									</div>
									
								</div>
							
                                        
                                        <div class="panel-body">
                                    
                               <p style="padding-left: 80px;"><strong><span id="main_table"></span></strong> has  <span id="col_val"></span> fields & <span id="row_val"></span> records</p>
                          <!--  <p style="padding-left: 100px;">Missing Values thresold is 50% <a href=""> Change</a></button></p> -->
                                      
                                      <!--  <h4 class="custom-font" style="padding-left: 50px;"><strong>Results By Quality Measure</strong></h4>
                                     <p class="text-red" style="padding-left: 50px;"><u>Excluded</u></p>-->
                                       
                                       <div class="col-md-4">
                                        <div class="panel panel-default" style="border-color:#fff !important;">
                                        <div class="panel-heading">
                                            <h3 class="panel-title custom-font"> <strong>Results By Quality Measure</strong></h3>
                                           
                                        </div>
                                        <div class="panel-body" style="padding: 2px 14px 0px;">
                                        
                                               
                                        </div>
                                            <p ><a> <span id="const_val"></span>  Fields</a> (<span id="const_per"></span> %) have Constant Values</p>

                                            <p ><a> <span id="out_val"></span> Fields</a> (<span id="out_per"></span>%) have Outliers</p>

                                         	<p ><a> <span id="skew_val"></span> Fields</a> (<span id="skew_per"></span>%) have Skewed Distributions</p>
                                              <div class="panel-heading">
                                            <!-- <h3 class="panel-title custom-font"> <strong>Interesting</strong></h3> -->
                                             
                                        </div>
                                      
                                    </div>
                                    </div> <!-- col 4 end -->
                                        </div>
                                  
								

                           </div>
                                      	
                              </div>      
							<!-- /tile body -->
					</section>
					<!-- /tile -->

				</div>
				<!-- /External -->
				
			</div>

		</section>
		<!--/ CONTENT -->






	</div>
	<!--/ Application Content -->




	<!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
     
	

	<script src="resources/assets/js/vendor/bootstrap/bootstrap.min.js"></script>
	

	<script src="resources/assets/js/vendor/jRespond/jRespond.min.js"></script>


	<script
		src="resources/assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>

	<script
		src="resources/assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>

	<script
		src="resources/assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>

	<!--  <script src="resources/assets/js/vendor/daterangepicker/moment.min.js"></script>

        <script src="resources/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>-->

	
	<script src="resources/assets/js/vendor/screenfull/screenfull.min.js"></script>
	<script src="resources/assets/js/vendor/slider/bootstrap-slider.min.js"></script>


	<!--/ vendor javascripts -->

	<script src="resources/assets/js/highstock.js"></script>
	<script src="resources/assets/js/exporting.js"></script>
	<script src="resources/assets/js/export-csv.js"></script>
	<script src="resources/assets/js/highcharts-more.js"></script>

	<script src="resources/assets/js/solid-gauge.js"></script>
	<script src="resources/assets/js/dataAudit.js"></script>




	<!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
	<script src="resources/assets/js/jQAllRangeSliders-withRuler-min.js"></script>
	<script src="resources/assets/js/main.js"></script>
	
	<script src="resources/assets/js/spin.js"></script>
	

	<!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->




	<!--/ Page Specific Scripts -->

	<style type="text/css">
.panel-default {
    border-color: #fff !important;}
    .panel-default>.panel-heading { background-color: #fff !important;}
    
    @media screen and (max-width: 600px) { .h4tag { visibility: hidden; } }

</style>
</body>
</html>
