

<html><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css">
div
{
width: 1100px;
height: 500px;
}
</style>
<script src="multiline/jquery.min.js"></script>
<script src="multiline/highcharts.js"></script>
  
<script type="text/javascript">
$(function () {
    
	var data2 = [{"MONTH":1, "TOT_QTY":111},
{"MONTH":2, "TOT_QTY":102},
{"MONTH":3, "TOT_QTY":101},
{"MONTH":4, "TOT_QTY":107},
{"MONTH":5, "TOT_QTY":48}]
    
   // console.log(data);
    
    newData0 = [];
    newData1 = [];
    newData2 = [];
    newData3 = [];
    newData4 = [];
    newData5 = [];
    newData6 = [];
    newData7 = [];
    items = [];
    var months = ['1','2','3','4','5','6','7'];
    var j = 0;


    for (i in data2)
    {

        newData1.push( [ parseInt(data2[i]['MONTH']) ] );
        newData0.push([ parseInt(data2[i]['TOT_QTY']) ] );
        newData2.push( [ parseInt(data2[i]['TOT_QTY']) ] );
        newData6.push( [ parseInt(data2[i]['TOT_QTY']) ] );
        newData7.push( [ parseInt(data2[i]['TOT_QTY']) ] ); 
        
        
    //    items.push( [ parseInt(data2[i]['MONTH']) ] );
       items.push(months[parseInt(data2[i]['MONTH'])]);
       j++;

    }
    console.log(j);
    
      

    newData2.push( [ parseInt(93.8) ] );
    newData6.push( [ parseInt(120.0) ] );
    newData7.push( [ parseInt(93.800396726289) ] );
    //items.push(' 6 ');
    items.push(months[parseInt('6 ')]);

       
    
      

    newData2.push( [ parseInt(93.8) ] );
    newData6.push( [ parseInt(102.8739) ] );
    newData7.push( [ parseInt(93.800396726289) ] );
    //items.push(' 7 ');
    items.push(months[parseInt('7 ')]);

       
    
    
   
   
    
/*      

    
    newData2.push( [ parseInt(93.800396726289) ] );
       
    
      

    
    newData2.push( [ parseInt(93.800396726289) ] );
       
    
    
*/
    
    //console.log(newData1);

    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container'
        },
        title: {
            text: 'Forecast Prediction'
        },
        xAxis: {
        	categories:items,
        	title: {
                text: 'CW'
            },
            plotLines: [{
           	    color: 'red', // Color value
           	    dashStyle: 'longdashdot', // Style of the plot line. Default to solid
           	    value: (j - 1), // Value of where the line will appear
           	    width: 2,// Width of the line  
           	    label:
           	    	{
           	    	   text: 'Predicted values',
           	    	   x: +10,
           	    	}
           	  }]
        },
        yAxis: {
        	title: {
                text: 'EACH'
            },
        },
        
        series: [
{
	showInLegend: false,
    data: newData0,
    name: 'Actual results',
   
    zoneAxis: 'x',
    zones: [{
        value: (j - 1)
    }, {
        dashStyle: 'dot'
    }]
},
                 
        {
            data: newData2,
            name: 'Arima pred',
           zoneAxis: 'x',
    	    zones: [{
    	        value: (j - 1)
    	    }, {
    	        dashStyle: 'dot'
    	    }]   
        },
        {
        	data: newData6,
        	name:'Crost Pred',
        	zoneAxis: 'x',
    	    zones: [{
    	        value: (j - 1)
    	    }, {
    	        dashStyle: 'dot'
    	    }]   
        },
        {
        	data: newData7,
        	name:'exps_pred',
        	zoneAxis: 'x',
    	    zones: [{
    	        value: (j - 1)
    	    }, {
    	        dashStyle: 'dot'
    	    }]
        
        }]
    });
});
</script>
</head>
<body>
<div id="container" data-highcharts-chart="0"><div class="highcharts-container" id="highcharts-0" style="position: relative; overflow: hidden; width: 800px; height: 500px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="500"><desc>Created with Highcharts 4.1.5</desc><defs><clippath id="highcharts-1"><rect x="0" y="0" width="716" height="343"></rect></clippath><clippath id="highcharts-4"><rect x="0" y="0" width="460" height="800"></rect></clippath><clippath id="highcharts-5"><rect x="460" y="0" width="205" height="800"></rect></clippath><clippath id="highcharts-6"><rect x="0" y="0" width="460" height="800"></rect></clippath><clippath id="highcharts-7"><rect x="460" y="0" width="205" height="800"></rect></clippath><clippath id="highcharts-8"><rect x="0" y="0" width="460" height="800"></rect></clippath><clippath id="highcharts-9"><rect x="460" y="0" width="205" height="800"></rect></clippath><clippath id="highcharts-10"><rect x="0" y="0" width="460" height="800"></rect></clippath><clippath id="highcharts-11"><rect x="460" y="0" width="205" height="800"></rect></clippath></defs><rect x="0" y="0" width="800" height="500" strokeWidth="0" fill="#FFFFFF" class=" highcharts-background"></rect><path fill="none" d="M 534 53 L 534 396" stroke="red" stroke-width="2" stroke-dasharray="16,6,2,6"></path><text x="544" text-anchor="undefined" transform="translate(0,0) rotate(90 544 63)" style="" y="63" visibility="visible"><tspan>Predicted values</tspan></text><g class="highcharts-grid" zIndex="1"></g><g class="highcharts-grid" zIndex="1"><path fill="none" d="M 74 396.5 L 790 396.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 74 327.5 L 790 327.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 74 259.5 L 790 259.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 74 190.5 L 790 190.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 74 122.5 L 790 122.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 74 52.5 L 790 52.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path></g><g class="highcharts-axis" zIndex="2"><path fill="none" d="M 175.5 396 L 175.5 406" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 278.5 396 L 278.5 406" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 380.5 396 L 380.5 406" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 482.5 396 L 482.5 406" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 584.5 396 L 584.5 406" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 687.5 396 L 687.5 406" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 790.5 396 L 790.5 406" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 73.5 396 L 73.5 406" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><text x="432" zIndex="7" text-anchor="middle" transform="translate(0,0)" class=" highcharts-xaxis-title" style="color:#707070;fill:#707070;" visibility="visible" y="438">MONTH</text><path fill="none" d="M 74 396.5 L 790 396.5" stroke="#C0D0E0" stroke-width="1" zIndex="7" visibility="visible"></path></g><g class="highcharts-axis" zIndex="2"><text x="28.125" zIndex="7" text-anchor="middle" transform="translate(0,0) rotate(270 28.125 224.5)" class=" highcharts-yaxis-title" style="color:#707070;fill:#707070;" visibility="visible" y="224.5"><tspan>AVERAGE VALUE</tspan></text></g><g class="highcharts-series-group" zIndex="3"><path fill="rgba(124,181,236,0.25)" d="M 0 0"></path><path fill="rgba(144,237,125,0.25)" d="M 0 0"></path><path fill="rgba(67,67,72,0.25)" d="M 0 0"></path><g class="highcharts-series" visibility="visible" zIndex="0.1" transform="translate(74,53) scale(1 1)" clip-path="url(#highcharts-1)"><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56" stroke="#7cb5ec" stroke-width="2" zIndex="1" stroke-linejoin="round" stroke-linecap="round" visibility="hidden"></path><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56" stroke="#7cb5ec" stroke-width="2" zIndex="1" stroke-linejoin="round" stroke-linecap="round" clip-path="url(#highcharts-4)"></path><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56" stroke="#7cb5ec" stroke-width="2" zIndex="1" stroke-dasharray="2,6" clip-path="url(#highcharts-5)"></path><path fill="none" d="M 41.142857142857146 99.47 L 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 470.28571428571433 315.56" stroke-linejoin="round" visibility="visible" stroke="rgba(192,192,192,0.0001)" stroke-width="22" zIndex="2" class=" highcharts-tracker" style=""></path></g><g class="highcharts-markers highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(74,53) scale(1 1)" clip-path="url(#highcharts-2)" style=""><path fill="#7cb5ec" d="M 460.28571428571433 311.56 C 465.6137142857143 311.56 465.6137142857143 319.56 460.28571428571433 319.56 C 454.95771428571436 319.56 454.95771428571436 311.56 460.28571428571433 311.56 Z" stroke-width="1"></path><path fill="#7cb5ec" d="M 358.00000000000006 109.19 C 363.32800000000003 109.19 363.32800000000003 117.19 358.00000000000006 117.19 C 352.6720000000001 117.19 352.6720000000001 109.19 358.00000000000006 109.19 Z" stroke-width="1"></path><path fill="#7cb5ec" d="M 255.71428571428572 129.76999999999998 C 261.0422857142857 129.76999999999998 261.0422857142857 137.76999999999998 255.71428571428572 137.76999999999998 C 250.38628571428572 137.76999999999998 250.38628571428572 129.76999999999998 255.71428571428572 129.76999999999998 Z" stroke-width="1"></path><path fill="#7cb5ec" d="M 153.42857142857144 126.34 C 158.75657142857145 126.34 158.75657142857145 134.34 153.42857142857144 134.34 C 148.10057142857144 134.34 148.10057142857144 126.34 153.42857142857144 126.34 Z" stroke-width="1"></path><path fill="#7cb5ec" d="M 51.142857142857146 95.47 C 56.47085714285715 95.47 56.47085714285715 103.47 51.142857142857146 103.47 C 45.81485714285714 103.47 45.81485714285714 95.47 51.142857142857146 95.47 Z" stroke-width="1"></path></g><g class="highcharts-series" visibility="visible" zIndex="0.1" transform="translate(74,53) scale(1 1)" clip-path="url(#highcharts-1)"><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 161.20999999999998 L 664.8571428571429 161.20999999999998" stroke="#434348" stroke-width="2" zIndex="1" stroke-linejoin="round" stroke-linecap="round" visibility="hidden"></path><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 161.20999999999998 L 664.8571428571429 161.20999999999998" stroke="#434348" stroke-width="2" zIndex="1" stroke-linejoin="round" stroke-linecap="round" clip-path="url(#highcharts-6)"></path><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 161.20999999999998 L 664.8571428571429 161.20999999999998" stroke="#434348" stroke-width="2" zIndex="1" stroke-dasharray="2,6" clip-path="url(#highcharts-7)"></path><path fill="none" d="M 41.142857142857146 99.47 L 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 161.20999999999998 L 664.8571428571429 161.20999999999998 L 674.8571428571429 161.20999999999998" stroke-linejoin="round" visibility="visible" stroke="rgba(192,192,192,0.0001)" stroke-width="22" zIndex="2" class=" highcharts-tracker" style=""></path></g><g class="highcharts-markers highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(74,53) scale(1 1)" clip-path="url(#highcharts-2)" style=""><path fill="#434348" d="M 664.8571428571429 157.20999999999998 L 668.8571428571429 161.20999999999998 664.8571428571429 165.20999999999998 660.8571428571429 161.20999999999998 Z" stroke-width="1"></path><path fill="#434348" d="M 562.5714285714286 157.20999999999998 L 566.5714285714286 161.20999999999998 562.5714285714286 165.20999999999998 558.5714285714286 161.20999999999998 Z" stroke-width="1"></path><path fill="#434348" d="M 460 311.56 L 464 315.56 460 319.56 456 315.56 Z"></path><path fill="#434348" d="M 358 109.19 L 362 113.19 358 117.19 354 113.19 Z"></path><path fill="#434348" d="M 255 129.76999999999998 L 259 133.76999999999998 255 137.76999999999998 251 133.76999999999998 Z"></path><path fill="#434348" d="M 153 126.34 L 157 130.34 153 134.34 149 130.34 Z"></path><path fill="#434348" d="M 51 95.47 L 55 99.47 51 103.47 47 99.47 Z"></path></g><g class="highcharts-series" visibility="visible" zIndex="0.1" transform="translate(74,53) scale(1 1)" clip-path="url(#highcharts-1)"><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 68.59999999999997 L 664.8571428571429 130.34" stroke="#90ed7d" stroke-width="2" zIndex="1" stroke-linejoin="round" stroke-linecap="round" visibility="hidden"></path><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 68.59999999999997 L 664.8571428571429 130.34" stroke="#90ed7d" stroke-width="2" zIndex="1" stroke-linejoin="round" stroke-linecap="round" clip-path="url(#highcharts-8)"></path><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 68.59999999999997 L 664.8571428571429 130.34" stroke="#90ed7d" stroke-width="2" zIndex="1" stroke-dasharray="2,6" clip-path="url(#highcharts-9)"></path><path fill="none" d="M 41.142857142857146 99.47 L 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 68.59999999999997 L 664.8571428571429 130.34 L 674.8571428571429 130.34" stroke-linejoin="round" visibility="visible" stroke="rgba(192,192,192,0.0001)" stroke-width="22" zIndex="2" class=" highcharts-tracker" style=""></path></g><g class="highcharts-markers highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(74,53) scale(1 1)" clip-path="url(#highcharts-2)" style=""><path fill="#90ed7d" d="M 660.8571428571429 126.34 L 668.8571428571429 126.34 668.8571428571429 134.34 660.8571428571429 134.34 Z" stroke-width="1"></path><path fill="#90ed7d" d="M 558.5714285714286 64.59999999999997 L 566.5714285714286 64.59999999999997 566.5714285714286 72.59999999999997 558.5714285714286 72.59999999999997 Z" stroke-width="1"></path><path fill="#90ed7d" d="M 456 311.56 L 464 311.56 464 319.56 456 319.56 Z"></path><path fill="#90ed7d" d="M 354 109.19 L 362 109.19 362 117.19 354 117.19 Z"></path><path fill="#90ed7d" d="M 251 129.76999999999998 L 259 129.76999999999998 259 137.76999999999998 251 137.76999999999998 Z"></path><path fill="#90ed7d" d="M 149 126.34 L 157 126.34 157 134.34 149 134.34 Z"></path><path fill="#90ed7d" d="M 47 95.47 L 55 95.47 55 103.47 47 103.47 Z"></path></g><g class="highcharts-series" visibility="visible" zIndex="0.1" transform="translate(74,53) scale(1 1)" clip-path="url(#highcharts-1)"><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 161.20999999999998 L 664.8571428571429 161.20999999999998" stroke="#f7a35c" stroke-width="2" zIndex="1" stroke-linejoin="round" stroke-linecap="round" visibility="hidden"></path><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 161.20999999999998 L 664.8571428571429 161.20999999999998" stroke="#f7a35c" stroke-width="2" zIndex="1" stroke-linejoin="round" stroke-linecap="round" clip-path="url(#highcharts-10)"></path><path fill="none" d="M 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 161.20999999999998 L 664.8571428571429 161.20999999999998" stroke="#f7a35c" stroke-width="2" zIndex="1" stroke-dasharray="2,6" clip-path="url(#highcharts-11)"></path><path fill="none" d="M 41.142857142857146 99.47 L 51.142857142857146 99.47 L 153.42857142857144 130.34 L 255.71428571428572 133.76999999999998 L 358.00000000000006 113.19 L 460.28571428571433 315.56 L 562.5714285714286 161.20999999999998 L 664.8571428571429 161.20999999999998 L 674.8571428571429 161.20999999999998" stroke-linejoin="round" visibility="visible" stroke="rgba(192,192,192,0.0001)" stroke-width="22" zIndex="2" class=" highcharts-tracker" style=""></path></g><g class="highcharts-markers highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(74,53) scale(1 1)" clip-path="url(#highcharts-2)" style=""><path fill="#f7a35c" d="M 664 157.20999999999998 L 668 165.20999999999998 660 165.20999999999998 Z"></path><path fill="#f7a35c" d="M 562 157.20999999999998 L 566 165.20999999999998 558 165.20999999999998 Z"></path><path fill="#f7a35c" d="M 460 311.56 L 464 319.56 456 319.56 Z"></path><path fill="#f7a35c" d="M 358 109.19 L 362 117.19 354 117.19 Z"></path><path fill="#f7a35c" d="M 255 129.76999999999998 L 259 137.76999999999998 251 137.76999999999998 Z"></path><path fill="#f7a35c" d="M 153 126.34 L 157 134.34 149 134.34 Z"></path><path fill="#f7a35c" d="M 51 95.47 L 55 103.47 47 103.47 Z"></path></g></g><text x="400" text-anchor="middle" class="highcharts-title" zIndex="4" style="color:#333333;font-size:18px;fill:#333333;width:736px;" y="24"><tspan>Forecast Prediction</tspan></text><g class="highcharts-legend" zIndex="7" transform="translate(244,456)"><g zIndex="1"><g><g class="highcharts-legend-item" zIndex="1" transform="translate(8,3)"><path fill="none" d="M 0 11 L 16 11" stroke="#434348" stroke-width="2"></path><path fill="#434348" d="M 8 7 L 12 11 8 15 4 11 Z"></path><text x="21" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2" y="15"><tspan>Arima pred</tspan></text></g><g class="highcharts-legend-item" zIndex="1" transform="translate(116.15625,3)"><path fill="none" d="M 0 11 L 16 11" stroke="#90ed7d" stroke-width="2"></path><path fill="#90ed7d" d="M 4 7 L 12 7 12 15 4 15 Z"></path><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2"><tspan>Crost Pred</tspan></text></g><g class="highcharts-legend-item" zIndex="1" transform="translate(220.09375,3)"><path fill="none" d="M 0 11 L 16 11" stroke="#f7a35c" stroke-width="2"></path><path fill="#f7a35c" d="M 8 7 L 12 15 4 15 Z"></path><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2">exps_pred</text></g></g></g></g><g class="highcharts-axis-labels highcharts-xaxis-labels" zIndex="7"><text x="125.14285714285714" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:92px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="415" opacity="1">Jan</text><text x="227.42857142857142" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:92px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="415" opacity="1">Feb</text><text x="329.7142857142857" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:92px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="415" opacity="1">Mar</text><text x="432" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:92px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="415" opacity="1">April</text><text x="534.2857142857144" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:92px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="415" opacity="1">May</text><text x="636.5714285714286" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:92px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="415" opacity="1">June</text><text x="738.8571428571429" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:92px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="415" opacity="1">July</text></g><g class="highcharts-axis-labels highcharts-yaxis-labels" zIndex="7"><text x="59" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:254px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="399" opacity="1">40</text><text x="59" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:254px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="330" opacity="1">60</text><text x="59" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:254px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="261" opacity="1">80</text><text x="59" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:254px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="193" opacity="1">100</text><text x="59" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:254px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="124" opacity="1">120</text><text x="59" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:254px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="56" opacity="1">140</text></g><g class="highcharts-tooltip" zIndex="8" style="cursor:default;padding:0;white-space:nowrap;" transform="translate(680,-9999)" opacity="0" visibility="visible"><path fill="none" d="M 3.5 0.5 L 115.5 0.5 C 118.5 0.5 118.5 0.5 118.5 3.5 L 118.5 44.5 C 118.5 47.5 118.5 47.5 115.5 47.5 L 65.5 47.5 59.5 53.5 53.5 47.5 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)" width="118" height="47"></path><path fill="none" d="M 3.5 0.5 L 115.5 0.5 C 118.5 0.5 118.5 0.5 118.5 3.5 L 118.5 44.5 C 118.5 47.5 118.5 47.5 115.5 47.5 L 65.5 47.5 59.5 53.5 53.5 47.5 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)" width="118" height="47"></path><path fill="none" d="M 3.5 0.5 L 115.5 0.5 C 118.5 0.5 118.5 0.5 118.5 3.5 L 118.5 44.5 C 118.5 47.5 118.5 47.5 115.5 47.5 L 65.5 47.5 59.5 53.5 53.5 47.5 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)" width="118" height="47"></path><path fill="rgba(249, 249, 249, .85)" d="M 3.5 0.5 L 115.5 0.5 C 118.5 0.5 118.5 0.5 118.5 3.5 L 118.5 44.5 C 118.5 47.5 118.5 47.5 115.5 47.5 L 65.5 47.5 59.5 53.5 53.5 47.5 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#434348" stroke-width="1"></path><text x="8" zIndex="1" style="font-size:12px;color:#333333;fill:#333333;" transform="translate(0,20)"><tspan style="font-size: 10px">July</tspan><tspan style="fill:" x="8" dy="15">&#9679;</tspan><tspan dx="0"> Arima pred: </tspan><tspan style="font-weight:bold" dx="0">93</tspan></text></g></svg></div></div>

</body></html>