<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">

    <!-- Bootstrap 3.3.4 -->
   <!-- Ion Slider -->
    <link href="ionslider/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
    <!-- ion slider Nice -->
    <link href="ionslider/ion.rangeSlider.skinNice.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap slider -->
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      

      <!-- Left side column. contains the logo and sidebar -->
      

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
               
                <div class="box-body">
                 
                  <div class="row margin">
                    <div class="col-sm-12">
                      <input id="range_5" type="text" name="range_5" value="" />
                    </div>
                    
                  </div>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

         
        </section><!-- /.content -->
		
		
      </div><!-- /.content-wrapper -->
     
      
      <!-- Control Sidebar -->      
      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class='control-sidebar-bg'></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="drag/jQuery-2.1.4.min.js"></script>
    
    <!-- Ion Slider -->
    <script src="ionslider/ion.rangeSlider.min.js" type="text/javascript"></script>

    <!-- Bootstrap slider -->
    <script src="ionslider/bootstrap-slider/bootstrap-slider.js" type="text/javascript"></script>

    <script type="text/javascript">
      $(function () {
        /* BOOTSTRAP SLIDER */
        $('.slider').slider();

        /* ION SLIDER */
        $("#range_1").ionRangeSlider({
          min: 0,
          max: 5000,
          from: 1000,
          to: 4000,
          type: 'double',
          step: 1,
          prefix: "$",
          prettify: false,
          hasGrid: true
        });
        $("#range_2").ionRangeSlider();

        $("#range_5").ionRangeSlider({
          min: 1,
          max: 53,
          type: 'single',
          step: 1,
          postfix: " Weeks",
          prettify: false,
          hasGrid: true
        });
        $("#range_6").ionRangeSlider({
          min: -50,
          max: 50,
          from: 0,
          type: 'single',
          step: 1,
          postfix: "°",
          prettify: false,
          hasGrid: true
        });

        $("#range_4").ionRangeSlider({
          type: "single",
          step: 100,
          postfix: " light years",
          from: 55000,
          hideMinMax: true,
          hideFromTo: false
        });
        $("#range_3").ionRangeSlider({
          type: "double",
          postfix: " miles",
          step: 10000,
          from: 25000000,
          to: 35000000,
          onChange: function (obj) {
            var t = "";
            for (var prop in obj) {
              t += prop + ": " + obj[prop] + "\r\n";
            }
            $("#result").html(t);
          },
          onLoad: function (obj) {
            //
          }
        });
      });
    </script>

  </body>
</html>
