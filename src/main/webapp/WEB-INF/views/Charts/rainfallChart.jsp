<!DOCTYPE html>
<html>
<head>
    <base href="http://demos.telerik.com/kendo-ui/line-charts/visuals">
    <style>html { font-size: 14px; font-family: Arial, Helvetica, sans-serif; }</style>
    <title></title>
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2015.2.624/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2015.2.624/styles/kendo.material.min.css" />

    <script src="http://cdn.kendostatic.com/2015.2.624/js/jquery.min.js"></script>
    <script src="http://cdn.kendostatic.com/2015.2.624/js/kendo.all.min.js"></script>
</head>
<body>
<div id="example">
    <div class="demo-section k-content">
        <div id="chart"></div>
    </div>
    <script>
        function createChart() {
            var foreCast = [{
                temperature: 15,
                weather: "cloudy",
                day: "Monday"
            }, {
                temperature: 16,
                weather: "rainy",
                day: "Tuesday"
            }, {
                temperature: 20,
                weather: "cloudy",
                day: "Wednesday"
            }, {
                temperature: 23,
                weather: "sunny",
                day: "Thursday"
            }, {
                temperature: 17,
                weather: "cloudy",
                day: "Friday"
            }, {
                temperature: 20,
                weather: "sunny",
                day: "Saturday"
            }, {
                temperature: 25,
                weather: "sunny",
                day: "Sunday"
            }];

            $("#chart").kendoChart({
                dataSource: foreCast,
                title: {
                    text: "Forecast"
                },
                seriesDefaults: {
                    type: "line",
                    style: "smooth"
                },
                series: [{
                    field: "temperature",
                    categoryField: "day",
                    highlight: {
                        toggle: function (e) {
                            // Don't create a highlight overlay,
                            // we'll modify the existing visual instead
                            e.preventDefault();

                            var visual = e.visual;
                            var transform = null;
                            if (e.show) {
                                var center = visual.rect().center();

                                // Uniform 1.5x scale with the center as origin
                                transform = kendo.geometry.transform().scale(1.5, 1.5, center);
                            }

                            visual.transform(transform);
                        }
                    },
                    markers: {
                        size: 32,
                        visual: function (e) {
                            var src = kendo.format("../content/dataviz/chart/images/{0}.png", e.dataItem.weather);
                            var image = new kendo.drawing.Image(src, e.rect);
                            return image;
                        }
                    }
                }],
                valueAxis: {
                    labels: {
                        template: "#:value#℃"
                    }
                },
                categoryAxis: {
                    majorGridLines: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: true,
                    format: "{0}%",
                    template: "#= category #: #= value #℃"
                }
            });
        }

        $(document).ready(createChart);
        $(document).bind("kendo:skinChange", createChart);
    </script>
</div>


</body>
</html>
