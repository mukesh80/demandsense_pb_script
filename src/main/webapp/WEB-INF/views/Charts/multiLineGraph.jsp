<!DOCTYPE html>
<meta charset="utf-8">
<style>


.d3-tip {
        line-height: 1;
        font-weight: bold;
        font-size: 11px;
        padding: 12px;
        background: rgba(0, 0, 0, 0.8);
        color: #fff;
        border-radius: 2px;
      }
 .datapoint:hover
 {
	fill: steelblue;
 }

body {
  font: 10px sans-serif;
}

.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.x.axis path {
  
}

.line {
  fill: none;
  stroke: steelblue;
  stroke-width: 1.5px;
}

</style>
<body>
<script src="../Scripts/d3.min.js"></script>
<script src="../Scripts/d3.tip.v0.6.3.js"></script>
<script>

var margin = {top: 20, right: 20, bottom: 30, left: 50},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var parseDate = d3.time.format("%Y%m").parse;
//var format = d3.time.format("%y").parse;
//var parseDate = d3.time.format("%a-%b-%d-%Y").parse;

var x = d3.time.scale()
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .ticks(20);

var line1 = d3.svg.line()
    .x(function(d) { return x(d.YearMonth); })
    .y(function(d) { return y(d.actual); });
    
var line2 = d3.svg.line()
.x(function(d) { return x(d.YearMonth); })
.y(function(d) { return y(d.arima_results); });

var line3 = d3.svg.line()
.x(function(d) { return x(d.YearMonth); })
.y(function(d) { return y(d.crost_results); });

var line4 = d3.svg.line()
.x(function(d) { return x(d.YearMonth); })
.y(function(d) { return y(d.holt_results); });

var line5 = d3.svg.line()
.x(function(d) { return x(d.YearMonth); })
.y(function(d) { return y(d.ses_results); });

var line6 = d3.svg.line()
.x(function(d) { return x(d.YearMonth); })
.y(function(d) { return y(d.hw_results); });

var line7 = d3.svg.line()
.x(function(d) { return x(d.YearMonth); })
.y(function(d) { return y(d.ce_results); });

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    
    
var tip1 = d3.tip()
.attr('class', 'd3-tip')
.style("background", "#F5A9A9")
.offset([120, 40])
.html(function(d) {
    return "<strong>" + d.date +"<br/>Actual Results:"+d.actual+"</strong>";
});

svg.call(tip1);


var tip2 = d3.tip()
.attr('class', 'd3-tip')
.style("background", "#FA8258")
.offset([120, 40])
.html(function(d) {
    return "<strong>" + d.date +"<br/>Arima Results:"+d.arima_results+"</strong>";
});

svg.call(tip2);



var tip3 = d3.tip()
.attr('class', 'd3-tip')
.style("background", "#FF8000")
.offset([120, 40])
.html(function(d) {
    return "<strong>" + d.date +"<br/>crost_results:"+d.crost_results+"</strong>";
});



svg.call(tip3);

var tip4 = d3.tip()
.attr('class', 'd3-tip')
.style("background", "#FFBF00")
.offset([120, 40])
.html(function(d) {
    return "<strong>" + d.date +"<br/>holt_results:"+d.holt_results+"</strong>";
});

svg.call(tip4);


var tip5 = d3.tip()
.attr('class', 'd3-tip')
.style("background", "#C8FE2E")
.offset([120, 40])
.html(function(d) {
    return "<strong>" + d.date +"<br/>ses_results:"+d.ses_results+"</strong>";
});



svg.call(tip5);



var tip6 = d3.tip()
.attr('class', 'd3-tip')
.style("background", "#00FF40")
.offset([120, 40])
.html(function(d) {
    return "<strong>" + d.date +"<br/>hw_results:"+d.hw_results+"</strong>";
});



svg.call(tip6);

var tip7 = d3.tip()
.attr('class', 'd3-tip')
.style("background", "#0080FF")
.offset([120, 40])
.html(function(d) {
    return "<strong>" + d.date +"<br/>ce_results:"+d.ce_results+"</strong>";
});



svg.call(tip7);



    
d3.json("../Data/MultiLineGraph_3.json", function(error, data) {
  data.forEach(function(d) 
  {
	d.datee = d.YearMonth.toString();
    d.YearMonth = parseDate((d.YearMonth).toString());
    d.date = d.datee.substring(4,6)+"-"+d.datee.substring(0,4);
    d.actual = +d.actual;
    d.arima_results = +d.arima_results;
    d.holt_results = +d.holt_results;
    d.crost_results = +d.crost_results;
    d.ce_results = +d.ce_results;
    d.hw_results = +d.hw_results;
    d.ses_results = +d.ses_results;
  });

  x.domain(d3.extent(data, function(d) { return d.YearMonth; }));
  y.domain([10000,d3.max(data, function(d) {if(d.actual>0){ return d.actual;}else
	  if(d.arima_results>0)
		  {
		  return d.arima_results;
		  }
	  else
		  if(d.holt_results>0)
			  {
			  return d.holt_results;
			  }
		  else
			  if(d.crost_results>0)
				  {
				  return d.crost_results;
				  }
			  else
				  if(d.ce_results>0)
					  {
					  return d.ce_results;
					  }
				  else
					  if(d.hw_results>0)
						  {
						  return d.hw_results;
						  }
					  else
						  if(d.hw_results>0)
						  {
						  return d.ses_results;
						  }
						  else
							  return 20000;
					  })]);

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("results");

  svg.append("path")
  .datum(data)
  .attr("class", "line")
  .style("stroke", "#F5A9A9")
  .attr("d", line1)
  
   svg.append("path")
  .datum(data)
  .attr("class", "line")
  .style("stroke", "#FA8258")
  .attr("d", line2)

svg.append("path") 
.datum(data)
.attr("class", "line")
.style("stroke", "#FF8000")
.attr("d", line3)

svg.append("path")
  .datum(data)
  .attr("class", "line")
  .style("stroke", "#FFBF00")
  .attr("d", line4)
  
  svg.append("path")
      .datum(data)
      .attr("class", "line")
      .style("stroke", "#C8FE2E")
      .attr("d", line5)
  
  svg.append("path") 
  .datum(data)
  .attr("class", "line")
  .style("stroke", "#00FF40")
  .attr("d", line6)
  
  svg.append("path")
      .datum(data)
      .attr("class", "line")
      .style("stroke", "#0080FF")
      .attr("d", line7);
  
  svg.selectAll(".dot")
  .data(data)
  .enter().append("circle")
  .attr('class', 'datapoint')
  .attr('cx', function(d) { if(d.actual>0){return x(d.YearMonth);} })
  .attr('cy', function(d) { return y(d.actual); })
  .attr('r', 6)
  .attr('fill', '#F5A9A9')
  .attr('stroke-width', '3')
  .on('mouseover', tip1.show)
  .on('mouseout', tip1.hide);
  
  
  svg.selectAll(".dot")
  .data(data)
  .enter().append("circle")
  .attr('class', 'datapoint')
  .attr('cx', function(d) { if(d.arima_results>0){return x(d.YearMonth);} })
  .attr('cy', function(d) { return y(d.arima_results); })
  .attr('r', 6)
  .attr('fill', '#FA8258')
  //.attr('stroke', 'steelblue')
  .attr('stroke-width', '3')
  //.on('mouseover', tip1.show)
  .on('mouseover', tip2.show)
  .on('mouseout', tip2.hide);
  
  svg.selectAll(".dot")
  .data(data)
  .enter().append("circle")
  .attr('class', 'datapoint')
  .attr('cx', function(d) { if(d.crost_results>0){ return x(d.YearMonth); } })
  .attr('cy', function(d) { if(d.crost_results!=null){return y(d.crost_results);} })
  .attr('r', 6)
  .attr('fill', '#FF8000')
  //.attr('stroke', 'steelblue')
  .attr('stroke-width', '3')
  //.on('mouseover', tip1.show)
  //.on('mouseover', tip3.show)
  
  
  .on('mouseover', tip3.show)
  .on('mouseout', tip3.hide);
  
  svg.selectAll(".dot")
  .data(data)
  .enter().append("circle")
  .attr('class', 'datapoint')
  .attr('cx', function(d) { if(d.holt_results>0){ return x(d.YearMonth);}})
  .attr('cy', function(d) { return y(d.holt_results); })
  .attr('r', 6)
  .attr('fill', '#FFBF00')
  //.attr('stroke', 'steelblue')
  .attr('stroke-width', '3')
  //.on('mouseover', tip1.show)
  .on('mouseover', tip4.show)
  .on('mouseout', tip4.hide);
  
  svg.selectAll(".dot")
  .data(data)
  .enter().append("circle")
  .attr('class', 'datapoint')
  .attr('cx', function(d) { if(d.ses_results>0){return x(d.YearMonth); }})
  .attr('cy', function(d) { return y(d.ses_results); })
  .attr('r', 6)
  .attr('fill', '#C8FE2E')
  //.attr('stroke', 'steelblue')
  .attr('stroke-width', '3')
  //.on('mouseover', tip1.show)
  .on('mouseover', tip5.show)
  .on('mouseout', tip5.hide);
  
  svg.selectAll(".dot")
  .data(data)
  .enter().append("circle")
  .attr('class', 'datapoint')
  .attr('cx', function(d) { if(d.hw_results>0){return x(d.YearMonth);} })
  .attr('cy', function(d) { return y(d.hw_results); })
  .attr('r', 6)
  .attr('fill', '#00FF40')
  //.attr('stroke', 'steelblue')
  .attr('stroke-width', '3')
  //.on('mouseover', tip1.show)
  .on('mouseover', tip6.show)
  .on('mouseout', tip6.hide);
  
  svg.selectAll(".dot")
  .data(data)
  .enter().append("circle")
  .attr('class', 'datapoint')
  .attr('cx', function(d) { if(d.ce_results>0){return x(d.YearMonth); }})
  .attr('cy', function(d) { return y(d.ce_results); })
  .attr('r', 6)
  .attr('fill', '#0080FF')
  //.attr('stroke', 'steelblue')
  .attr('stroke-width', '3')
  //.on('mouseover', tip1.show)
  .on('mouseover', tip7.show)
  .on('mouseout', tip7.hide);
  
  
  
});

</script>