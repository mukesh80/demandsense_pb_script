<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>rJava Admin</title>
<script type="text/javascript">

	function doValidation()	{
		var val = 0;
		var str1 = "", str2 = "";
		var flg = false;
		str1 = Frm1.Thread_Cnt.value;
		if(str1.trim().length>0) {
			val = parseInt(str1, 10);
			if(val <= 0) {
				alert("Please Enter Thread Count")
				return flg;
			}
		}
		
		str1 = Frm1.Exist_R_Files.value;
		str2 = Frm1.R_File.value;
		if(str1 == "select" && str2.trim().length > 0) {
			// Fine // New R file
		}
		else if(str1.trim().length > 0 && str2.trim().length == 0) {
			// Fine // Exist R file
		}
		else {
			alert("Check both Existing and New R File\r\n\"It Should be either one\"");
			return flg;
		}
		
		str1 = Frm1.Tbl_name.value;
		str2 = Frm1.Schema_name.value;
		if(str1.trim().length > 0 && str2.trim().length > 0) {
			// Fine // Schema and Table present
		}
		else {
			alert("Check for both \"Table\" and \"Schema\" name");
			return flg;
		}
		flg = true;
		return flg;
	}
	
	function doSave() {
		alert("doSave");
		// Frm1.R_File.value;
		var XHR = new XMLHttpRequest();
		var formData = new FormData();
		//var StrURL = "http://localhost:8080/DemandSense/rAdmin/rJavaMultiThreadSave";
		var StrURL = "/DemandSense/rAdmin/rJavaMultiThreadSave";
		
		formData.append("Thread_Cnt",Frm1.Thread_Cnt.value);
		formData.append("Exist_R_Files",Frm1.Exist_R_Files.value);
		formData.append("R_File",Frm1.R_File.value);
		formData.append("Tbl_name",Frm1.Tbl_name.value);
		formData.append("Schema_name",Frm1.Schema_name.value);
		formData.append("JVM_Ram_size",Frm1.JVM_Ram_size.value);
		formData.append("JVM_Heap_size",Frm1.JVM_Heap_size.value);
		alert("Data income completed");
		XHR.open("POST", StrURL, true);
		XHR.onreadystatechange=function(){
		    if(XHR.readyState==4 && XHR.status==200) {
		    	//alert("REST Return: " + XHR.responseText);
		    	var res = XHR.responseText;
		    	if(res == "true")
		    		doExecuteR();
		    	else
		    		alert("Thread settings saved Successfully");
		    }
		}
		XHR.send(formData);
	}
	
	function doExecute() {
		alert("doExecute");
		var XHR = new XMLHttpRequest();
		var formData = new FormData();
		var Fil_Obj = document.getElementById("R_File");
		var fil = Fil_Obj.files;
		//var StrURL = "http://localhost:8080/DemandSense/rAdmin/rJavaMultiThreadProc";
		var StrURL = "/DemandSense/rAdmin/rJavaMultiThreadProc";
		//alert("doExecute: " + Frm1.hR_File.value);
		formData.append(Fil_Obj.name, fil[0], fil[0].name);
		formData.append("hR_File", hR_File.value);
		formData.append("Thread_Cnt",Frm1.Thread_Cnt.value);
		formData.append("Tbl_name",Frm1.Tbl_name.value);
		formData.append("Schema_name",Frm1.Schema_name.value);
		formData.append("JVM_Ram_size",Frm1.JVM_Ram_size.value);
		formData.append("JVM_Heap_size",Frm1.JVM_Heap_size.value);
		//XHR.open("PUT", StrURL, true);
		XHR.open("POST", StrURL, true);
		XHR.onreadystatechange=function(){
		    if(XHR.readyState==4 && XHR.status==200) {
		    	//alert("REST Return: " + XHR.responseText);
		    	var res = XHR.responseText;
		    	if(res == "true")
		    		doExecuteR();
		    	else
		    		alert("R file upload filed");
		    }
		    else if(XHR.readyState==1) {
		    	//alert(StrURL + ": Server connection established");
	    	}
		    else if(XHR.readyState==2) {
		    	//alert(StrURL + ": Request received");
	    	}
		    else if(XHR.readyState==3) {
		    	//alert(StrURL + ": Processing request ");
	    	}
		}
		XHR.send(formData);
	}
	
	function doExecuteR(){
		alert("R Process Completed");
		//var XHR = new XMLHttpRequest();
	}
	
	function doStop_Proc(){
		alert("doStop_Proc");
	}
	
	function doShow_Log(){
		alert("doShow_Log");
	}
   
	function copyFile2Hid(Fil_Fld){
		var Fld_Nam = Fil_Fld.name;
		var Fld_Val = Fil_Fld.value;
		//alert("copyFile2Hid: " + Fld_Nam + " : " + Fld_Val);
		if(Fld_Nam.length > 0 && Fld_Val.length > 0 ){
			var HFld_Str = "h" + Fld_Nam;
			var HFld_Obj = document.getElementById(HFld_Str);
			HFld_Obj.value = Fld_Val;
			//alert("copyFile2Hid: " + Fld_Val);
		}
	}
	
	function copyFileName2Hid(){
		var Fld_Nam = Frm1.R_File.name;
		var Fld_Val = Frm1.R_File.value;
		//alert(Fld_Nam + " : " + Fld_Val);
		if(Fld_Nam.length > 0 && Fld_Val.length > 0 ){
			var HFld_Str = "h" + Fld_Nam;
			var HFld_Obj = document.getElementById(HFld_Str);
			HFld_Obj.value = Fld_Val;
			//alert("copyFileName2Hid: " + Fld_Val);
			//alert("copyFileName2Hid: " + Frm1.hR_File.value);
		}
	}
</script>
</head>
<body>
	<center>
	<h2><font color="##00ff00">Admin for rJava</font></h2>
	<br/>
	<hr/>
	<br/>
	<!-- 
	<table width="60%">
	 -->
	<form action="rJavaThread" method="post" name="Frm1" enctype="multipart/form-data">
		<table>
			<tr><td>Thread Count:&nbsp;</td><td><input name="Thread_Cnt" id="Thread_Cnt" size="20" value="${ThreadCount}" ></input></td></tr>
			<tr><td>Existing R Files:&nbsp;</td><td><select name="Exist_R_Files" id="Exist_R_Files" style="width: 168px;">${Existing_R_Files}</select></td></tr>
			<tr><td>R File (New):&nbsp;</td><td><input type="file" name="R_File" id="R_File" size="30" onblur="copyFile2Hid(this);" /></td></tr>
			<tr><td>Table name:&nbsp;</td><td><input name="Tbl_name" id="Tbl_name" size="20" value="${Table_Name}" /></td></tr> <!-- onfocus="copyFileName2Hid();" -->
			<tr><td>Schema name:&nbsp;</td><td><input name="Schema_name" id="Schema_name" size="20" value="${Schema_Name}" /></td></tr>
			<tr><td>JVM Ram size:&nbsp;</td><td><input name="JVM_Ram_size" id="JVM_Ram_size" size="20" value="${JVM_Ram_Size}" /> MB</td></tr>
			<tr><td>JVM Heap size:&nbsp;</td><td><input name="JVM_Heap_size" id="JVM_Heap_size" size="20" value="${JVM_Heap_Size}" /> MB</td></tr>
		</table>
		<br/>
		<table>
			<tr><td colspan="4"><textarea rows="4" cols="50">To show the Thread process updates</textarea></td></tr>
			<tr>
				<td><input type="button" name="Save" value="Save" onclick="doSave()" /></td>
				<td><input type="button" name="Execute" value="Execute" onclick="doExecute()" /></td>
				<td><input type="button" name="Stop" value="Stop Proc" onclick="doStop_Proc()" /></td>
				<td><input type="button" name="Show_Log" value="Show Log" onclick="doShow_Log()" /></td>
			</tr>
		</table>
		<input type="hidden" name="hR_File" id="hR_File" />
	</form>
	</center>
</body>
</html>