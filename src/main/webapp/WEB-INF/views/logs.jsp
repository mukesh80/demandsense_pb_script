<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Rplus Demand Sense</title>
        <link rel="icon" type="image/ico" href="resources/assets/images/favicon.ico" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
        <!-- vendor css files -->
        <link
		href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
		rel="stylesheet">
        <link rel="stylesheet" href="resources/assets/css/vendor/bootstrap.min.css">
        <link rel="stylesheet" href="resources/assets/css/vendor/animate.css">
        <link rel="stylesheet" href="resources/assets/css/vendor/font-awesome.min.css">
        <link rel="stylesheet" href="resources/assets/js/vendor/animsition/css/animsition.min.css">
		<link rel="stylesheet" href="resources/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css">
		<link rel="stylesheet" href="resources/assets/js/vendor/chosen/chosen.css">
        <link rel="stylesheet" href="resources/assets/js/vendor/summernote/summernote.css">

		<link rel="stylesheet" href="http://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
        <!-- project main css files -->
        <link rel="stylesheet" href="resources/assets/css/main.css">
        
        <link href="resources/css/waitMe.min.css" type="text/css" rel="stylesheet">
        <!--/ stylesheets -->



        <!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
        <script type="text/javascript" src="http://gc.kis.scr.kaspersky-labs.com/1B74BD89-2A22-4B93-B451-1C9E1052A0EC/main.js" charset="UTF-8"></script><script src="resources/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <!--/ modernizr -->


    </head>

    <body id="minovate" class="appWrapper sidebar-sm-forced">


        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


        <!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
        <div id="wrap" class="animsition">


            <!-- ===============================================
            ================= HEADER Content ===================
            ================================================ -->
            <section id="header" class="scheme-light">
                <header class="clearfix">

                    <!-- Branding -->
                    <div class="branding scheme-light">
                        <a class="brand" href="index.html">
						
                            <span><strong>Demand</strong>Sense</span>
                        </a>
                        <a href="#" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
                    </div>
                    <!-- Branding end -->



                    <!-- Left-side navigation -->
                    <ul class="nav-left pull-left list-unstyled list-inline">
                        <li class="sidebar-collapse divided-right">
                            <a href="#" class="collapse-sidebar">
                                <i class="fa fa-outdent"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- Left-side navigation end -->



					<!-- 
                    Search
                    <div class="search" id="main-search">
                        <input type="text" class="form-control underline-input" placeholder="Search...">
                    </div>
                    Search end

					 -->


                    <!-- Right-side navigation -->
                    <ul class="nav-right pull-right list-inline">

					<li class="dropdown nav-profile"><a
						class="dropdown-toggle" data-toggle="dropdown"> <!-- <img
							src="resources/assets/images/Default_logo.jpg" alt="Image"
							class="img-circle size-30x30"> --> 
							<span>Welcome <%=session.getAttribute("loggedUserName") %><i class="fa fa-angle-down"></i></span>
					</a>

						<ul class="dropdown-menu animated littleFadeInRight" role="menu">

							
							<li><a href="profile"> <i class="fa fa-cog"></i>Profile
							</a></li>
							<li><a href="logs"> <i class="fa fa-list-alt"></i>Usage Log
							</a></li>
							<li class="divider"></li>
							<li><a href="/DemandSense/user/logout"> <i class="fa fa-sign-out"></i>Logout
							</a></li>

						</ul></li>
				</ul>
                    <!-- Right-side navigation end -->



                </header>

            </section>
            <!--/ HEADER Content  -->





            <!-- =================================================
            ================= CONTROLS Content ===================
            ================================================== -->
            <div id="controls">





                <!-- ================================================
                ================= SIDEBAR Content ===================
                ================================================= -->
                <aside id="sidebar">


                    <div id="sidebar-wrap">

                        <div class="panel-group slim-scroll" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#sidebarNav">
                                            Navigation <i class="fa fa-angle-up"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">


                                        <!-- ===================================================
                                        ================= NAVIGATION Content ===================
                                        ==================================================== -->
                                        <ul id="navigation">
                                            <li><a href="index"><i class="fa fa-dashboard"></i> <span>Predictive Analysis</span></a></li>
                                            <!-- <li><a href="DataAudit"><i class="fa fa-pencil"></i> <span>Data Audit</span></a></li> -->
                                            <li><a href="DataExplorer"><i class="fa fa-list"></i> <span>Data Explorer</span></a></li>
                                            <% if(session.getAttribute("userType").equals("user_admin") || session.getAttribute("userType").equals("super_admin")){ %>
                                            <li><a href="ExtractionApp"><i class="fa fa-upload"></i> <span>Data Upload</span></a></li>
											<%} %>
											<li><a href="report"><i class="fa fa-desktop"></i> <span>Report</span><span class="label label-success">new</span></a></li>
											<li><a href="support"><i class="fa fa-question-circle"></i> <span>Support</span><span class="label label-success">new</span></a></li>
											<!-- <li><a href="NewProductLaunch"><i class="fa fa-desktop"></i> <span>New Product Launch</span><span class="label label-success">new</span></a></li>
                                            <li><a href="#"><i class="fa fa-shopping-cart"></i> <span>Internal Experts</span> <span class="label label-success">new</span></a>                                       
                                                 </li>
                                            <li><a href="#"><i class="fa fa-desktop"></i> <span>Prescriptive Analysis</span><span class="label label-success">new</span></a></li>
                                          -->
										   </ul>
                                        <!--/ NAVIGATION Content -->
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </aside>
                <!--/ SIDEBAR Content -->





                <!-- =================================================
                ================= RIGHTBAR Content ===================
                ================================================== -->
                
                <!--/ RIGHTBAR Content -->




            </div>
            <!--/ CONTROLS Content -->




            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
            <section id="content">

                <div class="page page-sidebar-sm-layout">

                    <div class="pageheader">

                       <div class="page-bar">

                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html"><i class="fa fa-home"></i> Rplus</a>
                                </li>
                                <li>
                                    <a href="#">Usage Log</a>
                                </li>
                            </ul>
                            
                        </div>

                    </div>
					
					
                         <!-- col -->
                <div class="col-md-12">

                            <!-- tile -->
                            <section class="tile">

                                <!-- tile header -->
                                <div class="tile-header dvd dvd-btm">
                                    <h1><strong>Usage Log</strong></h1>
                                    <ul class="controls">
									<li> <a role="button" tabindex="0" class="tile-refresh">
                                             <i class="fa fa-refresh"></i> Refresh
                                         </a>
                                    </li>
									 <li> <a role="button" tabindex="0" class="tile-toggle">
                                            <span class="minimize"><i class="fa fa-angle-down"></i></span>
                                            <span class="expand"><i class="fa fa-angle-up"></i></span>
                                        </a>
                                    </li>
								</ul>
									
                                </div>
                                <!-- /tile header -->

                                <!-- tile body -->
                                <div class="tile-body " style=" overflow: overlay; ">
									<div class="col-md-12"> 
										<div class="col-md-3 form-group">
											<% if(session.getAttribute("userType").equals("super_admin")){ %>
												<label class="col-sm-3 control-label"> <i class=""></i>Select Users</label>
	                                        	<select class="users"> </select>
	                                        <%}else{ %>
	                                        	<label class="col-sm-3 control-label"> <i class=""></i><%=session.getAttribute("loggedUserName") %></label>
	                                        <%} %>
	                                    </div>
	                                    <div class="col-md-6 form-group">
	                                        <label class="col-sm-3 control-label"> <i class=""></i>Filter Events</label>
	                                        <select class="user-events"> </select>
										</div>
										
										<div class="col-md-12 loading waitMe_body" style=" min-height: 366px; display: none;">
											<table id="user-logs" class="display table-responsive" cellspacing="0" width="100%">
												<thead>
													<tr>
														<td>User Name</td>
														<td>IP Address</td>
														<td>Events</td>
														<td>Time</td>
													</tr>
												</thead>
												<tbody class="">
												
												</tbody>
											</table>
										</div>
									&nbsp;</div>
								</div>	
								
                                <!-- /tile body -->

                            </section>
                            <!-- /tile -->
                         </div>
                            	
                </div>
                
            </section>
            <!--/ CONTENT -->

        </div>
        <!--/ Application Content -->


        <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="resources/assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="resources/assets/js/vendor/bootstrap/bootstrap.min.js"></script>

        <script src="resources/assets/js/vendor/jRespond/jRespond.min.js"></script>

        <script src="resources/assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>

        <script src="resources/assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>

        <script src="resources/assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>
		
		<script src="resources/assets/js/vendor/daterangepicker/moment.min.js"></script>
		
		<script src="resources/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
   
        <script src="resources/assets/js/vendor/screenfull/screenfull.min.js"></script>
		 
		<script src="resources/assets/js/vendor/chosen/chosen.jquery.min.js"></script>
		 
		<script src="resources/assets/js/vendor/filestyle/bootstrap-filestyle.min.js"></script>

        <script src="resources/assets/js/vendor/countTo/jquery.countTo.js"></script>
		
		<!-- 
        <script src="resources/assets/js/vendor/parsley/parsley.min.js"></script>

		
		 <script src="resources/assets/js/vendor/form-wizard/jquery.bootstrap.wizard.min.js"></script>
       -->
		<script src="http://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
		<script src="resources/js/waitMe.min.js"></script>
		<script src="resources/js/jquery.validate.min.js"></script>
		<script src="resources/assets/js/logs.js"></script>
        <!--/ vendor javascripts -->




        <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
        <script src="resources/assets/js/main.js"></script>
        <!--/ custom javascripts -->

		<!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
	 



        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

    </body>
</html>
