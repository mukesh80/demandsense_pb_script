<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Rplus analytics</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/bootstrap-responsive.min.css" rel="stylesheet">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
	rel="stylesheet">
<link href="resources/css/font-awesome.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">

<link href="resources/css/datepicker.css" rel="stylesheet">
<link href="resources/css/pages/dashboard.css" rel="stylesheet">

<!-----multiselect---------->
<link rel="stylesheet" type="text/css" media="screen"
	href="resources/css/token/jquery.qtip.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="resources/css/token/jquery.tokenize.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="resources/css/token/tokenize.css" />
<!-----drop down panel---------->

<link rel="stylesheet" type="text/css"
	href="resources/panel/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="resources/panel/css/font-awesome.min.css" />

<!---------D3.js---------------------->



<script type="text/javascript" src="resources/css/token/jquery.js"></script>
<script type="text/javascript"
	src="resources/css/token/jquery.tokenize.js"></script>


<script type="text/javascript">
function UploadFile2DB(Prod_Str)
{
	
	var XHR = new XMLHttpRequest();
	var StrURL = "";
	
	if(Prod_Str == "Product Master")
		StrURL = "GetProductMasterURL";
	else if(Prod_Str == "Outlet Master")
		StrURL = "GetOutletMasterURL";

	StrURL = "GetProductMasterURL";
	
	alert(StrURL);
	
	XHR.open("GET", StrURL, true);
	XHR.onreadystatechange=function()
	{
	    if(XHR.readyState==4 && XHR.status==200)
	    {
	    	//document.getElementById("ProdsDiv").innerHTML=XHR.responseText;
	    	alert(StrURL + ": File Updated to DB");
	    }
	}
	XHR.send();  
}
</script>

</head>
<body>



	<div class="subnavbar">
		<div class="subnavbar-inner">
			<div class="container">
				<ul class="mainnav">
					<li style="border-left: none; padding-top: 8px;"><a
						href="index"><img src="resources/images/logopng.png" alt=""></a></li>
					<li><a href="index"><i
							class="icon-dashboard"></i><span>Predective	Analysis</span> </a></li>
					<li><a href="charts"><i class="icon-cloud"></i><span>Data
								Explorer</span> </a></li>
					<li class="active"><a href="extraction"><i class=" icon-time"></i><span>Extraction App</span> </a></li>
					<li><a href="#"><i class="icon-crop"></i><span>Internal
								Experts</span> </a></li>
					<li><a href="#"><i class="icon-adjust"></i><span>Prescriptive
								Analysis</span> </a></li>
					<ul class="dropdown-menu">
						<li><a href="icons.html">Icons</a></li>
						<li><a href="faq.html">FAQ</a></li>
						<li><a href="pricing.html">Pricing Plans</a></li>
						<li><a href="login.html">Login</a></li>
						<li><a href="signup.html">Signup</a></li>
						<li><a href="error.html">404</a></li>
					</ul>
					</li>
				</ul>

				<ul class="nav pull-right">
					<li class="dropdown"
						style="border-left: none; color: #3AB70F !important;"><a
						href="#"> </i>Retail Shop<i class="caption-side"></i>
					</a></li>
					<li class="dropdown"><a href="#" role="button"
						class="dropdown-toggle" data-toggle="dropdown"> <i
							class="icon-user"></i> Vinesh <i class="caption-side"></i>
					</a>
						<ul class="dropdown-menu">
							<li><a tabindex="-1" href="#">Profile</a></li>
							<li class="divider"></li>
							<li><a tabindex="-1" href="login">Logout</a></li>
						</ul></li>
				</ul>
			</div>
			<!-- /container -->
		</div>
		<!-- /subnavbar-inner -->
		<div class="navbar">
			<!--<div class="navbar-inner">
	                                <ul class="breadcrumb">
									 <div class="pull-right"><span class="badge badge-warning">duplicate analysis</span>

                                </div>
	                                    <li class="active">
	                                        Selection: Feb 24th 2014 -Mar 23rd 2014
	                                    </li> </ul>
                            	</div>-->
		</div>
	</div>
	<!-- /subnavbar -->
	<div class="container">
		<div class="row">
			<div class="col-md-15">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Flat file Upload</h3>
						<span class="pull-right clickable"><i
							class="icon-chevron-down"></i></span>
					</div>
					<form action="index" method="post" enctype="multipart/form-data"
						name="Frm1">
						<div class="panel-body">
							<!-------------inside--------------->

							<div style="position: relative;">


								<div class="control-group">


									<div class="controls">
										Transaction Data &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
											class="input-file uniform_on" id="fileInput" type="file">
										<button type="submit" class="btn btn-primary">Load
											Data</button>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button class="btn btn-success">View Data</button>
									</div>

									<div class="controls">
										Customer Master &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
											class="input-file uniform_on" id="fileInput" type="file">
										<button type="submit" class="btn btn-primary">Load
											Data</button>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button class="btn btn-success">View Data</button>
									</div>


									<div class="controls">
										Outlet Master
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="input-file uniform_on" id="fileInput"
											type="file">
										<button type="submit" class="btn btn-primary">Load
											Data</button>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button class="btn btn-success">View Data</button>
									</div>
									<div class="controls">
										Product Master
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
											class="input-file uniform_on" id="fileInputProd"
											name="fileInputProd" type="file">
										<button type="button" class="btn btn-primary"
											onclick="UploadFile2DB('Product Master')">Load Data</button>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button class="btn btn-success">View Data</button>
									</div>
									<div class="controls">
										Local Calendar
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="input-file uniform_on" id="fileInput"
											type="file">
										<button type="submit" class="btn btn-primary">Load
											Data</button>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button class="btn btn-success">View Data</button>
									</div>
									<div class="controls">
										Fiscal Calendar
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="input-file uniform_on" id="fileInput"
											type="file">
										<button type="submit" class="btn btn-primary">Load
											Data</button>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button class="btn btn-success">View Data</button>
									</div>
									<div class="controls">
										Social Sentiments &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
											class="input-file uniform_on" id="fileInput" type="file">
										<button type="submit" class="btn btn-primary">Load
											Data</button>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button class="btn btn-success">View Data</button>
									</div>
									<div class="controls">
										External Data
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="input-file uniform_on" id="fileInput"
											type="file">
										<button type="submit" class="btn btn-primary">Load
											Data</button>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button class="btn btn-success">View Data</button>
									</div>

								</div>


							</div>



						</div>
					</form>
					<!-- Panel Ends -->
				</div>
			</div>

		</div>
	</div>
	<!-- /extra -->

	<div class="container">
		<div class="row">
			<div class="col-md-15">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">API's</h3>
						<span class="pull-right clickable"><i
							class="icon-chevron-down"></i></span>
					</div>
					<div class="panel-body">
						<!-------------inside--------------->
						<div class="input-daterange input-group" id="datepicker">
							From CW <input type="text" class="input-sm form-control"
								name="start" /> <span class="input-group-addon">to CW</span> <input
								type="text" class="input-sm form-control" name="end"
								style="border-color: none !important;" />
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-15">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Data Moniter</h3>
						<span class="pull-right clickable"><i
							class="icon-chevron-down"></i></span>
					</div>
					<div class="panel-body">
						<!-------------inside--------------->
						<div class="input-daterange input-group" id="datepicker">
							From CW <input type="text" class="input-sm form-control"
								name="start" /> <span class="input-group-addon">to CW</span> <input
								type="text" class="input-sm form-control" name="end"
								style="border-color: none !important;" />
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-15">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Data Audit</h3>
						<span class="pull-right clickable"><i
							class="icon-chevron-down"></i></span>
					</div>
					<div class="panel-body">
						<!-------------inside--------------->
						<div class="input-daterange input-group" id="datepicker">
							From CW <input type="text" class="input-sm form-control"
								name="start" /> <span class="input-group-addon">to CW</span> <input
								type="text" class="input-sm form-control" name="end"
								style="border-color: none !important;" />
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /extra -->



	<div class="footer">
		<div class="footer-inner">
			<div class="container">
				<div class="row">
					<div class="span12">
						contact &copy; 2015 <a href="http://www.rplusanalytics.com/">Rplus
							analytics</a>.
					</div>
					<!-- /span12 -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /footer-inner -->
	</div>
	<!-- /footer -->


	<!-- Le javascript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<style>
.btn-file {
	position: relative;
	overflow: hidden;
}

.btn-file input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	min-width: 100%;
	min-height: 100%;
	font-size: 100px;
	text-align: right;
	filter: alpha(opacity = 0);
	opacity: 0;
	outline: none;
	background: white;
	cursor: inherit;
	display: block;
}

.panel-heading span {
	margin-top: -20px;
	font-size: 15px;
}

.clickable {
	cursor: pointer;
}

circle {
	stroke: #000;
	stroke-opacity: .5;
}
</style>

	<script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('.input-daterange').datepicker({
                    todayBtn: "linked"
                });
            
            });
     </script>
	<script src="resources/jscript/jquery.min.js"></script>
	<script src="resources/jscript/kendo.all.min.js"></script>
	<script src="resources/content/shared/js/console.js"></script>


	<!--------drop down panel---------->
	<script type="text/javascript" src="resources/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>


	<script type="text/javascript">
    jQuery(function ($) {
        $('.panel-heading span.clickable').on("click", function (e) {
            if ($(this).hasClass('panel-collapsed')) {
                // expand the panel
                $(this).parents('.panel').find('.panel-body').slideDown();
                $(this).removeClass('panel-collapsed');
                $(this).find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            }
            else {
                // collapse the panel
                $(this).parents('.panel').find('.panel-body').slideUp();
                $(this).addClass('panel-collapsed');
                $(this).find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            }
        });
    });
	</script>


	<!---------D3.js---------------------->

	<script type="text/javascript" src="resources/d3/d3.js"></script>
	<script type="text/javascript" src="resources/d3/d3.geom.js"></script>
	<script type="text/javascript" src="resources/d3/d3.layout.js"></script>
	<!-- Collapsible Panels - END -->
	<script src="resources/js/jquery-1.7.2.min.js"></script>
	<script src="resources/js/excanvas.min.js"></script>
	<script src="resources/js/chart.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.js"></script>
	<script language="javascript" type="text/javascript"
		src="resources/js/full-calendar/fullcalendar.min.js"></script>

	<script src="resources/js/base.js"></script>

	<script src="resources/js/jquery-1.9.1.min.js"></script>
	<script src="resources/js/bootstrap-datepicker.js"></script>

</body>
</html>

<script src="assets/scripts.js"></script>



</body>
</html>
