

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->



    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Rplus Demand Sense</title>
        <link rel="icon" type="image/ico" href="resources/assets/images/favicon.ico" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">




        <!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
        <!-- vendor css files -->
        <link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
        <link rel="stylesheet" href="resources/assets/css/vendor/bootstrap.min.css">
        <link rel="stylesheet" href="resources/assets/css/vendor/animate.css">
        <link rel="stylesheet" href="resources/assets/css/vendor/font-awesome.min.css">
        <link rel="stylesheet" href="resources/assets/js/vendor/animsition/css/animsition.min.css">
		<link rel="stylesheet" href="resources/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css">
		 <link rel="stylesheet" href="resources/assets/js/vendor/chosen/chosen.css">
        <link rel="stylesheet" href="resources/assets/js/vendor/summernote/summernote.css">


        <!-- project main css files -->
        <link rel="stylesheet" href="resources/assets/css/main.css">
        <!--/ stylesheets -->



        <!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
        <script type="text/javascript" src="http://gc.kis.scr.kaspersky-labs.com/1B74BD89-2A22-4B93-B451-1C9E1052A0EC/main.js" charset="UTF-8"></script><script src="resources/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <!--/ modernizr -->
<script type="text/javascript">

	/* function doValidation()	{
		var val = 0;
		var str1 = "", str2 = "";
		var flg = false;
		str1 = rJavaFrm.threadCount.value;
		if(str1.trim().length>0) {
			val = parseInt(str1, 10);
			if(val <= 0) {
				alert("Please Enter Thread Count")
				return flg;
			}
		}
		
		str1 = rJavaFrm.existRFiles.value;
		str2 = rJavaFrm.rFile.value;
		if(str1 == "select" && str2.trim().length > 0) {
			// Fine // New R file
		}
		else if(str1.trim().length > 0 && str2.trim().length == 0) {
			// Fine // Exist R file
		}
		else {
			alert("Check both Existing and New R File\r\n\"It Should be either one\"");
			return flg;
		}
		
		str1 = rJavaFrm.dbTableName.value;
		str2 = rJavaFrm.dbSchemaName.value;
		if(str1.trim().length > 0 && str2.trim().length > 0) {
			// Fine // Schema and Table present
		}
		else {
			alert("Check for both \"Table\" and \"Schema\" name");
			return flg;
		}
		flg = true;
		return flg;
	}
	
	function doSave() {
		alert("doSave");
		// rJavaFrm.R_File.value;
		var XHR = new XMLHttpRequest();
		var formData = new FormData();
		//var StrURL = "http://localhost:8080/DemandSense/rAdmin/rJavaMultiThreadSave";
		var StrURL = "/DemandSense/rAdmin/rJavaMultiThreadSave";
		
		formData.append("Thread_Cnt",rJavaFrm.Thread_Cnt.value);
		formData.append("Exist_R_Files",rJavaFrm.Exist_R_Files.value);
		formData.append("R_File",rJavaFrm.R_File.value);
		formData.append("Tbl_name",rJavaFrm.Tbl_name.value);
		formData.append("Schema_name",rJavaFrm.Schema_name.value);
		formData.append("JVM_Ram_size",rJavaFrm.JVM_Ram_size.value);
		formData.append("JVM_Heap_size",rJavaFrm.JVM_Heap_size.value);
		alert("Data income completed");
		XHR.open("POST", StrURL, true);
		XHR.onreadystatechange=function(){
		    if(XHR.readyState==4 && XHR.status==200) {
		    	//alert("REST Return: " + XHR.responseText);
		    	var res = XHR.responseText;
		    	if(res == "true")
		    		doExecuteR();
		    	else
		    		alert("Thread settings saved Successfully");
		    }
		}
		XHR.send(formData);
	}
	
	function doExecute() {
		//alert("doExecute");
		var XHR = new XMLHttpRequest();
		var formData = new FormData();
		var fileObj = document.getElementById("rFile");
		var fil = fileObj.files;
		copyFile2Hid(fileObj);
		//var StrURL = "http://localhost:8080/DemandSense/rAdmin/rJavaMultiThreadProc";
		var StrURL = "/DemandSense/rAdmin/rJavaMultiThreadProc";
		//alert("doExecute: " + rJavaFrm.hR_File.value);
		formData.append(fileObj.name, fil[0], fil[0].name);
		formData.append("hrFile", rJavaFrm.hrFile.value);
		formData.append("threadCount",rJavaFrm.threadCount.value);
		formData.append("dbTableName",rJavaFrm.dbTableName.value);
		formData.append("dbSchemaName",rJavaFrm.dbSchemaName.value);
		formData.append("jvmRamSize",rJavaFrm.jvmRamSize.value);
		formData.append("jvmHeapSize",rJavaFrm.jvmHeapSize.value);
		XHR.open("POST", StrURL, true);
		XHR.onreadystatechange=function(){
		    if(XHR.readyState==4 && XHR.status==200) {
		    	//alert("REST Return: " + XHR.responseText);
		    	var res = XHR.responseText;
		    	if(res == "true")
		    		doExecuteR();
		    	else
		    		alert("R file upload failed");
		    }
		    else if(XHR.readyState==1) {
		    	//alert(StrURL + ": Server connection established");
	    	}
		    else if(XHR.readyState==2) {
		    	//alert(StrURL + ": Request received");
	    	}
		    else if(XHR.readyState==3) {
		    	//alert(StrURL + ": Processing request ");
	    	}
		}
		XHR.send(formData);
		return false;
	}
	
	function doExecuteR() {
		alert("R Process Completed");
		//var XHR = new XMLHttpRequest();
	}
	
	function doStopProc() {
		alert("doStopProc");
		
		var XHR = new XMLHttpRequest();
		var StrURL = "/DemandSense/rAdmin/rJavaThreadStop";
		XHR.open("POST", StrURL, true);
		XHR.onreadystatechange=function(){
		    if(XHR.readyState==4 && XHR.status==200) {
		    	//alert("REST Return: " + XHR.responseText);
		    	var res = XHR.responseText;
		    	if(res == "true")
		    		alert("R Java Thread process stopped");
		    	else
		    		alert("No thread process in back ground");
		    }
		    else if(XHR.readyState==1) {
		    	//alert(StrURL + ": Server connection established");
	    	}
		    else if(XHR.readyState==2) {
		    	//alert(StrURL + ": Request received");
	    	}
		    else if(XHR.readyState==3) {
		    	//alert(StrURL + ": Processing request ");
	    	}
		}
		XHR.send();
		return false;
	}
	
	function doShowLog() {
		//alert("doShowLog");
	}
   
	function copyFile2Hid(fileFld) {
		var fldName = fileFld.name;
		var fldValue = fileFld.value;
		if(fldName.length > 0 && fldValue.length > 0 ) {
			var hFldStr = "h" + fldName;
			var hFldObj = document.getElementById(hFldStr);
			hFldObj.value = fldValue;
			//alert("copyFileName2Hid: " + fldValue);
			//alert("copyFileName2Hid: " + rJavaFrm.hrFile.value);
		}
	}
	
	function copyFileName2Hid() {
		var fldName = fileFld.name;
		var fldValue = fileFld.value;
		//alert(fldName + " : " + fldValue);
		if(fldName.length > 0 && fldValue.length > 0 ){
			var hFldStr = "h" + fldName;
			var hFldObj = document.getElementById(hFldStr);
			hFldObj.value = fldValue;
			//alert("copyFileName2Hid: " + fldValue);
			//alert("copyFileName2Hid: " + rJavaFrm.hrFile.value);
		}
	} */
</script>



    </head>





    <body id="minovate" class="appWrapper sidebar-sm-forced">






        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->












        <!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
        <div id="wrap" class="animsition">






            <!-- ===============================================
            ================= HEADER Content ===================
            ================================================ -->
            <section id="header" class="scheme-light">
                <header class="clearfix">

                    <!-- Branding -->
                    <div class="branding scheme-light">
                        <a class="brand" href="index.html">
						
                            <span><strong>Demand</strong>Sense</span>
                        </a>
                        <a href="#" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
                    </div>
                    <!-- Branding end -->



                    <!-- Left-side navigation -->
                    <ul class="nav-left pull-left list-unstyled list-inline">
                        <li class="sidebar-collapse divided-right">
                            <a href="#" class="collapse-sidebar">
                                <i class="fa fa-outdent"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- Left-side navigation end -->



					<!-- 
                    Search
                    <div class="search" id="main-search">
                        <input type="text" class="form-control underline-input" placeholder="Search...">
                    </div>
                    Search end

					 -->


                    <!-- Right-side navigation -->
                    <ul class="nav-right pull-right list-inline">

					<li class="dropdown nav-profile"><a
						class="dropdown-toggle" data-toggle="dropdown"> <!-- <img
							src="resources/assets/images/Default_logo.jpg" alt="Image"
							class="img-circle size-30x30"> --> 
							<span>Welcome <%=session.getAttribute("loggedUserName") %><i class="fa fa-angle-down"></i></span>
					</a>

						<ul class="dropdown-menu animated littleFadeInRight" role="menu">

							
							<li><a href="profile"> <i class="fa fa-cog"></i>Profile
							</a></li>
							<li><a href="logs"> <i class="fa fa-list-alt"></i>Usage Log
							</a></li>
							<li class="divider"></li>
							<li><a href="/DemandSense/user/logout"> <i class="fa fa-sign-out"></i>Logout
							</a></li>

						</ul></li>
				</ul>
                    <!-- Right-side navigation end -->



                </header>

            </section>
            <!--/ HEADER Content  -->





            <!-- =================================================
            ================= CONTROLS Content ===================
            ================================================== -->
            <div id="controls">





                <!-- ================================================
                ================= SIDEBAR Content ===================
                ================================================= -->
                <aside id="sidebar">


                    <div id="sidebar-wrap">

                        <div class="panel-group slim-scroll" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#sidebarNav">
                                            Navigation <i class="fa fa-angle-up"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">


                                        <!-- ===================================================
                                        ================= NAVIGATION Content ===================
                                        ==================================================== -->
                                        <ul id="navigation">
                                            <li><a href="index"><i class="fa fa-dashboard"></i> <span>Predictive Analysis</span></a></li>
                                            <!-- <li><a href="DataAudit"><i class="fa fa-pencil"></i> <span>Data Audit</span></a></li> -->
                                            <li><a href="DataExplorer"><i class="fa fa-list"></i> <span>Data Explorer</span></a></li>
                                            <% if(session.getAttribute("userType").equals("user_admin") || session.getAttribute("userType").equals("super_admin")){ %>
                                            <li><a href="ExtractionApp"><i class="fa fa-upload"></i> <span>Data Upload</span></a></li>
											<%} %>
											<li><a href="report"><i class="fa fa-desktop"></i> <span>Report</span><span class="label label-success">new</span></a></li>
											<li><a href="support"><i class="fa fa-question-circle"></i> <span>Support</span><span class="label label-success">new</span></a></li>
											<!-- <li><a href="NewProductLaunch"><i class="fa fa-desktop"></i> <span>New Product Launch</span><span class="label label-success">new</span></a></li>
                                            <li><a href="#"><i class="fa fa-shopping-cart"></i> <span>Internal Experts</span> <span class="label label-success">new</span></a>                                       
                                                 </li>
                                            <li><a href="#"><i class="fa fa-desktop"></i> <span>Prescriptive Analysis</span><span class="label label-success">new</span></a></li>
                                          -->
										   </ul>
                                        <!--/ NAVIGATION Content -->
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </aside>
                <!--/ SIDEBAR Content -->





                <!-- =================================================
                ================= RIGHTBAR Content ===================
                ================================================== -->
                
                <!--/ RIGHTBAR Content -->




            </div>
            <!--/ CONTROLS Content -->




            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
            <section id="content">

                <div class="page page-sidebar-sm-layout">

                    <div class="pageheader">

                       <div class="page-bar">

                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html"><i class="fa fa-home"></i> Rplus</a>
                                </li>
                                <li>
                                    <a href="#">Profile</a>
                                </li>
                            </ul>
                            
                        </div>

                    </div>
					
					
                         <!-- col -->
                <div class="col-md-12">

                            <!-- tile -->
                            <section class="tile">

                                <!-- tile header -->
                                <div class="tile-header dvd dvd-btm">
                                    <h1><strong>Profile Edit</strong></h1>
                                    <ul class="controls">
									<li> <a role="button" tabindex="0" class="tile-refresh">
                                             <i class="fa fa-refresh"></i> Refresh
                                         </a>
                                    </li>
									 <li> <a role="button" tabindex="0" class="tile-toggle">
                                            <span class="minimize"><i class="fa fa-angle-down"></i></span>
                                            <span class="expand"><i class="fa fa-angle-up"></i></span>
                                        </a>
                                    </li>
								</ul>
									
                                </div>
                                <!-- /tile header -->

                                <!-- tile body -->
                                <div class="tile-body">
									<label class="sucess-msg1" style="color: #FFFFFF; width: auto; padding:5px 20px ; background-color: rgba(3, 128, 3, 0.44); display:none;">Your profile updated sucessfully </label>
                                    
                                    <form class="profile">


                                     <div class="row">

                                         <div class="form-group col-sm-6">
                                             <label for="first-name">First Name</label>
                                             <input type="text" name="firstName" class="form-control" id="first-name" value="${userDetails.firstName }">
                                         </div>

                                         <div class="form-group col-sm-6">
                                             <label for="last-name">Last Name</label>
                                             <input type="text" name="lastName" class="form-control" id="last-name" value="${userDetails.lastName }">
                                         </div>

                                     </div>

                                     
                                     <div class="row">

                                         <div class="form-group col-sm-6">
                                             <label for="email">E-mail</label>
                                             <input type="email" name="emailId" class="form-control" id="email" value="${userDetails.emailId }" readonly>
                                         </div>

                                          <div class="form-group col-sm-6">
                                             <label for="phone">Phone</label>
                                             <input type="text" class="form-control"  name="mobilePhone"id="phone" value="${userDetails.mobilePhone }">
                                             
                                         </div>


                                     </div>

                                     <!-- <div class="row">


                                         <div class="form-group col-sm-6">
                                             <label for="avatar">Picture</label>
                                             <input type="file" id="avatar" class="filestyle" data-buttonText="Find file" data-iconName="fa fa-inbox">
                                             <span class="help-block">Allowed files: gif, png, jpg. Max file size 1Mb</span>
                                         </div>

                                     </div> -->
                                   <div class="row">
                                      <div class="form-group col-sm-12">
                                       <button type="submit" class="btn btn-primary btn-sm mb-10 pull-right" >Update</button>
                                      </div>
                                     </div>
                                   
                                  

                                 </form>
									
								</div>	
                                <!-- /tile body -->

                            </section>
                            <!-- /tile -->
                         </div>
                            
                         <div class="col-md-12">
                            <!-- tile -->
                            <section class="tile">

                                <!-- tile header -->
                                <div class="tile-header dvd dvd-btm">
                                    <h1><strong>Password Change</strong></h1>
                                    <ul class="controls">
									<li> <a role="button" tabindex="0" class="tile-refresh">
                                                        <i class="fa fa-refresh"></i> Refresh
                                                    </a>
                                    </li>
									 <li> <a role="button" tabindex="0" class="tile-toggle">
                                                        <span class="minimize"><i class="fa fa-angle-down"></i></span>
                                                        <span class="expand"><i class="fa fa-angle-up"></i></span>
                                                    </a>
                                    </li>
									 </ul>
									
                                </div>
                                <!-- /tile header -->

                                <!-- tile body -->
                                <div class="tile-body">
                                	<label class="sucess-msg" style="color: #FFFFFF; width: auto; padding:5px 20px; background-color: rgba(3, 128, 3, 0.44); display:none;">Your new password updated sucessfully </label>
                                    <form class="password">

                                              <div class="row">

                                                  <div class="form-group col-sm-6">
                                                      <label for="new-password">New Password</label>
                                                      <input type="password" class="form-control" name="password" id="password1">
                                                  </div>

                                                  <div class="form-group col-sm-6">
                                                      <label for="new-password-repeat">Confirm Password</label>
                                                      <input type="password" class="form-control" name="cpassword">
                                                  </div>

                                              </div>
                                              <div class="row">
                                               <div class="form-group col-sm-12">
                                                <button type="submit" class="btn btn-primary btn-sm mb-10 pull-right " >Update</button>
                                               </div>
                                              </div>

                                             

                                          </form>
									
								</div>	
                                <!-- /tile body -->

                            </section>
                            <!-- /tile -->

                        </div>
                       
                		
							
                </div>
                
            </section>
            <!--/ CONTENT -->






        </div>
        <!--/ Application Content -->














        <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="resources/assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="resources/assets/js/vendor/bootstrap/bootstrap.min.js"></script>

        <script src="resources/assets/js/vendor/jRespond/jRespond.min.js"></script>

        <script src="resources/assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>

        <script src="resources/assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>

        <script src="resources/assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>
		
		<script src="resources/assets/js/vendor/daterangepicker/moment.min.js"></script>
		
		<script src="resources/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
   
         <script src="resources/assets/js/vendor/screenfull/screenfull.min.js"></script>
		 
		 <script src="resources/assets/js/vendor/chosen/chosen.jquery.min.js"></script>
		 
		   <script src="resources/assets/js/vendor/filestyle/bootstrap-filestyle.min.js"></script>

        <script src="resources/assets/js/vendor/countTo/jquery.countTo.js"></script>
		
		<!-- 
        <script src="resources/assets/js/vendor/parsley/parsley.min.js"></script>

		
		 <script src="resources/assets/js/vendor/form-wizard/jquery.bootstrap.wizard.min.js"></script>
       -->

		<script src="resources/js/jquery.validate.min.js"></script>
        <!--/ vendor javascripts -->




        <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
        <script src="resources/assets/js/main.js"></script>
        <!--/ custom javascripts -->

		<!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
	 <script>
            /* $(window).load(function(){

                $('#rootwizard').bootstrapWizard({
                    onTabShow: function(tab, navigation, index) {
                        var $total = navigation.find('li').length;
                        var $current = index+1;

                        // If it's the last tab then hide the last button and show the finish instead
                        if($current >= $total) {
                            $('#rootwizard').find('.pager .next').hide();
                            $('#rootwizard').find('.pager .finish').show();
                            $('#rootwizard').find('.pager .finish').removeClass('disabled');
                        } else {
                            $('#rootwizard').find('.pager .next').show();
                            $('#rootwizard').find('.pager .finish').hide();
                        }

                    },

                    onNext: function(tab, navigation, index) {

                        var form = $('form[name="step'+ index +'"]');

                        form.parsley().validate();

                        if (!form.parsley().isValid()) {
                            return false;
                        }

                    },

                    onTabClick: function(tab, navigation, index) {

                        var form = $('form[name="step'+ (index+1) +'"]');
                        form.parsley().validate();

                        if (!form.parsley().isValid()) {
                            return false;
                        }

                    }

                });

            }); */
        </script>

	<script type="text/javascript">
		$(document).ready(function(){
			
			$(".profile").validate({
    			rules: {
    				firstName: "required",
    				lastName: "required",
    				
    				emailId: {
    					required: true,
    					email: true
    				},
    				mobilePhone: {
    					required: true,
    					minlength: 10,
    					maxlength: 10
    				}
    			},
    			messages: {
    				firstName: "Please enter your firstname!",
    				lastName: "Please enter your lastname!",
    				
    				emailId: "Please enter a valid email address!",
    				mobilePhone: {
       					required: "Please enter mobile number!",
       					minlength: "mobile number should be 10 numbers!",
       					maxlength: "mobile number should be 10 numbers!"
       				}
    			},
    			showErrors: function(errorMap, errorList) {
    		          // Clean up any tooltips for valid elements
    		          $.each(this.validElements(), function (index, element) {
    		              var $element = $(element);
    		              $element.data("title", "") // Clear the title - there is no error associated anymore
    		                  .removeClass("error")
    		                  .tooltip("destroy");
    		          });
    		          // Create new tooltips for invalid elements
    		          $.each(errorList, function (index, error) {
    		              var $element = $(error.element);
    		              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
    		                  .data("title", error.message)
    		                  .addClass("error")
    		                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
    		          });
    		      },
    			submitHandler: function(form) {
    				//alert($(".profile").serialize());
    				updateprofile();
    			}
    		});
			
			
			$(".password").validate({
    			rules: {
    				password: "required",
    			    cpassword: {
    			    	required: true,
    			      	equalTo: "#password1"
    			    }
    			},
    			messages: {
    				password: "Please enter Password!",
    				cpassword: {
       					required: "Please enter Confirm Password!",
       					equalTo: "Password mismatch!"
       				}
    			},
    			showErrors: function(errorMap, errorList) {
    		          // Clean up any tooltips for valid elements
    		          $.each(this.validElements(), function (index, element) {
    		              var $element = $(element);
    		              $element.data("title", "") // Clear the title - there is no error associated anymore
    		                  .removeClass("error")
    		                  .tooltip("destroy");
    		          });
    		          // Create new tooltips for invalid elements
    		          $.each(errorList, function (index, error) {
    		              var $element = $(error.element);
    		              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
    		                  .data("title", error.message)
    		                  .addClass("error")
    		                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
    		          });
    		      },
    			submitHandler: function(form) {
    				//alert($("#password1").val());
    				updatepassword();
    				//return true;
    			}
    		});

		})
	function updateprofile(){
			$.ajax({
				url:"/DemandSense/user/userupdate",
				type:"POST",
				data: $(".profile").serialize(),
				success: function(result){
					if(result == "sucess"){
						$(".sucess-msg1").css({"background-color":"green"}).show();
						setTimeout('$(".sucess-msg1").fadeOut()',5000);
					}
				},
				error: function(result){
					$(".sucess-msg1").css({"background-color":"red"}).text("Oops! Password is not updated").show();
					setTimeout('$(".sucess-msg").fadeOut()',10000);
				},
				done: function(result){}
			})
		}
	function updatepassword(){
			$.ajax({
				url:"/DemandSense/user/passupdate",
				type:"POST",
				data: {
					pass: $("#password1").val()
				},
				success: function(result){
					if(result == "sucess"){
						$(".sucess-msg").css({"background-color":"green"}).show();
						$(".password")[0].reset();
						setTimeout('$(".sucess-msg").fadeOut()',5000);
					}
				},
				error: function(result){
					$(".sucess-msg").css({"background-color":"red"}).text("Oops! Password is not updated").show();
					setTimeout('$(".sucess-msg").fadeOut()',10000);
				}
			})
		}
	
	</script>
        
        <!--/ Page Specific Scripts -->




        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

    </body>
</html>
