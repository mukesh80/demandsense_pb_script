


/***********************************************# GLOBAL VARIABLES #**********************************************/
 
//From and to date savers
var fromDate = {};
var toDate = {};

//Type variable
var type = "no type";
var catMainVal = "no class";

//Global variable for city, from & to dates
var city = [];
var fromDateFinal;
var toDateFinal;

//Confirmation varibles for type, fromdate, todate and city
var _city_ok = false;
var _date_ok = false;

//citys list
var _citys = [];
var cityLength = null;

//Month in String
var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


/***********************************************# JQUERY READY FUNCTION #*****************************************/

$(document).ready(function(){
	
	
	/*
	 * Getting selected city from citys dropdown list
	 */
	$(".citys").change(function(){
		var vals = $(this).val();
		//console.log(vals);
		
		if($.inArray("all", vals)){
			
			//console.log("selected")
			if(cityLength == "all"){
				city = [];
			}
			cityLength = "single";
			city = vals;
			/*$.each(vals, function(i,v){
				
				if ($.inArray(v, city) < 0) {
					city.push(v);
					//$(".citys option[value='"+v+"']").css("background-color", "#16a085");
					//console.log(v+' not found');
		        }
		        else {
		        	city.splice( city.indexOf(v), 1 );
		        	//$(".citys option[value='"+v+"']").css("background-color", "#fff");
		        	//console.log(v+' found');
		        }
			})*/
		}else{
			//console.log("all")
			cityLength = "all";
			city = _citys;
		}
		if(city.length > 0){
			_city_ok = true;
			
		}else{
			_city_ok = false;
		}
		//console.log(city);
	})
	
	
	/*
	 * Submit form for report
	 */
	
	$(".submit-form").click(function(){
		
		if(_date_ok == true && _city_ok == true){
			$("#error").text("");
			$(".submit-form").text("Wait..");
			$(".export-button, .submit-form").prop("disabled",true);
			confirmation_input();

	 		
	 		waitme_function();
		}else{
			$("#error").text("Select all requiren selections.");
		}
	})
	
	
	/*
	 * Getting selected date || week || month from from-date
	 */
	$(".from-date").change(function(){
		fromDate.dwm = $(this).val();
		////console.log(fromDate);
		to_date_setter();
	})

	/*
	 * Getting selected month from from-date-month
	 
	$(".from-date-month").change(function(){
		fromDate.month = $(this).val();
		////console.log(fromDate);
		to_date_setter();
	})*/

	/*
	 * Getting selected month from from-year 
	 */
	$(".from-year").change(function(){
		fromDate.year = $(this).val();
		////console.log(fromDate);
		to_date_setter();
	})
	
	/*
	 * Getting selected date in daywise
	 */
	$(".dateSelect").change(function(){
		date = $(".dateSelect").datepicker('getDate');
		fromDate.year = date.getFullYear();
		fromDate.dwm = date.getDate();
		fromDate.month = date.getMonth()+1;
		//console.log(fromDate);
		to_date_setter();
	})

})

/***********************************************# FUNCTIONS #******************************************************/


	/*
	 * Ajax call for citys list
	 */
	function getAllList(val){
		_citys = [];
		$.ajax({
			url: "/DemandSense/report/"+val,
		 	type: "GET",
	
		 	success:function(result){
		 		////console.log(result);
		 		$(".citys").children().remove();
	 			if(result.length != 0){
	 				$("<option value='all'>Select All</option>").clone().appendTo(".citys");
	 				$.each(result, function(index, value){
		 				if(value != "NA" && index != "count"){
		 					_citys.push(index);
				 			$("<option value="+index+">"+value+"</option>").clone().appendTo(".citys");
				 		}
	 				})
	 			}else{
		 			alert("No Citys found");
		 		}
		 	}
		})
	}



/*
 * On Click function calling from html
 * Which gives details of which category (eg: sales, Distribution, Operation, Financial)
 * and
 * which data type (eg: week, day, month)
 */
function details(element){
	$(".temp").hide();
	$(".main-div-alanysis").hide();
	$("#error").text("");
	$(".init-body-div").show();
	$(".init-body").hide();
	$(".from-date, .to-date, .from-year, .to-year").children().remove();
	catMainVal = cat_main($(element));
	//console.log(catMainVal);
	// Resetting values
	fromDate = {};
	toDate = {};
	city = [];
	type = "no type";
	_city_ok = false;
	_date_ok = false;
	
	//$(".citys option").css("background-color", "#fff");
	
	
	
	var dataTypeVal = data_type($(element));
	
	if(dataTypeVal != "no type"){
		//console.log(dataTypeVal);
		$(".from-date-text").text("From "+dataTypeVal);
		$(".to-date-text").text("To "+dataTypeVal);
		
		$(".init-div").hide();
		$(".main-div").show();
	}
}



/*
 * calling Function to find Category 
 * and 
 * return Category
 */
function cat_main(e){
	
	if(e.hasClass("sales")){
		return "Sales";
	}else if(e.hasClass("Supply_Chain")){
		return "Supply Chain";
	}else if(e.hasClass("operation")){
		return "Operation";
	}else if(e.hasClass("financial")){
		return "Financial";
	}else{
		return "no class";
	}
	
}

/*
 * calling Function to find Data Type 
 * and 
 * return Data Type
 */
function data_type(e){
	var count = 0;
	
	$(".from-date-month, .to-date-month").hide();
	$("<option selected disabled>Year</option>").clone().appendTo(".from-year, .to-year");
	for(var d=2014;d<=2020;d++){
	    $("<option value="+d+">"+d+"</option>").clone().appendTo(".from-year, .to-year");
	}
	if(e.hasClass("week")){
		$(".city-text").text("Select Warehouse");
		$(".city-text1").text("Warehouse");
		//console.log(catMainVal);
		$(".cat-main-text").text("Weekly  "+catMainVal+" forecast by Warehouse (for max 5 Weeks)");
		$(".thead1-th").html("Week Number <br> dd-mm-yyyy");
		getAllList("citys");
		count = 53;
		type = "Week";
		$(".from-date, .to-date, .from-year, .to-year").show();
	}else if(e.hasClass("month")){
		$(".city-text").text("Select City");
		$(".city-text1").text("City");
		$(".cat-main-text").text("Monthly  "+catMainVal+" forecast by City (for max 3 Months)");
		$(".thead1-th").html("Month Number <br> mmm-yyyy");
		count = 12;
		type = "Month";
		getAllList("districts");
		$(".from-date, .to-date, .from-year, .to-year").show();
		/*// ------reports not available START -----//
		$(".init-div").show();
		$(".main-div").hide();
		alert("Month and Day reports not available now. Please select Week.")
		return "no type";
		//-----------------END-------------------//
*/		
		
	}else if(e.hasClass("day")){
		$(".city-text").text("Select Warehouse");
		$(".city-text1").text("Warehouse");
		$(".cat-main-text").text("Daily  "+catMainVal+" forecast by Warehouse (for max 7 Days)");
		$(".thead1-th").html("Date (dd-mm-yyyy)");
		getAllList("citys");
		count = 31;
		type = "Date";
		
		/*// ------reports not available START -----//
		$(".init-div").show();
		$(".main-div").hide();
		alert("Day reports not available now. Please select Week OR Month.")
		return "no type";
		//-----------------END-------------------//
		*/
		
		$(".from-date-month, .to-date-month").show();
		$(".from-date, .to-date, .from-year, .to-year").hide();
		
		
	}
	//console.log(count);
	$("<option selected disabled>"+type+"</option>").clone().appendTo(".from-date, .to-date");
	for(var d=1;d<=count;d++)
	{
		if(d < 10){
			$("<option value=0"+d+">0"+d+"</option>").clone().appendTo(".from-date, .to-date");
		}else{
			$("<option value="+d+">"+d+"</option>").clone().appendTo(".from-date, .to-date");
		}
	}
	return type;
}



/*
 * Function to set to-date automatically
 */
function to_date_setter(){
	if(Object.keys(fromDate).length == 3 && type == "Date"){
		
		var date = new Date(fromDate.month+"/"+fromDate.dwm+"/"+fromDate.year);
		//console.log("Date"+date)
		if(!isNaN(date.getTime())){
			date.setDate(date.getDate() + 6);
			var _end_date = date.toInputFormat();

			toDate.dwm = ("0" + (_end_date.getDate())).slice(-2);
			toDate.month = ("0" + (_end_date.getMonth()+1)).slice(-2);
			toDate.year = _end_date.getFullYear();
			
			/*$(".to-date option[value='"+toDate.dwm+"']").attr('selected', 'selected');
			$(".to-date-month option[value='"+toDate.month+"']").attr('selected', 'selected');
			$(".to-year option[value='"+toDate.year+"']").attr('selected', 'selected');
			*/
			$(".to-date-month").val(toDate.dwm+'-'+toDate.month+'-'+toDate.year);
			
			_date_ok = true;
			fromDateFinal = fromDate.year+"-"+fromDate.month+"-"+fromDate.dwm;
			toDateFinal = toDate.year+"-"+toDate.month+"-"+toDate.dwm;
			
		} else {
			alert("Invalid Date");  
		}
	}else if(Object.keys(fromDate).length == 2 && type == "Week"){
		//console.log("Week")
		var date = new Date(getDateOfWeek(fromDate.dwm , fromDate.year));
		
		if(!isNaN(date.getTime())){
			date.setDate(date.getDate() + 35);
			var _end_date = date.toInputFormat();
			var _to_week = _end_date.getWeekNumber();
			var _to_year = _end_date.getFullYear();
			if(fromDate.dwm >= 48 && _to_week <= 48){
				if(fromDate.year == _to_year){
					_to_year = _to_year +1;					
				}
			}

			toDate.dwm = ("0" + (_to_week)).slice(-2);
			toDate.year = _to_year;
			
			$(".to-date option[value='"+toDate.dwm+"']").attr('selected', 'selected');
			$(".to-year option[value='"+toDate.year+"']").attr('selected', 'selected');
			
			_date_ok = true;
			fromDateFinal = fromDate.year+fromDate.dwm;
			toDateFinal = toDate.year+toDate.dwm;
			
		} else {
			alert("Invalid Date");  
		}
	}else if(Object.keys(fromDate).length == 2 && type == "Month"){
		//console.log("Month")
		var date = new Date(fromDate.dwm +"/"+1+"/"+fromDate.year);
		//console.log(fromDate.dwm+"/"+1+"/"+fromDate.year)
		if(!isNaN(date.getTime())){
			date.setDate(date.getDate() + 90);
			var _end_date = date.toInputFormat();

			toDate.dwm = ("0" + (_end_date.getMonth()+1)).slice(-2);
			toDate.year = _end_date.getFullYear();
			
			$(".to-date option[value='"+toDate.dwm+"']").attr('selected', 'selected');
			$(".to-year option[value='"+toDate.year+"']").attr('selected', 'selected');
			
			_date_ok = true;
			fromDateFinal = fromDate.year+fromDate.dwm;
			toDateFinal = toDate.year+toDate.dwm;
			
		} else {
			alert("Invalid Date");  
		}
	}
}

/*
 * function to convert week and year to date format
 */
function getDateOfWeek(w, y) {
    var d = (-3 + (w - 1) * 7); // 1st of January + 7 days for each week
    //console.log(new Date(y, 0, d));
    return new Date(y, 0, d);
}

function yearWeek(w, y) {
    var d = (-3 + (w - 1) * 7); // 1st of January + 7 days for each week
    
    var date = new Date(y, 0, d)
    //console.log(date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear());
    
    return date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear();
}

/*
 * ProtoType to add n number of days
 * and 
 * returns end date
 */
Date.prototype.toInputFormat = function() {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth()+1).toString();
    var dd  = this.getDate().toString();
    return new Date((mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]) + "/" + yyyy); // padding
};

/*
 * ProtoType to get week number from date
 */
Date.prototype.getWeekNumber = function(){
    var d = new Date(+this);
    d.setHours(0,0,0);
    d.setDate(d.getDate()+4-(d.getDay()||7));
    return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);
};


/*
 * Function to check for all selections
 */
function confirmation_input(){
	
		$.ajax({
			url: "/DemandSense/report/"+type.toLowerCase(),
		 	type: "GET",
		 	data:"&cityid="+city.toString()+"&fromdate="+fromDateFinal+"&todate="+toDateFinal,

		 	success:function(result){
		 		
		 		//console.log(result);
		 		if(result.length == 0){
		 			$(".init-body-div")
		 				.text("Data not found.")
		 				.show();
		 			
		 			$(".init-body")
		 				.hide();
		 			
		 			$(".submit-form")
		 				.text("Submit");
		 			
	 				$(".export-button, .submit-form")
	 					.prop("disabled",false);
	 				
		 		}else{
			 		$(".thead, .result, .result1").children().remove();
			 		$(".init-body-div").hide();
			 		var datee = null;
			 		var _date_col = {};
			 		var _result = {};
			 		var region, state, city = null, city_check = null, product, year = null, f, a;
			 		
			 		/*
			 		 * setting json data in _result object in crossTab order 
			 		 */
			 		$.each(result, function(key,value){
			 			
			 			$.each(value, function(k,v){
			 				v = $.trim(v);
			 				k = $.trim(k);
				 			if(k == 0){
				 				region = v;
				 			}else if(k == 2){
				 				state = v;
				 			}else if(k == 5){
				 				var city1 = "";
				 				if(type == "Week" || type == "Date" ){
				 					var c = [];
				 					$.each(v.split(" "), function(i22, v22){
				 						c.push(v22);
				 					})
				 					
				 					for(var iii = 0; iii<c.length-2;iii++){
				 						city1 += " "+c[iii];
				 					}
				 				}else{
				 					city1 = v;
				 				}
				 				
				 				if(city == null || city != city1){
				 					
				 					city = city1;
					 				_result[city] = {};
					 				//console.log("city done");
				 				}
				 			}else if(k == 7){
				 				product = v;
				 			}else if(k == 8){
				 				//console.log(year+"------"+v);
				 				if(year == null || year != v ){
			 						//city_check = city;
				 					year = v;
				 					
				 					_result[city][year] = {};
				 					
				 					if(_result[city][year].product == undefined){
				 						_result[city][year].product = {};
				 					}
				 					//console.log("year done");
				 					
				 					if(_result[city][year].product[product] == undefined){
				 						_result[city][year].product[product] = {};
				 					}
				 					if(_result[city][year].product[product].f == undefined){
				 						_result[city][year].product[product].f = [];
				 					}
				 					if(_result[city][year].product[product].a == undefined){
				 						_result[city][year].product[product].a = [];
				 					}
					 				
					 				_result[city][year].region = region;
					 				_result[city][year].state = state;
					 				//console.log(_result);
				 				}else{
				 					//console.log(city+"------"+v);
				 					/*if(city != city_check || city_check == null){
				 						city_check = city;
				 					}*/
				 					if(_result[city][year] == undefined){
				 						_result[city][year] = {};
				 					}
				 					if(_result[city][year].product == undefined){
				 						_result[city][year].product = {};
				 					}
				 					if(_result[city][year].product[product] == undefined){
				 						_result[city][year].product[product] = {};
				 					}
				 					if(_result[city][year].product[product].f == undefined){
				 						_result[city][year].product[product].f = [];
				 					}
				 					if(_result[city][year].product[product].a == undefined){
				 						_result[city][year].product[product].a = [];
				 					}
					 				
					 				
					 				_result[city][year].region = region;
					 				_result[city][year].state = state;
					 				//console.log(_result[city][year].product[product].a);
					 				//console.log(_result);
				 				}
				 			}else if(k == 9){
				 				//a = Math.round(v/12);
				 				_result[city][year].product[product].a.push(Math.round(v/12));
				 			}else if(k == 10){
				 				/*var vvv = "";
			 					$.each(v.toString().split("."), function(ii,vv){
			 						if(ii == 0) vvv += Math.round(vv/12);
			 						//else vvv += "."+vv.substring(0, 2);
			 					})*/
				 				f = v;
				 				_result[city][year].product[product].f.push(Math.round(v/12));
				 			}
				 		})
			 			
			 		})
		 			
		 			/*
		 			 * variable to build table data
		 			 */
		 			var tbody = {};
		 			
		 			var tbody1 = {};
		 			
		 			var city = [];
		 			var th1 = th2 = th11 = th22 = '<tr>';
		 			var count_index = [];
		 			var city_count = 0;
		 			var city_count1 = null;
		 			//loop for citys
		 			$.each(_result, function(cityid, data){
		 				var td1 = '';
		 				city_count++;
		 				
		 				city.push(cityid);
		 				
		 				// loop for years
		 				$.each(data, function(year, data1){
		 					
		 					if(city_count == 1){
		 						var dFormat;
		 						if(type == "Week"){
		 							//console.log(year.toString().substring(0,4)+'----'+year.toString().substring(4,6));
		 							dFormat = yearWeek(year.toString().substring(4,6),year.toString().substring(0,4));
		 							th2 += '<th colspan="2"  style="text-align: center !important;">'+year+' <br> '+dFormat+'</th>';
				 					
		 						}else if(type == "Month"){
		 							dFormat = months[parseInt(year.substring(4,6))-1]+"-"+year.substring(0,4); //yearMonth(year.substring(0,3),year.substring(3,5));
		 							th2 += '<th colspan="2"  style="text-align: center !important;">'+year+' <br> '+dFormat+'</th>';
				 					
		 						}else if(type == "Date"){
		 							//var date = new Date(year)//months[parseInt(year.substring(4,6))-1]+"-"+year.substring(0,4); //yearMonth(year.substring(0,3),year.substring(3,5));
		 							dFormat = year.substring(8,10)+"-"+year.substring(5,7)+"-"+year.substring(0,4);
		 							th2 += '<th colspan="2"  style="text-align: center !important;">'+dFormat+'</th>';
				 					
		 						}
			 					
		 						th22 += '<th  style="text-align: right !important;    ">Forecast</th><th style="text-align: right !important;">Actual</th>';
		 						
		 					}
		 					
		 					$.each(data1, function(k, v){
		 						if(k == 'product'){
		 							$.each(v, function(p, af){
		 								if(tbody[cityid+p] == undefined){
		 									tbody[cityid+p] = '<td style="text-align: left !important;">'+data1.region+'</td><td style="text-align: left !important;">'+data1.state+'</td><td style="text-align: left !important;">'+cityid+'</td><td style="text-align: left !important;">'+p+'</td>';
		 								}
		 								if(cityid+p in tbody1){
		 									tbody1[cityid+p] += '<td  style="text-align: right !important; ">'+af.f+'</td><td style="text-align: right !important;">'+af.a+'</td>';
		 								}else{
		 									tbody1[cityid+p] = '<td style="text-align: right !important; ">'+af.f+'</td><td style="text-align: right !important;">'+af.a+'</td>';
		 								}
		 								
		 		 						
		 		 					})
		 						}
		 					})
		 					
		 				})
		 				count_index.push(tbody.length);
		 			})
		 			th2 += '<tr>';
		 			th22 += '<tr>';
	
	 				$(th2+th22).clone().appendTo(".thead");
	 				
	 				//console.log(tbody1);
	 				/*for(j=0;j<count_index.length;j++){
	 					var trr1="",trr2="";
	 					//console.log(count_index[j]);
		 				for(i=0;i<count_index[j];i++){
		 					trr1 = "<tr>";
		 					$.each(tbody[count_index[j-1]+i].split("::"), function(kk, vv){
		 						trr1 += '<td>'+vv+'</td>';
		 					})
		 					trr1 += "</tr>";
		 					trr2 = "<tr>"+tbody1[count_index[j-1]+i]+"</tr>";
		 					//console.log(trr2);
		 					$(trr2).clone().appendTo(".result1");
			 				$(trr1).clone().appendTo(".result");
		 				}
		 				
		 				
	 				}*/
	 				$.each(tbody, function(k, v){
	 					trr1 = "<tr>"+v+"</tr>";
	 					trr2 = "<tr>"+tbody1[k]+"</tr>";
	 					$(trr2).clone().appendTo(".result1");
		 				$(trr1).clone().appendTo(".result");
	 				})
	 				
	 				
	 				
		 				
		 			$(".init-body").show();
	 				$(".submit-form").text("Submit");
	 				$(".export-button, .submit-form").prop("disabled",false);
			 	}
		 		
		 		$('.loading').waitMe('hide');
		 	}
		})
	

	

}



function waitme_function(){
	$('.waitMe_body').css({display:'block'});
	$('.waitMe_body').waitMe({ 
		effect : 'orbit', 
		text : 'Data Loading...', 
		bg : 'rgba(255,255,255,0.7)', 
		color : '#7BAE15'
	});
}

$(".export-button").click(function(){
	var myTableArray = [];

	$(".result tr").each(function() {
	    var arrayOfThisRow = [];
	    var tableData = $(this).find('td');
	    if (tableData.length > 0) {
	        tableData.each(function() { arrayOfThisRow.push($(this).text()); });
	        myTableArray.push(arrayOfThisRow);
	    }
	});
	var myTableArray1 = [];
	$(".result1 tr").each(function() {
	    var arrayOfThisRow = [];
	    var tableData = $(this).find('td');
	    if (tableData.length > 0) {
	        tableData.each(function() { arrayOfThisRow.push($(this).text()); });
	        myTableArray1.push(arrayOfThisRow);
	    }
	});
	var thead = [];
	$(".thead1 tr").each(function() {
	    var arrayOfThisRow = [];
	    var tableData = $(this).find('th');
	    if (tableData.length > 0) {
	    	if(tableData.length ==1){
	    		var a = [" "," "," "," "];
	    		thead.push(a);
	    	}else{
	    		tableData.each(function() { arrayOfThisRow.push($(this).text()); });
		        thead.push(arrayOfThisRow);
	    	}
	        
	    }
	});
	var thead1 = [];
	$(".thead tr").each(function() {
	    var arrayOfThisRow = [];
	    var tableData = $(this).find('th');
	    if (tableData.length > 0) {
	        tableData.each(function() { arrayOfThisRow.push($(this).text()); });
	        thead1.push(arrayOfThisRow);
	    }
	});
	var main_table = [];
	$.each(thead,function(i,v){
		$.each(thead1[i],function(ii,vv){
			if(i ==0){
				v.push(vv);
				v.push(" ");
			}else{

				v.push(vv);
			}
		})
		main_table.push(v);
	})
	$.each(myTableArray,function(i,v){
		$.each(myTableArray1[i],function(ii,vv){
			v.push(vv);
		})
		main_table.push(v);
	})
	//console.log(myTableArray);
	
	JSON2CSV(main_table, "title", false);

})


function JSON2CSV(JSONData, ReportTitle, ShowLabel) {
	
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    //Set Report title in first row or line
    
    //CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';

    }
    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    var fileName = type+"Report_From_"+fromDateFinal+"_To_"+toDateFinal;
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    
    
    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);

}