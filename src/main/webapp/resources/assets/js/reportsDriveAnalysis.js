


/**
 * Global variables
 */

var result = {};
var json_data11 = {};


var region = [], region_temp = [];
var category = [], category_temp = [];
var year = null;

var col_head_text = "Region";
var detail_subcat_text = "";
var sf = "";

var detail_subcat = [];
var detail_reg = [];

var detail__subcat = [];

var	detail_subcat_temp = [];
var detail_reg_temp =[];

var _left = [];
var _top = [];


$(document).ready(function(){
	
	
	getList();
	
	
	$(".regions").change(function(){
		var val = $(this).val();
		if($.inArray("all", val)){
			region = val;
		}else{
			region = region_temp;
		}
		
		//console.log(region);
	})
	
	$(".subCat").change(function(){
		var val = $(this).val();
		if($.inArray("all", val)){
			category = val;
		}else{
			category = category_temp;
			
		}
		
		//console.log(category);
	})
	
	
	$(".from-year-analysis").change(function(){
		year = parseInt($(this).val());
		//console.log(year+2);
		$(".to-year-analysis [value='"+(year+2)+"']").attr('selected', 'selected');
	})
	
	
	$(".submit-form-analysis").click(function(){
		//console.log(region+"test year::: "+region.length);
		if(region.length <= 0){
			//console.log("test reg");
			$("#error-a").text("Please Select Region.").css({"color": "red"});
		}else if(category.length <= 0){
			//console.log("test cat");
			$("#error-a").text("Please Select Sub-Category.").css({"color": "red"});
		}else if(year == null || year == undefined){
			//console.log("test year"+year);
			$("#error-a").text("Please Select Year.").css({"color": "red"});
		}else{
			//console.log("test success");
			$("#error-a").text("").css({"color": "green"});
			$(".submit-form-analysis").text("Wait..");
			$(".export-button-analysis, .submit-form-analysis").prop("disabled",true);
			waitme_function();
			ajax_helper();
		}
	})
	
	
	$(".swap-category").click(function(){
		
		waitme_function();
		
		if($(this).hasClass("catt")){
			$(this).removeClass("catt").addClass("regg");
			$(".swap-text-p").text("Sales Drive Analysis - Region comparison");
			$(".swap-text-f").text("Swap  Region to Sub-Category");
			col_head_text = "Sub-Category";
			displaySubCategoryWise()
			
		}else if($(this).hasClass("regg")){
			$(this).removeClass("regg").addClass("catt");
			$(".swap-text-p").text("Sales Drive Analysis -Sub-Category comparison ");
			$(".swap-text-f").text("Swap Sub-Category to Region");
			col_head_text = "Region";
			displayReagionWise();
		}
		
		
	})
	
})




function ajax_helper(){
	
	
	//console.log("ajax");
	
	$.ajax({
		url: "/DemandSense/report/drive",
		type: "GET",
		
		data: "&regionId="+region.toString()+"&subCatId="+category.toString()+"&fromYear="+year+"&toYear="+(year+2),
		
		success: function(resultt){
			result.reg = resultt;
			//console.log(resultt);
			displayReagionWise();
		},
		
		error: function(resultt){
			console.log(resultt);
		}
	})
	
	
	
}



function displayReagionWise(){
	//console.log("display region");
	var json_data = {};
	var y = null, r = null, s = null;
	$("#analysis").children().remove();

	$(".init-analysis-div").hide();
	//var result = getCategoryAndProduct();
	if(result.reg.length <= 0){
		$(".init-analysis-div")
			.text("Data not found.")
			.show();
		
		$(".init-analysis")
			.hide();
	}else{
		$.each(result.reg, function(index, values){
			
			if(!json_data[values[0]]){
				json_data[values[0]] = {};
			}
			if(!json_data[values[0]][values[1]]){
				json_data[values[0]][values[1]] = {};
			}
			if(!json_data[values[0]][values[1]][values[2]]){
				json_data[values[0]][values[1]][values[2]] = {};
			}
			if(!json_data[values[0]][values[1]][values[2]]["factors"]){
				json_data[values[0]][values[1]][values[2]]["factors"] = {};
			}
			if(!json_data[values[0]][values[1]][values[2]]["factors"][values[1]+values[2]]){
				json_data[values[0]][values[1]][values[2]]["factors"][values[1]+values[2]] = [];
			} 
			
			json_data[values[0]][values[1]][values[2]]["factors"][values[1]+values[2]].push( values[3]+" ("+Math.round((values[4])*100)+" %)");
			
			})
		
		
		$(".init-analysis").show();
		
		var _th = [];
		var _td = [];
		
		_th[0] = '<th colspan="1"></th>';
		_th[1] = '<th colspan="1"></th>';
		_th[2] = '<th>'+col_head_text+'</th>';
		
		var y = [];
		var scat = [];
		var i = j = 0;
		var check = true;
		var r = null;
		$.each(json_data, function(reg, data){
			
			$.each(data, function(yr, data1){
				var year_index = 0;
				$.each(data1, function(sub, data2){
					if($.inArray(yr+sub , scat) <0){
						_th[1] += '<th class="nss top-clr">'+sub+'</th>';
						_th[2] += '<th>Significant Factor (%)</th>';
						scat.push(yr+sub)
					}
					year_index++;
					i = 0 + j;
					
					$.each(data2.factors, function(factors, data3){
						
						// to sort in descending order irrespective of signs (- or +)
						data3.sort(function(a, b){
										a = parseInt(a.split("(")[1]);
										b = parseInt(b.split("(")[1]);
										if(a < 0){
										  a = a*-1;
										} 
										if (b < 0){
											b = b*-1;
										}
							          return b-a
							       });
						
						$.each(data3, function(iii, data4){
							i = j+ iii;
							
							if(factors != undefined){
								if(_td[i] == undefined){
									_td[i] = [];
								
								}
								var indx = y.indexOf(factors.substring(0,4));
								var indx1 = scat.indexOf(factors);
								var indxx = (indx1);
								
								if(indxx == 0){
									_td[i][indxx] = '<td class="nr" style="font-weight: bold;" >'+reg+'</td><td class="ns" onClick="factors_call(this)" data-toggle = "tooltip" data-placement = "top" title = "click for same factors">'+data4+'</td>';
								}else{
									if(_td[i][0] == undefined){
										_td[i][0] = '<td class="nr" style="font-weight: bold;">'+reg+'</td><td> - </td>';
									}
									_td[i][indxx] = '<td class="ns" onClick="factors_call(this)" data-toggle = "tooltip" data-placement = "top" title = "click for same factors">'+data4+'</td>';
								}
								//console.log("----- "+i+" -----"+indx+1+"-"+factors.substring(0,4)+"-"+indx1+"--"+indxx);
							}
							
							
						})
					})
				})
				if($.inArray(yr , y) <0){
					_th[0] += '<th colspan='+year_index+'>'+yr+'</th>';
					y.push(yr);
				}
			})
			
			j = _td.length;
		})
		
		
		//console.log(_td);
		
		
		$.each(_th , function(index, ary){
			$('<tr>'+ary+'</tr>').clone().appendTo("#analysis").css({"min-width":"175px", "width":"auto !important"});
		})
		
		var td = "";
		$.each(_td , function(index, ary){
			//console.log("----- td -----");
			td = '<tr>';
			for(jj = 0; jj < scat.length; jj++){
				if(ary[jj]){
					td += ary[jj];
				}else{
					td += '<td> - </td>';
				}
			}
			td += '</tr>';
			$(td).clone().appendTo("#analysis");
		})
		
		
			
	}
	
	
	$(".submit-form-analysis").text("Submit");
	$(".export-button-analysis, .submit-form-analysis").prop("disabled",false);
	$("[data-toggle = 'tooltip']").tooltip();
	$('.loading').waitMe('hide');
	//console.log(json_data);
}


//var _view = false;


function view(e){
	$('.modal-body').css({display:'block'});
	//$(this).find('.tile-body').css({display:'block'});
	$('.modal-body').waitMe({ 
		effect : 'orbit', 
		text : 'Data Loading...', 
		bg : 'rgba(255,255,255,0.7)', 
		color : '#7BAE15'
	});
	//_view = true;
	
	//console.log("view---"+_view);
	$(".bs-example-modal-lg").modal("show");
	$('#tableHead').html('<td>'+$(e).text()+'</td>')
	var significant_factor = $.trim($(e).text());
	sf = significant_factor.split(" (")[0];
	
	//console.log("&regionId="+detail_reg.toString()+"&subCatId="+detail_subcat.toString()+"&fromYear="+year+"&toYear="+(year+2)+"&significant="+significant_factor.split(" (")[0])
	$.ajax({
		
		url: "/DemandSense/report/details",
		type: "GET",
		
		data: "&regionId="+region.toString()+"&subCatId="+detail_subcat.toString()+"&fromYear="+year+"&toYear="+(year+2)+"&significant="+significant_factor.split(" (")[0],
		
		success: function(result11){
			//console.log(result.region)
			$(".modal-title").text("Comparison - Sales Vs "+significant_factor.split(" (")[0]);
			json_data11 = {};
			$.each(result11, function(index, values){
				
				values[0] = result.region[values[0]];
				values[2] = result.category[values[2]];
				
				if(!json_data11[values[1]]){
					json_data11[values[1]] = {};
				}
				if(!json_data11[values[1]][values[2]]){
					json_data11[values[1]][values[2]] = {};
				}
				if(!json_data11[values[1]][values[2]][values[0]]){
					json_data11[values[1]][values[2]][values[0]] = {};
				}
				
				json_data11[values[1]][values[2]][values[0]].sales = values[3];
				json_data11[values[1]][values[2]][values[0]].correlation = values[4];
				
				
				
			})
			$(".details-select").children().remove();
			$.each($.unique(detail__subcat),function(i,v){
				$('<option value="'+v+'">'+v+'</option>').clone().appendTo(".details-select");
			})
			view_detail();
			
			$('.modal-body').waitMe('hide');
		},
		
		error: function(result){
			console.log(result);
		}
	})
	
	//return true;tableHead
}
$(".details-select").change(function(){
	detail_subcat_text = ($(this).val());
	//console.log(year+2);
	
	view_detail()
})
function view_detail(){
	$("#analysis_details").children().remove();
	
	var objT = {};
	var c1 = 1;
	var __th = [];
	
	__th[0] = '<th colspan="1"></th>';
	__th[1] = '<th colspan="1"></th>';
	__th[2] = '<th>Region</th>';
	$.each(json_data11, function(y, obj2){
		var colspan = 0;
		$.each(obj2, function(sub, obj1){
			if(sub == detail_subcat_text){

				$.each(obj1, function(reg, obj){
					
					if(!objT[reg]){
						objT[reg] = [];
						objT[reg][0] = '<td>'+reg+'</td>';
					}
					obj.sales = null_check1(obj.sales);
					obj.correlation = null_check(obj.correlation);
					objT[reg][c1] = '<td>'+obj.sales+'</td>';
					objT[reg][c1+1] = '<td>'+obj.correlation+'</td>';
					
				})
				c1 = c1+2;
				colspan++;
				__th[1] += '<th colspan="2">'+sub+'</th>';
				__th[2] += '<th>Sales in Quantity (Each)</th><th>'+sf+'</th>';
			}
		})
		__th[0] += '<th colspan="'+colspan*2+'">'+y+'</th>';
	})
	function null_check(n){
		if(n == null || n == " - "){
			n = " - ";
		}else{
			n = Math.round(n)
		}
		return n.toLocaleString("hi-IN");										
	}
	function null_check1(n){
		if(n == null){
			n = "0";
		}else{
			n = Math.round(n)
		}
		return n.toLocaleString("hi-IN");										
	}

	$.each(__th , function(index, ary){
		$('<tr>'+ary+'</tr>').clone().appendTo("#analysis_details").css({"min-width":"175px", "width":"auto !important"});
	})
	//console.log(objT)
	$.each(objT , function(index, ary){
		var __td = '<tr>';
		$.each(ary , function(i, v){
			if(v){
				__td += v;
			}else{
				__td += '<td> - </td>';
			}
		})
		__td += '</tr>';
		$(__td).clone().appendTo("#analysis_details");
	})
	$(".details-select [value='"+detail_subcat_text+"']").attr('selected', 'selected');
	
}




function factors_call(e){
	
	var e_index = $(e).index();
	
	
	
	/*if(_view == true){
		_view = false;
	}else{*/
		detail_subcat = [];
		detail_reg = [];
		
		var tpm = $(e).text().split(" (");

		
		if($(e).hasClass("ns")){
			
			_left = [];
			_top = [];
			
			$(e).removeClass("ns").addClass("s").css({background:'red',color:"white"});
			$("#analysis td").each(function(){
				var tp = $(this).text().split(" (");
				$(this).attr('title', 'click for same factors');
				if(tp[0] == tpm[0]){
					$(this).removeClass("ns")
						   .addClass("s")
						   .css({background:'red',color:"white"})
						   .attr('title', 'click for Sales Vs Factor');
						   /*.append(function(){
							   //console.log($(this).children().index());
							   if($(this).children().index() == -1){
								   return '<button class="view pull-right" onClick="view(this);" style="color:black;z-index:100;" data-toggle="modal" >Details</button>';
							   }else{
								   return false;
							   }
						   	});*/
					//$(this).parent().children(".nr").removeClass("nr").addClass("r").css({background:'green',color:"white"});data-target=".bs-example-modal-lg"
					var leftTmp = $(this).parent().index();
					if($.inArray(leftTmp , _left) <0){
						_left.push(leftTmp);
					}
					var topTmp = $(this).index();
					//console.log(_top);
					if($.inArray(topTmp , _top) <0){
						
						_top.push(topTmp);
						//top.push(topTmp);
					}
				}else if($(this).index() != 0){
					
					$(this).removeClass("s")
						   .addClass("ns")
						   .css({background:'white',color:"#616f77"})
						   .children()
						   .remove();
				}
			})
			$("#analysis tr").each(function(){

				if($.inArray($(this).index(), _left) >= 0){
					$(this).children(".nr").removeClass("nr").addClass("r").css({background:'green',color:"white"});
					
				}else{
					$(this).children(".r").removeClass("r").addClass("nr").css({background:'white',color:"#616f77"});
				}
			})
			
			
			$(".top-clr").each(function(){
				/*if($(this).index() == e_index){
					detail_subcat_text = $(this).text();
				}*/
				if($.inArray($(this).index(), _top) >= 0){
					$(this).removeClass("nss").addClass("ss").css({background:'green',color:"white"});
					
				}else{
					$(this).removeClass("ss").addClass("nss").css({background:'white',color:"#616f77"});
				}
			})
			$("[data-toggle = 'tooltip']").tooltip();
		}else if($(e).hasClass("s")){
			detail_subcat_temp = [];
			detail_reg_temp =[];
			
			/*$(e).removeClass("s").addClass("ns");//.css({background:'white',color:"#616f77"});
			$("#analysis td").each(function(){
				$(this).attr('title', 'click for Sales VS Factor');
				var tp = $(this).text().split(" (");
				
				if(tp[0] == tpm[0]){
					$(this).removeClass("s").addClass("ns");//.css({background:'white',color:"#616f77"})
					.children()
						   .remove();
					//$(this).parent().children(".r").removeClass("r").addClass("nr").css({background:'white',color:"#616f77"});
				}
			})*/
			//alert("else");
			$("#analysis tr").each(function(){

				if($.inArray($(this).index(), _left) >= 0){
					//$(this).children(".nr").removeClass("nr").addClass("r");//.css({background:'green',color:"white"});
					
					if($.inArray(detail_reg_temp , $.trim($(this).children(".r").text())) <0){
						detail_reg_temp.push($.trim($(this).children(".r").text()));
					}
				}
			})
			
			
			$(".top-clr").each(function(){
				if($(this).index() == e_index){
					detail_subcat_text = $(this).text();
				}
				if($.inArray($(this).index(), _top) >= 0){
					//$(this).removeClass("nss").addClass("ss");//.css({background:'green',color:"white"});
					if($.inArray(detail_subcat_temp , $.trim($(this).text())) <0){
						detail_subcat_temp.push($.trim($(this).text()));
					}
				}
			})
			//console.log(detail_subcat_temp)
			if(col_head_text == "Region"){
				$.each(result.region, function(k,v){
					$.each($.unique(detail_reg_temp), function(key,val){
						if(v == val){
							if($.inArray(detail_reg , k) <0){
								detail_reg.push(k);
							}
						}
					})
				})
			}else{
				$.each(result.region, function(k,v){
					$.each($.unique(detail_subcat_temp), function(key,val){
						if(v == val){
							if($.inArray(detail_reg , k) <0){
								detail_reg.push(k);
							}
						}
					})
				})
			}
			if(col_head_text == "Region"){
				$.each(result.category, function(k,v){
					$.each($.unique(detail_subcat_temp), function(key,val){
						if(v == val){
							if($.inArray(detail_subcat , k) <0){
								detail_subcat.push(k);
							}
						}
					})
				})

				detail__subcat = detail_subcat_temp;
			}else{
				detail_subcat_text = $(e).siblings(".r").text();
				
				$.each(result.category, function(k,v){
					$.each($.unique(detail_reg_temp), function(key,val){
						if(v == val){
							if($.inArray(detail_subcat , k) <0){
								detail_subcat.push(k);
							}
						}
					})
				})

				detail__subcat = detail_reg_temp;
			}

			view(e);
		}

		
		
		
		/*else if($(e).hasClass("nr")){
			$(e).removeClass("nr").addClass("r").css({background:'green',color:"white"});
			$("#analysis td").each(function(){
				var tp = $(this).text().split(" (");
				if(tp[0] == tpm[0]){
					//console.log("nr");
					$(this).removeClass("nr").addClass("r").css({background:'green',color:"white"});
					$(this).parent().each(function(){
						$(this).removeClass("nr").addClass("r").css({background:'green',color:"white"});
					})
				}
			})
			//alert("if");
		}else if($(e).hasClass("r")){
			$(e).removeClass("r").addClass("nr").css({background:'white',color:"black"});
			
			$("#analysis td").each(function(){
				var tp = $(this).text().split(" (");
				if(tp[0] == tpm[0]){
					//console.log("nr");
					$(this).removeClass("r").addClass("nr").css({background:'white',color:"black"});
					$(this).parent().each(function(){
						$(this).removeClass("r").addClass("nr").css({background:'white',color:"black"});
					})
				}
			})
				
				if(tp[0] == tpm[0]){
					$(this).removeClass("ns").addClass("s").css({background:'green',color:"white"});
				}else{
					
				}
			
			//alert("else");
		}else if($(e).hasClass("nss")){
			$(e).removeClass("nss").addClass("ss").css({background:'orange',color:"white"});
			$("#analysis th").each(function(){
				var tp = $(this).text().split(" (");
				if(tp[0] == tpm[0]){
					$(this).removeClass("nss").addClass("ss").css({background:'orange',color:"white"});
				}else{
					$(this).removeClass("ss").addClass("nss").css({background:'white',color:"black"});
				}
			})
			$("tr td["+e_index+"]").each(function(){
				var tp = $(this).text().split(" (");
				if(tp[0] != " - "){
					$(this).removeClass("nss").addClass("ss").css({background:'orange',color:"white"});
				}else{
					$(this).removeClass("ss").addClass("nss").css({background:'white',color:"black"});
				}
			})
			//alert("if");
		}else if($(e).hasClass("ss")){
			$(e).removeClass("ss").addClass("nss").css({background:'white',color:"black"});
			$("#analysis th").each(function(){
				
				var tp = $(this).text().split(" (");
				//
				if(tp[0] == tpm[0]){
					$(this).removeClass("ss").addClass("nss").css({background:'white',color:"black"});
				}
			})
			$("tr td["+e_index+"]").each(function(){
				var tp = $(this).text().split(" (");
				//
				if(tp[0] == tpm[0]){
					$(this).removeClass("ss").addClass("nss").css({background:'white',color:"black"});
				}
			})
			//alert("else");
		}*/
	//}
}

function displaySubCategoryWise(){
	//console.log("display SubCategory");
	var json_data = {};
	var y = null, r = null, s = null;
	$("#analysis").children().remove();

	$(".init-analysis-div").hide();
	//var result = getCategoryAndProduct();
	if(result.reg.length <= 0){
		$(".init-analysis-div")
			.text("Data not found.")
			.show();
		
		$(".init-analysis")
			.hide();
	}else{
		$.each(result.reg, function(index, values){
			
			if(!json_data[values[2]]){
				json_data[values[2]] = {};
			}
			if(!json_data[values[2]][values[1]]){
				json_data[values[2]][values[1]] = {};
			}
			if(!json_data[values[2]][values[1]][values[0]]){
				json_data[values[2]][values[1]][values[0]] = {};
			}
			if(!json_data[values[2]][values[1]][values[0]]["factors"]){
				json_data[values[2]][values[1]][values[0]]["factors"] = {};
			}
			if(!json_data[values[2]][values[1]][values[0]]["factors"][values[1]+values[0]]){
				json_data[values[2]][values[1]][values[0]]["factors"][values[1]+values[0]] = [];
			}
			//
			json_data[values[2]][values[1]][values[0]]["factors"][values[1]+values[0]].push( values[3]+" ("+Math.round((values[4])*100)+" %)");
			
			})
		
		
		$(".init-analysis").show();
		
		var _th = [];
		var _td = [];
		
		_th[0] = '<th colspan="1"></th>';
		_th[1] = '<th colspan="1"></th>';
		_th[2] = '<th>'+col_head_text+'</th>';
		
		var y = [];
		var scat = [];
		var i = j = 0;
		var check = true;
		var r = null;
		$.each(json_data, function(reg, data){
			//i = _td.length;
			
			$.each(data, function(yr, data1){
				var year_index = 0;
				$.each(data1, function(sub, data2){
					if($.inArray(yr+sub , scat) <0){
						_th[1] += '<th class="nss top-clr">'+sub+'</th>';
						_th[2] += '<th>Significant Factor (%)</th>';
						scat.push(yr+sub)
					}
					year_index++;
					i = 0 + j;
					
					$.each(data2.factors, function(factors, data3){
						
						// to sort in descending order irrespective of signs (- or +)
						data3.sort(function(a, b){
										a = parseInt(a.split("(")[1]);
										b = parseInt(b.split("(")[1]);
										if(a < 0){
										  a = a*-1;
										} 
										if (b < 0){
											b = b*-1;
										}
							          return b-a
							       });
						
						$.each(data3, function(iii, data4){
							i = j+ iii;
							
							if(factors != undefined){
								if(_td[i] == undefined){
									_td[i] = [];
									//_td[i][0] = '<td>'+reg+'</td>';
								}
								var indx = y.indexOf(factors.substring(0,4));
								var indx1 = scat.indexOf(factors);
								var indxx = (indx1);
								/*if(_td[i] == undefined){
									_td[i] = [];
								}*/
								if(indxx == 0){
									_td[i][indxx] = '<td class="nr" style="font-weight: bold;">'+reg+'</td><td class="ns" onClick="factors_call(this)"  data-toggle = "tooltip" data-placement = "top" title = "click for same factors">'+data4+'</td>';
								}else{
									if(_td[i][0] == undefined){
										_td[i][0] = '<td class="nr" style="font-weight: bold;">'+reg+'</td><td> - </td>';
									}
									_td[i][indxx] = '<td class="ns" onClick="factors_call(this)"  data-toggle = "tooltip" data-placement = "top" title = "click for same factors">'+data4+'</td>';
								}
								//_td[i][indxx] = '<td>'+data4+'</td>';
								//console.log("----- "+i+" -----"+indx+1+"-"+factors.substring(0,4)+"-"+indx1+"--"+indxx);
							}
							
							
						})
					})
				})
				if($.inArray(yr , y) <0){
					_th[0] += '<th colspan='+year_index+'>'+yr+'</th>';
					y.push(yr);
				}
			})
			
			j = _td.length;
		})
		
		
		//console.log(_td);
		
		
		$.each(_th , function(index, ary){
			$('<tr>'+ary+'</tr>').clone().appendTo("#analysis").css({"min-width":"175px", "width":"auto !important"});
		})
		
		var td = "";
		$.each(_td , function(index, ary){
			//console.log("----- td -----");
			td = '<tr>';
			for(jj = 0; jj < scat.length; jj++){
				if(ary[jj]){
					td += ary[jj];
				}else{
					td += '<td> - </td>';
				}
			}
			td += '</tr>';
			$(td).clone().appendTo("#analysis");
		})
		
		
			
	}
	
	
	$(".submit-form-analysis").text("Submit");
	$(".export-button-analysis, .submit-form-analysis").prop("disabled",false);
	$("[data-toggle = 'tooltip']").tooltip();
	$('.loading').waitMe('hide');
	//console.log(json_data);
}



/*
 * clearing all values
 */
function variablesClear(){
	region = [];
	region_temp = [];
	category = [];
	category_temp = [];
	year = null;
	$("#error-a").text("").css({"color": "green"});
}


/*
 * Calling function from sales drive analysis panel
 */

function driveAnalysis(e){
	
	variablesClear();
	
	$(".main-div").hide();
	$(".init-div").hide();
	$(".temp").text("Please Wait...").show();
	//var result = getCategoryAndProduct();
	
	if(result){
		
		$(".regions, .subCat, .from-year-analysis, .to-year-analysis").children().remove();
		$('<option value="all">Select all</option>').clone().appendTo(".regions, .subCat");
		
		$.each(result.region , function(key, value){
			region_temp.push(key);
			$('<option value="'+key+'">'+value+'</option>').clone().appendTo(".regions");
		})
		
		
		$.each(result.category , function(key, value){
			category_temp.push(key);
			$('<option value="'+key+'">'+value+'</option>').clone().appendTo(".subCat");
		})
		
		$("<option selected disabled>Year</option>").clone().appendTo(".from-year-analysis, .to-year-analysis");
		for(var d=2012;d<=2020;d++){
		    $("<option value="+d+">"+d+"</option>").clone().appendTo(".from-year-analysis, .to-year-analysis");
		}
		
		$(".temp").hide();
		$(".main-div-alanysis").show();
	}else{
		$(".temp").text("Oops!! Something went wrong.").show();
	}
	
	
	
	
	
}


function waitme_function(){
	$('.waitMe_body').css({display:'block'});
	//$(this).find('.tile-body').css({display:'block'});
	$('.waitMe_body').waitMe({ 
		effect : 'orbit', 
		text : 'Data Loading...', 
		bg : 'rgba(255,255,255,0.7)', 
		color : '#7BAE15'
	});
}


/*
 * Function to get region and Sub Category list
 */
function getList(){}
	
	$.ajax({
		url: "/DemandSense/report/region",
		type: "GET",
		
		success: function(result1){
			result.region = result1;
			//console.log(result1);
		},
		error: function(result1){
			console.log(result1);
		}
		
	})
	
	$.ajax({
		url: "/DemandSense/report/subcat",
		type: "GET",
		
		success: function(result1){
			result.category = result1;
			//console.log(result1);
		},
		error: function(result1){
			console.log(result1);
		}
		
	})
	
//}


