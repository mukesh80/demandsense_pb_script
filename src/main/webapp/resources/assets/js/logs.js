

var events = [];
var data = [];
	
	
	
	

$(document).ready(function(){
	
	ajax_caller("init");
	
	
})

function ajax_caller(controller){
	
	$.ajax({
		url: "/DemandSense/logs/"+controller,
		type: "post",
		
		success: function(data1){
			
			console.log(data1)
			var key = Object.keys(data1)[0];
			console.log(key);
			
			if(key == "admin"){
				
				/*
				 * Displaying users in dropdown
		      	 */
				
				$(".users").children().remove();
				$('<option value="all">Select All</option>').clone().appendTo(".users");
				$.each(data1[key].sort(), function(index, value){
					if(value){
						$('<option value="'+value+'">'+value.charAt(0).toUpperCase() + value.slice(1)+'</option>').clone().appendTo(".users");
					}
		    	 	
		     	})
		     	ajax_user_caller("userlog", "all");
			}else if(key == "user"){
				data = data1[key];
				console.log(data);
				waitme_function();
				table_display("all");
			}
          
		},
		
		error: function(result){
			console.log(result);
		}
	})
}

function waitme_function(){
	$('.waitMe_body').css({display:'block'});
	//$(this).find('.tile-body').css({display:'block'});
	$('.waitMe_body').waitMe({ 
		effect : 'orbit', 
		text : 'Data Loading...', 
		bg : 'rgba(255,255,255,0.7)', 
		color : '#7BAE15'
	});
}
function ajax_user_caller(controller, user){
	
	$.ajax({
		url: "/DemandSense/logs/"+controller,
		type: "post",
		data:{
			userName: user
		},
		success: function(data1){
			
			console.log(data1)
			var key = Object.keys(data1)[0];
			console.log(key);
			
			if(key == "user"){
				data = data1[key];
				console.log(data);
				waitme_function();
				table_display("all");
			}else{
				alert("Sorry no data found");
			}
          
		},
		
		error: function(result){
			console.log(result);
		}
	})
}


	/*
	 * Event dropdown handler
	 */
	$(".user-events").change(function(){
  		var val = $(this).val();
  		waitme_function();
  		console.log(val)
  		table_display(val);
	})
	
	/*
	 * users dropdown handler
	 */
	$(".users").change(function(){
  		var val = $(this).val();
  		waitme_function();
  		console.log(val)
  		ajax_user_caller("userlog", val);
	})
	
	
	/*
	 * Getting date and formating in custom format
	 */
	function dateConvertion(val){
		if(val){
			var d = new Date(val);
			function z(n){
				return (n<10?'0':'')+n
			}
			var months = ['Jan','Feb','Mar','Apr','May','Jun',
			                'Jul','Aug','Sep','Oct','Nov','Dec'];
			return d.getDate() + ' ' + months[d.getMonth()] + ', ' + d.getFullYear() +
	        ' at ' + z(d.getHours()%12 || 12) + ':' + z(d.getMinutes()) +
	        ' ' + (d.getHours() < 12? 'am':'pm');
		}else{
			return null;
		}
	}
	
	
	


function table_display(val){
	
	console.log("in table display")
	
	//$("#user-logs_wrapper").remove();
	var oTable =  $('#user-logs').dataTable({
	     "fnDrawCallback": function ( oSettings ){
	       if ( oSettings.bSorted || oSettings.bFiltered ){
	           for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
	               { }
	       }
	   }, 
	   //"scrollY": 400,
	   "pageLength": 25,
	   "scrollX": false,  
	   "bDestroy": true,
	   
	   "order": [[ 3, "desc" ]]
	});
	
	 var row = [];   
	 var i = 0;
	 
	 	$.each(data, function(i1, UserReport)    
	     {
		 	events.push(UserReport[2]);
		 	if(UserReport[2] == val || val == "all" ){
	
	      	   	row[i] =  [];
		       	row[i][0] = UserReport[0];
		        row[i][1] = UserReport[1];
		        row[i][2] = UserReport[2];
		        row[i][3] = dateConvertion(UserReport[3]);
		        i++;
	 		}
	     }); 
	 
	 $.unique(events);
	 events.sort();
	 /*
      * Displaying events in dropdown
      */
	 
	 $(".user-events").children().remove();
	 $('<option value="all">Select All</option>').clone().appendTo(".user-events");
     $.each(events, function(index, value){
   	  	if(val == value){
   	  		$('<option value="'+value+'" selected>'+value.charAt(0).toUpperCase() + value.slice(1)+'</option>').clone().appendTo(".user-events");
   	  	}else{
   	  		$('<option value="'+value+'">'+value.charAt(0).toUpperCase() + value.slice(1)+'</option>').clone().appendTo(".user-events");
   	  	}
    	 
     })
     
	 oTable.fnClearTable();
	 oTable.fnAddData(row);
	 $('.loading').waitMe('hide');
}