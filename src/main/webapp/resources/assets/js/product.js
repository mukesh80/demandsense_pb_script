
var en_date_sys_param, dynamic_check_external = [], dynamic_check_internal = [], dynamic_check_social = [];
var external_factor_values = {"minTemp":"Min Temp","maxTemp":"Max Temp","avgTemp":"Avg Temp"};//"feellikTemp":"Feel like Temp","feelLikeTemp":"Feel like Temp"};	//,"precipitation":"Fuel Price","wind":"Consumer Price Index","humidity":"UnEmployment"};
var internal_factor_values = {"price":"Price","promotion":"Sales Executive Count","discount":"Location Count"};
var social_factor_values = {"sentimental":"Sentimental","srs":"SRS"};
var factor_color = ["#5484ED","#DC2127","#DBADFF","#51B749","#FBD75B","#FF887C","#DC2127","#DEB6FD","#E1E1E1","#7AE7BF"];
var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];



	$.ajax({
	 	url: "/DemandSense/sysparams/getparam",
	 	type: "GET",

	 	success:function(msg){

	 		en_date_sys_param = msg.endDate;

submit_internal_graph = function () {

    gantt_chart = {
    		
	        chart: {
				defaultSeriesType: 'spline',
				renderTo: 'int_chart',
				height: 250,
				marginLeft: 84,
              
	        },
	        plotOptions: {
	              series: {
	                  lineWidth: 2,
	                  marker: {
	                      enabled: false
	                  }
	              }
	          },
	        title: {
	            text: 'Promotion'
	        },

	         xAxis: {
		        type: 'datetime',
                minorTickInterval: "auto",
                minorTickPosition:'outside',
                minorTickWidth: 1,
                minorTickLength: 4,
				minorGridLineWidth: 0,
				tickInterval: 7 * 24 * 36e5, // one week
		        labels: {
		            format: '{value: %W}'
		        },

                dateTimeLabelFormats: {
	                millisecond: '%H:%M:%S.%L',
	                second: '%H:%M:%S',
	                minute: '%H:%M',
	                hour: '%H:%M',
	                day: '%e. %b',
	                week: '%e. %b',
	                month: '%b \'%y',
	                year: '%Y'
           		 },

           		  plotLines: [{
			                  color: '#493D55',
			                  width: .8,
			                  value: get_data(en_date_sys_param)
			              }],
           	},

	        yAxis: {
	            title: '',
	            categories: [],
	            min: 0,
	            max: 1,
	            gridLineWidth: 0,
	            tickLength: 5,
              tickWidth: 1,
              tickPosition: 'outside',
              
              lineWidth:1,
	        },
	        
	        
	       

	         tooltip: {
	        	 shared: true,
                 useHTML: true,
                 headerFormat: '<small>{point.key}</small><table>',
                 pointFormat: '<tr><td style="color: {series.color}">{series.name}: </td>' +
                     '<td style="text-align: right"><b>{point.y}</b></td></tr>',
                 footerFormat: '</table>',
                 valueDecimals: 2
		            },
		            
		            credits: {
	                    enabled: false
	                },


	        series: [{
	        			showInLegend : false,
			            data: []
	       			}
	       		]

	       
	};

		var gen_gantt = function(){
			
			$.ajax({
		        url: "/DemandSense/predictions/internalfactor"+agr_value,
		        type: "GET",
		        data: '&cat='+catid+'&subcatid='+subcatid+'&product='+proid+'&con_id='+ conid +'&regionId='+ regid+'&state='+statid+'&district='+disid+'&city='+citid+'&location='+locid+'&date_start='+date_start+'&date_end='+date_end,
		      
		        success:function(msg){
		        	var dynamic_keys = [];
		        	var res_keys = [];
					for(var k in msg) {res_keys.push(k)};
					var res_keys1 = [];
					for(var f=0;f<res_keys.length;f++){
						res_keys1.push(res_keys[f].substring(0,6));
						
					}
					//gantt_chart.yAxis.categories = res_keys1;

					var act_data = [];
					
					//console.log(res_keys)

					for(var i=0;i<res_keys.length;i++){

						var dat_obj1 = {};
						//var dat_obj2 = {}
						
						
						var m_arr1 = [];
						var m_arr2 = []

						for(var j=0; j< msg[res_keys[i]].length;j++){
							var int_arr1 = [];
							var int_arr2 = [];
							if (msg[res_keys[i]][j].promotion == "1"){
								int_arr1 = [( get_data(msg[res_keys[i]][j].date.substring(0, 10)) ), parseInt(msg[res_keys[i]][j].promotion)];
								
							}else{
								int_arr1 = [( get_data(msg[res_keys[i]][j].date.substring(0, 10)) ), null];
								
							}
							
							m_arr1.push(int_arr1);
							
						}

						//console.log(int_arr1)
						
						dat_obj1.name = 'promotion';
						dat_obj1.data = m_arr1.sort();
						
						//dat_obj1.name = internal_factor;
						dat_obj1.showInLegend = true;
						dat_obj1.color = '#7BCAF5';

						act_data.push(dat_obj1);
						
						internal1_export.push(msg[res_keys[i] ])

					}

				//console.log(act_data)
					gantt_chart.series = (act_data) ;
					
					

					//var int_chart = new Highcharts.Chart(internal); 
					gantt_charts = new Highcharts.Chart(gantt_chart);
		        }

		     });
		        	
		        	/*
		        }
		        	var res_keys = [];
		        	var res_keys1 = [];
		        	
					for(var k in mes) {

						res_keys.push(k);						

					};
					for(var f=0;f<res_keys.length;f++){
						res_keys1.push(res_keys[f].substring(0,6));
						
					}
					gantt_chart.yAxis.categories = res_keys1;
					//gantt_chart.yAxis.max = res_keys.length;

					//console.log(res_keys)

					var int_arr1 = [];
					var int_arr22 = [];
					var tr = [];
					var c = 0;

					for(var i=0;i<res_keys.length;i++){
						////console.log(mes[res_keys[i]]);
						var dat_obj1 = {};
						//dat_obj1.name = res_keys[i]+' '+ internal_factor;
						
						for(var j=0; j< mes[res_keys[i]].length;j++){

							//tr = mes[res_keys[i]][j].date.slice(10);
						
							
							if (mes[res_keys[i]][j].promotion == "1"){

								
								dat_obj1.name = res_keys[i]+'(promotion)';
								
								int_arr1 = [( get_data(mes[res_keys[i]][j].date.substring(0, 10)) ), '1'];
								
								
								dat_obj1.x = get_data(mes[res_keys[i]][j].date.substring(0, 10) );
								////console.log(mes[res_keys[i]][j]+"----------"+mes[res_keys[i]][j].yearWeek);
								var t = new Date(mes[res_keys[i]][j].date);

								endDt = t.setDate(t.getDate() + 1);
								//console.log(endDt)
								dat_obj1.x2 = endDt;
								dat_obj1.y = i;
								int_arr1.push(dat_obj1);
								dat_obj1 = {};

							}else{
								dat_obj1.name = res_keys[i]+'(promotion)';
								
								int_arr1 = [( get_data(mes[res_keys[i]][j].date.substring(0, 10)) ), 'null'];
								
								
								dat_obj1.x = get_data(mes[res_keys[i]][j].date.substring(0, 10) );
								////console.log(mes[res_keys[i]][j]+"----------"+mes[res_keys[i]][j].yearWeek);
								var t = new Date(mes[res_keys[i]][j].date);

								endDt = t.setDate(t.getDate() + 1);
								//console.log(endDt)
								dat_obj1.x2 = endDt;
								dat_obj1.y = i;
								int_arr1.push(dat_obj1);
								dat_obj1 = {};
								
							}
							
							int_arr22.push(int_arr1);
							//console.log(int_arr22+' ------'+int_arr1.sort());
							if(c==0){

								var dt = mes[res_keys[i]][j].date.substring(0, 10);

							}
							
							c =1;
							
						}
					}
					
					gantt_chart.series[0].data = int_arr22.sort();
					
					internal1_export.push(mes[res_keys[i] ])

					//console.log(int_arr1)
					//console.log(gantt_chart)

					 gantt_charts = new Highcharts.Chart(gantt_chart); 
				}
			});*/
			}

		gen_gantt();

	};
	
	//function to get week nmb Ex: d = new Date();d.getWeek();
	Date.prototype.getWeek = function() {

	    var determinedate = new Date(); 
	    determinedate.setFullYear(this.getFullYear(), this.getMonth(), this.getDate()); 
	    var D = determinedate.getDay(); 
	    if(D == 0) D = 7; 
	    determinedate.setDate(determinedate.getDate() + (4 - D)); 
	    var YN = determinedate.getFullYear(); 
	    var ZBDoCY = Math.floor((determinedate.getTime() - new Date(YN, 0, 1, -6)) / 86400000); 
	    var WN = 1 + Math.floor(ZBDoCY / 7); 

	    return WN; 
	}
	


	weekdat = function getDateOfWeek(weeks, years) {
		var d = new Date(years, 0, 1);
        var dayNum = d.getDay();
        var diff = --weeks * 7;

        // If 1 Jan is Friday to Sunday, go to next week
        if (!dayNum || dayNum > 4) {
            diff += 7;
        }

        // Add required number of days
        d.setDate(d.getDate() - d.getDay() + ++diff);
   	    return (d);
   	}
	Date.prototype.getDateWithDateOrdinal = function() {
	    var d = this.getDate();  // from here on I've used Kennebec's answer, but improved it.
	    if(d>3 && d<21) return d+'th';
	    switch (d % 10) {
	        case 1:  return d+"st";
	        case 2:  return d+"nd";
	        case 3:  return d+"rd";
	        default: return d+"th";
	    }
	};

	Date.prototype.getMonthName = function(shorten) {
	    var monthsNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	    var monthIndex = this.getMonth();
	    var tempIndex = -1;
	    if (monthIndex == 0){ tempIndex = 0 };
	    if (monthIndex == 1){ tempIndex = 1 };
	    if (monthIndex == 2){ tempIndex = 2 };
	    if (monthIndex == 3){ tempIndex = 3 };
	    if (monthIndex == 4){ tempIndex = 4 };
	    if (monthIndex == 5){ tempIndex = 5 };
	    if (monthIndex == 6){ tempIndex = 6 };
	    if (monthIndex == 7){ tempIndex = 7 };
	    if (monthIndex == 8){ tempIndex = 8 };
	    if (monthIndex == 9){ tempIndex = 9 };
	    if (monthIndex == 10){ tempIndex = 10 };
	    if (monthIndex == 11){ tempIndex = 11 };

	    if (tempIndex > -1) {
	        this.monthName = (shorten) ? monthsNames[tempIndex].substring(0, 3) : monthsNames[tempIndex];
	    } else {
	        this.monthName = "";
	    }

	    return this.monthName;
	};
		changeFunc1 =   function () {

		    var selectBox = document.getElementById("ondat");
		    week_nm = selectBox.options[selectBox.selectedIndex].value;
		    
		    var selectBox = document.getElementById("onyar");
		    year_nm = selectBox.options[selectBox.selectedIndex].value;

		    if(week_nm.length == 1)
		    {
		    	week_nm = ("0" + week_nm).slice(-2);
		    }
		    
		    //console.log(week_nm);
		    var df = weekdat(week_nm, year_nm);
		    var d_print = df.getMonthName()+" "+df.getDateWithDateOrdinal()+" "+df.getFullYear();
		    $("#fron-date-display").text(d_print);
       								    
       	}
        changeFunc =  function () {
			    var selectBox = document.getElementById("onyar");
			    year_nm = selectBox.options[selectBox.selectedIndex].value;
			    
			    var selectBox = document.getElementById("ondat");
			    week_nm = selectBox.options[selectBox.selectedIndex].value;
			    
			    date_start = year_nm+week_nm;
			    
			    var df = weekdat(week_nm, year_nm);
			    var d_print = df.getMonthName()+" "+df.getDateWithDateOrdinal()+" "+df.getFullYear();
			    $("#fron-date-display").text(d_print);
			    					    
       	}

        changeFunc3 = function () {

		    var selectBox = document.getElementById("ondat1");
		    week_nm1 = selectBox.options[selectBox.selectedIndex].value;
		    var selectBox = document.getElementById("onyar1");
		    year_nm1 = selectBox.options[selectBox.selectedIndex].value;

		    if(week_nm1.length == 1)
		    {
		    	week_nm1 = ("0" + week_nm1).slice(-2);
		    }
		    
		   // ////console.log(week_nm1);
		    
		    var df = weekdat(week_nm1, year_nm1);
		    var d_print = df.getMonthName()+" "+df.getDateWithDateOrdinal()+" "+df.getFullYear();
		    $("#to-date-display").text(d_print);
       	}

        changeFunc4 = function () {

		    var selectBox = document.getElementById("onyar1");
		    year_nm1 = selectBox.options[selectBox.selectedIndex].value;
		    var selectBox = document.getElementById("ondat1");
		    week_nm1 = selectBox.options[selectBox.selectedIndex].value;

		    //console.log(year_nm1);
		    
		    
		    //console.log( year_nm1+week_nm1)
		    
		    date_end = year_nm1+week_nm1;
		    var df = weekdat(week_nm1, year_nm1);
		    var d_print = df.getMonthName()+" "+df.getDateWithDateOrdinal()+" "+df.getFullYear();
		    $("#to-date-display").text(d_print);
		}
        
        /*st_date = "2013-09-01";
    	en_date = "2014-04-01";
    	*/
    	
    	
        var current_week = new Date();
        var cur_week1 = current_week.getWeek();
        var cur_year1 = current_week.getFullYear();	
       
        
        var cur_week = cur_week1;
        var cur_year = cur_year1;
        	
        if(cur_week1 > 7){
        	cur_week = cur_week1 - 7;
        	cur_year = cur_year1;
        }else{
        	cur_week = 53 - (7 - cur_week1);
        	cur_year = cur_year- 1;
        }
        
        
        function week_less_ten(val){
    		if(val < 10){
    			val = "0"+val;
    		}
    		return val;
    	}
	
        var selectBox1 = document.getElementById("ondat");
		week_nm = selectBox1.value = week_less_ten(cur_week);

		var selectBox2 = document.getElementById("onyar");
		year_nm = selectBox2.value = cur_year;

		var selectBox3 = document.getElementById("ondat1");
		week_nm1 = selectBox3.value = week_less_ten(cur_week1);

		 var selectBox4 = document.getElementById("onyar1");
		year_nm1 = selectBox4.value = cur_year1;
		
		var df = weekdat(week_nm, year_nm);
	    var d_print = df.getMonthName()+" "+df.getDateWithDateOrdinal()+" "+df.getFullYear();
	    $("#fron-date-display").text(d_print);
	    
	    var df = weekdat(week_nm1, year_nm1);
	    var d_print = df.getMonthName()+" "+df.getDateWithDateOrdinal()+" "+df.getFullYear();
	    $("#to-date-display").text(d_print);

		d_start = weekdat(selectBox1.value, selectBox2.value);
		d_end = weekdat(selectBox3.value,selectBox4.value);

	two_sliders = function(){
		
		
		dt_start = new Date(  weekdat(week_nm, year_nm));
		dt_end =  new Date( weekdat(week_nm1, year_nm1));
		
		var st_date1 = new Date(  weekdat(week_nm, parseInt(year_nm)-1));
		var en_date1 =  new Date( weekdat(week_nm1, parseInt(year_nm1)+1));
		
		var weeks_range = (dt_end - dt_start) / 604800000;
		
		var scale_months = 1;
		
		function weeks_diff(val){
			var week_temp = 1;
			if(val >= 1 && val <= 5){
				week_temp = 1;
			}else if(val >= 6 && val <= 20){
				week_temp = 1;
			}else if(val >= 21 && val <= 40){
				week_temp = 15;
			}else if(val >= 41 && val <= 53){
				week_temp = 35;
			}
			return week_temp;
		}
		function weeks_diff1(val){
			var week_temp = 1;
			if(val >= 1 && val <= 5){
				week_temp = 6;
			}else if(val >= 6 && val <= 20){
				week_temp = 25;
			}else if(val >= 21 && val <= 40){
				week_temp = 45;
			}else if(val >= 41 && val <= 53){
				week_temp = 53;
			}
			return week_temp;
		}
		
		if(weeks_range >= 10 && weeks_range <= 79){
			var w1 = weeks_diff(week_nm);
			var w2 = weeks_diff1(week_nm1);
			st_date1 = new Date(  weekdat(w1, parseInt(year_nm)));
			en_date1 =  new Date( weekdat(w2, parseInt(year_nm1)));
		}
		else if(weeks_range >= 80 && weeks_range <= 160){
			scale_months = 3;
		}
		else if(weeks_range >= 161 && weeks_range <= 240){
			scale_months = 6;
		}
		else if(weeks_range >= 241){
			scale_months = 12;
		}
		
		/*
		Date.prototype.getWeek = function() {
			  var onejan = new Date(this.getFullYear(),0,1);
			  return Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+1)/7);
		}
		*/
		
		 $("#slider").dateRangeSlider({
		    bounds: {min: new Date(st_date1), max: new Date(en_date1)},
		    defaultValues: {min: new Date(dt_start), max: new Date(dt_end)},
		    
		    formatter: function(val){
		    	month = val.getMonth();
		    	year = val.getFullYear();
		    	
		    	weeks = val.getWeek();
		    	var weekNumber = val.getWeek();
		    	return year+'/'+weeks;
		    },
		    arrows:false,
		    scales: [{
		      first: function(value){ return value; },
		      end: function(value) {return value; },
		      next: function(value){
		        var next = new Date(value);
		        return new Date(next.setMonth(value.getMonth() + scale_months));
		      },
		      label: function(value){
		        return months[value.getMonth()];
		      },
		      format: function(tickContainer, tickStart, tickEnd){
		        tickContainer.addClass("myCustomClass");
		      }
		    }]
		});
		 
		$("#slider").bind("userValuesChanged", function(e, data){

			dt_start = data.values.min;
			dt_end = data.values.max;
			
			w_nm = String (data.values.max.getWeek() );
			w_nm1 = String(data.values.min.getWeek());
		/*	
			//console.log(w_nm, w_nm1)
			
			if(w_nm.length == 1)
		    {
		    	w_nm = ("0" + w_nm).slice(-2);
		    	//console.log(12)
		    }
			
			if(w_nm1.length == 1)
		    {
		    	w_nm1 = ("0" + w_nm1).slice(-2);
		    	//console.log(111)
		    }
		*/
		
			
			date_end = String(data.values.max.getFullYear() )+ String(w_nm );
			date_start = String(data.values.min.getFullYear() )+ String(w_nm1 )
			
			    //get_forcast_time();
				submit_forecast();
				submit_internal1();
				//submit_internal_graph()
				submit_external();
				submit_social();

			

		});

	}

	two_sliders(); 	//function to call two rulers
	
	//https://github.com/xoxco/jQuery-Tags-Input
	 $(function(){
		  catid = [];
		  subcatid = [];
		  proid =[];
		  conid = [];
		  regid = [];
		  statid = [];
		  disid = [];
		  citid = [];
		  locid = [];
		  
		  
		  catid1 = [];
		  subcatid1 = [];
		  proid1 =[];
		  conid1 = [];
		  regid1 = [];
		  statid1 = [];
		  disid1 = [];
		  citid1 = [];
		  locid1 = [];

		 /**
		  * Selection criteria from session
		  */

		 var criteria_session = $("#selected-criteria").val();
		 
		 if(criteria_session != "null"){
			 criteria_session = criteria_session.toString().substring(1, criteria_session.length-1);//JSON.stringify(criteria_session);
			 var session_temp = [];
		 	
		 	////console.log(JSON.parse(criteria_session).cat);
		 	$.each(criteria_session.split(", "), function(index, value){
		 		session_temp.push(value);
		 	})
		 	//var cat_end = null;
		 	$.each(session_temp, function(index, value){
		 		if(index==0 || index == 1 || index == 2){
		 			
		 			var value1 = value.split("=");
		 			if(value1[0] == "cat"){
		 				$.each(value1[1].split(","), function(i, v){
			 				////console.log(v);
			 				if(v != ""){
				 				catid.push(v.toString());
			 					$.each(v.split(":"), function(ii, vv){
			 						////console.log(vv);
			 						var id;
			 						if(ii == 0){
			 							id = vv;
			 							catid1.push($.trim(vv.toString().replace(/_AND_/g, "&").replace(/_COMMA_/g, ",")));
			 						}
			 						if(vv != "" && ii !=0){
				 						
				 						if (!$('#productInput').tagExist(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "))) {
				 						    $('#productInput').addTag(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "),{tagId:id});
				 							
				 						  }
			 						}
			 						
			 					})
			 				}
			 			})
		 			}else if(value1[0] == "subcatid"){
		 				$.each(value1[1].split(","), function(i, v){
			 				////console.log(v);  
			 				if(v != ""){
			 					 subcatid.push(v.toString());
			 					$.each(v.split(":"), function(ii, vv){
			 						////console.log(vv);
			 						var id;
			 						if(ii == 0){
			 							id = vv;
			 							subcatid1.push($.trim(vv.toString().replace(/_AND_/g, "&").replace(/_COMMA_/g, ",")));
			 						}
			 						if(vv != "" && ii !=0){
				 						
				 						if (!$('#productInput').tagExist(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "))) {
				 						    $('#productInput').addTag(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "),{tagId:id});
				 							
				 						  }
			 						}
			 						
			 					})
			 				}
			 			})
		 			}else if(value1[0] == "product"){
		 				$.each(value1[1].split(","), function(i, v){
			 				////console.log(v);
			 				if(v != ""){
			 					proid.push(v.toString());
			 					$.each(v.split(":"), function(ii, vv){
			 						////console.log(vv);
			 						var id;
			 						if(ii == 0){
			 							id = vv;
			 							proid1.push($.trim(vv.toString().replace(/_AND_/g, "&").replace(/_COMMA_/g, ",")));
			 						}
			 						if(vv != "" && ii !=0){
				 						
				 						if (!$('#productInput').tagExist(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "))) {
				 						    $('#productInput').addTag(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "),{tagId:id});
				 							
				 						  }
			 						}
			 						
			 					})
			 				}
			 			})
		 			}
		 			
		 			////console.log(catid+"----"+value1);
		 		}else if(index>2){
		 			var value1 = value.split("=");
		 			if(value1[0] == "date_start"){
		 				console.log(value1[1]);
		 				var s_week = value1[1].substring(4);
		 				if(s_week.length <=2){
		 					s_week = "0"+s_week;
		 				}
		 				var s_year = value1[1].substring(0,4);
		 				$('#ondat option').each(function () {
		 	                if (parseInt(this.text) == parseInt(s_week)) {
		 	                    $(this).prop('selected', true);
		 	                    changeFunc1();
		 	                }
		 	            });
		 				$('#onyar option').each(function () {
		 	                if (this.text == s_year) {
		 	                    $(this).prop('selected', true);
		 	                    changeFunc();
		 	                }
		 	            });
		 				//console.log(s_week+"===="+s_year);
		 			}else if(value1[0] == "date_end"){
		 				var e_week = value1[1].substring(4);
		 				var e_year = value1[1].substring(0,4);
		 				if(e_week.length <=2){
		 					e_week = "0"+e_week;
		 				}
		 				$('#ondat1 option').each(function () {
		 	                if (parseInt(this.text) == parseInt(e_week)) {
		 	                    $(this).prop('selected', true);
		 	                    changeFunc3();
		 	                }
		 	            });
		 				$('#onyar1 option').each(function () {
		 	                if (this.text == e_year) {
		 	                    $(this).prop('selected', true);
		 	                    changeFunc4();
		 	                }
		 	            });
		 				//console.log(e_week+"===="+e_year);
		 			}else if(value1[0] == "con_id"){
		 				$.each(value1[1].split(","), function(i, v){
			 				////console.log(v);
			 				if(v != ""){
			 					conid.push(v.toString());
			 					$.each(v.split(":"), function(ii, vv){
			 						////console.log(vv);
			 						var id;
			 						if(ii == 0){
			 							id = vv;
			 							conid1.push($.trim(vv.toString().replace(/_AND_/g, "&").replace(/_COMMA_/g, ",")));
			 						}
			 						if(vv != "" && ii !=0){
				 						
				 						if (!$('#locationInput').tagExist(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "))) {
				 						    $('#locationInput').addTag(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "),{tagId:id});
				 							
				 						  }
			 						}
			 						
			 					})
			 				}
			 			})
		 			}else if(value1[0] == "regionId"){
		 				$.each(value1[1].split(","), function(i, v){
			 				////console.log(v);
			 				if(v != ""){
			 					regid.push(v.toString());
			 					$.each(v.split(":"), function(ii, vv){
			 						////console.log(vv);
			 						var id;
			 						if(ii == 0){
			 							id = vv;
			 							regid1.push($.trim(vv.toString().replace(/_AND_/g, "&").replace(/_COMMA_/g, ",")));
			 						}
			 						if(vv != "" && ii !=0){
				 						
				 						if (!$('#locationInput').tagExist(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "))) {
				 						    $('#locationInput').addTag(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "),{tagId:id});
				 							
				 						  }
			 						}
			 						
			 					})
			 				}
			 			})
		 			}else if(value1[0] == "state"){
		 				$.each(value1[1].split(","), function(i, v){
			 				////console.log(v);
			 				if(v != ""){
			 					statid.push(v.toString());
			 					$.each(v.split(":"), function(ii, vv){
			 						////console.log(vv);
			 						var id;
			 						if(ii == 0){
			 							id = vv;
			 							statid1.push($.trim(vv.toString().replace(/_AND_/g, "&").replace(/_COMMA_/g, ",")));
			 						}
			 						if(vv != "" && ii !=0){
				 						
				 						if (!$('#locationInput').tagExist(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "))) {
				 						    $('#locationInput').addTag(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "),{tagId:id});
				 							
				 						  }
			 						}
			 						
			 					})
			 				}
			 			})
		 			}else if(value1[0] == "district"){
		 				$.each(value1[1].split(","), function(i, v){
			 				////console.log(v);
			 				if(v != ""){
			 					disid.push(v.toString());
			 					$.each(v.split(":"), function(ii, vv){
			 						////console.log(vv);
			 						var id;
			 						if(ii == 0){
			 							id = vv;
			 							disid1.push($.trim(vv.toString().replace(/_AND_/g, "&").replace(/_COMMA_/g, ",")));
			 						}
			 						if(vv != "" && ii !=0){
				 						
				 						if (!$('#locationInput').tagExist(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "))) {
				 						    $('#locationInput').addTag(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "),{tagId:id});
				 							
				 						  }
			 						}
			 						
			 					})
			 				}
			 			})
		 			}else if(value1[0] == "city"){
		 				$.each(value1[1].split(","), function(i, v){
			 				////console.log(v);
			 				if(v != ""){
			 					citid.push(v.toString());
			 					$.each(v.split(":"), function(ii, vv){
			 						////console.log(vv); 
			 						var id;
			 						if(ii == 0){
			 							id = vv;
			 							citid1.push($.trim(vv.toString().replace(/_AND_/g, "&").replace(/_COMMA_/g, ",")));
			 						}
			 						if(vv != "" && ii !=0){
				 						
				 						if (!$('#locationInput').tagExist(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "))) {
				 						    $('#locationInput').addTag(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "),{tagId:id});
				 							
				 						  }
			 						}
			 						
			 					})
			 				}
			 			})
		 			}else if(value1[0] == "location"){
		 				$.each(value1[1].split(","), function(i, v){
			 				////console.log(v);
		 					
			 				if(v != ""){
			 					locid.push(v.toString());
			 					$.each(v.split(":"), function(ii, vv){
			 						////console.log(vv);
			 						var id;
			 						if(ii == 0){
			 							id = vv;
			 							locid1.push($.trim(vv.toString().replace(/_AND_/g, "&").replace(/_COMMA_/g, ",")));
			 						}
			 						if(vv != "" && ii !=0){
				 						
				 						if (!$('#locationInput').tagExist(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "))) {
				 						    $('#locationInput').addTag(vv.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "),{tagId:id});
				 							
				 						  }
			 						}
			 						
			 					})
			 				}
			 			})
		 			}
		 		}
		 	})

		 	$(".Forcast-submit").trigger('click');
		 }

		 $('#productTabs li a').on('click', function(){
			 var tab = $(this).attr('id');
			 if(tab == 'countriesLink'){
				 renderAllCategories();
			 }
			 if(tab == 'subCategoryLink'){
				renderAllSubCategories();
			 } 
			 if(tab == 'productLink'){
			    renderAllProducts();
			 } 
		 });
		 renderAllCountries();
		 renderAllCategories();
		 renderAllSubCategories();
		 renderAllProducts();
		 renderAllRegions();
		 renderAllStates();
		 renderAllDistricts();
		 renderAllCities();
		 //renderAllLocations();
		 $('#location').on('click',function(){
		    	 $('#locationTabs li').removeClass('active');
					$('#locationTabs #countriesLink').parent().addClass('active');
					$('#locationTabsContent .tab-pane').removeClass('active');
					$('#locationTabsContent #countriesTab').addClass('active');
		    
		 });
		 $('#launchProduct').on('click',function(){
	    	 $('#productTabs li').removeClass('active');
				$('#productTabs #categoryLink').parent().addClass('active');
				$('#productTabsContent .tab-pane').removeClass('active');
				$('#productTabsContent #categoriesTab').addClass('active');	    
	 });
		 $('#locationTabs li a').on('click', function(){
			 var tab = $(this).attr('id');
			 if(tab == 'countryTab'){
					renderAllRegions();
				 } 
			 if(tab == 'regionTab'){
				renderAllRegions();
			 } 
			 if(tab == 'stateTab'){
				 renderAllStates();
			 } 
			 if(tab == 'districtTab'){
				 renderAllDistricts();
			 } 
			 if(tab == 'cityTab'){
				 renderAllCities();
			 } 
			 if(tab == 'locationTab'){
				 //renderAllLocations();
			 }
		 }); 
		 
		 $('#productInput').tagsInput( {onRemoveTag: removeProductTag, defaultText:"Select Category / Product"});
		 
		  $('#locationInput').tagsInput( {onRemoveTag: removeLocationTag, defaultText:"Select Location"});
		  
		  //document.getElementById("productInput_tag").setAttribute("data-default", "Select Product");
		 /* $.each(catid1, function(i,v){
			  if (!$('#productInput').tagExist(v.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "))) {
				    $('#productInput').addTag(v.replace(/_AND_/g, "&").replace(/_COMMA_/g, " "));
			  }
			  
		  })*/
		  
		  reset_btn = function()
		  {
			  $('#productInput').importTags('');
			  $('#locationInput').importTags('');

			  $('#countryTable').find(".countryName").each( function(){
				  $(this).next().find('i').removeClass("fa-check-square").addClass("fa-square-o");
			  });
			  $('#regionTable').find(".regionName").each( function(){
				  $(this).next().find('i').removeClass("fa-check-square").addClass("fa-square-o");
			  });
			  $('#stateTable').find(".stateName").each( function(){
				  $(this).next().find('i').removeClass("fa-check-square").addClass("fa-square-o");
			  });
			  $('#districtTable').find(".districtName").each( function(){
				  $(this).next().find('i').removeClass("fa-check-square").addClass("fa-square-o");
			  });
			  $('#cityTable').find(".cityName").each( function(){
				  $(this).next().find('i').removeClass("fa-check-square").addClass("fa-square-o");
			  });
			  $('#locationTable').find(".locationName").each( function(){
				  $(this).next().find('i').removeClass("fa-check-square").addClass("fa-square-o");
			  });
			  $('#categoryTable').find(".categoryName").each( function(){
				  $(this).next().find('i').removeClass("fa-check-square").addClass("fa-square-o");
			  });
			  $('#subcategoryTable').find(".subcategoryName").each( function(){
				  $(this).next().find('i').removeClass("fa-check-square").addClass("fa-square-o");
			  });
			  
			  catid1 = [];
			  subcatid1 = [];
			  proid1 =[];
			  conid1 = [];
			  regid1 = [];
			  statid1 = [];
			  disid1 = [];
			  citid1 = [];
			  locid1 = [];

			  			  
			  catid = [];
			  subcatid = [];
			  proid =[];
			  conid = [];
			  regid = [];
			  statid = [];
			  disid = [];
			  citid = [];
			  locid = [];
			  
			  var selectBox1 = document.getElementById("ondat");
				week_nm = selectBox1.value = cur_week;

				var selectBox2 = document.getElementById("onyar");
				year_nm = selectBox2.value = cur_year;

				var selectBox3 = document.getElementById("ondat1");
				week_nm1 = selectBox3.value = cur_week1;

				 var selectBox4 = document.getElementById("onyar1");
				year_nm1 = selectBox4.value = cur_year1;
				
				
				$(".tile").each(function(i,v){
					if($(this).find('.tile-body').attr('id') != "Select-section"){
						$(this).find('.tile-body').css({display:'none'});
					}
					
				})
				$("#chart1").children().remove();
				$("#chart2").children().remove();
				$("#chart3").children().remove();
				$("#social").children().remove();
				forecast_export = [];
				internal_export = [];
				internal1_export = [];
				external_export = [];
				social_export = [];
				
				$(".ruler-dis span").each(function(){
					if($(this).is(":visible")){
						if($(this).text() == "Hide Ruler"){
							$("#draggableToggle").trigger('click');
						}
					}
				});
				
				 $(".ruler-dis").prop('disabled', true);
				 $(".export-dis").prop('disabled', true);
				
			 resetCall();
			  //console.log(123)
		  }
		 function resetCall(){
			 $.ajax({
				 url: "/DemandSense/predictions/resetSelection",
				 	type: "GET",

				 	success:function(msg){
				 		
				 	}
				})
		  }
		 function renderAllRegions(){
			 $.ajax({
				  method: "GET",
				  url: "/DemandSense/location/allregions"
				})
				  .done(function( data ) {
					var html = '';
					var count = '';
				    for(name in data){
				    	var exists = false;
				    	$.each(regid1, function(i, v){if(v == name){exists = true;}})
				    	
				    	if(name !='count'){
				    		if(exists){
				    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="regionName" style="padding-left: 20px;" data-region-id="'+name+'">'+data[name]+'</td><td class="regionAdd" data-region-id="'+name+'" data-region-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
							}else{
				    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="regionName" style="padding-left: 20px;" data-region-id="'+name+'">'+data[name]+'</td><td class="regionAdd" data-region-id="'+name+'" data-region-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
							}
						}
				    	count = data['count'];
				    }				    
				    handleRegionTabFunc(count, html);
				    handleRegionAdd();
				    $('.regionName').on('click', function(){
						var regionId = $(this).attr('data-region-id');
						$.ajax({
							  method: "GET",
							  url: "/DemandSense/location/states",
							  data: {"regionId":regionId}
							})
							  .done(function( data ) {
								var html = '';
								var count = '';
								for(name in data){
							    	var exists = false;
							    	$.each(statid1, function(i, v){if(v == name){exists = true;}})
							    	
							    	if(name !='count'){
							    		if(exists){
							    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="stateName" style="padding-left: 20px;" data-state-id="'+name+'">'+data[name]+'</td><td class="stateAdd" data-state-id="'+name+'" data-state-name="'+data[name]+'" ><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
										}else{
											html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="stateName" style="padding-left: 20px;" data-state-id="'+name+'">'+data[name]+'</td><td class="stateAdd" data-state-id="'+name+'" data-state-name="'+data[name]+'" ><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
										}
									}
							    	count = data['count'];
							    }
							    $('#stateTable').append(html);
							    handleStateTabFunc(count,html);
							    renderClickStates();
							  });
				});	
				  });
		 }
		 function handleRegionTabFunc(count,html){
			 $('#regionTable').append(html);
				$('#regionCount').html(count);
				$('#regionTable tr').remove();
				 $('#regionTable').append(html);
				$('#regionCount').html('').html(count);
				$('#locationTabs li').removeClass('active');
				$('#locationTabs #regionTab').parent().addClass('active');
				$('#locationTabsContent .tab-pane').removeClass('active');
				$('#locationTabsContent #regionTab').addClass('active');
				$('#region-paginator').smartpaginator({ totalrecords: count,recordsperpage: 10,datacontainer: 'regionTable',dataelement: 'tr',theme: 'green' });
		 }
		 function renderAllStates(){
			 $.ajax({
				  method: "GET",
				  url: "/DemandSense/location/allstates"
				})
				  .done(function( data ) {
					var html = '';
					var count = '';
					for(name in data){
				    	var exists = false;
				    	$.each(statid1, function(i, v){if(v == name){exists = true;}})
				    	
				    	if(name !='count'){
				    		if(exists){
				    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="stateName" style="padding-left: 20px;" data-state-id="'+name+'">'+data[name]+'</td><td class="stateAdd" data-state-id="'+name+'" data-state-name="'+data[name]+'" ><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
							}else{
								html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="stateName" style="padding-left: 20px;" data-state-id="'+name+'">'+data[name]+'</td><td class="stateAdd" data-state-id="'+name+'" data-state-name="'+data[name]+'" ><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
							}
						}
				    	count = data['count'];
				    }
					handleStateTabFunc(count,html);
					renderClickStates();
					handleStateAdd();
				  });
		 }
		 function handleStateTabFunc(count,html){
			 $('#stateTable').append(html);
				$('#stateCount').html(count);
				$('#stateTable tr').remove();
				$('#stateCount').html('').html(count);
				$('#stateTable').append(html);
				$('#locationTabs li').removeClass('active');
				$('#locationTabs #stateTab').parent().addClass('active');
				$('#locationTabsContent .tab-pane').removeClass('active');
				$('#locationTabsContent #stateTab').addClass('active');
				$('#state-paginator').smartpaginator({ totalrecords: count,recordsperpage: 10,datacontainer: 'stateTable',dataelement: 'tr',theme: 'green' });

		 }
		 function handleDistrictTabFunc(count,html){
			 $('#districtCount').html(count);
				$('#districtTable tr').remove();
				$('#districtCount').html('').html(count);
				$('#locationTabs li').removeClass('active');
				$('#districtTable').append(html);
				$('#locationTabs #districtTab').parent().addClass('active');
				$('#locationTabsContent .tab-pane').removeClass('active');
				$('#locationTabsContent #districtTab').addClass('active');
				$('#district-paginator').smartpaginator({ totalrecords: count,recordsperpage: 10,datacontainer: 'districtTable',dataelement: 'tr',theme: 'green' });

		 }
		 function handleLocationTabFunc(count,html){
			
				$('#locationCount').html(count);
				$('#locationTable tr').remove();
				$('#locationCount').html('').html(count);
				 $('#locationTable').append(html);
				$('#locationTabs li').removeClass('active');
				$('#locationTabs #locationTab').parent().addClass('active');
				$('#locationTabsContent .tab-pane').removeClass('active');
				$('#locationTabsContent #locationTab').addClass('active');
				$('#location-paginator').smartpaginator({ totalrecords: count,recordsperpage: 10,datacontainer: 'locationTable',dataelement: 'tr',theme: 'green' });
		 
		 }
		 function renderAllCities(){
			 $.ajax({
				  method: "GET",
				  url: "/DemandSense/location/allcities"
				})
				  .done(function( data ) {
					var html = '';
					var count = '';
					for(name in data){
				    	var exists = false;
				    	$.each(citid1, function(i, v){if(v == name){exists = true;}})
				    	
				    	if(name !='count'){
				    		if(exists){
				    			 html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="cityName" style="padding-left: 20px;" data-city-id="'+name+'">'+data[name]+'</td><td class="cityAdd" data-city-id="'+name+'" data-city-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
							}else{
								 html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="cityName" style="padding-left: 20px;" data-city-id="'+name+'">'+data[name]+'</td><td class="cityAdd" data-city-id="'+name+'" data-city-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
							}
						}
				    	count = data['count'];
				    }
				    handleCityTabFunc(count,html);
				    //renderClickCities();
				    handleCityAdd();
				  });
		 }
		 function handleCityTabFunc(count, html){
			 $('#cityCount').html(count);
				$('#cityTable tr').remove();
				$('#cityCount').html('').html(count);
				$('#cityTable').append(html);
				$('#locationTabs li').removeClass('active');
				$('#locationTabs #cityTab').parent().addClass('active');
				$('#locationTabsContent .tab-pane').removeClass('active');
				$('#locationTabsContent #cityTab').addClass('active');
				$('#city-paginator').smartpaginator({ totalrecords: count,recordsperpage: 10,datacontainer: 'cityTable',dataelement: 'tr',theme: 'green' });

		 }
		 function renderAllDistricts(){
			 $.ajax({
				  method: "GET",
				  url: "/DemandSense/location/alldistricts"
				})
				  .done(function( data ) {
					var html = '';
					var count = '';
					for(name in data){
				    	var exists = false;
				    	$.each(disid1, function(i, v){if(v == name){exists = true;}})
				    	
				    	if(name !='count'){
				    		if(exists){
				    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="districtName" style="padding-left: 20px;" data-district-id="'+name+'">'+data[name]+'</td><td class="districtAdd" data-district-id="'+name+'" data-district-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
							}else{
								html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="districtName" style="padding-left: 20px;" data-district-id="'+name+'">'+data[name]+'</td><td class="districtAdd" data-district-id="'+name+'" data-district-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
							}
						}
				    	count = data['count'];
				    }
					$('#districtTable').append(html);
					handleDistrictTabFunc(count,html);
					renderClickDistricts();
					handleDistrictAdd();
				  });
		 }
		 
		 function renderAllLocations(){
			 $.ajax({
				  method: "GET",
				  url: "/DemandSense/location/alllocationIds"
				})
				  .done(function( data ) {
					var html = '';
					var count = '';
					for(name in data){
				    	var exists = false;
				    	$.each(locid1, function(i, v){if(v == name){exists = true;}})
				    	
				    	if(name !='count'){
				    		if(exists){
				    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="locationName" style="padding-left: 20px;" data-location-id="'+name+'">'+data[name]+'</td><td data-location-id="'+name+'" class="locationAdd" data-location-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
						    }else{
								html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="locationName" style="padding-left: 20px;" data-location-id="'+name+'">'+data[name]+'</td><td data-location-id="'+name+'" class="locationAdd" data-location-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
						    }
						}
				    	count = data['count'];
				    }
				    //handleLocationTabFunc(count,html);
				    handleLocationAdd();
			});
		 }
		 
		 function renderAllCountries(){
			 $.ajax({
				  method: "GET",
				  url: "/DemandSense/location/countryList"
				})
				  .done(function( data ) {
					var html = '';
					var count = '';
					for(name in data){
				    	var exists = false;
				    	$.each(conid1, function(i, v){if(v == name){exists = true;}})
				    	
				    	if(name !='count'){
				    		if(exists){
				    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="countryName" style="padding-left: 20px;" data-country-id="'+name+'">'+data[name]+'</td><td class="countryAdd" data-country-id="'+name+'" data-country-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678;cursor: pointer !important;"></i></td></tr>';
						    }else{
								html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="countryName" style="padding-left: 20px;" data-country-id="'+name+'">'+data[name]+'</td><td class="countryAdd" data-country-id="'+name+'" data-country-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678;cursor: pointer !important;"></i></td></tr>';
						    }
						}
				    	count = data['count'];
				    }
					$('#countryTable').append(html);
					$('#countryCount').html(count);
					 handleCountryAdd();
					 $('#locationTabs li').removeClass('active');
						$('#locationTabs #countriesLink').parent().addClass('active');
						$('#locationTabsContent .tab-pane').removeClass('active');
						$('#locationTabsContent #countriesTab').addClass('active');
					  $('.countryName').on('click', function(){
							var countryId = $(this).attr('data-country-id');
							$.ajax({
								  method: "GET",
								  url: "/DemandSense/location/regions",
								  data: {"countryId":countryId}
								})
								  .done(function( data ) {
									var html = '';
									var count = '';
									for(name in data){
								    	var exists = false;
								    	$.each(regid1, function(i, v){if(v == name){exists = true;}})
								    	
								    	if(name !='count'){
								    		if(exists){
								    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="regionName" style="padding-left: 20px;" data-region-id="'+name+'">'+data[name]+'</td><td class="regionAdd" data-region-id="'+name+'" data-region-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
											}else{
								    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="regionName" style="padding-left: 20px;" data-region-id="'+name+'">'+data[name]+'</td><td class="regionAdd" data-region-id="'+name+'" data-region-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
											}
										}
								    	count = data['count'];
								    }
								    $('#regionTable').append(html);
								    handleRegionTabFunc(count,html);
								    renderClickRegions();
								    handleRegionAdd();
								  });
					});	
					  
				  });
		 }
		 function renderClickRegions(){
			  $('.regionName').on('click', function(){
					var regionId = $(this).attr('data-region-id');
					$.ajax({
						  method: "GET",
						  url: "/DemandSense/location/states",
						  data: {"regionId":regionId}
						})
						  .done(function( data ) {
							var html = '';
							var count = '';
							for(name in data){
						    	var exists = false;
						    	$.each(statid1, function(i, v){if(v == name){exists = true;}})
						    	
						    	if(name !='count'){
						    		if(exists){
						    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="stateName" style="padding-left: 20px;" data-state-id="'+name+'">'+data[name]+'</td><td class="stateAdd" data-state-id="'+name+'" data-state-name="'+data[name]+'" ><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
									}else{
										html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="stateName" style="padding-left: 20px;" data-state-id="'+name+'">'+data[name]+'</td><td class="stateAdd" data-state-id="'+name+'" data-state-name="'+data[name]+'" ><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
									}
								}
						    	count = data['count'];
						    }
						    $('#stateTable').append(html);
						    handleStateTabFunc(count,html);
						    renderClickStates();
						    handleStateAdd();
						  });
			});	
		 }
		 function renderClickDistricts(){
			  $('.districtName').on('click', function(){
					var districtId = $(this).attr('data-district-id');
					$.ajax({
						  method: "GET",
						  url: "/DemandSense/location/cities",
						  data: {"districtId":districtId}
						})
						  .done(function( data ) {
							var html = '';
							var count = '';
							for(name in data){
						    	var exists = false;
						    	$.each(citid1, function(i, v){if(v == name){exists = true;}})
						    	
						    	if(name !='count'){
						    		if(exists){
						    			 html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="cityName" style="padding-left: 20px;" data-city-id="'+name+'">'+data[name]+'</td><td class="cityAdd" data-city-id="'+name+'" data-city-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
									}else{
										 html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="cityName" style="padding-left: 20px;" data-city-id="'+name+'">'+data[name]+'</td><td class="cityAdd" data-city-id="'+name+'" data-city-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
									}
								}
						    	count = data['count'];
						    }
						    $('#cityTable').append(html);
						    handleCityTabFunc(count,html);
						    //renderClickCities();
						    handleCityAdd();
						  });
			});	
		 }
		 function renderClickStates(){
			  $('.stateName').on('click', function(){
					var stateId = $(this).attr('data-state-id');
					$.ajax({
						  method: "GET",
						  url: "/DemandSense/location/districts",
						  data: {"stateId":stateId}
						})
						  .done(function( data ) {
							var html = '';
							var count = '';
							for(name in data){
						    	var exists = false;
						    	$.each(disid1, function(i, v){if(v == name){exists = true;}})
						    	
						    	if(name !='count'){
						    		if(exists){
						    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="districtName" style="padding-left: 20px;" data-district-id="'+name+'">'+data[name]+'</td><td class="districtAdd" data-district-id="'+name+'" data-district-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
									}else{
										html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="districtName" style="padding-left: 20px;" data-district-id="'+name+'">'+data[name]+'</td><td class="districtAdd" data-district-id="'+name+'" data-district-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
									}
								}
						    	count = data['count'];
						    }
						    $('#districtTable').append(html);
						    handleDistrictTabFunc(count,html);
						    renderClickDistricts();
						    handleDistrictAdd();
						  });
			});	
		 }
		 
		 function renderClickCities(){
			 $('.cityName').on('click', function(){
					var cityId = $(this).attr('data-city-id');
					$.ajax({
						  method: "GET",
						  url: "/DemandSense/location/locids",
						  data: {"cityId":cityId}
						})
						  .done(function( data ) {
							var html = '';
							var count = '';
							////console.log(data);
							for(name in data){
						    	var exists = false;
						    	$.each(locid1, function(i, v){if(v == name){exists = true;}})
						    	
						    	if(name !='count'){
						    		if(exists){
						    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="locationName" style="padding-left: 20px;" data-location-id="'+name+'">'+data[name]+'</td><td data-location-id="'+name+'" class="locationAdd" data-location-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
								    }else{
										html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="locationName" style="padding-left: 20px;" data-location-id="'+name+'">'+data[name]+'</td><td data-location-id="'+name+'" class="locationAdd" data-location-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
								    }
								}
						    	count = data['count'];
						    }
						    $('#locationTable').append(html);
						    //handleLocationTabFunc(count,html);
						    handleLocationAdd();
						  });
			});	
		 }
		 function renderAllSubCategories(){
			 $.ajax({
				  method: "GET",
				  url: "/DemandSense/Products/allSubCategorys"
				})
				  .done(function( data ) {

					var html = '';
					var count = '';
					for(name in data){
				    	var exists = false;
				    	$.each(subcatid1, function(i, v){if(v == name){exists = true;}})
				    	
				    	if(name !='count'){
				    		if(exists){
				    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="subCategoryName" style="padding-left: 20px;" data-subCategory-id="'+name+'">'+data[name]+'</td><td class="subCategoryAdd" data-subCategory-id="'+name+'" data-subCategory-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678;  cursor: pointer !important;"></i></td></tr>';
						    }else{
						    	html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="subCategoryName" style="padding-left: 20px;" data-subCategory-id="'+name+'">'+data[name]+'</td><td class="subCategoryAdd" data-subCategory-id="'+name+'" data-subCategory-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678;  cursor: pointer !important;"></i></td></tr>';
						    }
						}
				    	count = data['count'];
				    }
				    $('#subCategoryTable tr').remove();
					$('#subCategoryTable').append(html);
					$('#subCategoryCount').html('').html(count);
					$('#productTabs li').removeClass('active');
					$('#productTabs #subCategoryLink').parent().addClass('active');
					$('#productTabsContent .tab-pane').removeClass('active');
					$('#productTabsContent #subCategoryTab').addClass('active');
					$('#subcategory-paginator').smartpaginator({ totalrecords: count,recordsperpage: 10,datacontainer: 'subCategoryTable',dataelement: 'tr',theme: 'green' });
                    handleSubCategoryAdd();
					renderProductTabFunc();
				  });
		 }
		 
		 function renderAllProducts(){
			 $.ajax({
				  method: "GET",
				  url: "/DemandSense/Products/allProducts"
				})
				  .done(function( data ) {
					  
					var html = '';
					var count = '';
					for(name in data){
				    	var exists = false;
				    	$.each(proid1, function(i, v){if(v == name){exists = true;}})
				    	
				    	if(name !='count'){
				    		if(exists){
				    			 html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="productName" style="padding-left: 20px;" data-product-id="'+name+'">'+data[name]+'</td><td class="productAdd" data-product-id="'+name+'" data-product-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
						    }else{
						    	 html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="productName" style="padding-left: 20px;" data-product-id="'+name+'">'+data[name]+'</td><td class="productAdd" data-product-id="'+name+'" data-product-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
						    }
						}
				    	count = data['count'];
				    }
				    $('#productTable tr').remove();
					$('#productTable').append(html);
					$('#productCount').html('').html(count);
					$('#productTabs li').removeClass('active');
					$('#productTabs #productLink').parent().addClass('active');
					$('#productTabsContent .tab-pane').removeClass('active');
					$('#productTabsContent #productTab').addClass('active');
					$('#product-paginator').smartpaginator({ totalrecords: count,recordsperpage: 10,datacontainer: 'productTable',dataelement: 'tr',theme: 'green' });
                    handleProductAdd();
				  });
		 }
		 
		 function renderAllCategories(){
			  $.ajax({
				  method: "GET",
				  url: "/DemandSense/Products/categorys"
				})
				  .done(function( data ) {
					var html = '';
					var count = '';
					for(name in data){
				    	var exists = false;
				    	$.each(catid1, function(i, v){if(v == name){exists = true;}})
				    	if(name !='count'){
				    		if(exists){
				    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="categoryName" style="padding-left: 20px;" data-category-id="'+name+'">'+data[name]+'</td><td class="category-add" data-category-id="'+name+'" data-category-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
						    }else{
						    	html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="categoryName" style="padding-left: 20px;" data-category-id="'+name+'">'+data[name]+'</td><td class="category-add" data-category-id="'+name+'" data-category-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
						    }
						}
				    	count = data['count'];
				    }
					$('#categoryTable').append(html);
					$('#categoryCount').html(count);
					handleCategoryAdd();
					$('#productTabs li').removeClass('active');
					$('#productTabs #categoryLink').parent().addClass('active');
					$('#productTabsContent .tab-pane').removeClass('active');
					$('#productTabsContent #catagoriesTab').addClass('active');
					$('#category-paginator').smartpaginator({ totalrecords: count,recordsperpage: 10,datacontainer: 'categoryTable',dataelement: 'tr',theme: 'green' });
					
					$('.categoryName').on('click', function(){
							var categoryId = $(this).attr('data-category-id');
							$.ajax({
								  method: "GET",
								  url: "/DemandSense/Products/subCategorysByCategory",
								  data: {"categoryId":categoryId}
								})
								  .done(function( data ) {
									var html = '';
									var count = '';
									for(name in data){
								    	var exists = false;
								    	$.each(subcatid1, function(i, v){if(v == name){exists = true;}})
								    	
								    	if(name !='count'){
								    		if(exists){
								    			html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="subCategoryName" style="padding-left: 20px;" data-subCategory-id="'+name+'">'+data[name]+'</td><td class="subCategoryAdd" data-subCategory-id="'+name+'" data-subCategory-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678;  cursor: pointer !important;"></i></td></tr>';
										    }else{
										    	html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="subCategoryName" style="padding-left: 20px;" data-subCategory-id="'+name+'">'+data[name]+'</td><td class="subCategoryAdd" data-subCategory-id="'+name+'" data-subCategory-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678;  cursor: pointer !important;"></i></td></tr>';
										    }
										}
								    	count = data['count'];
								    }
								    $('#subCategoryTable tr').remove();
									$('#subCategoryTable').append(html);
									$('#subCategoryCount').html('').html(count);
									$('#productTabs li').removeClass('active');
									$('#productTabs #subCategoryLink').parent().addClass('active');
									$('#productTabsContent .tab-pane').removeClass('active');
									$('#productTabsContent #subCategoryTab').addClass('active');
									$('#subcategory-paginator').smartpaginator({ totalrecords: count,recordsperpage: 10,datacontainer: 'subCategoryTable',dataelement: 'tr',theme: 'green' });
	                                handleSubCategoryAdd();
									renderProductTabFunc();
								  });
					});	
					  
				  });
		 }
		 
		 function renderProductTabFunc(){
			  $('.subCategoryName').on('click', function(){
					var subCategoryId = $(this).attr('data-subCategory-id');
					$.ajax({
						  method: "GET",
						  url: "/DemandSense/Products/productsBySubCategory",
						  data: {"subCategoryId":subCategoryId}
						})
						  .done(function( data ) {
							var html = '';
							var count = '';
							for(name in data){
						    	var exists = false;
						    	$.each(proid1, function(i, v){if(v == name){exists = true;}})
						    	
						    	if(name !='count'){
						    		if(exists){
						    			 html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="productName" style="padding-left: 20px;" data-product-id="'+name+'">'+data[name]+'</td><td class="productAdd" data-product-id="'+name+'" data-product-name="'+data[name]+'"><i class="fa fa-check-square" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
								    }else{
								    	 html += '<tr><td style="padding-right: 20px; width: 25%;" >'+name+'</td><td class="productName" style="padding-left: 20px;" data-product-id="'+name+'">'+data[name]+'</td><td class="productAdd" data-product-id="'+name+'" data-product-name="'+data[name]+'"><i class="fa fa-square-o" style=" color: #AFD678; cursor: pointer !important;"></i></td></tr>';
								    }
								}
						    	count = data['count'];
						    }
						    $('#productTable tr').remove();
							$('#productTable').append(html);
							$('#productCount').html('').html(count);
							$('#productTabs li').removeClass('active');
							$('#productTabs #productLink').parent().addClass('active');
							$('#productTabsContent .tab-pane').removeClass('active');
							$('#productTabsContent #productTab').addClass('active');
							$('#product-paginator').smartpaginator({ totalrecords: count,recordsperpage: 10,datacontainer: 'productTable',dataelement: 'tr',theme: 'green' });
							handleProductAdd();
						  });
				  });
		  }
		  
		
		/* var catid1 = [];
		 catid=[];*/
		 cat_obj={}
		  function handleCategoryAdd(){
			  $('.category-add').on('click', function(){
				  categoryId = $.trim($(this).attr('data-category-id'));
				  var categoryName = ($(this).attr('data-category-name')).replace(/,/g, "_COMMA_");
				  categoryName = categoryName.replace(/&/g, "_AND_");
				  if($(this).children().hasClass('fa-square-o')){
					  catid1.push(categoryId);
					  $(this).find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square');
					   
					  if (!$('#productInput').tagExist($(this).attr('data-category-name').replace(/,/g, " "))) {
					    $('#productInput').addTag($(this).attr('data-category-name').replace(/,/g, " "),{tagId:categoryId});
						
					  }
					   catid.push(categoryId+":"+categoryName)	
					   cat_obj["categoryId"]=catid
					   ////console.log(cat_obj)
					  
				  }else if($(this).children().hasClass('fa-check-square')){
					  
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  for(var y=0;y<catid.length;y++){					  
						  if($.trim(catid[y].replace(/&/g, "_AND_").replace(/,/g, "_COMMA_")).localeCompare(categoryId+":"+categoryName) == 0){
							  catid.splice(y,1);	
							  catid1.splice(y,1);
							  //console.log(catid+"=============="+catid1);
							  
						  }
					  }
					  var categoryName1 = $(this).attr('data-category-name').replace(/,/g, " ");
					  $("#productInput_tagsinput .tag").each(function(){
						  if($.trim($(this).find("span").text()) == $.trim(categoryName1)){
							  $(this).find("a").trigger('click');
						  }
					  });
				  }
				  $('#launchProductModal').modal('hide');
				  $('.modal-backdrop').remove();
				  
			  });
		  }
		  
		 /* var subcatid1=[];
		   subcatid=[];*/
		      subcat_obj={}
		  function handleSubCategoryAdd(){
			  $('.subCategoryAdd').on('click', function(){
				   subCategoryId = $.trim($(this).attr('data-subcategory-id'));
				  var subCategoryName = ($(this).attr('data-subcategory-name')).replace(/,/g, "_COMMA_");
				  subCategoryName = subCategoryName.replace(/&/g, "_AND_");
				  
				  if($(this).children().hasClass('fa-square-o')){
					   subcatid1.push(subCategoryId);
					  $(this).find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square');
					  if (!$('#productInput').tagExist($(this).attr('data-subcategory-name').replace(/,/g, " "))) {
					    $('#productInput').addTag($(this).attr('data-subcategory-name').replace(/,/g, " "));
					  }
					   subcatid.push(subCategoryId+":"+subCategoryName)	
					   subcat_obj["subCategoryId"]=subcatid
					   ////console.log(subcat_obj)
				  }else if($(this).children().hasClass('fa-check-square')){
					  var categoryName1 = $(this).attr('data-subcategory-name').replace(/,/g, " ");
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  for(var y=0;y<subcatid.length;y++){					  
						  if($.trim(subcatid[y].replace(/&/g, "_AND_").replace(/,/g, "_COMMA_")).localeCompare(subCategoryId+":"+subCategoryName) == 0){
							  subcatid.splice(y,1);	
							  subcatid1.splice(y,1);	
						  }
					  }
					  $("#productInput_tagsinput .tag").each(function(){
						  if($.trim($(this).find("span").text()) == $.trim(categoryName1)){
							  $(this).find("a").trigger('click');
						  }
					  });
				  }
				  $('#launchProductModal').modal('hide');
				  $('.modal-backdrop').remove();
			  });
		  }
		  
		      
		  /* var proid1=[];
		   proid=[];*/
		   proid_obj={}
		  function handleProductAdd(){
			  $('.productAdd').on('click', function(){
				   productId = $(this).attr('data-product-id');
				   var productName = $(this).attr('data-product-name').replace(/,/g, "_COMMA_");
				   productName = productName.replace(/&/g, "_AND_");
				  
				  if($(this).children().hasClass('fa-square-o')){
					  proid1.push(productId);
					  $(this).find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square');
					  if (!$('#productInput').tagExist($(this).attr('data-product-name').replace(/,/g, " "))) {
					    $('#productInput').addTag($(this).attr('data-product-name').replace(/,/g, " "));
					  }
					   proid.push(productId+":"+productName)	
					   proid_obj["productId"]=proid
					   ////console.log(proid_obj)
				  }else if($(this).children().hasClass('fa-check-square')){
					  
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  for(var y=0;y<proid.length;y++){					  
						  if($.trim(proid[y].replace(/&/g, "_AND_").replace(/,/g, "_COMMA_")).localeCompare(productId+":"+productName) == 0){
							  proid.splice(y,1);		
							  proid1.splice(y,1);	
						  }
					  }
					  var categoryName1 = $(this).attr('data-product-name').replace(/,/g, " ");
					  //removeProductTag(categoryId1);
					  $("#productInput_tagsinput .tag").each(function(){
						  if($.trim($(this).find("span").text()) == $.trim(categoryName1)){
							  $(this).find("a").trigger('click');
						  }
					  });
				  }
				  $('#launchProductModal').modal('hide');
				  $('.modal-backdrop').remove();
			  });
		  }
		 
		  /* var conid1=[];
		   conid=[];*/
		   conid_obj={}
		  function handleCountryAdd(){
			   
			$('.countryAdd').on('click', function(){

				   countryId = $(this).attr('data-country-id');
				   
				  var countryName = $(this).attr('data-country-name').replace(/,/g, "_COMMA_");
				  countryName = countryName.replace(/&/g, "_AND_");
				  
				  
				 if($(this).children().hasClass('fa-square-o')){
					 conid1.push(countryId);
					  $(this).find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square');
					  $('#locationInput').tagsInput();
					  if (!$('#locationInput').tagExist($(this).attr('data-country-name'))) {
					    $('#locationInput').addTag($(this).attr('data-country-name'));
					  }
					 
					   conid.push(countryId+":"+countryName);
						conid_obj["countryId"]=conid;
					   ////console.log(conid_obj)
				  }else if($(this).children().hasClass('fa-check-square')){
					  
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  for(var y=0;y<conid.length;y++){					  
						  if($.trim(conid[y]).localeCompare(countryId+":"+countryName) == 0){
							  conid.splice(y,1);
							  conid1.splice(y,1);
						  }
					  }
					  var categoryName1 = $(this).attr('data-country-name');
					  $("#locationInput_tagsinput .tag").each(function(){
						  if($.trim($(this).find("span").text()) == $.trim(categoryName1)){
							  $(this).find("a").trigger('click');
						  }
					  });
				  }
				  $('#launchLocationModal').modal('hide');
				  $('.modal-backdrop').remove();
			  });
		  }
		   
		  /* var regid1=[];
		   regid=[];*/
		   regid_obj={}
		  function handleRegionAdd(){
			  $('.regionAdd').on('click', function(){

				   regionId = $(this).attr('data-region-id');
				   
				  var regionName = $(this).attr('data-region-name').replace(/,/g, "_COMMA_");
				  regionName = regionName.replace(/&/g, "_AND_");
				  
				  
				  if($(this).children().hasClass('fa-square-o')){
					  regid1.push(regionId);
					  $(this).find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square');
					  $('#locationInput').tagsInput();
					  if (!$('#locationInput').tagExist($(this).attr('data-region-name'))) {
					    $('#locationInput').addTag($(this).attr('data-region-name'));
					  }
					   regid.push(regionId+":"+regionName)	
					   regid_obj["regionId"]=regid
					   ////console.log(regid)
				  }else if($(this).children().hasClass('fa-check-square')){
					  
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  for(var y=0;y<regid.length;y++){					  
						  if($.trim(regid[y]).localeCompare(regionId+":"+regionName) == 0){
							  regid.splice(y,1);
							  regid1.splice(y,1);
						  }
					  }
					  
					  var categoryName1 = $(this).attr('data-region-name');
					  $("#locationInput_tagsinput .tag").each(function(){
						  if($.trim($(this).find("span").text()) == $.trim(categoryName1)){
							  $(this).find("a").trigger('click');
						  }
					  });
					  ////console.log(regid1+"--"+regid+"--"+regionId+":"+regionName)
				  }
				  $('#launchLocationModal').modal('hide');
				  $('.modal-backdrop').remove();
			  });
		  }
		 /* var statid1=[];
		  statid=[];*/
			statid_obj={}
		  function handleStateAdd(){
			  $('.stateAdd').on('click', function(){

				   stateId = $(this).attr('data-state-id');
				  var stateName = $(this).attr('data-state-name').replace(/,/g, "_COMMA_");
				  stateName = stateName.replace(/&/g, "_AND_");
				  
				  
				  if($(this).children().hasClass('fa-square-o')){
					   statid1.push(stateId);
					  $(this).find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square');
					  $('#locationInput').tagsInput();
					  if (!$('#locationInput').tagExist($(this).attr('data-state-name'))) {
					    $('#locationInput').addTag($(this).attr('data-state-name'));
					  }
					    statid.push(stateId+":"+stateName)
						statid_obj["stateId"]=statid
					   ////console.log(statid_obj)
				  }else if($(this).children().hasClass('fa-check-square')){
					  
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  for(var y=0;y<statid.length;y++){					  
						  if($.trim(statid[y]).localeCompare(stateId+":"+stateName) == 0){
							  statid.splice(y,1);	
							  statid1.splice(y,1);
						  }
					  }
					  var categoryName1 = $(this).attr('data-state-name');
					  //removeProductTag(categoryId1);
					  $("#locationInput_tagsinput .tag").each(function(){
						  if($.trim($(this).find("span").text()) == $.trim(categoryName1)){
							  $(this).find("a").trigger('click');
						  }
					  });
				  }
				   
				  $('#launchLocationModal').modal('hide');
				  $('.modal-backdrop').remove();
			  });
		  }
			
		  /* var disid1=[];
		   disid=[];*/
		   disid_obj={}
		  function handleDistrictAdd(){
			  $('.districtAdd').on('click', function(){
				   districtId = $(this).attr('data-district-id');
				   var districtName = $(this).attr('data-district-name').replace(/,/g, "_COMMA_");
				   districtName = districtName.replace(/&/g, "_AND_");
					  
					  
				  if($(this).children().hasClass('fa-square-o')){
					   disid1.push(districtId);
					  $(this).find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square');
					  $('#locationInput').tagsInput();
					  if (!$('#locationInput').tagExist($(this).attr('data-district-name'))) {
					    $('#locationInput').addTag($(this).attr('data-district-name'));
					  }
					  disid.push(districtId+":"+districtName)	
					  disid_obj["districtId"]=disid
					   ////console.log(disid_obj)
				  }else if($(this).children().hasClass('fa-check-square')){
					  
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  for(var y=0;y<disid.length;y++){					  
						  if($.trim(disid[y]).localeCompare(districtId+":"+districtName) == 0){
							  disid.splice(y,1);	
							  disid1.splice(y,1);	
						  }
					  }
					  var categoryName1 = $(this).attr('data-district-name');
					  //removeProductTag(categoryId1);
					  $("#locationInput_tagsinput .tag").each(function(){
						  if($.trim($(this).find("span").text()) == $.trim(categoryName1)){
							  $(this).find("a").trigger('click');
						  }
					  });
				  }
				  $('#launchLocationModal').modal('hide');
				  $('.modal-backdrop').remove();
			  });
		  }
		   /*var citid1=[];
		   citid=[];*/
		   citid_obj={}
		  function handleCityAdd(){
			  $('.cityAdd').on('click', function(){
				   cityId = $(this).attr('data-city-id');
				   var cityName = $(this).attr('data-city-name').replace(/,/g, "_COMMA_");
				   cityName = cityName.replace(/&/g, "_AND_");
					  
				  if($(this).children().hasClass('fa-square-o')){
					   citid1.push(cityId);
					  $(this).find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square');
					  $('#locationInput').tagsInput();
					  if (!$('#locationInput').tagExist($(this).attr('data-city-name'))) {
					    $('#locationInput').addTag($(this).attr('data-city-name'));
					  }
					  citid.push(cityId+":"+cityName)	
					  citid_obj["cityId"]=citid
					   ////console.log(citid_obj)
				  }else if($(this).children().hasClass('fa-check-square')){
					  
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  for(var y=0;y<citid.length;y++){					  
						  if($.trim(citid[y]).localeCompare(cityId+":"+cityName) == 0){
							  citid.splice(y,1);
							  citid1.splice(y,1);
						  }
					  }
					  var categoryName1 = $(this).attr('data-city-name');
					  //removeProductTag(categoryId1);
					  $("#locationInput_tagsinput .tag").each(function(){
						  if($.trim($(this).find("span").text()) == $.trim(categoryName1)){
							  $(this).find("a").trigger('click');
						  }
					  });
				  }
				  $('#launchLocationModal').modal('hide');
				  $('.modal-backdrop').remove();
			  });
		  }
		   /*var locid1=[];
		   locid=[];*/
		   locid_obj={}
		  function handleLocationAdd(){
			  $('.locationAdd').on('click', function(){

				  locationId = $(this).attr('data-location-id');
				  var locationName = $(this).attr('data-location-name').replace(/,/g, "_COMMA_");
				  locationName = locationName.replace(/&/g, "_AND_");
					  
				  
				  
				  if($(this).children().hasClass('fa-square-o')){
					  locid1.push(locationId);
					  $(this).find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square');
					  $('#locationInput').tagsInput();
					  if (!$('#locationInput').tagExist($(this).attr('data-location-name'))) {
					    $('#locationInput').addTag($(this).attr('data-location-name'));
					  }
					  locid.push(locationId+":"+locationName)	
					  locid_obj["locationId"]=locid
					   ////console.log(locid_obj)
				  }else if($(this).children().hasClass('fa-check-square')){
					  
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  for(var y=0;y<locid.length;y++){					  
						  if($.trim(locid[y]).localeCompare(locationId+":"+locationName) == 0){
							  locid.splice(y,1);
							  locid1.splice(y,1);
						  }
					  }
					  var categoryName1 = $(this).attr('data-location-name');
					  //removeProductTag(categoryId1);
					  $("#locationInput_tagsinput .tag").each(function(){
						  if($.trim($(this).find("span").text()) == $.trim(categoryName1)){
							  $(this).find("a").trigger('click');
						  }
					  });
				  }
				  $('#launchLocationModal').modal('hide');
				  $('.modal-backdrop').remove();
			  });
		  }
		  
		  function removeLocationTag(tag){
			  //console.log(tag);
			  $('.countryAdd').each(function(){
			  
				  if($.trim($(this).attr('data-country-name')) == tag){
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa fa-square-o');
					  	for(var y=0;y<conid.length;y++){
						  
						  org_str = conid[y].split(':')
						  
						  if($.trim(org_str[1]).localeCompare(tag) == 0){
							  conid.splice(y,1);
							  conid1.splice(y,1);
							  //console.log(conid)
							  
						  }
					  }
				  }
			  });
			  $('.regionAdd').each(function(){
				  if($.trim($(this).attr('data-region-name')) == tag){
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa fa-square-o');					  
					  for(var y=0;y<regid.length;y++){						  
						  org_str = regid[y].split(':')						  
						  if($.trim(org_str[1]).localeCompare(tag) == 0){
							  regid.splice(y,1);
							  regid1.splice(y,1);
							  //console.log(regid)							  
						  }
					  }
					  
				  }
			  });
			  $('.stateAdd').each(function(){
				  if($.trim($(this).attr('data-state-name')) == tag){
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa fa-square-o');
					  for(var y=0;y<statid.length;y++){						  
						  org_str = statid[y].split(':')						  
						  if($.trim(org_str[1]).localeCompare(tag) == 0){
							  statid.splice(y,1);
							  statid1.splice(y,1);
							  //console.log(statid)							  
						  }
					  }
				  }
			  });
			  $('.districtAdd').each(function(){
				  if($.trim($(this).attr('data-district-name')) == tag){
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa fa-square-o');
					  for(var y=0;y<disid.length;y++){						  
						  org_str = disid[y].split(':')						  
						  if($.trim(org_str[1]).localeCompare(tag) == 0){
							  disid.splice(y,1);
							  disid1.splice(y,1);
							  //console.log(disid)							  
						  }
					  }
				  }
			  });
			  $('.cityAdd').each(function(){
				  if($.trim($(this).attr('data-city-name')) == tag){
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa fa-square-o');
					  for(var y=0;y<citid.length;y++){						  
						  org_str = citid[y].split(':')						  
						  if($.trim(org_str[1]).localeCompare(tag) == 0){
							  citid.splice(y,1);
							  citid1.splice(y,1);
							  //console.log(citid)							  
						  }
					  }
				  }
			  });
			  $('.locationAdd').each(function(){
				  if($.trim($(this).attr('data-location-name')) == tag){
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  for(var y=0;y<locid.length;y++){						  
						  org_str = locid[y].split(':')						  
						  if($.trim(org_str[1]).localeCompare(tag) == 0){
							  locid.splice(y,1);
							  locid1.splice(y,1);
							  //console.log(locid)							  
						  }
					  }
				  }
			  });
		  }
		  function removeProductTag(tag){
			  
			  $('.category-add').each(function(){
				  
				  if($.trim($(this).attr('data-category-name').replace(/,/g, " ")) == $.trim(tag)){
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  for(var y=0;y<catid.length;y++){						  
						  
						  org_str = catid[y].replace(/_COMMA_/g, " ").replace(/_AND_/g, "&").split(':')		
						  if($.trim(org_str[1]).localeCompare(tag) == 0){
							  catid.splice(y,1);
							  catid1.splice(y,1);
							  //console.log(catid[y])
						  }
					  }
				  }
			  });
			  $('.subCategoryAdd').each(function(){
				  if($.trim($(this).attr('data-subcategory-name').replace(/,/g, " ")) == $.trim(tag)){
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  
					  for(var y=0;y<subcatid.length;y++){
						  
						  org_str = subcatid[y].split(':')
						  
						  if($.trim(org_str[1].replace(/_COMMA_/g, " ").replace(/_AND_/g, "&")).localeCompare(tag) == 0){
							  subcatid.splice(y,1);
							  subcatid1.splice(y,1);
							  
						  }
					  }
				  }
			  });
			  $('.productAdd').each(function(){
				  if($.trim($(this).attr('data-product-name').replace(/,/g, " ")) == $.trim(tag)){
					  $(this).find('.fa-check-square').removeClass('fa-check-square').addClass('fa-square-o');
					  
					  for(var y=0;y<proid.length;y++){
						  
						  org_str = proid[y].replace(/_COMMA_/g, " ").replace(/_AND_/g, "&").split(':')
						  
						  if($.trim(org_str[1]).localeCompare(tag) == 0){
							  proid.splice(y,1)
							  proid1.splice(y,1)
							  //console.log(proid)
							  
						  }
					  }
				  }
			  });
		  }
		 
	   });  
	  /* $(".externalFactorVal li").click(function(e){
		   
	   alert();
	   });*/
	 
	 $("#price").click(function(e){

			$(this).find('i').toggleClass('fa  fa-square fa fa-square-o');

		});

		$("#promo").click(function(e){

			$(this).find('i').toggleClass('fa  fa-square fa fa-square-o');
		});

		$("#ext").click(function(e){

			$(this).find('i').toggleClass('fa  fa-square fa fa-square-o');
		});

		$("#int").click(function(e){

			$(this).find('i').toggleClass('fa  fa-square fa fa-square-o');
		});

		$("#prec").click(function(e){

			$(this).find('i').toggleClass('fa  fa-square fa fa-square-o');
		});
		
		$("#srs").click(function(e){

			$(this).find('i').toggleClass('fa  fa-square fa fa-square-o');
		});
		$("#wind").click(function(e){

			$(this).find('i').toggleClass('fa  fa-square fa fa-square-o');
		});
		$("#humidity").click(function(e){

			$(this).find('i').toggleClass('fa  fa-square fa fa-square-o');
		});

		$("#discount").click(function(e){

			$(this).find('i').toggleClass('fa  fa-square fa fa-square-o');
		});

	   
	   
	 
	 
	 external_factor = 'maxTemp';
	 social_factor ='sentimental';
	internal_factor = 'price';		


		SortOut = function(vals){

				$.each(a.chartConfig["series"], function (k) {
					if (a.chartConfig["series"][k].k == vals) {
						a.chartConfig["series"].splice(k, 1);
						rootScope.duplicate = true;
						//return false;
					}
				});

				$.each(a.columnchart["series"], function (k) {
					if (a.columnchart["series"][k].k == vals) {
						a.columnchart["series"].splice(k, 1);
						rootScope.duplicate = true;
						//return false;
					}
				});
			}





 remove_dupli = function(nam){
	 
	 //console.log(nam)
	 var uniqueNames = [];
	 $.each(nam, function(i, el){
	     if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
	 });
	 //console.log(uniqueNames)
	 return uniqueNames;
 }


 radiobtn = document.getElementById("nag");
	radiobtn.checked = true;
	agr_value = 'NA';


remove_dupli = function(nam){

var uniqueNames = [];
$.each(nam, function(i, el){
  if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
});
//console.log(uniqueNames)
return uniqueNames;
}


/*function JSON2CSV(objArray) {
	
	//console.log(objArray)
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;

    var str = '';
    var line = '';

   
    var head = array[0];
   
    for (var index in array[0]) {
        var value = index + "";
        line += '"' + value.replace(/"/g, '""') + '",';
    }
    
    line = line.slice(0, -1);
    str += line + '\r\n';
    

    for (var i = 0; i < array.length; i++) {
        var line = '';

        if ($("#quote").is(':checked')) {
            for (var index in array[i]) {
                var value = array[i][index] + "";
                line += '"' + value.replace(/"/g, '""') + '",';
            }
        } else {
            for (var index in array[i]) {
                line += array[i][index] + ',';
            }
        }

        line = line.slice(0, -1);
        str += line + '\r\n';
    }
    return str;
    
}*/

function JSON2CSV(JSONData, ReportTitle, ShowLabel) {
	
	
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    //Set Report title in first row or line
    
    //CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0][0]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        
        for (var j = 0; j < arrData[i].length; j++) {
        	var row = "";
	        //2nd loop will extract each column and convert it in string comma-seprated
	        for (var index in arrData[i][j]) {
	            row += '"' + arrData[i][j][index] + '",';
	        }
	        row.slice(0, row.length - 1);
	        
	        //add a line break after each row
	        CSV += row + '\r\n';
        }
        

    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    var fileName = "MyReport_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    
    
    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);

}

get_data = function(nmb){

	var myDate = new Date(nmb)
	var uc_date = Date.UTC(myDate.getUTCFullYear(), myDate.getUTCMonth(), myDate.getUTCDate());
	
	return uc_date;
}

	forecast_export = [];
	internal_export = [];
	internal1_export = [];
	external_export = [];
	social_export = [];
	
	all_list = {"forecast_export":forecast_export,"internal_export":internal_export,"external_export":external_export,"social_export":social_export};

		export_data = function(){
		
		//console.log(forecast_export)
		
		//var json = JSON.stringify(forecast_export[0])

			for(var k in all_list) {
				var csv = JSON2CSV(all_list[k], k, true);
			};
		
		
		//console.log(csv)
		
		//window.open("data:text/csv;charset=utf-8," + escape(csv))
//		for(var i=0;i<all_list.length;i++){
//			
//			//var f_n = 
//		}
			
			
		}
		
	cats = {};
	subcats = {};
	region = {};
	dis = {};
	loc = {};
	citys = {};
	country = {};
	product = {};
	states = {};
	
	appends = false;
	
	updategr = function(val){
		//console.log("------------------------------------------------------");
		//console.log(val.id);
		var parnt_cls = $("#"+val.id).attr('chartt');
		
		
		if(parnt_cls == "externalFact"){
			//console.log(val.id);
			//console.log(dynamic_check_external);
			if($("#"+val.id).find('i').hasClass('fa-square-o')){
				
				dynamic_check_external.push(val.id);
				$("#"+val.id).find('i').removeClass('fa-square-o');
				$("#"+val.id).find('i').addClass('fa-check-square');
				////console.log(dynamic_check_external);
				external_factor = val.id;
				appends = true;
				submit_external();
			}else{
				 $.each(dynamic_check_external, function(index , value){
			          if(value == val.id){
			        	  dynamic_check_external.splice(dynamic_check_external.indexOf(value),1);
			          }
			        });
				 external_factor = val.id;
				 appends = false;
				 submit_external();
				 //console.log(dynamic_check_external);
				$("#"+val.id).find('i').removeClass('fa-check-square');
				$("#"+val.id).find('i').addClass('fa-square-o');
			}
			
			////console.log("----------------------external--------------------------------");
			
		}else if(parnt_cls == "internalFact"){
			//console.log(val.id);
			//console.log(dynamic_check_internal);
			if($("#"+val.id).find('i').hasClass('fa-square-o')){
				
				dynamic_check_internal.push(val.id);
				$("#"+val.id).find('i').removeClass('fa-square-o');
				$("#"+val.id).find('i').addClass('fa-check-square');
				////console.log(dynamic_check_external);
				internal_factor = val.id;
				appends = true;
				submit_internal1();
			}else{
				 $.each(dynamic_check_internal, function(index , value){
			          if(value == val.id){
			        	  dynamic_check_internal.splice(dynamic_check_internal.indexOf(value),1);
			          }
			        });
				 internal_factor = val.id;
				 appends = false;
				 submit_internal1();
				 ////console.log(dynamic_check_external);
				$("#"+val.id).find('i').removeClass('fa-check-square');
				$("#"+val.id).find('i').addClass('fa-square-o');
			}
			
			////console.log("----------------------internal--------------------------------");
			
		}else if(parnt_cls == "socialFact"){
			if($("#"+val.id).find('i').hasClass('fa-square-o')){
				
				dynamic_check_social.push(val.id);
				$("#"+val.id).find('i').removeClass('fa-square-o');
				$("#"+val.id).find('i').addClass('fa-check-square');
				////console.log(dynamic_check_external);
				social_factor = val.id;
				appends = true;
				submit_social();
			}else{
				 $.each(dynamic_check_social, function(index , value){
			          if(value == val.id){
			        	  dynamic_check_social.splice(dynamic_check_social.indexOf(value),1);
			          }
			        });
				 social_factor = val.id;
				 appends = false;
				 submit_social();
				 ////console.log(dynamic_check_external);
				$("#"+val.id).find('i').removeClass('fa-check-square');
				$("#"+val.id).find('i').addClass('fa-square-o');
			}
			
			
			////console.log("----------------------social--------------------------------");
		}

		

		
	}

	$("#f_forecast").hide();
	$("#f_actual").hide();
	$('#f_ft').hide();
	
	var ht = 0;
/////////////////////// Submit //////////////////////////
$("#submit").click(function(){
	
	ht = 0;
	
	dynamic_check_external = [];
	dynamic_check_internal = [];
	dynamic_check_social = [];
	var pro = false; loc = false; dat = false;
	    
	var w1 = parseInt($("#ondat").val());
	var y1 = parseInt($("#onyar").val());
	
	var w2 = parseInt($("#ondat1").val());
	var y2 = parseInt($("#onyar1").val());
	
	
	if(y2 > y1){
		dat = true;
	}else if(y2 < y1){
		dat = false;
	}else if(y2 = y1){
		if(w2 > w1){
			dat = true;
		}else{
			dat = false;
		}
	} 
	
	if(catid.length >0){
		pro = true;
	}else if(subcatid.length >0){
		pro = true;
	}else if(proid.length >0){
		pro = true;
	}
		
	if(conid.length >0){
		loc = true;
	}else if(regid.length >0){
		loc = true;
	}else if(statid.length >0){
		loc = true;
	}else if(disid.length >0){
		loc = true;
	}else if(citid.length >0){
		loc = true;
	}else if(locid.length >0){
		loc = true;
	}
	
	
	  if(dat == false){
		  $(".week-error").remove();
		  $(".product-error").remove();
		  $(".location-error").remove();
		  if($(".week-error").text() == ""){
			  $('<span class="week-error" style="color:red; margin-left: 8.5%;">To Week should be greater than From Week</span>').insertBefore(".input-week");
		  }
		  
		  
	  }else if(pro == false){
		  $(".week-error").remove();
		  $(".product-error").remove();
		  $(".location-error").remove();
		  if($(".product-error").text() == ""){
			  $('<span class="product-error" style="color:red; margin-left: 1.5%;">Please select Product</span>').insertBefore(".productInput");
		  }
		  
		  
	  }else if(loc == false){
		  $(".week-error").remove();
		  $(".product-error").remove();
		  $(".location-error").remove();
		  if($(".location-error").text() == ""){
			  $('<span class="location-error" style="color:red; margin-left: 1.5%;">Please select Location</span>').insertBefore(".locationInput");
		  }
	  }else{
		  $(".week-error").remove();
		  $(".product-error").remove();
		  $(".location-error").remove();
		$(".tile").each(function(i,v){
			//$(this).find('.tile-body').insertBefore('<div class="loading"></div>');
			$(this).find('.waitMe_body').css({display:'block'});
			//$(this).find('.tile-body').css({display:'block'});
			$('.waitMe_body').waitMe({ 
				effect : 'orbit', 
				text : 'Data Loading...', 
				bg : 'rgba(255,255,255,0.7)', 
				color : '#7BAE15'
			});
			ht += $(this).height();
		})
	
	/*$.ajax({
	 	url: "/DemandSense/predictions/inputParams",
	 	type: "GET",

	 	success:function(msg){
	 		//console.log(msg)
	 		
	 		cats = $.map(msg.categorys, function(v){
	 			return v;
	 		})
	 		
	 		subcats = $.map(msg.subCategorys, function(v){
	 			return v;
	 		})
	 		
	 		region = $.map(msg.regions, function(v){
	 			return v;
	 		})
	 		
	 		dis = $.map(msg.districts, function(v){
	 			return v;
	 		})
	 		
	 		loc = $.map(msg.locations, function(v){
	 			return v;
	 		})
	 		
	 		citys = $.map(msg.citys, function(v){
	 			return v;
	 		});
	 		
	 		country = $.map(msg.countrys, function(v){
	 			return v;
	 		})
	 		
	 		product = $.map(msg.products, function(v){
	 			return v;
	 		})
	 		states = $.map(msg.states, function(v){
	 			return v;
	 		})
	 			//msg.categorys
			
	 	}
	 });*/
	
	$("#slider").dateRangeSlider("destroy");
	
	d_start = weekdat(week_nm, year_nm);
    d_end = weekdat(week_nm1, year_nm1);
    
    
	two_sliders();

    date_start = (year_nm+String(week_nm) );
    date_end =  year_nm1 + String( week_nm1);

    //console.log(date_start, date_end)
    
    
    
    setTimeout(function(){
    	
    	//get_forcast_time();
    	submit_forecast();
    	submit_internal1();
    	//submit_internal_graph()
    	submit_external();
    	submit_social();
    	submit_input();
    	
    }, 2000);
    
     $(".ruler-dis").prop('disabled', false);
	 $(".export-dis").prop('disabled', false);
	
	  }
	//cat_obj = remove_dupli(cat_obj['categoryId']);
	  
	  
})

 		$('body').append("<div id='draggable'><div id='ruler'></div></div>")
        $("#draggable").hide();
        $("#draggable").draggable({axis: "x", containment: ".tile", scroll: false});        
        $("#draggableToggle").on('click', function(){
            $(this).find('span').toggle();
            
            $("#draggable").css('height', ht-365).toggle();
        });  
        
        
	submit_input = function(){
	
	$.ajax({
        url: "/DemandSense/predictions/inputParams",
        type: "GET",
        data: '&cat='+catid+'&subcatid='+subcatid+'&product='+proid+'&con_id='+ conid +'&regionId='+ regid+'&state='+statid+'&district='+disid+'&city='+citid+'&location='+locid+'&date_start='+date_start+'&date_end='+date_end,

        success:function(msg){
        	
        	//console.log(msg)
        }
	})

}
       

	submit_forecast = function(){
				$.ajax({
			        url: "/DemandSense/predictions/forecastMap"+agr_value,
			        type: "GET",
			        data: '&cat='+catid+'&subcatid='+subcatid+'&product='+proid+'&con_id='+ conid +'&regionId='+ regid+'&state='+statid+'&district='+disid+'&city='+citid+'&location='+locid+'&date_start='+date_start+'&date_end='+date_end,

			        success:function(msg){
			        	$("#chart1").children().remove();
			        	
			        	var res_keys = [];
						for(var k in msg) {res_keys.push(k)};
						//if(res_keys.length <= 0){
							forecast_export = [];
			        	//}

						var act_data = [];
						forecast_data = []

						for(var i=0;i<res_keys.length;i++){

							var dat_obj1 = {};
							var dat_obj2 = {}

							dat_obj1.name = res_keys[i]+'( A )';
							dat_obj2.name = res_keys[i]+'( F )';
							
							var m_arr1 = [];
							var m_arr2 = []

							for(var j=0; j< msg[res_keys[i]].length;j++){
								var int_arr1 = [];
								var int_arr2 = [];

								dat_obj1.value = 'actual';
								dat_obj2.value = 'forecasted'

								//int_arr1.push( msg[res_keys[i]][j].date );
								
								int_arr1 = [( get_data(msg[res_keys[i]][j].date.substring(0, 10)) ), ( parseInt(msg[res_keys[i]][j].actual) )];

								
								//int_arr2.push( get_data(msgs[res_keys[i]][j].date) );
								int_arr2 = [get_data(msg[res_keys[i]][j].date.substring(0, 10)), ( parseInt(msg[res_keys[i]][j].forecasted) ) ];
							
								m_arr1.push(int_arr1);
								m_arr2.push(int_arr2)
								
								//msg[res_keys[i]][j].location = region+','+ dis+','+loc+','+citys+','+country+','+states;
								//msg[res_keys[i]][j].product = product+','+cats+','+subcats
							}

							//console.log(int_arr1)
							
							dat_obj1.data = m_arr1.sort();
							dat_obj1.showInLegend = false;
							dat_obj1.color = '#7BAE15'; //  #7BAE15

							dat_obj2.showInLegend = false;
							dat_obj2.data = m_arr2.sort();
							dat_obj2.color =  '#9C0B06'; //  #7BCAF5

							act_data.push(dat_obj2);
							act_data.push(dat_obj1);
							
							forecast_export.push(msg[res_keys[i] ]);

						}

						////console.log("::::::"+act_data);

						forecast.series = act_data;

						//console.log(forecast)

						var chart = new Highcharts.Chart(forecast); 

						$('#f_forecast').show();
						$('#f_actual').show();
						

						if(agr_value == ''){

						 //agr_value = '';
						 $('#f_forecast').text('Actual');
						 $('#f_actual').text('Forecasted');
						  }
						  else{
							  $('#f_forecast').text('Actual');
								 $('#f_actual').text('Forecasted');
						  }
						
					$('.loading1').waitMe('hide');
			        }
		    	
			    }); 	//success ends
		    	
		    	
		    	////console.log(en_date);
    		}

submit_internal1 = function(){
	$.ajax({
        url: "/DemandSense/predictions/internalfactor"+agr_value,
        type: "GET",
        data: '&cat='+catid+'&subcatid='+subcatid+'&product='+proid+'&con_id='+ conid +'&regionId='+ regid+'&state='+statid+'&district='+disid+'&city='+citid+'&location='+locid+'&date_start='+date_start+'&date_end='+date_end,

        success:function(msg){
        	$("#chart2").children().remove();
        	$(".internalFactorVal").children().remove();
        	
        	var dynamic_keys = [];
        	var color_index = -1;
        	var res_keys = [];
			for(var k in msg) {res_keys.push(k)};
			//if(res_keys.length <= 0){
				internal.series = [];
        	//}

			var act_data = [];
			
			//console.log(res_keys)

			for(var i=0;i<res_keys.length;i++){

				var dat_obj1 = {};
				//var dat_obj2 = {}
				for(var k in  msg[res_keys[i]][0]) {
					////console.log("legend dynamic values---"+k)
					if(k != "date" && k != "yearWeek"){
						dat_obj1.name = res_keys[i]+' '+k;
						if($.inArray(k, dynamic_keys) === -1){
							dynamic_keys.push(k);
						}
						if(k == "price" && dynamic_check_internal.length == 0){
							appends = true;
							internal_factor=k;
							dynamic_check_internal.push(k);
						}
					}
					//res_keys.push(k)
					};
				
				var m_arr1 = [];
				var m_arr2 = []

				for(var j=0; j< msg[res_keys[i]].length;j++){
					var int_arr1 = [];
					var int_arr2 = [];

					int_arr1 = [( get_data(msg[res_keys[i]][j].date.substring(0, 10)) ), parseInt(msg[res_keys[i]][j][internal_factor].substring(0,3))];
					
					m_arr1.push(int_arr1);
					
				}

				//console.log(int_arr1)
				
				dat_obj1.data = m_arr1.sort();
				$.each(internal_factor_values, function(k, v){
					color_index++;
					if(k == internal_factor){
						if(res_keys[0] == "aggregateData"){
							dat_obj1.name = v;
						}else{
							dat_obj1.name = v+" ("+res_keys[i]+")";
						}
						
						dat_obj1.color = factor_color[color_index];
						
					}
				})
				color_index = -1;
				//dat_obj1.name = internal_factor;
				dat_obj1.showInLegend = true;
				

				act_data.push(dat_obj1);
				
				internal_export.push(msg[res_keys[i] ])

			}

			if(appends == true){
				if(dynamic_check_internal.length == 1){
					internal.series = (act_data) ;
				}else{
					
					$.each(act_data, function(k, v){
						internal.series.push(v) ;
					})
					
				}
			}
		var delete_names = [];
			if(appends == false){
				$.each(res_keys, function(index , value){
					if(value == "aggregateData"){
						$.each(dynamic_check_internal, function(index2 , value2){
							$.each(internal_factor_values, function(k, v){
								if(k == value2){
									var name = v;
									delete_names.push(name);
									
								}
							});
							
						});
						
					}else{
						$.each(dynamic_check_internal, function(index2 , value2){
							$.each(internal_factor_values, function(k, v){
								if(k == value2){
									var name = v+" ("+value+")";
									delete_names.push(name);
									
								}
							});
							
						});
						
					}
				});
				
				$.each(internal.series, function(index1 , value1){
					var i = 0;
					if(value1 != null){
						$.each(delete_names, function(index , value){
							if(value1.name.toLowerCase() === value.toLowerCase()){
								i = 1;
							}
							
						});
						
						if(i==0){
							var delete1 = internal.series[index1];
							delete1.data = [];
							delete1.showInLegend = false;
							
						}
					}
				});
				
					/*$.each(dynamic_check_internal, function(index , value){
						$.each(internal_factor_values, function(k, v){
							
							if(k == value){
								if(value1.name == v){
									
									i=1;
								}
							}
						})
					});*/
					/*$.each(dynamic_check_internal, function(index , value){
						if(value1.name == value){i=1;}
					});
					*/
				/*if(i==0){
					////console.log("length----"+external.series.length);
					internal.series.splice(index1,1);
				}*/
					
				
			}
			
			////console.log(external)
			for(i=0; i<dynamic_keys.length;i++){
				$.each(internal_factor_values, function(k, v){
					color_index++;
					if(k == dynamic_keys[i]){
						$('<li> <a id="'+k+'" onClick="updategr(this)" chartt="internalFact" role="button" class="tile-refresh"><i class="fa fa-square-o" style=" color: '+factor_color[color_index]+';"></i>'+v+'</a></li>').clone().appendTo(".internalFactorVal");
					}
					
				})	
				color_index = -1;              
			}
			
			$.each(dynamic_check_internal, function(index , value){
				$("#"+value).find('i').removeClass('fa-square-o');
				$("#"+value).find('i').addClass('fa-check-square');
		     });


			var int_chart = new Highcharts.Chart(internal); 
			$('.loading2').waitMe('hide');
        }

     }); 	//success ends

}

    		submit_external = function(){
		    	$.ajax({
			        //url: "/DemandSense/predictions/external"+agr_value,
			        url: "/DemandSense/predictions/external"+agr_value,
		    		type: "GET",
			        data: '&cat='+catid+'&subcatid='+subcatid+'&product='+proid+'&con_id='+ conid +'&regionId='+ regid+'&state='+statid+'&district='+disid+'&city='+citid+'&location='+locid+'&date_start='+date_start+'&date_end='+date_end,

			        success:function(msg_ext){
			        	$("#chart3").children().remove();
			        	$(".externalFactorVal").children().remove();
			        	/*var res_keys = [];
			        	var ii = 0;
			        	var external_act_data = [];
			        	
			        	
			        	var m_arr2 = [];
			        	
			        	for(var k in msg_ext) {
							res_keys.push(k)
							
							
							for(var j=0; j< msg_ext[k].length; j++){
								var i1 = ii*5;
								
									$.each(external_factor_values, function(k1, v){
										var m_arr1 = [];
										var m_arr2 = [];
										var dat_obj1 = {};
										if(external_act_data[i1] != undefined){
											
											var obj = external_act_data[i1];
											var m_arr2 = obj.data;
											
										}else{
											var m_arr2 = [];
										}
										
										m_arr1.push( get_data(msg_ext[k][j].calDay.substring(0,10)) );
										m_arr1.push( parseInt(msg_ext[k][j][k1]));
										
										m_arr2.push(m_arr1);
										dat_obj1.data = m_arr2;
										dat_obj1.name = v+"("+k+")";
										dat_obj1.showInLegend = true;
										
										external_act_data[i1] = dat_obj1;
										i1++;
									})
									
							}
							
							
							ii++;
						};
						var color_index = 0;
						$.each(external_factor_values, function(k, v){
							color_index++;
							
								$('<li class="'+k+' mainFactor" onmouseover="updategr1(this)"><a chartt="externalFact" class="tile-refresh"> <i class="fa fa-square-o" style=" color: '+factor_color[color_index]+';"></i>'+v+'</a></li>').clone().appendTo(".externalFactorVal");
								$.each(res_keys, function(kk, vv){
									$('<li style="margin-left: 20%;display: none;"> <a id="'+k+'--'+vv+'" onClick="updategr2(this)" chartt="externalFact1" role="button" class="tile-refresh"><i class="fa fa-square-o" style=" color: '+factor_color[color_index]+';"></i>  '+vv+'</a></li>').clone().appendTo("."+k);
								})
							
							
						})
						external.series.push(external_act_data[0]);*/
			        	
			        	var dynamic_keys = [];
			        	var color_index = -1;
			        	var res_keys = [];
			        	
						for(var k in msg_ext) {
							res_keys.push(k)
						};
						
						//if(res_keys.length <= 0){
							external.series = [];
			        	//}
						var act_data = [];

						for(var i=0;i<res_keys.length;i++){

							var dat_obj1 = {};
							for(var k in  msg_ext[res_keys[i]][0]) {
								if(k != "date" && k != "yearWeek"){
									if($.inArray(k, dynamic_keys) === -1){
										dynamic_keys.push(k);
									}
									if(k == "maxTemp" && dynamic_check_external.length == 0){
										appends = true;
										external_factor=k;
										dynamic_check_external.push(k);
									}
								}
							};
							
							
							var m_arr1 = [];

							for(var j=0; j< msg_ext[res_keys[i]].length;j++){
								var int_arr1 = [];
								var int_arr2 = [];
								
								int_arr1.push( get_data(msg_ext[res_keys[i]][j].calDay.substring(0,10)) );
								int_arr1.push( parseInt(msg_ext[res_keys[i]][j][external_factor]) );
								
								m_arr1.push(int_arr1);
							}

							
							dat_obj1.data = m_arr1;
							$.each(external_factor_values, function(k, v){
								color_index++;
								if(k == external_factor){
									if(res_keys[0] == "Agg Data"){
										dat_obj1.name = v;
									}else{
										dat_obj1.name = v+" ("+res_keys[i]+")";
									}
									
									dat_obj1.color = factor_color[color_index];
									
								}
							})
							color_index = -1;						
							dat_obj1.showInLegend = true;
							
							
							act_data.push(dat_obj1);
							////console.log(act_data);
							external_export.push(msg_ext[res_keys[i] ]);
							
							
							
						}
						
						if(appends == true){
							if(dynamic_check_external.length == 1){
								external.series = (act_data) ;
							}else{
								$.each(act_data, function(k, v){
									external.series.push(v) ;
								})
							}
							
						}
						var delete_names = [];
						if(appends == false){
							$.each(res_keys, function(index , value){
								if(value =="Agg Data"){
									$.each(dynamic_check_external, function(index2 , value2){
										$.each(external_factor_values, function(k, v){
											if(k == value2){
												var name = v;
												delete_names.push(name);
												
											}
										});
									});
								}else{
									$.each(dynamic_check_external, function(index2 , value2){
										$.each(external_factor_values, function(k, v){
											if(k == value2){
												var name = v+" ("+value+")";
												delete_names.push(name);
												
											}
										});
									});
								}
								
							});
							
							$.each(external.series, function(index1 , value1){
								var i = 0;
								if(value1 != null){
									$.each(delete_names, function(index , value){
										if(value1.name.toLowerCase() === value.toLowerCase()){
											i = 1;
										}
										
									});
									
									if(i==0){
										var delete1 = external.series[index1];
										delete1.data = [];
										delete1.showInLegend = false;
										
									}
								}
							});
						}
						/*if(appends == false){
							$.each(external.series, function(index1 , value1){
								var i = 0;
							if(value1 != null){
								
								$.each(dynamic_check_external, function(index , value){
									//console.log(value1.name+"------"+value+" ("+res_keys[i]+")");
									$.each(external_factor_values, function(k, v){
										if(k == value){
											if(value1.name == v){
												i=1;
											}
										}
									})
								});
								
								if(i==0){
									external.series.splice(index1,1);
								}
								
							}
						});
						}*/
						//console.log(external.series);
						/*
						 * Looping for dynamic external Factors and printing them in predective page
						 *  
						 */
						
						for(i=0; i<dynamic_keys.length;i++){
							$.each(external_factor_values, function(k, v){
								color_index++;
								//console.log(dynamic_keys[i]+"------"+k);
								if(k == dynamic_keys[i]){
									//console.log(dynamic_keys[i]);
									$('<li> <a id="'+k+'" onClick="updategr(this)" chartt="externalFact" role="button" class="tile-refresh"><i class="fa fa-square-o" style=" color: '+factor_color[color_index]+';"></i>'+v+'</a></li>').clone().appendTo(".externalFactorVal");
									/*$.each(res_keys, function(kk, vv){
										$('<li style="margin-left: 20%;display: none;"> <a id="'+k+'--'+vv+'" onClick="updategr(this)" chartt="externalFact" role="button" class="tile-refresh"><i class="fa fa-square-o" style=" color: '+factor_color[color_index]+';"></i>  '+vv+'</a></li>').clone().appendTo("."+k);
									})*/
								}
								
							})	
							color_index = -1;
						}
						
						/*
						 * Code to display plus box or tick box for non selected and selected values
						 *  
						 */
						$.each(dynamic_check_external, function(index , value){
							$("#"+value).find('i').removeClass('fa-square-o');
							$("#"+value).find('i').addClass('fa-check-square');
					     });

						var chart_ext = new Highcharts.Chart(external); 
						$('.loading3').waitMe('hide');
			        }

			    }); 	//success ends

    		}
    		var submit_social = function(){
    			
		    	$.ajax({
			        url: "/DemandSense/predictions/socialfactor"+agr_value,
			        type: "GET",
			        data: '&cat='+catid+'&subcatid='+subcatid+'&product='+proid+'&con_id='+ conid +'&regionId='+ regid+'&state='+statid+'&district='+disid+'&city='+citid+'&location='+locid+'&date_start='+date_start+'&date_end='+date_end,

			        success:function(msg){
			        	$("#social").children().remove();
			        	$(".socialFactorVal").children().remove();
			        	
			        	var dynamic_keys = [];
			        	var color_index = -1;
			        	var res_keys = [];
						for(var k in msg) {res_keys.push(k)};
						
			        	//if(res_keys.length <= 0){
			        		social.series = [];
			        	//}
			        	
						var act_data = [];

						for(var i=0;i<res_keys.length;i++){

							var dat_obj1 = {};
							//var dat_obj2 = {}
							for(var k in  msg[res_keys[i]][0]) {
								////console.log("legend dynamic values---"+k)
								if(k != "date" && k != "yearWeek"){
									dat_obj1.name = res_keys[i]+' '+k;
									if($.inArray(k, dynamic_keys) === -1){
										dynamic_keys.push(k);
									}
									if(k == "sentimental" && dynamic_check_social.length == 0){
										appends = true;
										social_factor=k;
										dynamic_check_social.push(k);
									}
								}
								//res_keys.push(k)
								};
							
							
							var m_arr1 = [];
							var m_arr2 = [];

							for(var j=0; j< msg[res_keys[i]].length;j++){
								var int_arr1 = [];
								var int_arr2 = [];

								/*dat_obj1.value = 'sentimental';
								dat_obj2.value = 'srs'*/

								//int_arr1.push( msg[res_keys[i]][j].date );
								
								//int_arr1.push( (msg[res_keys[i]][j].yearWeek).match(/\d{4}(?=\d{2,3})|\d+/g).join("-") );
									int_arr1.push( get_data(msg[res_keys[i]][j].date.substring(0,10)) );
									int_arr1.push( parseInt(msg[res_keys[i]][j][social_factor]) );

								//int_arr2.push( (msg[res_keys[i]][j].yearWeek).match(/\d{4}(?=\d{2,3})|\d+/g).join("-") );
								/*int_arr2.push( get_data(msg[res_keys[i]][j].date.substring(0,10)) );
								int_arr2.push( parseInt(msg[res_keys[i]][j].srs) );*/
								
								m_arr1.push(int_arr1);
								//m_arr2.push(int_arr2)
							}

							//console.log(int_arr1)
							
							dat_obj1.data = m_arr1.sort();
							$.each(social_factor_values, function(k, v){
								color_index++;
								if(k == social_factor){
									if(res_keys[0] == "Agg Data"){
										dat_obj1.name = v;
									}else{
										dat_obj1.name = v+" ("+res_keys[i]+")";
									}
									
									dat_obj1.color = factor_color[color_index];
									
								}
							})
							color_index = -1;
							//dat_obj1.name = social_factor;
							dat_obj1.showInLegend = true;
							

							/*dat_obj2.showInLegend = false;
							dat_obj2.data = m_arr2.sort();
							dat_obj2.color = '#E47BF5';
							*/
							
							act_data.push(dat_obj1);
							
							social_export.push(msg[res_keys[i] ]);

						}

					//console.log(act_data)
						if(appends == true){
							if(dynamic_check_social.length == 1){
								social.series = (act_data) ;
							}else{
								$.each(act_data, function(k, v){
									social.series.push(v) ;
								})
								
							}
						}
						var delete_names = [];
						if(appends == false){
							$.each(res_keys, function(index , value){
								if(value == "Agg Data"){
									$.each(dynamic_check_social, function(index2 , value2){
										$.each(social_factor_values, function(k, v){
											if(k == value2){
												var name = v;
												delete_names.push(name);
												
											}
										});
									});
								}else{
									$.each(dynamic_check_social, function(index2 , value2){
										$.each(social_factor_values, function(k, v){
											if(k == value2){
												var name = v+" ("+value+")";
												delete_names.push(name);
												
											}
										});
									});
								}
								
							});
							
							$.each(social.series, function(index1 , value1){
								var i = 0;
								if(value1 != null){
									$.each(delete_names, function(index , value){
										if(value1.name.toLowerCase() === value.toLowerCase()){
											i = 1;
										}
										
									});
									
									if(i==0){
										var delete1 = social.series[index1];
										delete1.data = [];
										delete1.showInLegend = false;
										
									}
								}
							});
						}
						/*if(appends == false){
							$.each(social.series, function(index1 , value1){
							var i = 0;
							if(value1 != null){
								$.each(dynamic_check_social, function(index , value){
									$.each(social_factor_values, function(k, v){
										if(k == value){
											if(value1.name == v){
												i=1;
											}
										}
									})
								});
								$.each(dynamic_check_social, function(index , value){
									if(value1.name == value){i=1;}
								});
								
							if(i==0){
								////console.log("length----"+external.series.length);
								social.series.splice(index1,1);
							}
								
							}
						});
						}*/
						
						//console.log(social)
						for(i=0; i<dynamic_keys.length;i++){
							$.each(social_factor_values, function(k, v){
								color_index++;
								if(k == dynamic_keys[i]){
									$('<li> <a id="'+k+'" onClick="updategr(this)" chartt="socialFact" role="button" class="tile-refresh"><i class="fa fa-square-o" style=" color: '+factor_color[color_index]+';"></i>'+v+'</a></li>').clone().appendTo(".socialFactorVal"); 
								}
								
							})	
							color_index = -1;             
						}
						
						$.each(dynamic_check_social, function(index , value){
							$("#"+value).find('i').removeClass('fa-square-o');
							$("#"+value).find('i').addClass('fa-check-square');
					     });


						var chart_social = new Highcharts.Chart(social); 
						$('.loading4').waitMe('hide');
			        }

			    }); 	//success ends
		    	
    		}


	         /////////////////// charts //////////////////////
    		
    		Highcharts.dateFormats = {
    			    W: function (timestamp) {
    			        var date = new Date(timestamp),
    			            day = date.getUTCDay() == 0 ? 7 : date.getUTCDay(),
    			            dayNumber;
    			        date.setDate(date.getUTCDate() + 4 - day);
    			        dayNumber = Math.floor((date.getTime() - new Date(date.getUTCFullYear(), 0, 1, -6)) / 86400000);
    			        return 1 + Math.floor(dayNumber / 7);

    			    }
    			}
	              
    		var forecast = {
    				
                      tooltip: {
                    	  shared: true,
                          useHTML: true,
                          headerFormat: '<small>{point.key}</small><table>',
                          pointFormat: '<tr><td style="color: {series.color}">{series.name}: </td>' +
                              '<td style="text-align: right"><b>{point.y}</b></td></tr>',
                          footerFormat: '</table>',
                          valueDecimals: 2
		                /*formatter: function() {
		                    return  Highcharts.dateFormat('%a | %e - %b - %Y',
		                                              new Date(this.x))+'<br/>'+
		                    '<b>' + this.series.name +'</b>' + ':' + this.y;
		                }/*,
		                positioner: function(boxWidth, boxHeight, point) {         
		        	        return {x:point.plotX - 160,y:point.plotY};         
		        	    }*/
		            },

                      title: {
                          text: 'Forecast'
                      },
                      
                      chart: {
                          //width: 110,
                          height: 350,
                          renderTo: 'chart1',
                          type: 'line',
                          marginLeft: 84,
                      },

                      plotOptions: {
                          series: {
                              lineWidth: 2,
                              marker: {
                                  enabled: false
                              }
                          }
                      },
                      
                      credits: {
                          enabled: false
                      },

                      xAxis: {
					        type: 'datetime',
			                minorTickInterval: "auto",
			                minorTickPosition:'outside',
			                minorTickWidth: 1,
			                minorTickLength: 4,
							minorGridLineWidth: 0,

							tickInterval: 7 * 24 * 36e5, // one week
					        labels: {
					            format: '{value: %W}'
					        },

			                dateTimeLabelFormats: {
				                millisecond: '%H:%M:%S.%L',
				                second: '%H:%M:%S',
				                minute: '%H:%M',
				                hour: '%H:%M',
				                day: '%e. %b',
				                week: '%e. %b',
				                month: '%b \'%y',
				                year: '%Y'
		               		 },

		               		plotLines: [{
				                  color: '#493D55',
				                  width: .8,
				                  //value: Date.UTC(2010, 10, 7)
				                  value: get_data(en_date_sys_param)
				            }],
				            
	               	},

                      yAxis: {
                      gridLineWidth: 0,
                      //minorGridLineWidth: 0,
                      minTickInterval: 25,
                      title: {
                          text: 'Each'
                      },
                      tickLength: 5,
                      tickWidth: 1,
                      tickPosition: 'outside',
                      
                      lineWidth:1,
                  },

                  series: []
                };
          
          var internal = {
        		  
        		  tooltip: {
        			  shared: true,
                      useHTML: true,
                      headerFormat: '<small>{point.key}</small><table>',
                      pointFormat: '<tr><td style="color: {series.color}">{series.name}: </td>' +
                          '<td style="text-align: right"><b>{point.y}</b></td></tr>',
                      footerFormat: '</table>',
                      valueDecimals: 2
		                /*formatter: function() {
		                    return  Highcharts.dateFormat('%a | %e - %b - %Y',
		                                              new Date(this.x))+'<br/>'+
		                    '<b>' + this.series.name +'</b>' + ':' + this.y;
		                }*/
		            },

		          navigator: {
	                  enabled: true,
	                  adaptToUpdatedData: false,
	                  height: 12,
		                series : {
		                    
		                }
	              },

	          title: {
	              text: 'Internal Factors'
	          },

	          credits: {
                    enabled: false
                },
		          
		          chart: {
		              //width: 110,
		              height: 350,
		              renderTo: 'chart2',
		              type: 'line',
		              marginLeft: 84,
		          },

		          plotOptions: {
		              series: {
		                  lineWidth: 2,
		                  marker: {
		                      enabled: false
		                  }
		              }
		          },

		           xAxis: {
					        type: 'datetime',
			                minorTickInterval: "auto",
			                minorTickPosition:'outside',
			                minorTickWidth: 1,
			                minorTickLength: 4,
							minorGridLineWidth: 0,

							tickInterval: 7 * 24 * 36e5, // one week
					        labels: {
					            format: '{value: %W}'
					        },

			                dateTimeLabelFormats: {
				                millisecond: '%H:%M:%S.%L',
				                second: '%H:%M:%S',
				                minute: '%H:%M',
				                hour: '%H:%M',
				                day: '%e. %b',
				                week: '%e. %b',
				                month: '%b \'%y',
				                year: '%Y'
		               		 },

		               		plotLines: [{
				                  color: '#493D55',
				                  width: .8,
				                  value: get_data(en_date_sys_param)
				            }],
	               	},
		           
		          yAxis: {
		              gridLineWidth: 0,
		              //minorGridLineWidth: 0,
		              title: {
		                  text: 'values'
		              },
		              tickLength: 5,
		              tickWidth: 1,
		              tickPosition: 'outside',
		              
		              lineWidth:1,
		          },

		          series: []
		    };

		 
              
          var external = {
              
		       
		          navigator: {
	                  enabled: true,
	                  adaptToUpdatedData: false,
	                  height: 12,
		                series : {
		                    
		                }
	              },

	          title: {
	              text: 'External Factors'
	          },

	          credits: {
                    enabled: false
                },
		          
		          chart: {
		              //width: 110,
		              height: 350,
		              renderTo: 'chart3',
		              type: 'line',
		              marginLeft: 84,
		          },

		          tooltip: {
		        	  shared: true,
                      useHTML: true,
                      headerFormat: '<small>{point.key}</small><table>',
                      pointFormat: '<tr><td style="color: {series.color}">{series.name}: </td>' +
                          '<td style="text-align: right"><b>{point.y}</b></td></tr>',
                      footerFormat: '</table>',
                      valueDecimals: 2
		                /*formatter: function() {
		                    return  Highcharts.dateFormat('%a | %e - %b - %Y',
		                                              this.x)+'<br/>'+
		                    '<b>' + this.series.name +'</b>' + ':' + this.y;
		                }*/
		            },

		          plotOptions: {
		              series: {
		                  lineWidth: 2,
		                  marker: {
		                      enabled: false
		                  }
		              }
		          },
		           xAxis: {
					        type: 'datetime',
			                minorTickInterval: "auto",
			                minorTickPosition:'outside',
			                minorTickWidth: 1,
			                minorTickLength: 4,
							minorGridLineWidth: 0,

							tickInterval: 7 * 24 * 36e5, // one week
					        labels: {
					            format: '{value: %W}'
					        },

			                dateTimeLabelFormats: {
				                millisecond: '%H:%M:%S.%L',
				                second: '%H:%M:%S',
				                minute: '%H:%M',
				                hour: '%H:%M',
				                day: '%e. %b',
				                week: '%e. %b',
				                month: '%b \'%y',
				                year: '%Y'
		               		 },

		               		plotLines: [{
				                  color: '#493D55',
				                  width: .8,
				                  value: get_data(en_date_sys_param)
				            }],
	               	},
		          
		        
		          yAxis: {
		              gridLineWidth: 0,
		              //minorGridLineWidth: 0,
		              title: {
		                  text: 'values'
		              },
		              tickLength: 5,
		              tickWidth: 1,
		              tickPosition: 'outside',
		              
		              lineWidth:1,
		          },

		          series: []
		    };

       	var social = {
              
	        options: {
	              
	              rangeSelector: {
	                  selected: 1
	              },
	              
	          },

	          navigator: {
                  enabled: true,
                  adaptToUpdatedData: false,
                  height: 12,
	                series : {
	                    
	                }
              },

          title: {
              text: 'Social Factors'
          },

          credits: {
                enabled: false
            },
	          
	          chart: {
	              //width: 110,
	              height: 350,
	              renderTo: 'social',
	              type: 'line',
	              marginLeft: 84,
	          },

	          plotOptions: {
	              series: {
	                  lineWidth: 2,
	                  marker: {
	                      enabled: false
	                  }
	              }
	          },

	          tooltip: {
	        	  shared: true,
                  useHTML: true,
                  headerFormat: '<small>{point.key}</small><table>',
                  pointFormat: '<tr><td style="color: {series.color}">{series.name}: </td>' +
                      '<td style="text-align: right"><b>{point.y}</b></td></tr>',
                  footerFormat: '</table>',
                  valueDecimals: 2
		               /* formatter: function() {
		                    return  Highcharts.dateFormat('%a | %e - %b - %Y',
		                                              this.x)+'<br/>'+
		                    '<b>' + this.series.name +'</b>' + ':' + this.y;
		                }*/
		            },
	           xAxis: {
					        type: 'datetime',
			                minorTickInterval: "auto",
			                minorTickPosition:'outside',
			                minorTickWidth: 1,
			                minorTickLength: 4,
							minorGridLineWidth: 0,

							tickInterval: 7 * 24 * 36e5, // one week
					        labels: {
					            format: '{value: %W}'
					        },

			                dateTimeLabelFormats: {
				                millisecond: '%H:%M:%S.%L',
				                second: '%H:%M:%S',
				                minute: '%H:%M',
				                hour: '%H:%M',
				                day: '%e. %b',
				                week: '%e. %b',
				                month: '%b \'%y',
				                year: '%Y'
		               		 },

		               		plotLines: [{
				                  color: '#493D55',
				                  width: .8,
				                  value: get_data(en_date_sys_param)
				            }],
	               	},
	           
	          yAxis: {
	              gridLineWidth: 0,
	              //minorGridLineWidth: 0,
	              title: {
	                  text: 'values'
	              },
	              tickLength: 5,
	              tickWidth: 1,
	              tickPosition: 'outside',
	              
	              lineWidth:1,
	          },

	          series: []
	    };
       	
//////////////////////////week date ///////////////////////

        
       							 
         
         
         $('#myForm input').on('change', function() {
      	   var agr=($('input[name=customRadio]:checked', '#myForm').val()); 

      	   //console.log(agr)
      	   if(agr=='aggregation'){

      	   		agr_value = '';
      	   }
      	   else{
      	   		agr_value = 'NA';
      	   }
      	   
      	   external.series = [];
      	 internal.series = [];
      	   
      	});
       	
			                                     
		/*This is json data parse : forcast datatable*/
        
         
         
  //created by ankit for submit
         $("#submit1").click(function(){  	 
        	
                  $.ajax(
                      {
                         url: "/DemandSense/outputdata/getlist",
                         type: "GET",
                             contentType: "application/json",
                             dataType: "json",
                             data: '&cat='+catid+'&subcatid='+subcatid+'&product='+proid+'&con_id='+ conid +'&regionId='+ regid+'&state='+statid+'&district='+disid+'&city='+citid+'&location='+locid+'&date_start='+date_start+'&date_end='+date_end,

                            success: function (data) 
                            {   
                                 var oTable =  $('#example').dataTable(
                                {
//                                    "fnDrawCallback": function ( oSettings )
//                                      {
//                                      if ( oSettings.bSorted || oSettings.bFiltered )
//                                      {
//                                          for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
//                                              { }
//                                      }
//                                  }, 
                                  "scrollY": 400,
                                  "pageLength": 10,
                                  "scrollX": false,       
                                  }
                      );
                                  
                                var row = [];             
                                $.each(data, function(i, UserReport)    
                                    {
                                        row[i] =  [];
                                        row[i][0] = UserReport['categoryId'];
                                        row[i][1] = UserReport['subCategoryId'];
                                        row[i][2] = UserReport['productId'];
                                        row[i][3] = UserReport['locationId'];    
                                        row[i][4] = UserReport['productId'];
                                        row[i][5] = UserReport['quantity'];
                                        row[i][6] = UserReport['dataType'];
                                        row[i][7] = UserReport['owQty'];
                                        row[i][8] = UserReport['yearWeek'];    
                                       
                                    }); 
                                        oTable.fnClearTable();
                                      oTable.fnAddData(row);
                            }
                            });
                  
                  /*This is significant Datatable*/
                  $.ajax(
                          {
                             url: "/DemandSense/outputSFdata/getSFlist",
                             type: "GET",
                                 contentType: "application/json",
                                 dataType: "json",
                                 data: '&cat='+catid+'&subcatid='+subcatid+'&product='+proid+'&con_id='+ conid +'&regionId='+ regid+'&state='+statid+'&district='+disid+'&city='+citid+'&location='+locid+'&date_start='+date_start+'&date_end='+date_end,

                                success: function (data) 
                                {   
                                      var oTable =  $('#example1').dataTable(
                                     {
                                        "fnDrawCallback": function ( oSettings )
                                          {
                                          if ( oSettings.bSorted || oSettings.bFiltered )
                                          {
                                              for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                                                  { }
                                          }
                                      }, 
                                      "scrollY": 400,
                                      "pageLength": 10,
                                      "scrollX": false,       
                                      });
                                      
                                    var row = [];             
                                    $.each(data, function(i, UserReport)    
                                        {
                                 	   row[i] =  [];
                                        row[i][0] = UserReport['categoryId'];
                                        row[i][1] = UserReport['subCategoryId'];
                                        row[i][2] = UserReport['productId'];
                                        row[i][3] = UserReport['regionId'];
                                        row[i][4] = UserReport['stateId']; 
                                        row[i][5] = UserReport['locationId'];
                                        row[i][6] = UserReport['uId'];
                                        row[i][7] = UserReport['significantFactors'];   
                                        row[i][8] = UserReport['correlation'];
                                           
                                        }); 
                                            oTable.fnClearTable();
                                            oTable.fnAddData(row);
                                }
                                });
                      /*This is a Accuracy Table*/
                  $.ajax(
                          {
                             url: "/DemandSense/outputAdata/getAdata",
                             type: "GET",
                                 contentType: "application/json",
                                 dataType: "json",
                                 data: '&cat='+catid+'&subcatid='+subcatid+'&product='+proid+'&con_id='+ conid +'&regionId='+ regid+'&state='+statid+'&district='+disid+'&city='+citid+'&location='+locid+'&date_start='+date_start+'&date_end='+date_end,

                                success: function (data) 
                                {   
                                      var oTable =  $('#example2').dataTable(
                                     {
                                        "fnDrawCallback": function ( oSettings )
                                          {
                                          if ( oSettings.bSorted || oSettings.bFiltered )
                                          {
                                              for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                                                  { }
                                          }
                                      }, 
                                      "scrollY": 400,
                                      "pageLength": 10,
                                      "scrollX": false,       
                                      });
                                      
                                    var row = [];             
                                    $.each(data, function(i, UserReport)    
                                        {
                                            row[i] =  [];
                                            row[i][0] = UserReport['categoryId'];
                                            row[i][1] = UserReport['subCategoryId'];
                                            row[i][2] = UserReport['productId'];
                                            row[i][3] = UserReport['countryId'];
                                            row[i][4] = UserReport['locationId'];
                                            row[i][5] = UserReport['uid'];
                                            row[i][6] = UserReport['rmse'];
                                            row[i][7] = UserReport['mae'];    
                                            row[i][8] = UserReport['mpe'];
                                            row[i][9] = UserReport['mape'];
                                            row[i][10] = UserReport['mase'];
                                            
                                            
                                         
                                        }); 
                                            oTable.fnClearTable();
                                            oTable.fnAddData(row);
                                }
                                });
         });
              
         
         
  //ends submit button       
     	
	 	}
	 });
	
	function updategr1(e){
		//$(e).parent().parent().addClass('open').children("dropdown-toggle").attr("aria-expanded","true");
		$(".mainFactor").children("li").css({"display":"none"});
		$(e).children("li").toggle();
	}
	
	function updategr2(e){
		var id = $(e).attr('id');    //$(e).parent().parent().children('a').find('i').removeClass("fa-square-o").addClass("fa-square");
		var id1 = [];
		$.each(id.split('--'), function(k , v){
			id1.push($.trim(v));
		})
		
		if($(e).find('i').hasClass('fa-square-o')){
			
			$(e).find('i').removeClass('fa-square-o');
			$(e).find('i').addClass('fa-check-square');
			
		}else{
			$(e).find('i').removeClass('fa-check-square');
			$(e).find('i').addClass('fa-square-o');
		}
		
		
		
		$(e).parent().parent().children('a').find('i').removeClass("fa-square").addClass("fa-square-o");
		
		$(e).parent().parent().children('li').each(function(){
			if($(this).find('i').hasClass('fa-check-square')){
				//console.log($(e).find('i').attr('class'));
				$(e).parent().parent().children('a').find('i').removeClass("fa-square-o").addClass("fa-square");
			}
		})
	}
	
	
	