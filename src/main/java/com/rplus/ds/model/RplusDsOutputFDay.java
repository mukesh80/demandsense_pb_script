package com.rplus.ds.model;
// Generated 30 Jul, 2015 12:03:10 AM by Hibernate Tools 4.0.0


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Rafeeq 
 */
@Entity
@Table(name = "rplus_ds_output_f_day")
public class RplusDsOutputFDay implements java.io.Serializable {

	 private static final long serialVersionUID = 1L;
	 private String countryId;
	 private String locationId;
	 private String regionId;
	 private String stateId;
	 private String districtId;
	 private String cityId;
	 private String productId;
	 private String categoryId;
	 private String subCategoryId;
	 private String timestamp;
	 private String version;
	 private String uid;
	 private Integer yearWeek;   
	 private Integer quantity;
	 private String dataType;
	 private Double arimaMinValue;
	 private Double arimaMean;
	 private Double arimaMaxValue;
	 private Double crostonMean;
	 private Double holtMinValue;
	 private Double holtMean;
	 private Double holtMaxValue;
	 private Double tslmMinValue;
	 private Double tslmMean;
	 private Double tslmMaxValue;
	 private Double sesMinValue;
	 private Double sesMean;
	 private Double sesMaxValue;
	 private Double bestMinValue;
	 private Double bestMeanValue;
	 private Double bestMaxValue;
	 private String bestModel;
	 private Double owQty;
	 private Double finalQty;
	 private Integer countId;
	 private Integer calYear;
	 private Integer calHalfYear;
	 private Integer calQuarter;
	 private Integer calMonth;
	 private Integer calWeek;
	 private Date calDay;
	 
	 	@Id
		@Column(name="count_id")
		public Integer getCountId() {
		    return this.countId;
		}

		public void setCountId(Integer countId) {
		    this.countId = countId;
		}
	 
	   @Column(name="cal_year")
	   public Integer getCalYear() {
			return calYear;
		}

		public void setCalYear(Integer calYear) {
			this.calYear = calYear;
		}

		@Column(name="cal_halfyear")
		public Integer getCalHalfYear() {
			return calHalfYear;
		}

		public void setCalHalfYear(Integer calHalfYear) {
			this.calHalfYear = calHalfYear;
		}

		@Column(name="cal_quarter")
		public Integer getCalQuarter() {
			return calQuarter;
		}

		public void setCalQuarter(Integer calQuarter) {
			this.calQuarter = calQuarter;
		}
		
		@Column(name="cal_month")
		public Integer getCalMonth() {
			return calMonth;
		}

		public void setCalMonth(Integer calMonth) {
			this.calMonth = calMonth;
		}
		
		@Column(name="cal_week")
		public Integer getCalWeek() {
			return calWeek;
		}

		public void setCalWeek(Integer calWeek) {
			this.calWeek = calWeek;
		}

	public RplusDsOutputFDay() {
	}

	public RplusDsOutputFDay(String countryId, String locationId, String regionId, String stateId, String districtId, String cityId, String productId, String categoryId, String subCategoryId, String timestamp, String version, String uid, Integer yearWeek, Integer quantity, String dataType, Double arimaMinValue, Double arimaMean, Double arimaMaxValue, Double crostonMean, Double holtMinValue, Double holtMean, Double holtMaxValue, Double tslmMinValue, Double tslmMean, Double tslmMaxValue, Double sesMinValue, Double sesMean, Double sesMaxValue, Double bestMinValue, Double bestMeanValue, Double bestMaxValue, String bestModel, Double owQty, Double finalQty, Integer countId, Integer calWeek,Date calDay) {
	   this.countryId = countryId;
	   this.locationId = locationId;
	   this.regionId = regionId;
	   this.stateId = stateId;
	   this.districtId = districtId;
	   this.cityId = cityId;
	   this.productId = productId;
	   this.categoryId = categoryId;
	   this.subCategoryId = subCategoryId;
	   this.timestamp = timestamp;
	   this.version = version;
	   this.uid = uid;
	   this.yearWeek = yearWeek;
	   this.quantity = quantity;
	   this.dataType = dataType;
	   this.arimaMinValue = arimaMinValue;
	   this.arimaMean = arimaMean;
	   this.arimaMaxValue = arimaMaxValue;
	   this.crostonMean = crostonMean;
	   this.holtMinValue = holtMinValue;
	   this.holtMean = holtMean;
	   this.holtMaxValue = holtMaxValue;
	   this.tslmMinValue = tslmMinValue;
	   this.tslmMean = tslmMean;
	   this.tslmMaxValue = tslmMaxValue;
	   this.sesMinValue = sesMinValue;
	   this.sesMean = sesMean;
	   this.sesMaxValue = sesMaxValue;
	   this.bestMinValue = bestMinValue;
	   this.bestMeanValue = bestMeanValue;
	   this.bestMaxValue = bestMaxValue;
	   this.bestModel = bestModel;
	   this.owQty = owQty;
	   this.finalQty = finalQty;
	   this.countId = countId;
	}


	@Column(name="country_id", length=15)
	public String getCountryId() {
	    return this.countryId;
	}

	public void setCountryId(String countryId) {
	    this.countryId = countryId;
	}


	@Column(name="location_id", length=15)
	public String getLocationId() {
	    return this.locationId;
	}

	public void setLocationId(String locationId) {
	    this.locationId = locationId;
	}


	@Column(name="region_id", length=15)
	public String getRegionId() {
	    return this.regionId;
	}

	public void setRegionId(String regionId) {
	    this.regionId = regionId;
	}


	@Column(name="state_id", length=15)
	public String getStateId() {
	    return this.stateId;
	}

	public void setStateId(String stateId) {
	    this.stateId = stateId;
	}


	@Column(name="district_id", length=15)
	public String getDistrictId() {
	    return this.districtId;
	}

	public void setDistrictId(String districtId) {
	    this.districtId = districtId;
	}


	@Column(name="city_id", length=15)
	public String getCityId() {
	    return this.cityId;
	}

	public void setCityId(String cityId) {
	    this.cityId = cityId;
	}


	@Column(name="product_id", length=15)
	public String getProductId() {
	    return this.productId;
	}

	public void setProductId(String productId) {
	    this.productId = productId;
	}


	@Column(name="category_id", length=15)
	public String getCategoryId() {
	    return this.categoryId;
	}

	public void setCategoryId(String categoryId) {
	    this.categoryId = categoryId;
	}


	@Column(name="sub_category_id", length=15)
	public String getSubCategoryId() {
	    return this.subCategoryId;
	}

	public void setSubCategoryId(String subCategoryId) {
	    this.subCategoryId = subCategoryId;
	}


	@Column(name="timestamp")
	public String getTimestamp() {
	    return this.timestamp;
	}

	public void setTimestamp(String timestamp) {
	    this.timestamp = timestamp;
	}


	@Column(name="version")
	public String getVersion() {
	    return this.version;
	}

	public void setVersion(String version) {
	    this.version = version;
	}


	@Column(name="uid")
	public String getUid() {
	    return this.uid;
	}

	public void setUid(String uid) {
	    this.uid = uid;
	}


	@Column(name="year_week")
	public Integer getYearWeek() {
	    return this.yearWeek;
	}

	public void setYearWeek(Integer yearWeek) {
	    this.yearWeek = yearWeek;
	}
	@Column(name="cal_day")
	public Date getCalDay() {
			return calDay;
		}
		public void setCalDay(Date calDay) {
			this.calDay = calDay;
		}


	@Column(name="quantity")
	public Integer getQuantity() {
	    return this.quantity;
	}

	public void setQuantity(Integer quantity) {
	    this.quantity = quantity;
	}


	@Column(name="data_type")
	public String getDataType() {
	    return this.dataType;
	}

	public void setDataType(String dataType) {
	    this.dataType = dataType;
	}


	@Column(name="arima_min_value", precision=17, scale=17)
	public Double getArimaMinValue() {
	    return this.arimaMinValue;
	}

	public void setArimaMinValue(Double arimaMinValue) {
	    this.arimaMinValue = arimaMinValue;
	}


	@Column(name="arima_mean", precision=17, scale=17)
	public Double getArimaMean() {
	    return this.arimaMean;
	}

	public void setArimaMean(Double arimaMean) {
	    this.arimaMean = arimaMean;
	}


	@Column(name="arima_max_value", precision=17, scale=17)
	public Double getArimaMaxValue() {
	    return this.arimaMaxValue;
	}

	public void setArimaMaxValue(Double arimaMaxValue) {
	    this.arimaMaxValue = arimaMaxValue;
	}


	@Column(name="croston_mean", precision=17, scale=17)
	public Double getCrostonMean() {
	    return this.crostonMean;
	}

	public void setCrostonMean(Double crostonMean) {
	    this.crostonMean = crostonMean;
	}


	@Column(name="holt_min_value", precision=17, scale=17)
	public Double getHoltMinValue() {
	    return this.holtMinValue;
	}

	public void setHoltMinValue(Double holtMinValue) {
	    this.holtMinValue = holtMinValue;
	}


	@Column(name="holt_mean", precision=17, scale=17)
	public Double getHoltMean() {
	    return this.holtMean;
	}

	public void setHoltMean(Double holtMean) {
	    this.holtMean = holtMean;
	}


	@Column(name="holt_max_value", precision=17, scale=17)
	public Double getHoltMaxValue() {
	    return this.holtMaxValue;
	}

	public void setHoltMaxValue(Double holtMaxValue) {
	    this.holtMaxValue = holtMaxValue;
	}


	@Column(name="tslm_min_value", precision=17, scale=17)
	public Double getTslmMinValue() {
	    return this.tslmMinValue;
	}

	public void setTslmMinValue(Double tslmMinValue) {
	    this.tslmMinValue = tslmMinValue;
	}


	@Column(name="tslm_mean", precision=17, scale=17)
	public Double getTslmMean() {
	    return this.tslmMean;
	}

	public void setTslmMean(Double tslmMean) {
	    this.tslmMean = tslmMean;
	}


	@Column(name="tslm_max_value", precision=17, scale=17)
	public Double getTslmMaxValue() {
	    return this.tslmMaxValue;
	}

	public void setTslmMaxValue(Double tslmMaxValue) {
	    this.tslmMaxValue = tslmMaxValue;
	}


	@Column(name="ses_min_value", precision=17, scale=17)
	public Double getSesMinValue() {
	    return this.sesMinValue;
	}

	public void setSesMinValue(Double sesMinValue) {
	    this.sesMinValue = sesMinValue;
	}


	@Column(name="ses_mean", precision=17, scale=17)
	public Double getSesMean() {
	    return this.sesMean;
	}

	public void setSesMean(Double sesMean) {
	    this.sesMean = sesMean;
	}


	@Column(name="ses_max_value", precision=17, scale=17)
	public Double getSesMaxValue() {
	    return this.sesMaxValue;
	}

	public void setSesMaxValue(Double sesMaxValue) {
	    this.sesMaxValue = sesMaxValue;
	}


	@Column(name="best_min_value", precision=17, scale=17)
	public Double getBestMinValue() {
	    return this.bestMinValue;
	}

	public void setBestMinValue(Double bestMinValue) {
	    this.bestMinValue = bestMinValue;
	}


	@Column(name="best_mean_value", precision=17, scale=17)
	public Double getBestMeanValue() {
	    return this.bestMeanValue;
	}

	public void setBestMeanValue(Double bestMeanValue) {
	    this.bestMeanValue = bestMeanValue;
	}


	@Column(name="best_max_value", precision=17, scale=17)
	public Double getBestMaxValue() {
	    return this.bestMaxValue;
	}

	public void setBestMaxValue(Double bestMaxValue) {
	    this.bestMaxValue = bestMaxValue;
	}


	@Column(name="best_model")
	public String getBestModel() {
	    return this.bestModel;
	}

	public void setBestModel(String bestModel) {
	    this.bestModel = bestModel;
	}


	@Column(name="ow_qty", precision=17, scale=17)
	public Double getOwQty() {
	    return this.owQty;
	}

	public void setOwQty(Double owQty) {
	    this.owQty = owQty;
	}


	@Column(name="final_qty", precision=17, scale=17)
	public Double getFinalQty() {
	    return this.finalQty;
	}

	public void setFinalQty(Double finalQty) {
	    this.finalQty = finalQty;
	}


	}

