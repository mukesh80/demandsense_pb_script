package com.rplus.ds.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sys_param")
public class SystemParam implements java.io.Serializable {

	private static final long serialVersionUID = 1L;	
	private Integer rowNames;
	private Date currentTime;
	private Date minSaleDay;
	private Integer deltaReloadHorizon;
	private Integer fcstHorizon;
	private Integer activeHorizon;
	
	
		
	/**
	 * @return the rowNames
	 */
	@Id
	@Column(name = "row_names")
	public Integer getRowNames() {
		return rowNames;
	}
	/**
	 * @param rowNames the rowNames to set
	 */
	public void setRowNames(Integer rowNames) {
		this.rowNames = rowNames;
	}
	
	/**
	 * @return the currentTime
	 */	
	@Column(name = "cur_time")
	public Date getCurrentTime() {
		return currentTime;
	}
	/**
	 * @param currentTime the currentTime to set
	 */
	public void setCurrentTime(Date currentTime) {
		this.currentTime = currentTime;
	}
	/**
	 * @return the minSaleDay
	 */
	@Column(name = "min_saleday")
	public Date getMinSaleDay() {
		return minSaleDay;
	}
	/**
	 * @param minSaleDay the minSaleDay to set
	 */
	public void setMinSaleDay(Date minSaleDay) {
		this.minSaleDay = minSaleDay;
	}
	/**
	 * @return the deltaReloadHorizon
	 */
	@Column(name = "delta_reload_horizon")
	public Integer getDeltaReloadHorizon() {
		return deltaReloadHorizon;
	}
	/**
	 * @param deltaReloadHorizon the deltaReloadHorizon to set
	 */
	public void setDeltaReloadHorizon(Integer deltaReloadHorizon) {
		this.deltaReloadHorizon = deltaReloadHorizon;
	}
	/**
	 * @return the fcstHorizon
	 */
	@Column(name = "fcst_horizon")
	public Integer getFcstHorizon() {
		return fcstHorizon;
	}
	/**
	 * @param fcstHorizon the fcstHorizon to set
	 */
	public void setFcstHorizon(Integer fcstHorizon) {
		this.fcstHorizon = fcstHorizon;
	}
	/**
	 * @return the activeHorizon
	 */
	@Column(name = "active_horizon")
	public Integer getActiveHorizon() {
		return activeHorizon;
	}
	/**
	 * @param activeHorizon the activeHorizon to set
	 */
	public void setActiveHorizon(Integer activeHorizon) {
		this.activeHorizon = activeHorizon;
	}
	

	  
}
