package com.rplus.ds.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="task_track")
public class TaskTrack {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="task_track_run_id_seq")
    @SequenceGenerator(name="task_track_run_id_seq", sequenceName="task_track_run_id_seq", allocationSize=1)
	@Column(name="run_id")
	private Integer runId;
	
	@Column(name="task_seq_id")
	private String taskSeqId;
	
	@Column(name="task_start_time")
	private Date taskStartTime;
	
	@Column(name="task_end_time")
	private Date taskEndTime ;
	
	@Column(name="run_by")
	private String runBy;
	
	@Column(name="manual_auto")
	private String manualOrAuto;
	
	public Integer getRunId() {
		return runId;
	}

	public void setRunId(Integer runId) {
		this.runId = runId;
	}

	public String getTaskSeqId() {
		return taskSeqId;
	}

	public void setTaskSeqId(String taskSeqId) {
		this.taskSeqId = taskSeqId;
	}

	public Date getTaskStartTime() {
		return taskStartTime;
	}

	public void setTaskStartTime(Date taskStartTime) {
		this.taskStartTime = taskStartTime;
	}

	public Date getTaskEndTime() {
		return taskEndTime;
	}

	public void setTaskEndTime(Date taskEndTime) {
		this.taskEndTime = taskEndTime;
	}

	public String getRunBy() {
		return runBy;
	}

	public void setRunBy(String runBy) {
		this.runBy = runBy;
	}

	public String getManualOrAuto() {
		return manualOrAuto;
	}

	public void setManualOrAuto(String manualOrAuto) {
		this.manualOrAuto = manualOrAuto;
	}

	public Date getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(Date scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name="schedule_time")
	private Date scheduleTime;
	
	@Column(name="update_time")
	private Date updateTime;
	
	
}
