package com.rplus.ds.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="sql_job_group")
public class SQLJobGroup {

	@Column(name="id")
	private Integer id;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sql_job_group_id_seq")
    @SequenceGenerator(name="sql_job_group_id_seq", sequenceName="sql_job_group_id_seq", allocationSize=1)
	@Column(name="group_id")
	private String groupId;
	
	@Column(name="group_desc")
	private String groupDesc;
	
	@Column(name="sql_script_path")
	private String sqlScriptPath;
	
	@Column(name="last_update")
	private Date lastUpdate;
	
	@Column(name="sub_group")
	private String subGroup;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupDesc() {
		return groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

	public String getSqlScriptPath() {
		return sqlScriptPath;
	}

	public void setSqlScriptPath(String sqlScriptPath) {
		this.sqlScriptPath = sqlScriptPath;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getSubGroup() {
		return subGroup;
	}

	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}
	
	
	
	
}
