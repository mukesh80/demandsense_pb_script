package com.rplus.ds.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * DimProduct generated by hbm2java
 */
@Entity
@Table(name = "dim_product")
public class DimProduct implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String productId;
	private String productIdDescription;
	private String categoryId;
	private String categoryIdDescription;
	private String subcategoryId;
	private String subcategoryIdDescription;
	private String productType;
	private String productTypeDescription;

	public DimProduct() {
	}

	public DimProduct(String productId) {
		this.productId = productId;
	}

	public DimProduct(String productId, String productIdDescription,
			String categoryId, String categoryIdDescription,
			String subcategoryId, String subcategoryIdDescription,
			String productType, String productTypeDescription) {
		this.productId = productId;
		this.productIdDescription = productIdDescription;
		this.categoryId = categoryId;
		this.categoryIdDescription = categoryIdDescription;
		this.subcategoryId = subcategoryId;
		this.subcategoryIdDescription = subcategoryIdDescription;
		this.productType = productType;
		this.productTypeDescription = productTypeDescription;
	}

	@Id
	@Column(name = "product_id", unique = true, nullable = false, length = 15)
	public String getProductId() {
		return this.productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "product_id_desc", length = 50)
	public String getProductIdDescription() {
		return this.productIdDescription;
	}

	public void setProductIdDescription(String productIdDescription) {
		this.productIdDescription = productIdDescription;
	}

	@Column(name = "category_id", length = 15)
	public String getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name = "category_id_desc", length = 50)
	public String getCategoryIdDescription() {
		return this.categoryIdDescription;
	}

	public void setCategoryIdDescription(String categoryIdDescription) {
		this.categoryIdDescription = categoryIdDescription;
	}

	@Column(name = "sub_category_id", length = 15)
	public String getSubcategoryId() {
		return this.subcategoryId;
	}

	public void setSubcategoryId(String subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	@Column(name = "sub_category_id_desc", length = 50)
	public String getSubcategoryIdDescription() {
		return this.subcategoryIdDescription;
	}

	public void setSubcategoryIdDescription(String subcategoryIdDescription) {
		this.subcategoryIdDescription = subcategoryIdDescription;
	}

	@Column(name = "product_type", length = 15)
	public String getProductType() {
		return this.productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@Column(name = "product_type_desc", length = 50)
	public String getProductTypeDescription() {
		return this.productTypeDescription;
	}

	public void setProductTypeDescription(String productTypeDescription) {
		this.productTypeDescription = productTypeDescription;
	}

}
