package com.rplus.ds.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Rafeeq
 * 
 */
@Entity
@Table(name = "rplus_prod_loc_time_week")
public class ProdLocTimeByWeek implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	//private ProdLocTimeIdByWeek id;
	private String uid;
	private String countryId;
	private String regionId;
	private String stateId;
	private String districtId;
	private String cityId;
	private String categoryCode;
	private String subcategoryId;
	private Integer quantity;
	private BigDecimal price;
	private BigDecimal discount;
	private BigDecimal maxTemp;
	private BigDecimal minTemp;
	private BigDecimal precipitation;
	private BigDecimal humidity;
	private BigDecimal wind;
	private BigDecimal elevation;
	private BigDecimal feellikeTemp;
	private BigDecimal solar;
	private Integer tableCount;
	private Integer peopleMax;
	private BigDecimal locationArea;
	private BigDecimal sentimentIndex;
	private BigDecimal srs;
	private BigDecimal infentualScore;
	private BigDecimal latitude;
	private BigDecimal longitude;
	private BigDecimal gdp;
	private BigDecimal promotionFlag;
	private BigDecimal specialEvent;
	private String productId;
	private String locationId;
	private Integer yearWeek;
	// New fields added	
	private String locCategory;
	private String locAmbience;
	private String locationType;
	private String productType;
	private Double inflation;
	private Integer calWeek;

	public ProdLocTimeByWeek() {
	}	

	public ProdLocTimeByWeek(Integer yearWeek,String productId, String locationId,  String uid,
			String countryId, String regionId, String stateId,
			String districtId, String cityId, String categoryCode,
			String subcategoryId, Integer quantity, BigDecimal price,
			BigDecimal discount, BigDecimal maxTemp, BigDecimal minTemp,
			BigDecimal precipitation, BigDecimal humidity, BigDecimal wind,
			BigDecimal elevation, BigDecimal feellikeTemp, BigDecimal solar,
			Integer tableCount, Integer peopleMax, BigDecimal locationArea,
			BigDecimal sentimentIndex, BigDecimal srs,
			BigDecimal infentualScore, BigDecimal latitude,
			BigDecimal longitude, BigDecimal gdp, BigDecimal promotionFlag,
			BigDecimal specialEvent,String locCategory,
			String locAmbience, String locationType, String productType,
			Double inflation, Integer calWeek) {
		this.yearWeek = yearWeek;
		this.productId = productId;
		this.locationId = locationId;
		this.uid = uid;
		this.countryId = countryId;
		this.regionId = regionId;
		this.stateId = stateId;
		this.districtId = districtId;
		this.cityId = cityId;
		this.categoryCode = categoryCode;
		this.subcategoryId = subcategoryId;
		this.quantity = quantity;
		this.price = price;
		this.discount = discount;
		this.maxTemp = maxTemp;
		this.minTemp = minTemp;
		this.precipitation = precipitation;
		this.humidity = humidity;
		this.wind = wind;
		this.elevation = elevation;
		this.feellikeTemp = feellikeTemp;
		this.solar = solar;
		this.tableCount = tableCount;
		this.peopleMax = peopleMax;
		this.locationArea = locationArea;
		this.sentimentIndex = sentimentIndex;
		this.srs = srs;
		this.infentualScore = infentualScore;
		this.latitude = latitude;
		this.longitude = longitude;
		this.gdp = gdp;
		this.promotionFlag = promotionFlag;
		this.specialEvent = specialEvent;		
		this.locCategory = locCategory;
		this.locAmbience = locAmbience;
		this.locationType = locationType;
		this.productType = productType;
		this.inflation = inflation;
		this.calWeek = calWeek;

	}
	
	@Id
	@Column(name = "product_id")
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
	@Id
	@Column(name = "location_id")
	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	@Id
	@Column(name = "year_week")
	public Integer getYearWeek() {
		return yearWeek;
	}

	public void setYearWeek(Integer yearWeek) {
		this.yearWeek = yearWeek;
	}
	

	@Column(name = "uid", length = 100)
	public String getUid() {
		return this.uid;
	}
	
	public void setUid(String uid) {
		this.uid = uid;
	}

	@Column(name = "country_id", length = 15)
	public String getCountryId() {
		return this.countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	@Column(name = "region_id", length = 15)
	public String getRegionId() {
		return this.regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	@Column(name = "state_id", length = 15)
	public String getStateId() {
		return this.stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	@Column(name = "district_id", length = 15)
	public String getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}

	@Column(name = "city_id", length = 15)
	public String getCityId() {
		return this.cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	@Column(name = "category_code", length = 15)
	public String getCategoryCode() {
		return this.categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	@Column(name = "subcategory_id", length = 15)
	public String getSubcategoryId() {
		return this.subcategoryId;
	}

	public void setSubcategoryId(String subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	@Column(name = "quantity")
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name = "price", precision = 15, scale = 4)
	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Column(name = "discount", precision = 15, scale = 4)
	public BigDecimal getDiscount() {
		return this.discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	@Column(name = "max_temp", precision = 10, scale = 4)
	public BigDecimal getMaxTemp() {
		return this.maxTemp;
	}

	public void setMaxTemp(BigDecimal maxTemp) {
		this.maxTemp = maxTemp;
	}

	@Column(name = "min_temp", precision = 10, scale = 4)
	public BigDecimal getMinTemp() {
		return this.minTemp;
	}

	public void setMinTemp(BigDecimal minTemp) {
		this.minTemp = minTemp;
	}

	@Column(name = "precipitation", precision = 10, scale = 4)
	public BigDecimal getPrecipitation() {
		return this.precipitation;
	}

	public void setPrecipitation(BigDecimal precipitation) {
		this.precipitation = precipitation;
	}

	@Column(name = "humidity", precision = 10, scale = 4)
	public BigDecimal getHumidity() {
		return this.humidity;
	}

	public void setHumidity(BigDecimal humidity) {
		this.humidity = humidity;
	}

	@Column(name = "wind", precision = 10, scale = 4)
	public BigDecimal getWind() {
		return this.wind;
	}

	public void setWind(BigDecimal wind) {
		this.wind = wind;
	}

	@Column(name = "elevation", precision = 10, scale = 4)
	public BigDecimal getElevation() {
		return this.elevation;
	}

	public void setElevation(BigDecimal elevation) {
		this.elevation = elevation;
	}

	@Column(name = "feellike_temp", precision = 10, scale = 4)
	public BigDecimal getFeellikeTemp() {
		return this.feellikeTemp;
	}

	public void setFeellikeTemp(BigDecimal feellikeTemp) {
		this.feellikeTemp = feellikeTemp;
	}

	@Column(name = "solar", precision = 10, scale = 4)
	public BigDecimal getSolar() {
		return this.solar;
	}

	public void setSolar(BigDecimal solar) {
		this.solar = solar;
	}

	@Column(name = "table_count")
	public Integer getTableCount() {
		return this.tableCount;
	}

	public void setTableCount(Integer tableCount) {
		this.tableCount = tableCount;
	}

	@Column(name = "people_max")
	public Integer getPeopleMax() {
		return this.peopleMax;
	}

	public void setPeopleMax(Integer peopleMax) {
		this.peopleMax = peopleMax;
	}

	@Column(name = "location_area", precision = 131089, scale = 0)
	public BigDecimal getLocationArea() {
		return this.locationArea;
	}

	public void setLocationArea(BigDecimal locationArea) {
		this.locationArea = locationArea;
	}

	@Column(name = "sentiment_index", precision = 10, scale = 4)
	public BigDecimal getSentimentIndex() {
		return this.sentimentIndex;
	}

	public void setSentimentIndex(BigDecimal sentimentIndex) {
		this.sentimentIndex = sentimentIndex;
	}

	@Column(name = "srs", precision = 10, scale = 4)
	public BigDecimal getSrs() {
		return this.srs;
	}

	public void setSrs(BigDecimal srs) {
		this.srs = srs;
	}

	@Column(name = "infentual_score", precision = 10, scale = 4)
	public BigDecimal getInfentualScore() {
		return this.infentualScore;
	}

	public void setInfentualScore(BigDecimal infentualScore) {
		this.infentualScore = infentualScore;
	}

	@Column(name = "latitude", precision = 14, scale = 10)
	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	@Column(name = "longitude", precision = 14, scale = 10)
	public BigDecimal getLongitude() {
		return this.longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	@Column(name = "gdp", precision = 131089, scale = 0)
	public BigDecimal getGdp() {
		return this.gdp;
	}

	public void setGdp(BigDecimal gdp) {
		this.gdp = gdp;
	}

	@Column(name = "promotion_flag", precision = 131089, scale = 0)
	public BigDecimal getPromotionFlag() {
		return this.promotionFlag;
	}

	public void setPromotionFlag(BigDecimal promotionFlag) {
		this.promotionFlag = promotionFlag;
	}

	@Column(name = "special_event", precision = 131089, scale = 0)
	public BigDecimal getSpecialEvent() {
		return this.specialEvent;
	}

	public void setSpecialEvent(BigDecimal specialEvent) {
		this.specialEvent = specialEvent;
	}	

	@Column(name = "location_category")
	public String getLocCategory() {
		return locCategory;
	}

	public void setLocCategory(String locCategory) {
		this.locCategory = locCategory;
	}

	@Column(name = "location_ambience")
	public String getLocAmbience() {
		return locAmbience;
	}

	public void setLocAmbience(String locAmbience) {
		this.locAmbience = locAmbience;
	}

	@Column(name = "location_type")
	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	@Column(name = "product_type")
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@Column(name = "inflation")
	public Double getInflation() {
		return inflation;
	}

	public void setInflation(Double inflation) {
		this.inflation = inflation;
	}
	
	@Column(name = "cal_week")
	public Integer getCalWeek() {
		return calWeek;
	}

	public void setCalWeek(Integer calWeek) {
		this.calWeek = calWeek;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((locationId == null) ? 0 : locationId.hashCode());
		result = prime * result
				+ ((productId == null) ? 0 : productId.hashCode());
		result = prime * result
				+ ((yearWeek == null) ? 0 : yearWeek.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdLocTimeByWeek other = (ProdLocTimeByWeek) obj;
		if (locationId == null) {
			if (other.locationId != null)
				return false;
		} else if (!locationId.equals(other.locationId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (yearWeek == null) {
			if (other.yearWeek != null)
				return false;
		} else if (!yearWeek.equals(other.yearWeek))
			return false;
		return true;
	}
	
	

}
