package com.rplus.ds.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rplus_ds_output_sf_day")
public class RplusDsOutputSFDay implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String countryId;
	private String locationId;
	private String regionId;
	private String stateId;
	private String districtId;
	private String cityId;
	private String productId;
	private String categoryId;
	private String subCategoryId;
	private Integer yearWeek;
	private Integer calYear;
	private Integer calHalfyear;
	private Integer calQuarter;
	private Integer calMonth;
	private Integer calWeek;
	private String rowNames;
	private String timestamp;
	private String uId;
	private String significantFactors;
	private Double correlation; 
	private String model;
	private Date calDay;
	private Double owQty; 
	private Double finalQty;
	
	
	@Id
    @GeneratedValue
    @Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="country_id")
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	
	 @Column(name="location_id")
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	 @Column(name="region_id")
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	
	 @Column(name="state_id")
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	
	 @Column(name="district_id")
	public String getDistrictId() {
		return districtId;
	}
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}
	
	 @Column(name="city_id")
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	
	 @Column(name="product_id")
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	 @Column(name="category_id")
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	
	 @Column(name="sub_category_id")
	public String getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	
	 @Column(name="year_week")
	public Integer getYearWeek() {
		return yearWeek;
	}
	public void setYearWeek(Integer yearWeek) {
		this.yearWeek = yearWeek;
	}
	
	 @Column(name="cal_year")
	public Integer getCalYear() {
		return calYear;
	}
	public void setCalYear(Integer calYear) {
		this.calYear = calYear;
	}
	
	 @Column(name="cal_halfyear")
	public Integer getCalHalfyear() {
		return calHalfyear;
	}
	public void setCalHalfyear(Integer calHalfyear) {
		this.calHalfyear = calHalfyear;
	}
	
	 @Column(name="cal_quarter")
	public Integer getCalQuarter() {
		return calQuarter;
	}
	public void setCalQuarter(Integer calQuarter) {
		this.calQuarter = calQuarter;
	}
	
	 @Column(name="cal_month")
	public Integer getCalMonth() {
		return calMonth;
	}
	public void setCalMonth(Integer calMonth) {
		this.calMonth = calMonth;
	}
	
	 @Column(name="cal_week")
	public Integer getCalWeek() {
		return calWeek;
	}
	public void setCalWeek(Integer calWeek) {
		this.calWeek = calWeek;
	}
	
	 @Column(name="row_names") 
	public String getRowNames() {
		return rowNames;
	}
	public void setRowNames(String rowNames) {
		this.rowNames = rowNames;
	}
	 
	 @Column(name="timestamp")
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	 
	 @Column(name="uid")
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	
	 @Column(name="significantfactors")
	public String getSignificantFactors() {
		return significantFactors;
	}
	public void setSignificantFactors(String significantFactors) {
		this.significantFactors = significantFactors;
	}
	 
	 @Column(name="correlation")
	public Double getCorrelation() {
		return correlation;
	}
	public void setCorrelation(Double correlation) {
		this.correlation = correlation;
	}
	
	 @Column(name="model")
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	
	 @Column(name="cal_day")
	public Date getCalDay() {
		return calDay;
	}
	public void setCalDay(Date calDay) {
		this.calDay = calDay;
	}
	
	 @Column(name="ow_qty")
	public Double getOwQty() {
		return owQty;
	}
	public void setOwQty(Double owQty) {
		this.owQty = owQty;
	}
	
	 @Column(name="final_qty")
	public Double getFinalQty() {
		return finalQty;
	}
	public void setFinalQty(Double finalQty) {
		this.finalQty = finalQty;
	}
	
	
	
	
}
