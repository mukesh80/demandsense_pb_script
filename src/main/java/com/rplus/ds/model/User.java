package com.rplus.ds.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="rplus_dim_user_auth") //,schema="rplus_ds_common"
public class User {
 
 
 @Id
 @Column(name = "user_id", unique = true, nullable = false, length = 15)
 private String username;
 
 @Column(name="password",length=50,nullable = false)
 private String password;
 
 @Column(name="first_name",length=50,nullable = false)
 private String firstName;
 
 @Column(name="last_name",length=50,nullable = false)
 private String lastName;
 
 @Column(name="mail_id",length=50,nullable = false)
 private String emailId;
 
 @Column(name="location",length=50,nullable = true)
 private String location;
 
 @Column(name="birthday",nullable = true)
 private Date birthDate;
 
 @Column(name="gender",nullable = true)
 private String gender;
 
 @Column(name="mobile_phone",nullable=true)
 private String mobilePhone;
 
 @Column(name="auth_prod_text",nullable=true)
 private String authProdText;

 @Column(name="auth_loc_text",nullable=true)
 private String auth_loc_text;
 
 @Column(name="user_type")
 private String userType;
 
 @Column(name="schema_name")
 private String schemaName;
 
 
 public String getSchemaName() {
	return schemaName;
}

public void setSchemaName(String schemaName) {
	this.schemaName = schemaName;
}

public String getUserType() {
	return userType;
}

public void setUserType(String userType) {
	this.userType = userType;
}

public User(){}
 
 public User(int id,String username,String password,
   String firstName,String lastName,String emailId,String location,String productName)
  {
   this.username=username;
   this.password=password;
   this.firstName=firstName;
   this.lastName=lastName;
   this.emailId=emailId;
   this.location=location;
  }
 
 
 

 public String getUsername() {
  return username;
 }

 public void setUsername(String username) {
  this.username = username;
 }

 public String getPassword() {
  return password;
 }

 public void setPassword(String password) {
  this.password = password;
 }

 public String getFirstName() {
  return firstName;
 }

 public void setFirstName(String firsName) {
  this.firstName = firsName;
 }

 public String getLastName() {
  return lastName;
 }

 public void setLastName(String lastName) {
  this.lastName = lastName;
 }

 public String getEmailId() {
  return emailId;
 }

 public void setEmailId(String emailId) {
  this.emailId = emailId;
 }

 public String getLocation() {
  return location;
 }

 public void setLocation(String location) {
  this.location = location;
 }

 
 public Date getBirthDate() {
  return birthDate;
 }

 public void setBirthDate(Date birthDate) {
  this.birthDate = birthDate;
 }

 public String getGender() {
  return gender;
 }

 public void setGender(String gender) {
  this.gender = gender;
 }

 public String getMobilePhone() {
  return mobilePhone;
 }

 public void setMobilePhone(String mobilePhone) {
  this.mobilePhone = mobilePhone;
 }

 public String getAuthProdText() {
  return authProdText;
 }

 public void setAuthProdText(String authProdText) {
  this.authProdText = authProdText;
 }

 public String getAuth_loc_text() {
  return auth_loc_text;
 }

 public void setAuth_loc_text(String auth_loc_text) {
  this.auth_loc_text = auth_loc_text;
 }
 
 
}