package com.rplus.ds.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fact_audit1")
public class FactAuditReport implements java.io.Serializable {

	
	private static final long serialVersionUID = 1L;
	private Integer rownames;
	private String timestamp;
	private String granularity;
	private String tableName;
	private String tableId; // two new columns are added as vinesh said -tableId,columnId for rplus_ds_retail.fact_audit1
	private String columnName;	
	private String columnId;
	private Double noOfRows;
	private Double noOfColumns;
	private Double noOfNulls;
	private Double averageQuality;	
	private Double constValFields;
	private Double constValFieldsPercent;
	private Double outlierFields;
	private Double outlierFieldsPercent;
	private Double skewDistributionFields;
	private Double skewDistributionFieldsPercent;	
	private Double noOfZeros;	
	private Double mean;
	private Double median;
	private Double mode;
	private Double minimum;
	private Double maximum;	
	private Double standardDeviation;
	private Double variance;
	private Double iqr;
	private Double skewness;
	private Double kurtosis;
	private Double outlier;
	
	
	
	/**
	 * @return the row_names
	 */
	@Id
	@Column(name = "row_names")
	public Integer getRownames() {
		return rownames;
	}
	/**
	 * @param row_names the row_names to set
	 */
	public void setRownames(Integer rownames) {
		this.rownames = rownames;
	}
	/**
	 * @return the timestamp
	 */
	@Id
	@Column(name = "timestamp")
	public String getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}	
	
	/**
	 * @return the granularity
	 */
	@Column(name = "granularity")
	public String getGranularity() {
		return granularity;
	}
	/**
	 * @param granularity the granularity to set
	 */
	public void setGranularity(String granularity) {
		this.granularity = granularity;
	}
	/**
	 * @return the table_name
	 */
	@Column(name = "table_name")
	public String getTableName() {
		return tableName;
	}
	/**
	 * @param table_name the table_name to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	/**
	 * @return the column_name
	 */
	@Column(name = "column_name")
	public String getColumnName() {
		return columnName;
	}
	/**
	 * @param column_name the column_name to set
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	/**
	 * @return the no_of_rows
	 */
	@Column(name = "no_of_rows")
	public Double getNoOfRows() {
		return noOfRows;
	}
	/**
	 * @param no_of_rows the no_of_rows to set
	 */
	public void setNoOfRows(Double noOfRows) {
		this.noOfRows = noOfRows;
	}
	/**
	 * @return the no_of_columns
	 */
	@Column(name = "no_of_columns")
	public Double getNoOfColumns() {
		return noOfColumns;
	}
	/**
	 * @param no_of_columns the no_of_columns to set
	 */
	public void setNoOfColumns(Double noOfColumns) {
		this.noOfColumns = noOfColumns;
	}
	/**
	 * @return the no_of_nulls
	 */
	@Column(name = "no_of_nulls")
	public Double getNoOfNulls() {
		return noOfNulls;
	}
	/**
	 * @param no_of_nulls the no_of_nulls to set
	 */
	public void setNoOfNulls(Double noOfNulls) {
		this.noOfNulls = noOfNulls;
	}
	/**
	 * @return the average_quality
	 */
	@Column(name = "average_quality")
	public Double getAverageQuality() {
		return averageQuality;
	}
	/**
	 * @param average_quality the average_quality to set
	 */
	public void setAverageQuality(Double averageQuality) {
		this.averageQuality = averageQuality;
	}	
	/**
	 * @return the constValFields
	 */
	
	@Column(name = "constant_value_fields")
	public Double getConstValFields() {
		return constValFields;
	}
	/**
	 * @param constValFields the constValFields to set
	 */
	public void setConstValFields(Double constValFields) {
		this.constValFields = constValFields;
	}
	/**
	 * @return the constValFieldsPercent
	 */
	@Column(name = "constant_value_fields_percentage")
	public Double getConstValFieldsPercent() {
		return constValFieldsPercent;
	}
	/**
	 * @param constValFieldsPercent the constValFieldsPercent to set
	 */
	public void setConstValFieldsPercent(Double constValFieldsPercent) {
		this.constValFieldsPercent = constValFieldsPercent;
	}
	/**
	 * @return the outlierFields
	 */
	@Column(name = "outlier_fields")
	public Double getOutlierFields() {
		return outlierFields;
	}
	/**
	 * @param outlierFields the outlierFields to set
	 */
	public void setOutlierFields(Double outlierFields) {
		this.outlierFields = outlierFields;
	}
	/**
	 * @return the outlierFieldsPercent
	 */
	@Column(name = "outlier_fields_percentage")
	public Double getOutlierFieldsPercent() {
		return outlierFieldsPercent;
	}
	/**
	 * @param outlierFieldsPercent the outlierFieldsPercent to set
	 */
	public void setOutlierFieldsPercent(Double outlierFieldsPercent) {
		this.outlierFieldsPercent = outlierFieldsPercent;
	}
	/**
	 * @return the skewDistributionFields
	 */
	@Column(name = "skew_distribution_fields")
	public Double getSkewDistributionFields() {
		return skewDistributionFields;
	}
	/**
	 * @param skewDistributionFields the skewDistributionFields to set
	 */
	public void setSkewDistributionFields(Double skewDistributionFields) {
		this.skewDistributionFields = skewDistributionFields;
	}
	/**
	 * @return the skewDistributionFieldsPercent
	 */
	@Column(name = "skew_distribution_fields_percentage")
	public Double getSkewDistributionFieldsPercent() {
		return skewDistributionFieldsPercent;
	}
	/**
	 * @param skewDistributionFieldsPercent the skewDistributionFieldsPercent to set
	 */
	public void setSkewDistributionFieldsPercent(
			Double skewDistributionFieldsPercent) {
		this.skewDistributionFieldsPercent = skewDistributionFieldsPercent;
	}
		
	/**
	 * @return the no_of_zeros
	 */
	@Column(name = "no_of_zeros")
	public Double getNoOfZeros() {
		return noOfZeros;
	}
	/**
	 * @param no_of_zeros the no_of_zeros to set
	 */
	public void setNoOfZeros(Double noOfZeros) {
		this.noOfZeros = noOfZeros;
	}
	/**
	 * @return the mean
	 */
	@Column(name = "mean")
	public Double getMean() {
		return mean;
	}
	/**
	 * @param mean the mean to set
	 */
	public void setMean(Double mean) {
		this.mean = mean;
	}
	/**
	 * @return the median
	 */
	@Column(name = "median")
	public Double getMedian() {
		return median;
	}
	/**
	 * @param median the median to set
	 */
	public void setMedian(Double median) {
		this.median = median;
	}
	/**
	 * @return the mode
	 */
	@Column(name = "mode")
	public Double getMode() {
		return mode;
	}
	/**
	 * @param mode the mode to set
	 */
	public void setMode(Double mode) {
		this.mode = mode;
	}
	/**
	 * @return the minimum
	 */
	@Column(name = "minimum")
	public Double getMinimum() {
		return minimum;
	}
	/**
	 * @param minimum the minimum to set
	 */
	public void setMinimum(Double minimum) {
		this.minimum = minimum;
	}
	/**
	 * @return the maximum
	 */
	@Column(name = "maximum")
	public Double getMaximum() {
		return maximum;
	}
	/**
	 * @param maximum the maximum to set
	 */
	public void setMaximum(Double maximum) {
		this.maximum = maximum;
	}
	/**
	 * @return the standard_deviation
	 */
	@Column(name = "standard_deviation")
	public Double getStandardDeviation() {
		return standardDeviation;
	}
	/**
	 * @param standard_deviation the standard_deviation to set
	 */
	public void setStandardDeviation(Double standardDeviation) {
		this.standardDeviation = standardDeviation;
	}
	/**
	 * @return the variance
	 */
	@Column(name = "variance")
	public Double getVariance() {
		return variance;
	}
	/**
	 * @param variance the variance to set
	 */
	public void setVariance(Double variance) {
		this.variance = variance;
	}
	
	/**
	 * @return the iqr
	 */
	@Column(name = "iqr")
	public Double getIqr() {
		return iqr;
	}
	/**
	 * @param iqr the iqr to set
	 */
	public void setIqr(Double iqr) {
		this.iqr = iqr;
	}	
	/**
	 * @return the skewness
	 */
	@Column(name = "skewness")
	public Double getSkewness() {
		return skewness;
	}
	/**
	 * @param skewness the skewness to set
	 */
	public void setSkewness(Double skewness) {
		this.skewness = skewness;
	}
	/**
	 * @return the kurtosis
	 */
	@Column(name = "kurtosis")
	public Double getKurtosis() {
		return kurtosis;
	}
	/**
	 * @param kurtosis the kurtosis to set
	 */
	public void setKurtosis(Double kurtosis) {
		this.kurtosis = kurtosis;
	}
	/**
	 * @return the outlier
	 */
	@Column(name = "outlier")
	public Double getOutlier() {
		return outlier;
	}
	/**
	 * @param outlier the outlier to set
	 */
	public void setOutlier(Double outlier) {
		this.outlier = outlier;
	}
	
	@Column(name="table_id")
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	
	@Column(name="column_id")
	public String getColumnId() {
		return columnId;
	}
	public void setColumnId(String columnId) {
		this.columnId = columnId;
	}	
	
		

}
