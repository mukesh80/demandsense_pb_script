package com.rplus.ds.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dim_location")
public class LocationDimensions implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;
	private String locationId;
	private String locationIdDesc;
	private String locationPostCode;	
	private String stateId;
	private String stateIdDesc;
	private String regionId;
	private String regionIdDesc;
	private String districtId;
	private String districtIdDesc;
	private String cityId;
	private String cityIdDesc;
	private Integer locationArea;
	private String areaUnit;	
	private String locationCategory;
	private String locationAmbience;
	private String locationType;
	private String countryId;
	private String countryDesc;
	
	@Column(name = "country_id")
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}	
	@Column(name = "country_desc")
	public String getCountryDesc() {
		return countryDesc;
	}
	public void setCountryDesc(String countryDesc) {
		this.countryDesc = countryDesc;
	}
	@Id	
	@Column(name = "location_id")
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	@Column(name = "state_id")
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	@Column(name = "state_id_desc")
	public String getStateIdDesc() {
		return stateIdDesc;
	}
	public void setStateIdDesc(String stateIdDesc) {
		this.stateIdDesc = stateIdDesc;
	}
	@Column(name = "region_id")
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	@Column(name = "region_id_desc")
	public String getRegionIdDesc() {
		return regionIdDesc;
	}
	public void setRegionIdDesc(String regionIdDesc) {
		this.regionIdDesc = regionIdDesc;
	}
	@Column(name = "district_id")
	public String getDistrictId() {
		return districtId;
	}
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}
	@Column(name = "district_id_desc")
	public String getDistrictIdDesc() {
		return districtIdDesc;
	}
	public void setDistrictIdDesc(String districtIdDesc) {
		this.districtIdDesc = districtIdDesc;
	}
	@Column(name = "city_id")
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	@Column(name = "city_id_desc")
	public String getCityIdDesc() {
		return cityIdDesc;
	}
	public void setCityIdDesc(String cityIdDesc) {
		this.cityIdDesc = cityIdDesc;
	}
			
	@Column(name = "location_ambience")
	public String getLocationAmbience() {
		return locationAmbience;
	}
	
	public void setLocationAmbience(String locationAmbience) {
		this.locationAmbience = locationAmbience;
	}
	@Column(name = "location_type")
	public String getLocationType() {
		return locationType;
	}
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	/**
	 * @return the locationDesc
	 */
	@Column(name = "location_desc")
	public String getLocationIdDesc() {
		return locationIdDesc;
	}
	/**
	 * @param locationDesc the locationDesc to set
	 */
	public void setLocationIdDesc(String locationDesc) {
		this.locationIdDesc = locationDesc;
	}
	/**
	 * @return the locationPostCode
	 */
	@Column(name = "location_post_code")
	public String getLocationPostCode() {
		return locationPostCode;
	}
	/**
	 * @param locationPostCode the locationPostCode to set
	 */
	public void setLocationPostCode(String locationPostCode) {
		this.locationPostCode = locationPostCode;
	}
	/**
	 * @return the locationArea
	 */
	@Column(name = "location_area")
	public Integer getLocationArea() {
		return locationArea;
	}
	/**
	 * @param locationArea the locationArea to set
	 */
	public void setLocationArea(Integer locationArea) {
		this.locationArea = locationArea;
	}
	/**
	 * @return the areaUnit
	 */
	
	@Column(name = "area_unit")
	public String getAreaUnit() {
		return areaUnit;
	}
	/**
	 * @param areaUnit the areaUnit to set
	 */
	public void setAreaUnit(String areaUnit) {
		this.areaUnit = areaUnit;
	}
	/**
	 * @return the locationCategory
	 */
	@Column(name = "location_category")
	public String getLocationCategory() {
		return locationCategory;
	}
	/**
	 * @param locationCategory the locationCategory to set
	 */
	public void setLocationCategory(String locationCategory) {
		this.locationCategory = locationCategory;
	} 
	
	
	
	

}
