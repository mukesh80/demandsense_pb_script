package com.rplus.ds.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name="user_log")
public class UserLog implements java.io.Serializable{

	
	
	@Id
	@Column(name="user_log_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="user_log_user_log_id_seq")
    @SequenceGenerator(name="user_log_user_log_id_seq", sequenceName="user_log_user_log_id_seq", allocationSize=1)
	private Integer userLogId;
	
	public Integer getUserLogId() {
		return userLogId;
	}

	public void setUserLogId(Integer userLogId) {
		this.userLogId = userLogId;
	}

	@Column(name="user_name")
	private String userName;
	
	@Column(name="ip_address")
	private String ipAddress;
	
	@Column(name="time_stamp")
	@Temporal(TemporalType.TIMESTAMP) 
	private Date timestamp;
	
	@Column(name="activity")
	private String tabVisit;
	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date date) {
		this.timestamp = date;
	}

	public String getTabVisit() {
		return tabVisit;
	}

	public void setTabVisit(String tabVisit) {
		this.tabVisit = tabVisit;
	}

	
}
