package com.rplus.ds.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * Model Class for Weather in External Factors
 * @version 1.0.0	17 Aug 2015
 * @author Jerald Joseph
 */
public class Weather_Dt  implements Serializable{
	
	private Date date;
	private BigDecimal maxTemp;
	private BigDecimal minTemp;
	private BigDecimal precipitation;
	private BigDecimal feellikeTemp;
	private BigDecimal solar;
	
	public BigDecimal getMaxTemp() {
		return maxTemp;
	}
	public void setMaxTemp(BigDecimal maxTemp) {
		this.maxTemp = maxTemp;
	}
	public BigDecimal getMinTemp() {
		return minTemp;
	}
	public void setMinTemp(BigDecimal minTemp) {
		this.minTemp = minTemp;
	}
	public BigDecimal getPrecipitation() {
		return precipitation;
	}
	public void setPrecipitation(BigDecimal precipitation) {
		this.precipitation = precipitation;
	}
	public BigDecimal getFeellikeTemp() {
		return feellikeTemp;
	}
	public void setFeellikeTemp(BigDecimal feellikeTemp) {
		this.feellikeTemp = feellikeTemp;
	}
	public BigDecimal getSolar() {
		return solar;
	}
	public void setSolar(BigDecimal solar) {
		this.solar = solar;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Weather_Dt(BigDecimal maxTemp, BigDecimal minTemp, BigDecimal precipitation, BigDecimal feellikeTemp, BigDecimal solar, Date date)
	{
		this.feellikeTemp = feellikeTemp;
		this.maxTemp = maxTemp;
		this.minTemp = minTemp;
		this.precipitation = precipitation;
		this.solar = solar;
		this.date = date;
	}
	
	public String toString()
	{
		String RetVal = "MaxTemp = " + maxTemp + ", MinTemp = " + minTemp + ", Rain Fall = " + precipitation;
		return RetVal;
	}
}
