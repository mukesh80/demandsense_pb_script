package com.rplus.ds.model.upload;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Rafeeq
 * 
 */
@Entity(name = "com.rplus.ds.model.upload.DimProduct")
@Table(name = "product_invoice_pb_delta")
public class DimProduct implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String productId;
	private String productIdDesc;
	private String categoryId;
	private String categoryIdDesc;
	private String subcategoryId;
	private String subcategoryIdDescr;
	private String productType;
	private String productTypeDesc;
	private String noType;
	private String refProd;
	private String clusterId;
	private String prodAttr1;
	private String prodAttr2;
	private String prodAttr3;
	private String prodAttr4;
	private String prodAttr5;
	
	
	private Date productStartDate;
	
	
	

	
	@Column(name="product_start_date")
	public Date getProductStartDate() {
		return productStartDate;
	}

	public void setProductStartDate(Date productStartDate) {
		this.productStartDate = productStartDate;
	}

	@Id
	@Column(name = "product_id")
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "product_id_desc")
	public String getProductIdDesc() {
		return productIdDesc;
	}

	public void setProductIdDesc(String productIdDesc) {
		this.productIdDesc = productIdDesc;
	}

	@Column(name = "category_id")
	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name = "category_id_desc")
	public String getCategoryIdDesc() {
		return categoryIdDesc;
	}

	public void setCategoryIdDesc(String categoryIdDesc) {
		this.categoryIdDesc = categoryIdDesc;
	}

	@Column(name = "sub_category_id")
	public String getSubcategoryId() {
		return subcategoryId;
	}

	public void setSubcategoryId(String subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	@Column(name = "sub_category_id_desc")
	public String getSubcategoryIdDescr() {
		return subcategoryIdDescr;
	}

	public void setSubcategoryIdDescr(String subcategoryIdDescr) {
		this.subcategoryIdDescr = subcategoryIdDescr;
	}

	@Column(name = "product_type")
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@Column(name = "product_type_desc")
	public String getProductTypeDesc() {
		return productTypeDesc;
	}

	public void setProductTypeDesc(String productTypeDesc) {
		this.productTypeDesc = productTypeDesc;
	}

	@Column(name = "no_type")
	public String getNoType() {
		return noType;
	}

	public void setNoType(String noType) {
		this.noType = noType;
	}

	@Column(name = "ref_prod")
	public String getRefProd() {
		return refProd;
	}

	public void setRefProd(String refProd) {
		this.refProd = refProd;
	}

	@Column(name = "cluster_id")
	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	@Column(name = "prod_attr1")
	public String getProdAttr1() {
		return prodAttr1;
	}

	public void setProdAttr1(String prodAttr1) {
		this.prodAttr1 = prodAttr1;
	}

	@Column(name = "prod_attr2")
	public String getProdAttr2() {
		return prodAttr2;
	}

	public void setProdAttr2(String prodAttr2) {
		this.prodAttr2 = prodAttr2;
	}

	@Column(name = "prod_attr3")
	public String getProdAttr3() {
		return prodAttr3;
	}

	public void setProdAttr3(String prodAttr3) {
		this.prodAttr3 = prodAttr3;
	}

	@Column(name = "prod_attr4")
	public String getProdAttr4() {
		return prodAttr4;
	}

	public void setProdAttr4(String prodAttr4) {
		this.prodAttr4 = prodAttr4;
	}

	@Column(name = "prod_attr5")
	public String getProdAttr5() {
		return prodAttr5;
	}

	public void setProdAttr5(String prodAttr5) {
		this.prodAttr5 = prodAttr5;
	}

}
