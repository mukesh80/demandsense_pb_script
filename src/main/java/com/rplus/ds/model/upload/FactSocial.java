
package com.rplus.ds.model.upload;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Rafeeq
 * 
 */

@Entity
@Table(name = "social_invoice_pb_delta")
public class FactSocial implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	//private Long id;
	private String soicialId;
	private Date calDay;
	private Integer positiveSentiment;
	private Integer negativeSentiment;
	private Integer neutralSentiment;
	private Double infentualScore;
	private Double sentimentalIndex;
	private Double srs;
	
	/*@Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }
 
    public void setId(Long id) {
        this.id = id;
    }*/
	
	@Column(name = "soicial_id")
	public String getSoicialId() {
		return soicialId;
	}

	public void setSoicialId(String soicialId) {
		this.soicialId = soicialId;
	}
	
	@Id
	@Column(name = "cal_day")
	public Date getCalDay() {
		return calDay;
	}

	public void setCalDay(Date calDay) {
		this.calDay = calDay;
	}
	@Column(name = "positive_sentiment")
	public Integer getPositiveSentiment() {
		return positiveSentiment;
	}

	public void setPositiveSentiment(Integer positiveSentiment) {
		this.positiveSentiment = positiveSentiment;
	}
	@Column(name = "negative_sentiment")
	public Integer getNegativeSentiment() {
		return negativeSentiment;
	}

	public void setNegativeSentiment(Integer negativeSentiment) {
		this.negativeSentiment = negativeSentiment;
	}
	@Column(name = "neutral_sentiment")
	public Integer getNeutralSentiment() {
		return neutralSentiment;
	}

	public void setNeutralSentiment(Integer neutralSentiment) {
		this.neutralSentiment = neutralSentiment;
	}
	@Column(name = "infentual_score")
	public Double getInfentualScore() {
		return infentualScore;
	}

	public void setInfentualScore(Double infentualScore) {
		this.infentualScore = infentualScore;
	}
	@Column(name = "sentimental_index")
	public Double getSentimentalIndex() {
		return sentimentalIndex;
	}

	public void setSentimentalIndex(Double sentimentalIndex) {
		this.sentimentalIndex = sentimentalIndex;
	}
	@Column(name = "srs")
	public Double getSrs() {
		return srs;
	}

	public void setSrs(Double srs) {
		this.srs = srs;
	}

}
