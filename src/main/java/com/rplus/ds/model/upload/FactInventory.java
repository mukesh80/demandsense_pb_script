package com.rplus.ds.model.upload;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "inventory_invoice_pb_delta")
public class FactInventory implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	private String productId;//id
	private String  locationId;//id
	private Date  calDay;//id
	private String  stockType;
	private String  stockCategory;
	private Integer  totalStock;
	private String  tsUnit;
	@Id
	@Column(name="product_id")
	public String getProduct_id() {
		return productId;
	}
	public void setProduct_id(String product_id) {
		this.productId = product_id;
	}
	@Id
	@Column(name="location_id")
	public String getLocation_id() {
		return locationId;
	}
	public void setLocation_id(String location_id) {
		this.locationId = location_id;
	}
	@Id
	@Column(name="cal_day")
	public Date getCal_day() {
		return calDay;
	}
	public void setCal_day(Date cal_day) {
		this.calDay = cal_day;
	}
	@Column(name="stock_type")
	public String getStock_type() {
		return stockType;
	}
	public void setStock_type(String stock_type) {
		this.stockType = stock_type;
	}
	@Column(name="stock_category")
	public String getStock_category() {
		return stockCategory;
	}
	public void setStock_category(String stock_category) {
		this.stockCategory = stock_category;
	}
	@Column(name="total_stock")
	public Integer getTotal_stock() {
		return totalStock;
	}
	public void setTotal_stock(Integer total_stock) {
		this.totalStock = total_stock;
	}
	@Column(name="ts_unit")
	public String getTs_unit() {
		return tsUnit;
	}
	public void setTs_unit(String ts_unit) {
		this.tsUnit = ts_unit;
	}

}
