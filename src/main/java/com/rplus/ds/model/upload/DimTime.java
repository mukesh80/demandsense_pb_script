package com.rplus.ds.model.upload;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Rafeeq
 * 
 */

@Entity
@Table(name = "time_invoice_pb_delta")
public class DimTime implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	//private Long id;
	private Date calDay;
	private Integer calWeek;
	private Integer calMonth;
	private String calMonthDesc;
	private Integer calQuarter;
	private Integer calHalfYear;
	private Integer calYear;
	private String fiscalVar;
	private Date fisDay;
	private Integer fiscalWeek;
	private Integer fiscalMonth;
	private String fiscalMonthDesc;
	private Integer fiscalQuarter;
	private Integer fiscalHalfYear;
	private Integer fiscalYear;
	private Integer yearWeek;
	private Integer yearMonth;

	/*@Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }
 
    public void setId(Long id) {
        this.id = id;
    }*/
	
	@Id
	@Column(name = "cal_day")
	public Date getCalDay() {
		return calDay;
	}

	public void setCalDay(Date calDay) {
		this.calDay = calDay;
	}

	@Column(name = "cal_week")
	public Integer getCalWeek() {
		return calWeek;
	}

	public void setCalWeek(Integer calWeek) {
		this.calWeek = calWeek;
	}

	@Column(name = "cal_month")
	public Integer getCalMonth() {
		return calMonth;
	}

	public void setCalMonth(Integer calMonth) {
		this.calMonth = calMonth;
	}

	@Column(name = "cal_month_desc")
	public String getCalMonthDesc() {
		return calMonthDesc;
	}

	public void setCalMonthDesc(String calMonthDesc) {
		this.calMonthDesc = calMonthDesc;
	}

	@Column(name = "cal_quarter")
	public Integer getCalQuarter() {
		return calQuarter;
	}

	public void setCalQuarter(Integer calQuarter) {
		this.calQuarter = calQuarter;
	}

	@Column(name = "cal_halfyear")
	public Integer getCalHalfYear() {
		return calHalfYear;
	}

	public void setCalHalfYear(Integer calHalfYear) {
		this.calHalfYear = calHalfYear;
	}

	@Column(name = "cal_year")
	public Integer getCalYear() {
		return calYear;
	}

	public void setCalYear(Integer calYear) {
		this.calYear = calYear;
	}

	@Column(name = "fis_var")
	public String getFiscalVar() {
		return fiscalVar;
	}

	public void setFiscalVar(String fiscalVar) {
		this.fiscalVar = fiscalVar;
	}

	@Column(name = "fis_day")
	public Date getFisDay() {
		return fisDay;
	}

	public void setFisDay(Date fisDay) {
		this.fisDay = fisDay;
	}

	@Column(name = "fis_week")
	public Integer getFiscalMonth() {
		return fiscalMonth;
	}

	public void setFiscalMonth(Integer fiscalMonth) {
		this.fiscalMonth = fiscalMonth;
	}

	@Column(name = "fis_month")
	public Integer getFiscalWeek() {
		return fiscalWeek;
	}

	public void setFiscalWeek(Integer fiscalWeek) {
		this.fiscalWeek = fiscalWeek;
	}

	@Column(name = "fis_month_desc")
	public String getFiscalMonthDesc() {
		return fiscalMonthDesc;
	}

	public void setFiscalMonthDesc(String fiscalMonthDesc) {
		this.fiscalMonthDesc = fiscalMonthDesc;
	}

	@Column(name = "fis_quarter")
	public Integer getFiscalQuarter() {
		return fiscalQuarter;
	}

	public void setFiscalQuarter(Integer fiscalQuarter) {
		this.fiscalQuarter = fiscalQuarter;
	}

	@Column(name = "fis_halfyear")
	public Integer getFiscalHalfYear() {
		return fiscalHalfYear;
	}

	public void setFiscalHalfYear(Integer fiscalHalfYear) {
		this.fiscalHalfYear = fiscalHalfYear;
	}

	@Column(name = "fis_year")
	public Integer getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(Integer fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	@Column(name = "year_week")
	public Integer getYearWeek() {
		return yearWeek;
	}

	public void setYearWeek(Integer yearWeek) {
		this.yearWeek = yearWeek;
	}

	@Column(name = "year_month")
	public Integer getYearMonth() {
		return yearMonth;
	}

	public void setYearMonth(Integer yearMonth) {
		this.yearMonth = yearMonth;
	}

}
