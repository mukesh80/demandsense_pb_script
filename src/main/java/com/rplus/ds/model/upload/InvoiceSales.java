package com.rplus.ds.model.upload;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "sales_invoice_pb_delta")
public class InvoiceSales  implements java.io.Serializable{

		
	@Id
	@Column(name="slno")
	private Integer slNo;
	
	@Column(name="invoice_id")
	private String invoiceId;
	
	@Column(name="bill_no")
	private Integer billNo;
	
	@Column(name="order_id")
	private Integer orderId;
	
	@Column(name="pjpoutlet")
	private Integer pjpOutlet;
	
	@Column(name="sales_date")
	private Date salesDate;
	
	@Column(name="sales_time")
	private String salesTime;
	
	@Column(name="outlet_id")
	private String outletId;
	
	@Column(name="outlet")
	private String outlet;
	
	
	@Column(name = "product_id")
	private String productId;
	
	@Column(name="address")
	private String address;
	
	@Column(name="type")
	private String type;
	
	@Column(name="latitude")
	private Double latitude;
	
	@Column(name="longitude")
	private Double longitude;
	
	@Column(name="phone_no")
	private String phoneNo;
	
	@Column(name="email")
	private String email;
	
	@Column(name="owner_name")
	private String ownerName;
	
	@Column(name="mobile_no")
	private String mobileNo;
	
	@Column(name="city")
	private String city;
	
	@Column(name="state")
	private String state;
	
	@Column(name="pin")
	private Integer pin;
	
	@Column(name="area")
	private String area;
	
	@Column(name="warehouse")
	private String warehouse;
	
	@Column(name="user_name")
	private String user;
	
	@Column(name="emp_id")
	private String empId;
	
	@Column(name="fordate")
	private String fordate;
	
	@Column(name="sku_placed")
	private String skuPlaced;
	
	@Column(name="sku_code")
	private String skuCode;
	
	@Column(name="category")
	private String category;
	
	@Column(name="quantity")
	private Double quantity;
	
	@Column(name="scheme_rate")
	private Double schemeRate;
	
	@Column(name="qps_scheme")
	private Integer qpsScheme;
	
	@Column(name="discount_scheme")
	private Double discountScheme;
	
	@Column(name="unit_price")
	private Double unitPrice;
	
	@Column(name="amount")
	private Double amount;
	
	@Column(name="beat")
	private String beat;
	
	@Column(name="no_of_picture")
	private Integer noOfPicture;
	
	@Column(name="approved_display_for_qpds")
	private String approvedDisplayForQpds;
	
	@Column(name="reject_display_for_qpds")
	private String rejectDisplayForQpds;
	
	@Column(name="existing_outletcode")
	private String existingOutletcode;
	
	@Column(name="is_hector")
	private String isHector;
	

	public InvoiceSales() {
		
	}

	
	
	
	public InvoiceSales(Integer slNo, String invoiceId, Integer billNo,
			Integer orderId, Integer pjpOutlet, Date salesDate,
			String salesTime, String outletId, String outlet, String productId,
			String address, String type, Double latitude, Double longitude,
			String phoneNo, String email, String ownerName, String mobileNo,
			String city, String state, Integer pin, String area,
			String warehouse, String user, String empId, String fordate,
			String skuPlaced, String skuCode, String category, Double quantity,
			Double schemeRate, Integer qpsScheme, Double discountScheme,
			Double unitPrice, Double amount, String beat, Integer noOfPicture,
			String approvedDisplayForQpds, String rejectDisplayForQpds,
			String existingOutletcode, String isHector) {
		super();
		this.slNo = slNo;
		this.invoiceId = invoiceId;
		this.billNo = billNo;
		this.orderId = orderId;
		this.pjpOutlet = pjpOutlet;
		this.salesDate = salesDate;
		this.salesTime = salesTime;
		this.outletId = outletId;
		this.outlet = outlet;
		this.productId = productId;
		this.address = address;
		this.type = type;
		this.latitude = latitude;
		this.longitude = longitude;
		this.phoneNo = phoneNo;
		this.email = email;
		this.ownerName = ownerName;
		this.mobileNo = mobileNo;
		this.city = city;
		this.state = state;
		this.pin = pin;
		this.area = area;
		this.warehouse = warehouse;
		this.user = user;
		this.empId = empId;
		this.fordate = fordate;
		this.skuPlaced = skuPlaced;
		this.skuCode = skuCode;
		this.category = category;
		this.quantity = quantity;
		this.schemeRate = schemeRate;
		this.qpsScheme = qpsScheme;
		this.discountScheme = discountScheme;
		this.unitPrice = unitPrice;
		this.amount = amount;
		this.beat = beat;
		this.noOfPicture = noOfPicture;
		this.approvedDisplayForQpds = approvedDisplayForQpds;
		this.rejectDisplayForQpds = rejectDisplayForQpds;
		this.existingOutletcode = existingOutletcode;
		this.isHector = isHector;
	}




	public String getProductId() {
		return productId;
	}




	public void setProductId(String productId) {
		this.productId = productId;
	}




	public Integer getSlNo() {
		return slNo;
	}




	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}




	public String getInvoiceId() {
		return invoiceId;
	}




	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}




	public Integer getBillNo() {
		return billNo;
	}




	public void setBillNo(Integer billNo) {
		this.billNo = billNo;
	}




	public Integer getOrderId() {
		return orderId;
	}




	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}




	public Integer getPjpOutlet() {
		return pjpOutlet;
	}




	public void setPjpOutlet(Integer pjpOutlet) {
		this.pjpOutlet = pjpOutlet;
	}




	public Date getSalesDate() {
		return salesDate;
	}




	public void setSalesDate(Date salesDate) {
		this.salesDate = salesDate;
	}




	public String getSalesTime() {
		return salesTime;
	}




	public void setSalesTime(String salesTime) {
		this.salesTime = salesTime;
	}




	public String getOutletId() {
		return outletId;
	}




	public void setOutletId(String outletId) {
		this.outletId = outletId;
	}




	public String getOutlet() {
		return outlet;
	}




	public void setOutlet(String outlet) {
		this.outlet = outlet;
	}




	public String getAddress() {
		return address;
	}




	public void setAddress(String address) {
		this.address = address;
	}




	public String getType() {
		return type;
	}




	public void setType(String type) {
		this.type = type;
	}




	public Double getLatitude() {
		return latitude;
	}




	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}




	public Double getLongitude() {
		return longitude;
	}




	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}




	public String getPhoneNo() {
		return phoneNo;
	}




	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}




	public String getOwnerName() {
		return ownerName;
	}




	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}




	public String getMobileNo() {
		return mobileNo;
	}




	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}




	public String getCity() {
		return city;
	}




	public void setCity(String city) {
		this.city = city;
	}




	public String getState() {
		return state;
	}




	public void setState(String state) {
		this.state = state;
	}




	public Integer getPin() {
		return pin;
	}




	public void setPin(Integer pin) {
		this.pin = pin;
	}




	public String getArea() {
		return area;
	}




	public void setArea(String area) {
		this.area = area;
	}




	public String getWarehouse() {
		return warehouse;
	}




	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}




	public String getUser() {
		return user;
	}




	public void setUser(String user) {
		this.user = user;
	}




	public String getEmpId() {
		return empId;
	}




	public void setEmpId(String empId) {
		this.empId = empId;
	}




	public String getFordate() {
		return fordate;
	}




	public void setFordate(String fordate) {
		this.fordate = fordate;
	}




	public String getSkuPlaced() {
		return skuPlaced;
	}




	public void setSkuPlaced(String skuPlaced) {
		this.skuPlaced = skuPlaced;
	}




	public String getSkuCode() {
		return skuCode;
	}




	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}




	public String getCategory() {
		return category;
	}




	public void setCategory(String category) {
		this.category = category;
	}




	public Double getQuantity() {
		return quantity;
	}




	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}




	public Double getSchemeRate() {
		return schemeRate;
	}




	public void setSchemeRate(Double schemeRate) {
		this.schemeRate = schemeRate;
	}




	public Integer getQpsScheme() {
		return qpsScheme;
	}




	public void setQpsScheme(Integer qpsScheme) {
		this.qpsScheme = qpsScheme;
	}




	public Double getDiscountScheme() {
		return discountScheme;
	}




	public void setDiscountScheme(Double discountScheme) {
		this.discountScheme = discountScheme;
	}




	public Double getUnitPrice() {
		return unitPrice;
	}




	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}




	public Double getAmount() {
		return amount;
	}




	public void setAmount(Double amount) {
		this.amount = amount;
	}




	public String getBeat() {
		return beat;
	}




	public void setBeat(String beat) {
		this.beat = beat;
	}




	public Integer getNoOfPicture() {
		return noOfPicture;
	}




	public void setNoOfPicture(Integer noOfPicture) {
		this.noOfPicture = noOfPicture;
	}




	public String getApprovedDisplayForQpds() {
		return approvedDisplayForQpds;
	}




	public void setApprovedDisplayForQpds(String approvedDisplayForQpds) {
		this.approvedDisplayForQpds = approvedDisplayForQpds;
	}




	public String getRejectDisplayForQpds() {
		return rejectDisplayForQpds;
	}




	public void setRejectDisplayForQpds(String rejectDisplayForQpds) {
		this.rejectDisplayForQpds = rejectDisplayForQpds;
	}




	public String getExistingOutletcode() {
		return existingOutletcode;
	}




	public void setExistingOutletcode(String existingOutletcode) {
		this.existingOutletcode = existingOutletcode;
	}




	public String getIsHector() {
		return isHector;
	}




	public void setIsHector(String isHector) {
		this.isHector = isHector;
	}

	

	
	
}
