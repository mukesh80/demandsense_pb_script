package com.rplus.ds.model.upload;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer_invoice_pb_delta")
public class DimCustomer implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	
	private String customeId;
	private String customerIdDesc;
	private String loyaltycardId;
	private String countryId;
	private String postcode;
	private Date dob;
	private Integer age;
	private Integer contactNo;
	private String mailId;
	private String favPd1;
	private String favPd2;
	private String custChar1;
	private String custChar2;
	private String custChar3;
	private Integer custKf1;
	private Integer custKf2;
	private Integer custKf3;
	
	@Id
	@Column(name = "customer_id")
	public String getCustomeId() {
		return customeId;
	}
	public void setCustomeId(String customeId) {
		this.customeId = customeId;
	}
	
	@Column(name = "customer_id_desc")
	public String getCustomerIdDesc() {
		return customerIdDesc;
	}
	public void setCustomerIdDesc(String customerIdDesc) {
		this.customerIdDesc = customerIdDesc;
	}
	@Column(name = "loyaltycard_id")
	public String getLoyaltycardId() {
		return loyaltycardId;
	}
	public void setLoyaltycardId(String loyaltycardId) {
		this.loyaltycardId = loyaltycardId;
	}
	@Column(name = "country_id")
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	@Column(name = "postcode")
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	
	@Column(name = "dob")
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	@Column(name = "age")
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	@Column(name = "contact_no")
	public Integer getContactNo() {
		return contactNo;
	}
	public void setContactNo(Integer contactNo) {
		this.contactNo = contactNo;
	}
	@Column(name = "mail_id")
	public String getMailId() {
		return mailId;
	}
	public void setMailId(String mailId) {
		this.mailId = mailId;
	}
	@Column(name = "fav_pd1")
	public String getFavPd1() {
		return favPd1;
	}
	public void setFavPd1(String favPd1) {
		this.favPd1 = favPd1;
	}
	@Column(name = "fav_pd2")
	public String getFavPd2() {
		return favPd2;
	}
	public void setFavPd2(String favPd2) {
		this.favPd2 = favPd2;
	}
	@Column(name = "cust_char1")
	public String getCustChar1() {
		return custChar1;
	}
	public void setCustChar1(String custChar1) {
		this.custChar1 = custChar1;
	}
	@Column(name = "cust_char2")
	public String getCustChar2() {
		return custChar2;
	}
	public void setCustChar2(String custChar2) {
		this.custChar2 = custChar2;
	}
	@Column(name = "cust_char3")
	public String getCustChar3() {
		return custChar3;
	}
	public void setCustChar3(String custChar3) {
		this.custChar3 = custChar3;
	}
	@Column(name = "cust_kf1")
	public Integer getCustKf1() {
		return custKf1;
	}
	public void setCustKf1(Integer custKf1) {
		this.custKf1 = custKf1;
	}
	@Column(name = "cust_kf2")
	public Integer getCustKf2() {
		return custKf2;
	}
	public void setCustKf2(Integer custKf2) {
		this.custKf2 = custKf2;
	}
	@Column(name = "cust_kf3")
	public Integer getCustKf3() {
		return custKf3;
	}
	public void setCustKf3(Integer custKf3) {
		this.custKf3 = custKf3;
	}
	
	



}
