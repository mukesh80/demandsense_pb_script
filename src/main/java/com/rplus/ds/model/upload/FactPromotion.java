package com.rplus.ds.model.upload;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "promotion_invoice_pb_delta")
public class FactPromotion implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	private String productId;//id
	private String  locationId;//id
	private String  promoId;//id
	private Date  validFrom;
	private Date  validTo;
	private String  promoType;
	private String  promoCategory;
	private Double  discount;
	private String  promoDesc;
	private Integer  estimatedSalesQty;
	@Id
	@Column(name="product_id")
	public String getProduct_id() {
		return productId;
	}
	public void setProduct_id(String product_id) {
		this.productId = product_id;
	}
	@Id
	@Column(name="location_id")
	public String getLocation_id() {
		return locationId;
	}
	public void setLocation_id(String location_id) {
		this.locationId = location_id;
	}
	@Id
	@Column(name="promo_id")
	public String getPromo_id() {
		return promoId;
	}
	public void setPromo_id(String promo_id) {
		this.promoId = promo_id;
	}
	@Column(name="valid_from")
	public Date getValid_from() {
		return validFrom;
	}
	public void setValid_from(Date valid_from) {
		this.validFrom = valid_from;
	}
	@Column(name="valid_to")
	public Date getValid_to() {
		return validTo;
	}
	public void setValid_to(Date valid_to) {
		this.validTo = valid_to;
	}
	@Column(name="promo_type")
	public String getPromo_type() {
		return promoType;
	}
	public void setPromo_type(String promo_type) {
		this.promoType = promo_type;
	}
	@Column(name="promo_category")
	public String getPromo_category() {
		return promoCategory;
	}
	public void setPromo_category(String promo_category) {
		this.promoCategory = promo_category;
	}
	@Column(name="discount")
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	@Column(name="promo_desc")
	public String getPromo_desc() {
		return promoDesc;
	}
	public void setPromo_desc(String promo_desc) {
		this.promoDesc = promo_desc;
	}
	@Column(name="estimated_sales_qty")
	public Integer getEstimated_sales_qty() {
		return estimatedSalesQty;
	}
	public void setEstimated_sales_qty(Integer estimated_sales_qty) {
		this.estimatedSalesQty = estimated_sales_qty;
	}
	
	  

}
