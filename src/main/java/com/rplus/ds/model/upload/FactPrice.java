package com.rplus.ds.model.upload;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "price_invoice_pb_delta")
public class FactPrice implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	private String productId;//id
	private String  locationId;//id
	private Date  calDay;//id
	private Double price;
	private String priceCurrency;
	@Id
	@Column(name="product_id")
	public String getProduct_id() {
		return productId;
	}
	public void setProduct_id(String product_id) {
		this.productId = product_id;
	}
	@Id
	@Column(name="location_id")
	public String getLocation_id() {
		return locationId;
	}
	public void setLocation_id(String location_id) {
		this.locationId = location_id;
	}
	@Id
	@Column(name="cal_day")
	public Date getCal_day() {
		return calDay;
	}
	public void setCal_day(Date cal_day) {
		this.calDay = cal_day;
	}
	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Column(name="price_currency")
	public String getPrice_currency() {
		return priceCurrency;
	}
	public void setPrice_currency(String price_currency) {
		this.priceCurrency = price_currency;
	}
	
	

}
