package com.rplus.ds.model.upload;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "event_invoice_pb_delta")
public class FactEvent implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Date calDay;
	private Double longitude;//double
    private Double latitude;//double
	private String postcode;
	private String eventType;
	private String  locationId;//id
    private Boolean eventIndicator;
    private Boolean holidayIndicator;
    private Boolean specialIndicator;
    private String dayType;
    private String calDayText;
	private Integer calDayId;//id
	private Integer weekDay;
	
	@Column(name = "cal_day")
	public Date getCal_day() {
		return calDay;
	}
	
	public void setCal_day(Date cal_day) {
		this.calDay = cal_day;
	}
	@Column(name = "longitude")
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double integer) {
		this.longitude = integer;
	}
	@Column(name = "latitude")
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double integer) {
		this.latitude = integer;
	}
	@Column(name = "postcode")
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	@Column(name = "event_type")
	public String getEvent_type() {
		return eventType;
	}
	public void setEvent_type(String event_type) {
		this.eventType = event_type;
	}
	@Id
	@Column(name = "location_id")
	public String getLocation_id() {
		return locationId;
	}
	public void setLocation_id(String location_id) {
		this.locationId = location_id;
	}
	@Column(name="event_indicator")
	public Boolean getEvent_indicator() {
		return eventIndicator;
	}
	public void setEvent_indicator(Boolean event_indicator) {
		this.eventIndicator = event_indicator;
	}
	@Column(name = "holiday_indicator")
	public Boolean getHoliday_indicator() {
		return holidayIndicator;
	}
	public void setHoliday_indicator(Boolean holiday_indicator) {
		this.holidayIndicator = holiday_indicator;
	}
	@Column(name= "special_indicator")
	public Boolean getSpecial_indicator() {
		return specialIndicator;
	}
	public void setSpecial_indicator(Boolean special_indicator) {
		this.specialIndicator = special_indicator;
	}
	@Column(name="day_type")
	public String getDay_type() {
		return dayType;
	}
	public void setDay_type(String day_type) {
		this.dayType = day_type;
	}
	@Column(name="cal_day_text")
	public String getCal_day_text() {
		return calDayText;
	}
	public void setCal_day_text(String cal_day_text) {
		this.calDayText = cal_day_text;
	}
	@Id
	@Column(name = "cal_day_id")
	public Integer getCal_day_id() {
		return calDayId;
	}
	public void setCal_day_id(Integer cal_day_id) {
		this.calDayId = cal_day_id;
	}
	@Column(name="week_day")
	public Integer getWeek_day() {
		return weekDay;
	}
	public void setWeek_day(Integer week_day) {
		this.weekDay = week_day;
	}
	
	
	  

}
