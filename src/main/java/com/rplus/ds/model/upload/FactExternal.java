package com.rplus.ds.model.upload;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Rafeeq
 * 
 */

@Entity
@Table(name = "external_invoice_pb_delta")
public class FactExternal implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	//private Long id;
	private Double longitude;
	private Double latitude;
	private String postcode;
	private Date calDay;
	private String season;
	private Double maxTemp;
	private String maxTempUnit;
	private Double minTemp;
	private String minTempUnit;
	private Double avgTemp;
	private String avgTempUnit;
	private Double rainfall;
	private String rainfallUnit;
	private Double wind;
	private String windUnit;
	private Double feellikeTemp;
	private String feellikeTempUnit;
	private Double density;
	private String densityUnit;
	private Double gdp;
	private String gdpUnit;
	private Double incomeLevel;
	private String incomeLevelUnit;
	private Double elevation;
	private String elevationUnit;
	private Double precipitation;
	private String precipitationUnit;
	private Double relative_humidity;
	private String relativeHumidityUnit;
	private Double solar;
	private String solarUnit;

	/*@Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }
 
    public void setId(Long id) {
        this.id = id;
    }*/
	
	@Id
	@Column(name = "longitude")
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	@Id
	@Column(name = "latitude")
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	@Column(name = "postcode")
	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	
	@Id
	@Column(name = "cal_day")
	public Date getCalDay() {
		return calDay;
	}

	public void setCalDay(Date calDay) {
		this.calDay = calDay;
	}
	@Column(name = "season")
	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}
	@Column(name = "max_temp")
	public Double getMaxTemp() {
		return maxTemp;
	}

	public void setMaxTemp(Double maxTemp) {
		this.maxTemp = maxTemp;
	}
	@Column(name = "max_temp_unit")
	public String getMaxTempUnit() {
		return maxTempUnit;
	}

	public void setMaxTempUnit(String maxTempUnit) {
		this.maxTempUnit = maxTempUnit;
	}
	@Column(name = "min_temp")
	public Double getMinTemp() {
		return minTemp;
	}

	public void setMinTemp(Double minTemp) {
		this.minTemp = minTemp;
	}
	@Column(name = "min_temp_unit")
	public String getMinTempUnit() {
		return minTempUnit;
	}

	public void setMinTempUnit(String minTempUnit) {
		this.minTempUnit = minTempUnit;
	}
	@Column(name = "avg_temp")
	public Double getAvgTemp() {
		return avgTemp;
	}

	public void setAvgTemp(Double avgTemp) {
		this.avgTemp = avgTemp;
	}
	@Column(name = "avg_temp_unit")
	public String getAvgTempUnit() {
		return avgTempUnit;
	}

	public void setAvgTempUnit(String avgTempUnit) {
		this.avgTempUnit = avgTempUnit;
	}
	@Column(name = "rainfall")
	public Double getRainfall() {
		return rainfall;
	}

	public void setRainfall(Double rainfall) {
		this.rainfall = rainfall;
	}
	@Column(name = "rainfall_unit")
	public String getRainfallUnit() {
		return rainfallUnit;
	}

	public void setRainfallUnit(String rainfallUnit) {
		this.rainfallUnit = rainfallUnit;
	}
	@Column(name = "wind")
	public Double getWind() {
		return wind;
	}

	public void setWind(Double wind) {
		this.wind = wind;
	}
	@Column(name = "wind_unit")
	public String getWindUnit() {
		return windUnit;
	}

	public void setWindUnit(String windUnit) {
		this.windUnit = windUnit;
	}
	@Column(name = "feellike_temp")
	public Double getFeellikeTemp() {
		return feellikeTemp;
	}

	public void setFeellikeTemp(Double feellikeTemp) {
		this.feellikeTemp = feellikeTemp;
	}
	@Column(name = "feellike_temp_unit")
	public String getFeellikeTempUnit() {
		return feellikeTempUnit;
	}

	public void setFeellikeTempUnit(String feellikeTempUnit) {
		this.feellikeTempUnit = feellikeTempUnit;
	}
	@Column(name = "density")
	public Double getDensity() {
		return density;
	}

	public void setDensity(Double density) {
		this.density = density;
	}
	@Column(name = "density_unit")
	public String getDensityUnit() {
		return densityUnit;
	}

	public void setDensityUnit(String densityUnit) {
		this.densityUnit = densityUnit;
	}
	@Column(name = "gdp")
	public Double getGdp() {
		return gdp;
	}

	public void setGdp(Double gdp) {
		this.gdp = gdp;
	}
	@Column(name = "gdp_unit")
	public String getGdpUnit() {
		return gdpUnit;
	}

	public void setGdpUnit(String gdpUnit) {
		this.gdpUnit = gdpUnit;
	}
	@Column(name = "income_level")
	public Double getIncomeLevel() {
		return incomeLevel;
	}

	public void setIncomeLevel(Double incomeLevel) {
		this.incomeLevel = incomeLevel;
	}
	@Column(name = "income_level_unit")
	public String getIncomeLevelUnit() {
		return incomeLevelUnit;
	}

	public void setIncomeLevelUnit(String incomeLevelUnit) {
		this.incomeLevelUnit = incomeLevelUnit;
	}
	@Column(name = "elevation")
	public Double getElevation() {
		return elevation;
	}

	public void setElevation(Double elevation) {
		this.elevation = elevation;
	}
	@Column(name = "elevation_unit")
	public String getElevationUnit() {
		return elevationUnit;
	}

	public void setElevationUnit(String elevationUnit) {
		this.elevationUnit = elevationUnit;
	}
	@Column(name = "precipitation")
	public Double getPrecipitation() {
		return precipitation;
	}

	public void setPrecipitation(Double precipitation) {
		this.precipitation = precipitation;
	}
	@Column(name = "precipitation_unit")
	public String getPrecipitationUnit() {
		return precipitationUnit;
	}

	public void setPrecipitationUnit(String precipitationUnit) {
		this.precipitationUnit = precipitationUnit;
	}
	@Column(name = "relative_humidity")
	public Double getRelative_humidity() {
		return relative_humidity;
	}

	public void setRelative_humidity(Double relative_humidity) {
		this.relative_humidity = relative_humidity;
	}
	@Column(name = "relative_humidity_unit")
	public String getRelativeHumidityUnit() {
		return relativeHumidityUnit;
	}

	public void setRelativeHumidityUnit(String relativeHumidityUnit) {
		this.relativeHumidityUnit = relativeHumidityUnit;
	}
	@Column(name = "solar")
	public Double getSolar() {
		return solar;
	}

	public void setSolar(Double solar) {
		this.solar = solar;
	}
	@Column(name = "solar_unit")
	public String getSolarUnit() {
		return solarUnit;
	}

	public void setSolarUnit(String solarUnit) {
		this.solarUnit = solarUnit;
	}

}
