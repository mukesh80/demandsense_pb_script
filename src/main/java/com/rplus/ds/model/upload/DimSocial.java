package com.rplus.ds.model.upload;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "social_invoice_pb_delta")
public class DimSocial implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	  private String socialMediatype;
	  private String socialTopic;
	  private String socialProduct;
	  private String socialLocation;
	  private String socialId;
	
	  @Column(name = "social_mediatype")
	public String getSocial_mediatype() {
		return socialMediatype;
	}
	public void setSocial_mediatype(String social_mediatype) {
		this.socialMediatype = social_mediatype;
	}
	 @Column(name = "social_topic")
	public String getSocial_topic() {
		return socialTopic;
	}
	public void setSocial_topic(String social_topic) {
		this.socialTopic = social_topic;
	}
	 @Column(name = "social_product")
	public String getSocial_product() {
		return socialProduct;
	}
	public void setSocial_product(String social_product) {
		this.socialProduct = social_product;
	}
	 @Column(name = "social_location")
	public String getSocial_location() {
		return socialLocation;
	}
	public void setSocial_location(String social_location) {
		this.socialLocation = social_location;
	}
	@Id
	 @Column(name = "social_id")
	public String getSocial_id() {
		return socialId;
	}
	public void setSocial_id(String social_id) {
		this.socialId = social_id;
	}
	  
	  
}
