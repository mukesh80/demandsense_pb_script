package com.rplus.ds.model.upload;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Rafeeq
 * 
 */

@Entity
@Table(name = "sales_invoice_pb_delta")
public class FactSales implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	//private Long id;
	private String productId;
	private String locationId;
	private Date calDay;
	private String categoryId;
	private String customerId;
	private String cust1;
	private String cust2;
	private String cust3;
	private Double price;
	private String priceCurrency;
	private Double quantity;
	private String qtyUnit;
	private Double amount;
	private String amtCurrency;
	private Double tax;
	private String taxCurrency;
	private Double serviceCharge;
	private String scCurrency;
	private Double serviceTax;
	private String stCurrency;
	private Double totalSales;
	private String tsCurrency;
	private Double discount;
	private String discountCurrency;
	private Double billValue;
	private String bvCurrency;
	private Date maxTdate;
	private Time avgtimespend;
	private Integer kotno;
	private Integer isno;
	private Integer custKf1;
	private Integer custKf2;
	private Integer custKf3;
	private String promoId;

	/*@Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }
 
    public void setId(Long id) {
        this.id = id;
    }*/
	
	@Id
	@Column(name = "product_id")
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Id
	@Column(name = "location_id")
	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	@Id
	@Column(name = "cal_day")
	public Date getCalDay() {
		return calDay;
	}

	public void setCalDay(Date calDay) {
		this.calDay = calDay;
	}
	@Column(name = "category_id")
	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	
	@Id
	@Column(name = "customer_id")
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	@Column(name = "cust1")
	public String getCust1() {
		return cust1;
	}

	public void setCust1(String cust1) {
		this.cust1 = cust1;
	}
	@Column(name = "cust2")
	public String getCust2() {
		return cust2;
	}

	public void setCust2(String cust2) {
		this.cust2 = cust2;
	}
	@Column(name = "cust3")
	public String getCust3() {
		return cust3;
	}

	public void setCust3(String cust3) {
		this.cust3 = cust3;
	}
	
	@Column(name = "price_currency")
	public String getPriceCurrency() {
		return priceCurrency;
	}

	public void setPriceCurrency(String priceCurrency) {
		this.priceCurrency = priceCurrency;
	}
	@Column(name = "quantity")
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	@Column(name = "qty_unit")
	public String getQtyUnit() {
		return qtyUnit;
	}

	public void setQtyUnit(String qtyUnit) {
		this.qtyUnit = qtyUnit;
	}
	@Column(name = "amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Column(name = "amt_currency")
	public String getAmtCurrency() {
		return amtCurrency;
	}

	public void setAmtCurrency(String amtCurrency) {
		this.amtCurrency = amtCurrency;
	}
	@Column(name = "tax")
	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}
	@Column(name = "tax_currency")
	public String getTaxCurrency() {
		return taxCurrency;
	}

	public void setTaxCurrency(String taxCurrency) {
		this.taxCurrency = taxCurrency;
	}
	@Column(name = "service_charge")
	public Double getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(Double serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	@Column(name = "sc_currency")
	public String getScCurrency() {
		return scCurrency;
	}

	public void setScCurrency(String scCurrency) {
		this.scCurrency = scCurrency;
	}
	@Column(name = "service_tax")
	public Double getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(Double serviceTax) {
		this.serviceTax = serviceTax;
	}
	@Column(name = "st_currency")
	public String getStCurrency() {
		return stCurrency;
	}

	public void setStCurrency(String stCurrency) {
		this.stCurrency = stCurrency;
	}
	@Column(name = "total_sales")
	public Double getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(Double totalSales) {
		this.totalSales = totalSales;
	}
	@Column(name = "ts_currency")
	public String getTsCurrency() {
		return tsCurrency;
	}

	public void setTsCurrency(String tsCurrency) {
		this.tsCurrency = tsCurrency;
	}
	@Column(name = "discount")
	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	@Column(name = "discount_currency")
	public String getDiscountCurrency() {
		return discountCurrency;
	}

	public void setDiscountCurrency(String discountCurrency) {
		this.discountCurrency = discountCurrency;
	}
	@Column(name = "bill_value")
	public Double getBillValue() {
		return billValue;
	}

	public void setBillValue(Double billValue) {
		this.billValue = billValue;
	}
	@Column(name = "bv_currency")
	public String getBvCurrency() {
		return bvCurrency;
	}

	public void setBvCurrency(String bvCurrency) {
		this.bvCurrency = bvCurrency;
	}
	@Column(name = "max_tdate")
	public Date getMaxTdate() {
		return maxTdate;
	}

	public void setMaxTdate(Date maxTdate) {
		this.maxTdate = maxTdate;
	}
	
	
	@Column(name = "kotno")
	public Integer getKotno() {
		return kotno;
	}

	public void setKotno(Integer kotno) {
		this.kotno = kotno;
	}
	@Column(name = "isno")
	public Integer getIsno() {
		return isno;
	}

	public void setIsno(Integer isno) {
		this.isno = isno;
	}
	@Column(name = "cust_kf1")
	public Integer getCustKf1() {
		return custKf1;
	}

	public void setCustKf1(Integer custKf1) {
		this.custKf1 = custKf1;
	}
	@Column(name = "cust_kf2")
	public Integer getCustKf2() {
		return custKf2;
	}

	public void setCustKf2(Integer custKf2) {
		this.custKf2 = custKf2;
	}
	@Column(name = "cust_kf3")
	public Integer getCustKf3() {
		return custKf3;
	}

	public void setCustKf3(Integer custKf3) {
		this.custKf3 = custKf3;
	}
	@Column(name = "promo_id")
	public String getPromoId() {
		return promoId;
	}

	public void setPromoId(String promoId) {
		this.promoId = promoId;
	}

	@Column(name="price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "avgtimespend")
	public Time getAvgtimespend() {
		return avgtimespend;
	}

	public void setAvgtimespend(Time avgtimespend) {
		this.avgtimespend = avgtimespend;
	}

}
