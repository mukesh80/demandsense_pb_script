package com.rplus.ds.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rplus_ds_output_a_day")
public class RplusDsOutputADay implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String countryId;
	private String locationId;
	private String regionId;
	private String stateId;
	private String districtId;
	private String cityId;
	private String productId;
	private String categoryId;
	private String subCategoryId;
	//private String rowNames;
	private String timestamp;
	private String uid;
	private Integer yearWeek;
	private Integer calWeek;
	private Integer calMonth;
	private Integer calYear;
	private Integer calHalfyear;
	private Integer calQuarter;
	private String model;
	private Double me;
	private Double rmse ;
	private Double mae;
	private Double mpe;
	private Double mape;
	private Double mase;
	private Double acf1;
	private Date calDay;
	private Double owQty;
	private Double finalQty;
	private String bmId;
	
	
	@Column(name = "bm_id")
	public String getBmId() {
		return bmId;
	}
	public void setBmId(String bmId) {
		this.bmId = bmId;
	}
	@Id
    @GeneratedValue
    @Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "country_id")
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	
	@Column(name = "location_id")
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	@Column(name = "region_id")
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	
	@Column(name = "state_id")
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	
	@Column(name = "district_id")
	public String getDistrictId() {
		return districtId;
	}
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}
	
	@Column(name = "city_id")
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	
	@Column(name = "product_id")
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	@Column(name = "category_id")
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	
	@Column(name = "sub_category_id")
	public String getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	
	/*@Column(name = "row_names")
	public String getRowNames() {
		return rowNames;
	}
	public void setRowNames(String rowNames) {
		this.rowNames = rowNames;
	}*/
	
	@Column(name = "timestamp")
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	@Column(name = "uid")
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	@Column(name = "year_week")
	public Integer getYearWeek() {
		return yearWeek;
	}
	public void setYearWeek(Integer yearWeek) {
		this.yearWeek = yearWeek;
	}
	
	@Column(name = "cal_week")
	public Integer getCalWeek() {
		return calWeek;
	}
	public void setCalWeek(Integer calWeek) {
		this.calWeek = calWeek;
	}
	
	@Column(name = "cal_month")
	public Integer getCalMonth() {
		return calMonth;
	}
	public void setCalMonth(Integer calMonth) {
		this.calMonth = calMonth;
	}
	
	@Column(name = "cal_year")
	public Integer getCalYear() {
		return calYear;
	}
	public void setCalYear(Integer calYear) {
		this.calYear = calYear;
	}
	
	@Column(name = "cal_halfyear")
	public Integer getCalHalfyear() {
		return calHalfyear;
	}
	public void setCalHalfyear(Integer calHalfyear) {
		this.calHalfyear = calHalfyear;
	}
	
	@Column(name = "cal_quarter")
	public Integer getCalQuarter() {
		return calQuarter;
	}
	public void setCalQuarter(Integer calQuarter) {
		this.calQuarter = calQuarter;
	}
	
	
	@Column(name = "model")
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	
	@Column(name = "me")
	public Double getMe() {
		return me;
	}
	public void setMe(Double me) {
		this.me = me;
	}
	
	@Column(name = "rmse")
	public Double getRmse() {
		return rmse;
	}
	public void setRmse(Double rmse) {
		this.rmse = rmse;
	}
	
	@Column(name = "mae")
	public Double getMae() {
		return mae;
	}
	public void setMae(Double mae) {
		this.mae = mae;
	}
		
	@Column(name = "mase")
	public Double getMase() {
		return mase;
	}
	public void setMase(Double mase) {
		this.mase = mase;
	}
	
	@Column(name = "acf1")
	public Double getAcf1() {
		return acf1;
	}
	public void setAcf1(Double acf1) {
		this.acf1 = acf1;
	}
	
	@Column(name = "cal_day")
	public Date getCalDay() {
		return calDay;
	}
	public void setCalDay(Date calDay) {
		this.calDay = calDay;
	}
	
	@Column(name = "ow_qty")
	public Double getOwQty() {
		return owQty;
	}
	public void setOwQty(Double owQty) {
		this.owQty = owQty;
	}
	
	@Column(name = "final_qty")
	public Double getFinalQty() {
		return finalQty;
	}
	public void setFinalQty(Double finalQty) {
		this.finalQty = finalQty;
	}
	public Double getMape() {
		return mape;
	}
	public void setMape(Double mape) {
		this.mape = mape;
	}
	public Double getMpe() {
		return mpe;
	}
	public void setMpe(Double mpe) {
		this.mpe = mpe;
	}
	
	
	
}
