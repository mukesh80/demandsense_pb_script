package com.rplus.ds.config;

import java.io.InputStream;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.rplus.ds.controller.UserController;
import com.rplus.ds.model.DimProduct;
import com.rplus.ds.model.LocationDimensions;
import com.rplus.ds.model.RplusDsOutputADay;
import com.rplus.ds.model.RplusDsOutputFDay;
import com.rplus.ds.model.RplusDsOutputSFDay;
import com.rplus.ds.model.SystemParam;
import com.rplus.ds.model.upload.DimLocation;

@SuppressWarnings("deprecation")
public class HibernateUtility {

	
	private static SessionFactory concreteSessionFactory;
	
	@Autowired
	HttpSession httpSession;
	

	HibernateUtility(){}
	
	HibernateUtility(SessionFactory concreteSessionFactory )
	{
		HibernateUtility.concreteSessionFactory=concreteSessionFactory;
	}
	
	static{
		final String resourceName = "application.properties";
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try (InputStream resourceStream = loader.getResourceAsStream(resourceName)){
			
			
			Properties prop= new Properties();
			
			prop.load(resourceStream);
			String currentSchema="";
			if(UserController.userSchemaName!=null){
				
				currentSchema=UserController.userSchemaName;
				
			}else{
				throw new NoSchemaException("No schema found");
			}
			
			prop.setProperty("hibernate.connection.url", prop.getProperty("jdbc.url"));
			prop.setProperty("hibernate.connection.username", prop.getProperty("jdbc.username"));
			prop.setProperty("hibernate.connection.password", prop.getProperty("jdbc.password"));
			prop.setProperty("hibernate.dialect", prop.getProperty("hibernate.dialect"));
			prop.setProperty("hibernate.format_sql", prop.getProperty("hibernate.format_sql"));
			prop.setProperty("hibernate.show_sql", prop.getProperty("hibernate.show_sql"));
			prop.setProperty("hibernate.default_schema", currentSchema);
			
			concreteSessionFactory = new AnnotationConfiguration()
		   .addPackage("com.rplus.ds.model")
				   .addProperties(prop)
				   .addAnnotatedClass(DimProduct.class)
				   .addAnnotatedClass(DimLocation.class)
				   .addAnnotatedClass(SystemParam.class)
				   .addAnnotatedClass(LocationDimensions.class)
				   .addAnnotatedClass(DimLocation.class)
				   .addAnnotatedClass(SystemParam.class)
				   .addAnnotatedClass(RplusDsOutputFDay.class)
				   .addAnnotatedClass(RplusDsOutputSFDay.class)
				   .addAnnotatedClass(RplusDsOutputADay.class)
				   .buildSessionFactory();
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}
	 
	public static SessionFactory getSession()
			throws HibernateException {
		return concreteSessionFactory;
	}
	
	@ExceptionHandler( value=Exception.class)
	public String handleNoSchemaException(NoSchemaException e){
		System.out.println("Exception occured");
		e.printStackTrace();
		return "redirect:/error-page";
	}
}
