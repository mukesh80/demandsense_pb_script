package com.rplus.ds.config;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

	//private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
	static HttpSession httpSession;
	
	@ExceptionHandler( value=NoSchemaException.class)
	public String handleNoSchemaException(NoSchemaException e){
		System.out.println("Exception occured");
		e.printStackTrace();
		return "redirect:/error-page";
	}
	
	@ExceptionHandler( value=Exception.class)
	public String handleException(Exception e){
		System.out.println("Exception occured");
		e.printStackTrace();
		return "redirect:/error-page";
	}
	
/*	@ExceptionHandler( value=NoSchemaException.class)
	public String handleNoSchemaException(NoSchemaException e,HttpServletRequest req)
	{
		 httpSession.invalidate();
		logger.error("NoSchemaException Request: " + req.getRequestURL() + " raised " + e.getStackTrace());
		System.out.println("NullPointerException Request: " + req.getRequestURL() + " raised " + e);
		System.out.println(" NullPointerException Request: " + req.getRequestURL() + " raised " + e.getStackTrace());
		
	    ModelAndView mav = new ModelAndView();
	    mav.addObject("exception", e);
	    mav.addObject("url", req.getRequestURL());
	    mav.setViewName("exception");
	
	    return "exception";
	
	}
	
	@ExceptionHandler( value=NullPointerException.class)
	public ModelAndView handleNullPointerException(NullPointerException e,HttpServletRequest req)
	{
		 httpSession.invalidate();
		logger.error("NullPointerException Request: " + req.getRequestURL() + " raised " + e.getStackTrace());
		System.out.println(" NullPointerException Request: " + req.getRequestURL() + " raised " + e);
		System.out.println(" NullPointerException Request: " + req.getRequestURL() + " raised " + e.getStackTrace());
		
	    ModelAndView mav = new ModelAndView();
	    mav.addObject("exception", e);
	    mav.addObject("url", req.getRequestURL());
	    mav.setViewName("exception");
	    return mav;
	
	}
	
	@ExceptionHandler( value=org.postgresql.util.PSQLException.class)
	public ModelAndView handlePSQLException(org.postgresql.util.PSQLException e,HttpServletRequest req)
	{
		 httpSession.invalidate();
		logger.error(" PSQLException Request: " + req.getRequestURL() + " raised " + e);
		System.out.println("  PSQLException Request: " + req.getRequestURL() + " raised " + e);
		System.out.println("  PSQLException Request: " + req.getRequestURL() + " raised " + e.getStackTrace());
		
	    ModelAndView mav = new ModelAndView();
	    mav.addObject("exception", e);
	    mav.addObject("url", req.getRequestURL());
	    mav.setViewName("exception");
	   
	    return mav;
	
	}
	
	@ExceptionHandler( value=ConnectException.class)
	public ModelAndView handleConnectException(ConnectException e,HttpServletRequest req)
	{
		 httpSession.invalidate();
		logger.error("ConnectException Request: " + req.getRequestURL() + " raised " + e.getStackTrace());
		System.out.println("  ConnectException Request: " + req.getRequestURL() + " raised " + e);
		System.out.println(" ConnectException Request: " + req.getRequestURL() + " raised " + e.getStackTrace());
		
	    ModelAndView mav = new ModelAndView();
	    mav.addObject("exception", e);
	    mav.addObject("url", req.getRequestURL());
	    mav.setViewName("exception");
	   
	    return mav;
	
	}*/
	
	
	
}
