package com.rplus.ds.config;

public class NoSchemaException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	NoSchemaException(String s){
		super(s);
	}

}
