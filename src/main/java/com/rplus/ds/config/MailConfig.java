package com.rplus.ds.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@PropertySource(value = { "classpath:application.properties" })
public class MailConfig {

	
    
    @Autowired
    private Environment environment;
    
    @Bean
    public JavaMailSenderImpl javaMailService() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

        javaMailSender.setHost(environment.getRequiredProperty("spring.mail.host"));
        javaMailSender.setPort(Integer.parseInt(environment.getRequiredProperty("spring.mail.port")));
        javaMailSender.setUsername("info@rplusanalytics.com");
        javaMailSender.setPassword("rukshaya@123");
        javaMailSender.setJavaMailProperties(getMailProperties());

        return javaMailSender;
    }
   
   
   public Properties getMailProperties() {
       Properties properties = new Properties();
       properties.setProperty("mail.transport.protocol", "smtp");
       properties.setProperty("mail.smtp.auth", "true");
       properties.setProperty("mail.smtp.starttls.enable", "true");
       properties.setProperty("mail.debug", "true");
       properties.setProperty("mail.imap.ssl.trust", environment.getRequiredProperty("mail.imap.ssl.trust"));
       
       return properties;
   }

}
