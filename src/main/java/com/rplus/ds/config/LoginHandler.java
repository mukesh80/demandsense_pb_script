package com.rplus.ds.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.rplus.ds.dao.UserDao;
import com.rplus.ds.dao.UserLogDao;
public class LoginHandler implements HandlerInterceptor{

	@Autowired
	UserDao userDao;
	
	@Autowired
	HttpSession userSession;
	
	@Autowired
	UserLogDao useLogDao;
	
	private static String userSchemaName;
	
	
	
	public static String getUserSchemaName() {
		return userSchemaName;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		System.out.println("Pre handle methos execied");
		//
		if(request.getSession()!=null){
			System.out.println("User session is active ");
			return true;
			
		}
		else{
			System.out.println("User session is NOT active ");
			response.sendRedirect("redirect:/");
			return false;
		}
			
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		System.out.println("postHandle() method Initializing Hibernate Utility....");
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		System.out.println("afterCompletion() method ");		
	}

}




/*
 		String username=request.getParameter("username");
		String password=request.getParameter("password");
		boolean status=userDao.validateUser(username,password);
		System.out.println("After validation ::::::::::in UserController");
		if(status){
			User userInfo=userDao.getUserInfo(username);
			String firstNm=userInfo.getFirstName();
			String lastNm=userInfo.getLastName();
			
			if(firstNm == null){
				firstNm = userInfo.getUsername();
			}
			
			if(lastNm==null)
			{
				lastNm=" ";
			}
			String fullName=(firstNm.substring(0, 1).toUpperCase() + firstNm.substring(1))+" "
					+(lastNm.substring(0, 1).toUpperCase() + lastNm.substring(1));
			System.out.println("First and last name Full name :   "+fullName);
			request.getSession().setAttribute("loggedUserName", fullName);
			
			
			request.getSession().setAttribute("loggedUser", userInfo.getUsername());
			request.getSession().setAttribute("userType", userInfo.getUserType());
			//request.getSession().setAttribute("schemaName",userInfo.getSchemaName());
			userSchemaName=userInfo.getSchemaName();
			
			userSession.setAttribute("userEmail1", userInfo.getEmailId());
			userSession.setAttribute("loggedUser1", userInfo.getUsername());
			
			useLogDao.saveUserLog("login");
			
			response.sendRedirect("redirect:/index");
			//return new ModelAndView("redirect:/index");
			
		}
		else {
			request.setAttribute("Error","Sorry! Username or Password Error. plz Enter Correct Detail or Register");
			 String message = "Invalid username or password!";
			 //redirectAttributes.addFlashAttribute("invaliduser", message);
			 request.getSession().setAttribute("Loginmsg","plz sign in first");
			 response.sendRedirect("redirect:/");
			//return new ModelAndView("redirect:/");
		}

		
 */



