package com.rplus.ds.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rplus.ds.model.RplusDsOutputADay;

/**
 * @author Rafeeq-PC
 *
 */
@Component
public interface RplusOutputADayDao {	
	@Autowired(required=true)
	public List<RplusDsOutputADay> getRplusOutputAdata();

}
