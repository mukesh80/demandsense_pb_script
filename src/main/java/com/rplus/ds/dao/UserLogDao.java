package com.rplus.ds.dao;

import java.net.UnknownHostException;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public interface UserLogDao {

	
	public void saveUserLog(String activity) throws UnknownHostException;
	
	@SuppressWarnings("rawtypes")
	public List getUserList();

	@SuppressWarnings("rawtypes")
	public List getUserLogs(String username);
}
