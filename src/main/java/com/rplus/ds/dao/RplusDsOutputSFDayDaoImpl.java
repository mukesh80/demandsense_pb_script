package com.rplus.ds.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.config.HibernateUtility;
import com.rplus.ds.model.RplusDsOutputSFDay;

/**
 * @author Rafeeq
 * currently not in use instead of this we are using prdective DAO for filteration
 *
 */
@Repository
@Component
public class RplusDsOutputSFDayDaoImpl implements RplusDsOutputSFDayDao {
	private static final Logger logger = Logger.getLogger(RplusDsOutputSFDayDaoImpl.class);

	@Override
	@SuppressWarnings("unchecked")
	public List<RplusDsOutputSFDay> getRplusOutputSFdata() {
		logger.info("getRplusOutputSFdata() method ");
		 Session session =HibernateUtility.getSession().openSession();
         List<RplusDsOutputSFDay> rplusDsOutputSFList = session.createQuery("from RplusDsOutputSFDay").list();
         session.close();
		return rplusDsOutputSFList;
	}

	
}
