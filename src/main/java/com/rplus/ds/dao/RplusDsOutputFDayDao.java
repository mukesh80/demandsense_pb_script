package com.rplus.ds.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rplus.ds.model.RplusDsOutputFDay;

/**
 * @author Rafeeq
 *
 */
@Component
public interface RplusDsOutputFDayDao {	
	
	@Autowired(required=true)
	public List<RplusDsOutputFDay> getRplusOutputdata();
	

}
