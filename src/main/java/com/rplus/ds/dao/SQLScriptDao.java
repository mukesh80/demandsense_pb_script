package com.rplus.ds.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rplus.ds.model.SQLJobGroup;
import com.rplus.ds.model.TaskTrack;
import com.rplus.ds.model.User;

@Component
public interface SQLScriptDao {
	
	@Autowired(required = true)
	public List<SQLJobGroup> getSQLGroupdata();
	
	@Autowired(required = true)
	public List<User> getUsers();
	
	@Autowired(required = true)
	public boolean saveTaskTrack(TaskTrack tasktrack);
 }
