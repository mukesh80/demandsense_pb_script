package com.rplus.ds.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.config.HibernateUtility;
import com.rplus.ds.model.DimProduct;

/**
 * Created by freakster on 23/7/15.
 */
@Repository
@Component
public class DimProductDAOImpl implements DimProductDAO {

    @Autowired
    private SessionFactory sessionFactory;

    
    private static final Log log = LogFactory.getLog(DimProductDAOImpl.class);

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(DimProduct dimProduct) {
        log.debug("persisting DimProduct instance");
        try
        {
            getCurrentSession().save(dimProduct);
            log.debug("persist successful");
        }
        catch (RuntimeException re)
        {
            log.error("persist failed", re);
            throw re;
        }
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<DimProduct> listOfProducts()
    {

        Session session =HibernateUtility.getSession().openSession();
        List<DimProduct> prodList = session.createQuery("from DimProduct").list();
        session.close();
        System.out.println("prodList = " + prodList);
        return prodList;
    }

    @SuppressWarnings("unchecked")
	public List<DimProduct> listofProducts( String keyword)
    {
        String hql = "from dim_product where description like :keyword";

        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("keyword", "%" + keyword + "%");

        List<DimProduct> productList = query.list();

        return productList;

    }
    
    /**
     * This method is used to get the categorys list
     */
    @SuppressWarnings("unchecked")
	public HashMap<String,String> getCategorysList(){
    	Session session =HibernateUtility.getSession().openSession();
    	String hql = "select dp.categoryId,dp.categoryIdDescription from DimProduct dp group by dp.categoryId,dp.categoryIdDescription";
    	Query query = session.createQuery(hql);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> categoryIdAndDescMap = new HashMap<String,String>();
    	for(Object[] categoryIdAndDesc:resultList){
    		categoryIdAndDescMap.put((String)categoryIdAndDesc[0],(String)categoryIdAndDesc[1]);
    	}
    	categoryIdAndDescMap.put("count", ((Integer)resultList.size()).toString());
    	return categoryIdAndDescMap;
    }
    
    /**
     * This method is used to get the sub categorys list by category
     */
    @SuppressWarnings("unchecked")
	public HashMap<String,String> getSubCategorysByCategory(String categoryId){
    	Session session =HibernateUtility.getSession().openSession();
    	String hql = "select dp.subcategoryId,dp.subcategoryIdDescription from DimProduct dp where dp.categoryId=:categoryId group by subcategoryId,dp.subcategoryIdDescription";
    	Query query = session.createQuery(hql);
    	query.setParameter("categoryId", categoryId);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> subCategoryIdAndDescMap = new HashMap<String,String>();
    	for(Object[] subCategoryIdAndDesc:resultList){
    		subCategoryIdAndDescMap.put((String)subCategoryIdAndDesc[0],(String)subCategoryIdAndDesc[1]);
    	}
    	subCategoryIdAndDescMap.put("count", ((Integer)resultList.size()).toString());
    	return subCategoryIdAndDescMap;
    }
    
    /**
     * This method is used to get the products by subcategory
     */
    @SuppressWarnings("unchecked")
	public HashMap<String,String> getProductsBySubCategory(String subCategoryId){
    	Session session =HibernateUtility.getSession().openSession();
    	String hql = "select dp.productId,dp.productIdDescription from DimProduct dp where dp.subcategoryId=:subcategoryId";
    	Query query = session.createQuery(hql);
    	query.setParameter("subcategoryId", subCategoryId);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> productIdAndDescMap = new HashMap<String,String>();
    	for(Object[] productIdAndDesc:resultList){
    		productIdAndDescMap.put((String)productIdAndDesc[0],(String)productIdAndDesc[1]);
    	}
    	productIdAndDescMap.put("count", ((Integer)resultList.size()).toString());
    	return productIdAndDescMap;
    }

    /**
     * This method is used to get all available subcategorys
     */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getAllSubCategeorys() {
		Session session =HibernateUtility.getSession().openSession();
		String hql = "select dp.subcategoryId,dp.subcategoryIdDescription from DimProduct dp group by dp.subcategoryId,dp.subcategoryIdDescription";
		Query query = session.createQuery(hql);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> subCategoryIdsandDescMap = new HashMap<String,String>();
    	for(Object[] subCategoryIdAndDesc:resultList){
    		subCategoryIdsandDescMap.put((String)subCategoryIdAndDesc[0],(String)subCategoryIdAndDesc[1]);
    	}
    	subCategoryIdsandDescMap.put("count", ((Integer)resultList.size()).toString());
    	return subCategoryIdsandDescMap;
	}
	
	/**
	 * This method is used to get all available products
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getAllProducts() {
		Session session =HibernateUtility.getSession().openSession();
		String hql = "select dp.productId,dp.productIdDescription from DimProduct dp group by dp.productId,dp.productIdDescription";
		Query query = session.createQuery(hql);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> productIdsandDescMap = new HashMap<String,String>();
    	for(Object[] productIdAndDesc:resultList){
    		productIdsandDescMap.put((String)productIdAndDesc[0],(String)productIdAndDesc[1]);
    	}
    	productIdsandDescMap.put("count", ((Integer)resultList.size()).toString());
    	return productIdsandDescMap;
	}

	@Override
	public DimProduct getProductById(String productId) {
		Session session =HibernateUtility.getSession().openSession();
		String hql = "from DimProduct dp where dp.productId=:productId";
		Query query = session.createQuery(hql);
		query.setParameter("productId",productId );
    	DimProduct dimProduct= (DimProduct)query.uniqueResult();
		return dimProduct;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getProductsBySubCategoryList(
			List<String> subCategoryIdList) {
		Session session =HibernateUtility.getSession().openSession();
    	String hql = "select dp.productId,dp.productIdDescription from DimProduct dp where dp.subcategoryId in (:subCategoryIdList) group by dp.productId,dp.productIdDescription";
    	Query query = session.createQuery(hql);
    	query.setParameterList("subCategoryIdList", subCategoryIdList);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> subCategoryIdAndDescMap = new HashMap<String,String>();
    	for(Object[] subCategoryIdAndDesc:resultList){
    		subCategoryIdAndDescMap.put((String)subCategoryIdAndDesc[0],(String)subCategoryIdAndDesc[1]);
    	}
    	return subCategoryIdAndDescMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getCategorysByCategoryList(
			List<String> categoryIdList) {
		Session session =HibernateUtility.getSession().openSession();
    	String hql = "select dp.categoryId,dp.categoryIdDescription from DimProduct dp where dp.categoryId in (:categoryIdList) group by dp.categoryId,dp.categoryIdDescription";
    	Query query = session.createQuery(hql);
    	query.setParameterList("categoryIdList", categoryIdList);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> subCategoryIdAndDescMap = new HashMap<String,String>();
    	for(Object[] subCategoryIdAndDesc:resultList){
    		subCategoryIdAndDescMap.put((String)subCategoryIdAndDesc[0],(String)subCategoryIdAndDesc[1]);
    	}
    	return subCategoryIdAndDescMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getProductsByProductList(
			List<String> productIdList) {
		Session session =HibernateUtility.getSession().openSession();
    	String hql = "select dp.productId,dp.productIdDescription from DimProduct dp where dp.productId in (:productIdList) group by dp.productId,dp.productIdDescription";
    	Query query = session.createQuery(hql);
    	query.setParameterList("productIdList", productIdList);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> subCategoryIdAndDescMap = new HashMap<String,String>();
    	for(Object[] subCategoryIdAndDesc:resultList){
    		subCategoryIdAndDescMap.put((String)subCategoryIdAndDesc[0],(String)subCategoryIdAndDesc[1]);
    	}
    	return subCategoryIdAndDescMap;
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String,String> getSubCategorysByCategoryList(List<String> categoryIdList){
    	Session session =HibernateUtility.getSession().openSession();
    	String hql = "select dp.subcategoryId,dp.subcategoryIdDescription from DimProduct dp where dp.categoryId in (:categoryIdList) group by subcategoryId,dp.subcategoryIdDescription";
    	Query query = session.createQuery(hql);
    	query.setParameterList("categoryIdList", categoryIdList);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> subCategoryIdAndDescMap = new HashMap<String,String>();
    	for(Object[] subCategoryIdAndDesc:resultList){
    		subCategoryIdAndDescMap.put((String)subCategoryIdAndDesc[0],(String)subCategoryIdAndDesc[1]);
    	}
    	return subCategoryIdAndDescMap;
    }
    
}
