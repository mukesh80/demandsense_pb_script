package com.rplus.ds.dao;

import java.util.List;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rplus.ds.model.User;

@Component
public interface UserDao {
	
	@Autowired(required=true)
	public void save(User user);
	
	@Autowired(required=true)
	public void getAllUser();
	
	public void insertRow(User user);
	
	public List<User> getList();

	public boolean validateUser(String username, String password);
	
	public boolean findUserByUserName(String userId);

	public String updateUser(User user);
	
	public User getUserInfo(String username);

	public String updatePassword(String pass);

}
