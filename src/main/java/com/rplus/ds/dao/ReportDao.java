package com.rplus.ds.dao;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public interface ReportDao {

	public List<Object> getMonthReport(Map<String, String> allParams);

	public List<Object> getWeekReport(Map<String, String> allParams);
	
	public List<Object> getDayReport(Map<String, String> allParams)throws ParseException ;

	public List<Object> getDriveAnalysisReport(Map<String, String> allParams);

	public HashMap<String, String> getAllRegions();

	public HashMap<String, String> getAllSubCategeorys();

	public List<Object> getDriveDetails(Map<String, String> allParams);
}
