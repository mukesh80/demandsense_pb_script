package com.rplus.ds.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.config.HibernateUtility;
import com.rplus.ds.model.TimeDimensions;

/**
 * 
 * @author Mallikarjuna
 *
 */
@Repository
@Component
public class TimeDimensionsDAOImpl implements TimeDimensionsDAO{
	
     
    @Override
    public void save(TimeDimensions timeDimensions) {
      /*  Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(timeDimensions);
        tx.commit();
        session.close();*/
    	HibernateUtility.getSession().openSession().save(timeDimensions);
    }
 
    @SuppressWarnings("unchecked")
    @Override
    public List<TimeDimensions> list() {
        Session session =HibernateUtility.getSession().openSession();
        List<TimeDimensions> personList = session.createQuery("from TimeDimensions").list();
        session.close();
                
        List<TimeDimensions> timeDimentions = new ArrayList<TimeDimensions>();

		for (TimeDimensions timeDimention : personList) {
			timeDimention.getCalDay();
			timeDimention.getYearWeek();
			timeDimention.getYearMonth();
			timeDimentions.add(timeDimention);
		}		
			
        return timeDimentions;
    }
    
    
}
