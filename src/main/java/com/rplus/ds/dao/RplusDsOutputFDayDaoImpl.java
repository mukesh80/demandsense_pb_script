package com.rplus.ds.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.config.HibernateUtility;
import com.rplus.ds.model.RplusDsOutputFDay;

/**
 * @author Rafeeq
 * currently not in use instead of this we are using prdective DAOImpl for filteration
 *
 */
@Repository
@Component
public class RplusDsOutputFDayDaoImpl implements RplusDsOutputFDayDao {
	
	private static final Logger logger = Logger.getLogger(RplusDsOutputFDayDaoImpl.class);
   
	@Override
	@SuppressWarnings("unchecked")
	public List<RplusDsOutputFDay> getRplusOutputdata() {
		logger.info("Inside getRplusOutputdata :RplusDsOutputDaoImpl ");
		 Session session =HibernateUtility.getSession().openSession();
         List<RplusDsOutputFDay> rplusDsOutputList = session.createQuery("from RplusDsOutputFDay").list();
         session.close();
		return rplusDsOutputList;
	}

}
