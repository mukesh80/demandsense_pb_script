package com.rplus.ds.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rplus.ds.model.Location;
/**
 * 
 * @author Rafeeq
 * 
 */
@Component
public interface LocationDao {
	@Autowired(required = true)
	public List<Location> getLocList();

	@Autowired(required = true)
	public HashMap<String, String> getCountries();

	@Autowired(required = true)
	public HashMap<String, String> getRegionsByCountryId(String countryId);

	@Autowired(required = true)
	public HashMap<String, String> getStatesByRegionId(String regionId);

	@Autowired(required = true)
	public HashMap<String, String> getDistrictsByStateId(String stateId);

	@Autowired(required = true)
	public HashMap<String, String> getCitiesByDistrictId(String districtId);

	@Autowired(required = true)
	public HashMap<String, String> getLocationIdsByCityId(String cityId);

	@Autowired(required = true)
	public HashMap<String, String> getAllRegions();

	@Autowired(required = true)
	public HashMap<String, String> getAllStates();
	
	@Autowired(required = true)
	public HashMap<String, String> getAllDistricts();

	@Autowired(required = true)
	public HashMap<String, String> getAllCities();
	
	@Autowired(required = true)
	public HashMap<String, String> getAllLocationIds();

	@Autowired(required = true)
	public HashMap<String, String> getRegionsByCountryList(List<String> countryList);

	@Autowired(required = true)
	public HashMap<String, String> getStatesByRegionList(List<String> regionList);

	@Autowired(required = true)
	public HashMap<String, String> getDistrictsByStateList(List<String> stateList);

	@Autowired(required = true)
	public HashMap<String, String> getCitiesByDistrictList(List<String> districtList);

	@Autowired(required = true)
	public HashMap<String, String> getLocationsByCityList(List<String> cityList);	
	
	@Autowired(required = true)
	public HashMap<String, String> getLocationsByLocationList(List<String> locationList);	

}
