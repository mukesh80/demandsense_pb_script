package com.rplus.ds.dao;


import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.model.UserLog;

@Repository
@Component
public class UserLogDaoImpl implements UserLogDao {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	HttpSession httpSession;
	
	UserLog userLog  = new UserLog();

	 public void setSessionFactory(SessionFactory sessionFactory) {
	        this.sessionFactory = sessionFactory;
	    }
	 
	
	@Override
	public void saveUserLog(String activity) throws UnknownHostException {
		String username = (String)httpSession.getAttribute("loggedUser");
		if(username != null){
			System.out.println("UserLogImpl:::::::::::::::::::Entryy");
			Session session = sessionFactory.openSession();
			userLog.setIpAddress((String) httpSession.getAttribute("ip"));
			userLog.setUserName(username);
			userLog.setTimestamp(new Date());
			userLog.setTabVisit(activity);
			Transaction tx = (Transaction) session.beginTransaction();
			System.out.println("UserLogDaoImpl saveUserLog():::::::::::::::::::::::"+userLog.getUserName());
			session.save(userLog);
			
			
			tx.commit();
		}
		

	}
	
	
	@SuppressWarnings({ "rawtypes" })
	public List getUserList(){
		
		Session session = sessionFactory.openSession();
		Query query=session.createQuery("select userName from UserLog group by userName");
		
		List list = (List) query.list();
		System.out.println("geting all the userName in UserLog table");
		return list;
		
	}


	@SuppressWarnings("rawtypes")
	@Override
	public List getUserLogs(String username) {
		Session session = sessionFactory.openSession();
		List list;
		if(username.equals("all")){
			Query query=session.createQuery("select userName, ipAddress, tabVisit, timestamp from UserLog");
			
			list = (List) query.list();
		}else{
			Query query=session.createQuery("select userName, ipAddress, tabVisit, timestamp from UserLog where userName in (:uname)");
			query.setParameter("uname", username);
			list = (List) query.list();
		}
		System.out.println("geting all logs of userName in UserLog table");
		return list;
	}
	
	

}
