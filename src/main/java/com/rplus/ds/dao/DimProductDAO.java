package com.rplus.ds.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rplus.ds.model.DimProduct;

/**
 * Created by freakster on 23/7/15.
 */
@Component
public interface DimProductDAO {

    @Autowired(required=true)
    public void save(DimProduct dimProduct);

    @Autowired(required=true)
    public List<DimProduct> listOfProducts();

    @Autowired(required=true)
    public HashMap<String,String> getCategorysList(); 
    
    @Autowired(required=true)
    public HashMap<String,String> getSubCategorysByCategory(String categoryId); 
    
    @Autowired(required=true)
    public HashMap<String,String> getProductsBySubCategory(String subCategoryId); 
    
    @Autowired(required=true)
    public HashMap<String,String> getAllSubCategeorys(); 
    
    @Autowired(required=true)
    public HashMap<String,String> getAllProducts(); 
    
    @Autowired(required=true)
    public DimProduct getProductById(String productId); 
    
    @Autowired(required=true)
    public HashMap<String,String> getSubCategorysByCategoryList(List<String> categoryIdList);
    
    @Autowired(required=true)
    public HashMap<String,String> getProductsBySubCategoryList(List<String> subCategoryIdList);
    
    @Autowired(required=true)
    public HashMap<String,String> getCategorysByCategoryList(List<String> categoryIdList);
    
    @Autowired(required=true)
    public HashMap<String,String> getProductsByProductList(List<String> productIdList);

}
