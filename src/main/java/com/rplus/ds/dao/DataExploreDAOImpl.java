package com.rplus.ds.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.rplus.ds.config.HibernateUtility;
import com.rplus.ds.service.PredictiveService;
/**
 * 
 * @author Mallikarjuna(Courtesy) and Jerald Joseph J
 *
 */
@Service("deDAO")
@Transactional
@Repository
@Component
public class DataExploreDAOImpl implements DataExploreDAO {
	
	@Autowired
	PredictiveService predictiveService;
	
	@Autowired
	PredictiveDAOImpl predictiveDAO;

	public void setSessionFactory(SessionFactory sessionFactory) {
	}

	/**
	 * This method used to query the DB to get the forecasts.
	 */
	public List<Object[]> getForecastData(
			Map<String, String> allParams) {
		Session session =HibernateUtility.getSession().openSession();
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		StringBuilder hqlQuery = new StringBuilder();
		hqlQuery.append("select res.yearWeek,sum(res.quantity),sum(res.finalQty),res.calDay from RplusDsOutputByDay res where (");
		hqlQuery.append(predictiveDAO.getCommonHqlQuery(allParamsMap));
		hqlQuery.append("group by res.productId,res.yearWeek,res.calDay order by res.productId,res.yearWeek,res.calDay");
		Query query = session.createQuery(hqlQuery.toString());

		
		query = predictiveDAO.setCommonQueryParams(allParamsMap, query);
		@SuppressWarnings("unchecked")
		List<Object[]> resultList = query.list();

		return resultList;
	}
	
	/**
	 * This method used to get the forecast data for non aggregate
	 * graph
	 */
	@SuppressWarnings({ })
	@Override
	public List<Object[]> getForecastNonAggregageData(
			Map<String, String> allParams) {
		Session session =HibernateUtility.getSession().openSession();
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		
		String selctionId = null;
		String querySelectionId = null;
		
		List<Object[]> prodResultList = null;
		List<Object[]> categoryResultList = null;
		List<Object[]> subCategoryResultList = null;
		List<Object[]> totalResultsList = new ArrayList<Object[]>();
		if (allParamsMap.get("products") != null) {
			selctionId = "products";
			querySelectionId = "product_id";
			
			prodResultList = getNonAggregateCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(prodResultList != null && prodResultList.size() >0)
			totalResultsList.addAll(prodResultList);
		}
		if (allParamsMap.get("categorys") != null) {
			selctionId = "categorys";
			querySelectionId = "sub_category_id";
			
			categoryResultList = getNonAggregateCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(categoryResultList != null && categoryResultList.size() >0)
			totalResultsList.addAll(categoryResultList);
		}
		if (allParamsMap.get("subCategorys") != null) {
			selctionId = "subCategorys";
			querySelectionId = "product_id";
			
			subCategoryResultList = getNonAggregateCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(subCategoryResultList != null && subCategoryResultList.size() >0)
			totalResultsList.addAll(subCategoryResultList);
		}
	
		return totalResultsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getNonAggregateCommonQuery(Session session,HashMap<String, HashMap<String, String>> allParamsMap,String selectionId,String querySelectionId){
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select res."+querySelectionId+" as prodId,res.year_week as yearWeek,sum(res.quantity) as actual,sum(res.final_qty) as forecast,res.cal_day as calDay from ");
		sqlQuery.append(" (select "+querySelectionId+",year_week,quantity,final_qty,cal_day from rplus_ds_customer.rplus_ds_output_f_day where (");
		sqlQuery.append(predictiveDAO.getCommonSqlQuery(allParamsMap,selectionId));
		sqlQuery.append("order by year_week,cal_day) as res ");
		sqlQuery.append(" group by res."+querySelectionId+",res.year_week,res.cal_day order by res."+querySelectionId+",res.year_week,res.cal_day");

		Query query = session.createSQLQuery(sqlQuery.toString());

		String startDate = allParamsMap.get("dates").get("startDate");
		String endDate = allParamsMap.get("dates").get("endDate");
		query.setParameter("fromYearWeek", Integer.parseInt(startDate));
		query.setParameter("toYearWeek", Integer.parseInt(endDate));
		query = predictiveDAO.setSQLCommonParameters(allParamsMap, query,selectionId);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getForecastData200(String locId) {
		Session session =HibernateUtility.getSession().openSession();
		String sqlStr = "select city_id, category_id, location_id, final_qty, product_id, sub_category_id, best_model, best_mean_value, best_max_value, best_min_value from rplus_ds_customer.rplus_ds_output_f_day where quantity>2 limit 20";
		Query query = session.createSQLQuery(sqlStr);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getForecastDataAllParams(String hqlStr, HashMap<String, HashMap<String, String>> allParamsMap) {
		Session session =HibernateUtility.getSession().openSession();
		//String sqlStr = "select city_id, category_id, location_id, final_qty, product_id, sub_category_id, best_model, best_mean_value, best_max_value, best_min_value from rplus_ds_customer.rplus_ds_output_f_day where quantity>2 limit 20";
		//Query query = session.createSQLQuery(hqlStr);
		Query query = session.createQuery(hqlStr);
		query = predictiveDAO.setCommonQueryParams(allParamsMap, query);
		String finalHqlStr = query.getQueryString();
		System.out.print(finalHqlStr);
		return query.list();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getSignificantData200() {
		Session session =HibernateUtility.getSession().openSession();
		String sqlStr = "select country_id, location_id, region_id, state_id, district_id, city_id, product_id, category_id, sub_category_id, significantfactors, correlation from rplus_ds_customer.rplus_ds_output_sf_day limit 20";
		Query query = session.createSQLQuery(sqlStr);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getSignificantDataAllParams(String hqlStr, HashMap<String, HashMap<String, String>> allParamsMap) {
		Session session =HibernateUtility.getSession().openSession();
		Query query = session.createQuery(hqlStr);
		query = predictiveDAO.setCommonQueryParams(allParamsMap, query);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getAccuracyData200() {
		Session session =HibernateUtility.getSession().openSession();
		String sqlStr = "select country_id, location_id, region_id, state_id, district_id, city_id, product_id, category_id, sub_category_id, model, rmse from rplus_ds_customer.rplus_ds_output_a_day limit 20";
		Query query = session.createSQLQuery(sqlStr);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getAccuracyDataAllParams(String hqlStr, HashMap<String, HashMap<String, String>> allParamsMap) {
		Session session =HibernateUtility.getSession().openSession();
		Query query = session.createQuery(hqlStr);
		query = predictiveDAO.setCommonQueryParams(allParamsMap, query);
		return query.list();
	}
}






