package com.rplus.ds.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.config.HibernateUtility;
import com.rplus.ds.controller.UserController;
import com.rplus.ds.model.RplusDsOutputADay;
import com.rplus.ds.model.RplusDsOutputFDay;
import com.rplus.ds.model.RplusDsOutputSFDay;
import com.rplus.ds.service.PredictiveService;

/**
 * @author guDDus
 *
 */
@Repository
@Component
@PropertySource(value = { "classpath:application.properties" })
public class PredictiveDAOImpl implements PredictiveDAO {
	
	int forecastcount=0;
	
	@Autowired
	PredictiveService predictiveService;
	
/*	Properties prop = new Properties();
	InputStream input = null;
	final static String resourceName = "application.properties";
	ClassLoader loader = Thread.currentThread().getContextClassLoader();
	
	 
	 
	 *//**
	  *  Non static init block to get schema name from properties file.
	  *//*
	 {
		 try(InputStream resourceStream = loader.getResourceAsStream(resourceName)) {

				
				prop.load(resourceStream);
				defaultSchema=prop.getProperty("hibernate.default_schema")+".";
				System.out.println(prop.getProperty("hibernate.default_schema"));

			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		 
	 }*/
	 
	
	// String defaultSchema=UserController.userSchemaName;
		
	
	public void setSessionFactory(SessionFactory sessionFactory) {
	}

	/**
	 * This method used to query the DB to get the predictions.
	 */
	public List<Object[]> getForecastData(
			Map<String, String> allParams) {
		Session session =HibernateUtility.getSession().openSession();
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		StringBuilder hqlQuery = new StringBuilder();
		hqlQuery.append("select res.yearWeek,sum(res.quantity),sum(res.finalQty),res.calDay from RplusDsOutputByDay res where (");
		hqlQuery.append(getCommonHqlQuery(allParamsMap));
		hqlQuery.append("group by res.productId,res.yearWeek,res.calDay order by res.productId,res.yearWeek,res.calDay");
		Query query = session.createQuery(hqlQuery.toString());

		
		query = setCommonQueryParams(allParamsMap, query);
		@SuppressWarnings("unchecked")
		List<Object[]> resultList = query.list();
		System.out.println("Forcast counting:::::::::::::::::::::::::In PredectiveDao"+forecastcount++);
		return resultList;
	}

	/**
	 * This method used to get the forecast data for non aggregate forecast
	 * graph
	 */
	@SuppressWarnings({ })
	@Override
	public List<Object[]> getForecastNonAggregageData(
			Map<String, String> allParams) {
		Session session =HibernateUtility.getSession().openSession();
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		
		String selctionId = null;
		String querySelectionId = null;
		
		List<Object[]> prodResultList = null;
		List<Object[]> categoryResultList = null;
		List<Object[]> subCategoryResultList = null;
		List<Object[]> totalResultsList = new ArrayList<Object[]>();
		if (allParamsMap.get("products") != null) {
			selctionId = "products";
			querySelectionId = "product_id";
			
			prodResultList = getNonAggregateCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(prodResultList != null && prodResultList.size() >0)
			totalResultsList.addAll(prodResultList);
		}
		if (allParamsMap.get("categorys") != null) {
			selctionId = "categorys";
			querySelectionId = "sub_category_id";
			
			categoryResultList = getNonAggregateCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(categoryResultList != null && categoryResultList.size() >0)
			totalResultsList.addAll(categoryResultList);
		}
		if (allParamsMap.get("subCategorys") != null) {
			selctionId = "subCategorys";
			querySelectionId = "product_id";
			
			subCategoryResultList = getNonAggregateCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(subCategoryResultList != null && subCategoryResultList.size() >0)
			totalResultsList.addAll(subCategoryResultList);
		}
	
		return totalResultsList;
	}

	@Override
	public List<Object[]> getNASocialFactorData(
			Map<String, String> allParams) {
		
		Session session =HibernateUtility.getSession().openSession();
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		
		String selctionId = null;
		String querySelectionId = null;
		
		List<Object[]> prodResultList = null;
		List<Object[]> categoryResultList = null;
		List<Object[]> subCategoryResultList = null;
		List<Object[]> totalResultsList = new ArrayList<Object[]>();
		if (allParamsMap.get("products") != null) {
			selctionId = "products";
			querySelectionId = "product_id";
			
			prodResultList = getNonAggregateSocialCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(prodResultList != null && prodResultList.size() >0)
			totalResultsList.addAll(prodResultList);
		}
		if (allParamsMap.get("categorys") != null) {
			selctionId = "categorys";
			querySelectionId = "sub_category_id";
			
			categoryResultList = getNonAggregateSocialCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(categoryResultList != null && categoryResultList.size() >0)
			totalResultsList.addAll(categoryResultList);
		}
		if (allParamsMap.get("subCategorys") != null) {
			selctionId = "subCategorys";
			querySelectionId = "product_id";
			
			subCategoryResultList = getNonAggregateSocialCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(subCategoryResultList != null && subCategoryResultList.size() >0)
			totalResultsList.addAll(subCategoryResultList);
		}
		
		
		System.out.println(" resultList  size ::" + totalResultsList.size());

		return totalResultsList;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getSocialFactorData(Map<String,String> allParams) {
		
		Session session =HibernateUtility.getSession().openSession();
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		StringBuilder hqlQuery = new StringBuilder();
		hqlQuery.append("select res.yearWeek,avg(res.sentimentIndex),max(res.srs),res.calDay from ProdLocTimeByDay res  where (");
											// changed sum to avg for res.sentimentIndex Mukesh
		hqlQuery.append(getCommonHqlQuery(allParamsMap));
		hqlQuery.append("group by res.productId,res.yearWeek,res.calDay order by res.productId,res.yearWeek,res.calDay");
		//String hql = "select res.yearWeek,sum(res.sentimentIndex),max(res.srs),res.calDay from ProdLocTimeByDay res  where res.cityId in (:cityIdList) and res.productId in (:productIdList) and (res.yearWeek>=:fromYearWeek and res.yearWeek<=:toYearWeek) group by res.productId,res.yearWeek,res.calDay order by res.productId,res.yearWeek,res.calDay";
		Query query = session.createQuery(hqlQuery.toString());

		query.setParameter("fromYearWeek", 201410);
		query.setParameter("toYearWeek", 201420);
		query = setCommonQueryParams(allParamsMap, query);
		List<Object[]> resultList = query.list();
		System.out.println(" resultList  size ::" + resultList.size());

		return resultList;

	}

	/**
	 * This method used to query the DB to get the predictions.
	 */
	@Override
	public LinkedHashMap<String, List<HashMap<String, String>>> getInternalFactorData(Map<String,String> allParams) {		
		
		Session session =HibernateUtility.getSession().openSession();

		//HashMap<String,HashMap<String,String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		HashMap<String,HashMap<String,String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);

		String selctionId = null;
		String querySelectionId = null;
		
		List<Object[]> prodResultList = null;
		List<Object[]> categoryResultList = null;
		List<Object[]> subCategoryResultList = null;
		List<Object[]> totalResultsList = new ArrayList<Object[]>();
		if (allParamsMap.get("products") != null) {
			selctionId = "products";
			querySelectionId = "product_id";
			
			prodResultList = getNonAggregateInternalQuery(session,allParamsMap,selctionId,querySelectionId);
			if(prodResultList != null && prodResultList.size() >0)
			totalResultsList.addAll(prodResultList);
		}
		if (allParamsMap.get("categorys") != null) {
			selctionId = "categorys";
			querySelectionId = "sub_category_id";
			
			categoryResultList = getNonAggregateInternalQuery(session,allParamsMap,selctionId,querySelectionId);
			if(categoryResultList != null && categoryResultList.size() >0)
			totalResultsList.addAll(categoryResultList);
		}
		if (allParamsMap.get("subCategorys") != null) {
			selctionId = "subCategorys";
			querySelectionId = "product_id";
			
			subCategoryResultList = getNonAggregateInternalQuery(session,allParamsMap,selctionId,querySelectionId);
			if(subCategoryResultList != null && subCategoryResultList.size() >0)
			totalResultsList.addAll(subCategoryResultList);
		}
		
		HashMap<String, String> productIdAndNameMap = null;
		HashMap<String, String> categoryIdAndNameMap = null;
		HashMap<String, String> subCategoryAndNameMap = null;
		if (allParams.get("product") != null && !(allParams.get("product").equals("") || allParams.get("product").equals(" "))) {
			HashMap<String, String> productParamMap = predictiveService.getInputParamMap(
					allParams, "product");
			productIdAndNameMap = predictiveService.prodIdAndNameMap(new ArrayList<String>(productParamMap.keySet()));
		}
		if (allParams.get("cat") != null && !(allParams.get("cat").equals("") || allParams.get("cat").equals(" "))) {
			HashMap<String, String> categoryParamMap = predictiveService.getInputParamMap(
					allParams, "cat");
			subCategoryAndNameMap = predictiveService.subCategoryIdAndNameMapByCategory(new ArrayList<String>(categoryParamMap.keySet()));
			
		}
		if (allParams.get("subcatid") != null && !(allParams.get("subcatid").equals("") || allParams.get("subcatid").equals(" "))) {
			HashMap<String, String> subCategoryParamMap = predictiveService.getInputParamMap(
					allParams, "subcatid");
			productIdAndNameMap = predictiveService.prodIdAndNameMapBySubCategory(new ArrayList<String>(subCategoryParamMap.keySet()));
		}

		LinkedHashMap<String, List<HashMap<String, String>>> internalNonAggregateMap = new LinkedHashMap<String, List<HashMap<String, String>>>();
		List<HashMap<String, String>> internalInfoList = new ArrayList<HashMap<String, String>>();
		String productId = null;
		int count = 0;

		
		
		for (Object[] resultListObject : totalResultsList) {

			count++;
			if (productId == null
					|| (productId != null && !productId	.equals(resultListObject[0].toString()))
					|| count == totalResultsList.size()) {
				if (productId != null) {
					String prodName = "";
					if (productIdAndNameMap != null && productIdAndNameMap.get(productId) != null) {
						prodName = productIdAndNameMap.get(productId);
					} else if (categoryIdAndNameMap!=null && categoryIdAndNameMap.get(productId) != null) {
						prodName = categoryIdAndNameMap.get(productId);
					} else if (subCategoryAndNameMap != null && subCategoryAndNameMap.get(productId) != null) {
						prodName = subCategoryAndNameMap.get(productId);
					}
					internalNonAggregateMap.put(prodName, internalInfoList);
				}
				productId = resultListObject[0].toString();
				internalInfoList = new ArrayList<HashMap<String, String>>();
			}
			LinkedHashMap<String, String> priceAndPromotionMap = new LinkedHashMap<String, String>();
			priceAndPromotionMap.put("yearWeek", resultListObject[1].toString());
			priceAndPromotionMap.put("date", resultListObject[4].toString());
			priceAndPromotionMap.put("price", resultListObject[2].toString());
			priceAndPromotionMap.put("promotion",resultListObject[3].toString());
			priceAndPromotionMap.put("discount",resultListObject[5].toString());
			internalInfoList.add(priceAndPromotionMap);

		}	
		return internalNonAggregateMap;
	
		/* Mallikarjuna work
		 * StringBuilder hqlQuery = new StringBuilder();
		hqlQuery.append("select res.yearWeek,sum(res.price),sum(res.promotionFlag),res.calDay,sum(discount) from ProdLocTimeByDay res where (");
		hqlQuery.append(getCommonHqlQuery(allParamsMap));	
		hqlQuery.append("group by res.productId,res.yearWeek,res.calDay order by res.productId,res.yearWeek,res.calDay");	
		Query query = session.createQuery(hqlQuery.toString());				
		query = setCommonQueryParams(allParamsMap, query);		
		List<Object[]> resultList = query.list();		
		LinkedHashMap<String, List<HashMap<String, String>>> internalFactorMap = new LinkedHashMap<String, List<HashMap<String, String>>>();
		List<HashMap<String, String>> internalInfoList = new ArrayList<HashMap<String, String>>();
		for (Object[] resultListObject : resultList) {

			LinkedHashMap<String, String> priceAndPromotionMap = new LinkedHashMap<String, String>();
			priceAndPromotionMap.put("yearWeek", resultListObject[0].toString());
			priceAndPromotionMap.put("date", resultListObject[3].toString());
			priceAndPromotionMap.put("price", resultListObject[1].toString());
			priceAndPromotionMap.put("promotion",resultListObject[2].toString());
			priceAndPromotionMap.put("discount",resultListObject[4].toString());
			internalInfoList.add(priceAndPromotionMap);
			internalFactorMap.put("aggregateData", internalInfoList);

		}
		return internalFactorMap;
*/	
		
	}

	@Override
	public LinkedHashMap<String, List<HashMap<String, String>>> getInternalNonAggregageData(Map<String,String> allParams) {
		Session session =HibernateUtility.getSession().openSession();


		HashMap<String,HashMap<String,String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);

		String selctionId = null;
		String querySelectionId = null;
		
		List<Object[]> prodResultList = null;
		List<Object[]> categoryResultList = null;
		List<Object[]> subCategoryResultList = null;
		List<Object[]> totalResultsList = new ArrayList<Object[]>();
		if (allParamsMap.get("products") != null) {
			selctionId = "products";
			querySelectionId = "product_id";
			
			prodResultList = getNonAggregateInternalQuery(session,allParamsMap,selctionId,querySelectionId);
			if(prodResultList != null && prodResultList.size() >0)
			totalResultsList.addAll(prodResultList);
		}
		if (allParamsMap.get("categorys") != null) {
			selctionId = "categorys";
			querySelectionId = "sub_category_id";
			
			categoryResultList = getNonAggregateInternalQuery(session,allParamsMap,selctionId,querySelectionId);
			if(categoryResultList != null && categoryResultList.size() >0)
			totalResultsList.addAll(categoryResultList);
		}
		if (allParamsMap.get("subCategorys") != null) {
			selctionId = "subCategorys";
			querySelectionId = "product_id";
			
			subCategoryResultList = getNonAggregateInternalQuery(session,allParamsMap,selctionId,querySelectionId);
			if(subCategoryResultList != null && subCategoryResultList.size() >0)
			totalResultsList.addAll(subCategoryResultList);
		}
		
		HashMap<String, String> productIdAndNameMap = null;
		HashMap<String, String> categoryIdAndNameMap = null;
		HashMap<String, String> subCategoryAndNameMap = null;
		if (allParams.get("product") != null && !(allParams.get("product").equals("") || allParams.get("product").equals(" "))) {
			HashMap<String, String> productParamMap = predictiveService.getInputParamMap(
					allParams, "product");
			productIdAndNameMap = predictiveService.prodIdAndNameMap(new ArrayList<String>(productParamMap.keySet()));
		}
		if (allParams.get("cat") != null && !(allParams.get("cat").equals("") || allParams.get("cat").equals(" "))) {
			HashMap<String, String> categoryParamMap = predictiveService.getInputParamMap(
					allParams, "cat");
			subCategoryAndNameMap = predictiveService.subCategoryIdAndNameMapByCategory(new ArrayList<String>(categoryParamMap.keySet()));
			
		}
		if (allParams.get("subcatid") != null && !(allParams.get("subcatid").equals("") || allParams.get("subcatid").equals(" "))) {
			HashMap<String, String> subCategoryParamMap = predictiveService.getInputParamMap(
					allParams, "subcatid");
			productIdAndNameMap = predictiveService.prodIdAndNameMapBySubCategory(new ArrayList<String>(subCategoryParamMap.keySet()));
		}

		LinkedHashMap<String, List<HashMap<String, String>>> internalNonAggregateMap = new LinkedHashMap<String, List<HashMap<String, String>>>();
		List<HashMap<String, String>> internalInfoList = new ArrayList<HashMap<String, String>>();
		String productId = null;
		int count = 0;

		
		
		for (Object[] resultListObject : totalResultsList) {

			count++;
			if (productId == null
					|| (productId != null && !productId	.equals(resultListObject[0].toString()))
					|| count == totalResultsList.size()) {
				if (productId != null) {
					String prodName = "";
					if (productIdAndNameMap != null && productIdAndNameMap.get(productId) != null) {
						prodName = productIdAndNameMap.get(productId);
					} else if (categoryIdAndNameMap!=null && categoryIdAndNameMap.get(productId) != null) {
						prodName = categoryIdAndNameMap.get(productId);
					} else if (subCategoryAndNameMap != null && subCategoryAndNameMap.get(productId) != null) {
						prodName = subCategoryAndNameMap.get(productId);
					}
					internalNonAggregateMap.put(prodName, internalInfoList);
				}
				productId = resultListObject[0].toString();
				internalInfoList = new ArrayList<HashMap<String, String>>();
			}
			LinkedHashMap<String, String> priceAndPromotionMap = new LinkedHashMap<String, String>();
			priceAndPromotionMap.put("yearWeek", resultListObject[1].toString());
			priceAndPromotionMap.put("date", resultListObject[4].toString());
			priceAndPromotionMap.put("price", resultListObject[2].toString());
			priceAndPromotionMap.put("promotion",resultListObject[3].toString());
			priceAndPromotionMap.put("discount",resultListObject[5].toString());
			internalInfoList.add(priceAndPromotionMap);

		}	
		return internalNonAggregateMap;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getExternalAggregateData(Map<String,String> allParams){
		
		Session session =HibernateUtility.getSession().openSession();
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		StringBuilder hqlQuery = new StringBuilder();
		hqlQuery.append("select res.yearWeek,avg(res.maxTemp),avg(res.minTemp),avg(res.avgTemp),res.calDay from ProdLocTimeByDay res  where (");
		hqlQuery.append(getCommonHqlQuery(allParamsMap));
		hqlQuery.append("group by res.yearWeek,res.calDay order by res.yearWeek,res.calDay");
		Query query = session.createQuery(hqlQuery.toString());
		System.out.println(" query  ::" + query);
		query = setCommonQueryParams(allParamsMap, query);
		List<Object[]> resultList = query.list();
		System.out.println(" resultList  size ::" + resultList.size());

		return resultList;
	}
	
	@Override
	public List<Object[]> getNAExternalData(
			Map<String, String> allParams) {
		
		Session session =HibernateUtility.getSession().openSession();
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		
		String selctionId = null;
		String querySelectionId = null;
		
		List<Object[]> countryResultList = null;
		List<Object[]> regionResultList = null;
		List<Object[]> stateResultList = null;
		List<Object[]> districtResultList = null;
		List<Object[]> cityResultList = null;
		List<Object[]> locationResultList = null;
		List<Object[]> totalResultsList = new ArrayList<Object[]>();
		
		
		if (allParamsMap.get("countrys") != null) {
			selctionId = "countrys";
			querySelectionId = "region_id";
			
			countryResultList = getNonAggregateExternalCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(countryResultList != null && countryResultList.size() >0)
			totalResultsList.addAll(countryResultList);
		}
		if (allParamsMap.get("regions") != null) {
			selctionId = "regions";
			querySelectionId = "state_id";
			
			regionResultList = getNonAggregateExternalCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(regionResultList != null && regionResultList.size() >0)
			totalResultsList.addAll(regionResultList);
		}
		if (allParamsMap.get("states") != null) {
			selctionId = "states";
			querySelectionId = "district_id";
			
			stateResultList = getNonAggregateExternalCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(stateResultList != null && stateResultList.size() >0)
			totalResultsList.addAll(stateResultList);
		}
		
		if (allParamsMap.get("districts") != null) {
			selctionId = "districts";
			querySelectionId = "city_id";
			
			districtResultList = getNonAggregateExternalCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(districtResultList != null && districtResultList.size() >0)
			totalResultsList.addAll(districtResultList);
		}
		
		if (allParamsMap.get("citys") != null) {
			selctionId = "citys";
			querySelectionId = "location_id";
			
			cityResultList = getNonAggregateExternalCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(cityResultList != null && cityResultList.size() >0)
			totalResultsList.addAll(cityResultList);
		}
		
		if (allParamsMap.get("locations") != null) {
			selctionId = "locations";
			querySelectionId = "location_id";
			
			locationResultList = getNonAggregateExternalCommonQuery(session,allParamsMap,selctionId,querySelectionId);
			if(locationResultList != null && locationResultList.size() >0)
			totalResultsList.addAll(locationResultList);
		}
		
		
		System.out.println(" external factors NA resultList  size ::" + totalResultsList.size());

		return totalResultsList;
	}

	public String getCommonHqlQuery(
			HashMap<String, HashMap<String, String>> allParamsMap) {
		StringBuilder commonQuery = new StringBuilder();
		if (allParamsMap.get("products") != null) {
			commonQuery.append("res.productId in (:productIdList) or ");
		}
		if (allParamsMap.get("categorys") != null) {
			commonQuery.append("res.categoryId in (:categoryIdList) or ");
		}
		if (allParamsMap.get("subCategorys") != null) {
			commonQuery
					.append("res.subCategoryId in (:subCategoryIdList) ");
		}
		commonQuery.append(") and (");
		if (allParamsMap.get("countrys") != null) {
			commonQuery.append("res.countryId in (:countryIdList) or ");
		}
		if (allParamsMap.get("regions") != null) {
			commonQuery.append("res.regionId in (:regionIdList) or ");
		}
		if (allParamsMap.get("states") != null) {
			commonQuery.append("res.stateId in (:stateIdList) or ");
		}
		if (allParamsMap.get("districts") != null) {
			commonQuery.append("res.districtId in (:districtId) or ");
		}
		if (allParamsMap.get("locations") != null) {
			commonQuery.append("res.locationId in (:locationIdList) or ");
		}
		if (allParamsMap.get("citys") != null) {
			commonQuery.append("res.cityId in (:cityIdList) ");
		}
		commonQuery.append(") and ");
		commonQuery
				.append("(res.yearWeek>=:fromYearWeek and res.yearWeek<=:toYearWeek) ");
		String lastQuery = commonQuery.toString();
		String xy = "or ) and";
		String yz = ") and";
		lastQuery = lastQuery.replace(xy,yz);
		System.out.println("last query:::::::::::::::::::::::::::::::::::"+lastQuery);
		return lastQuery;
	}

	public String getCommonSqlQuery(
			HashMap<String, HashMap<String, String>> allParamsMap,String prodSelectionType) {
		StringBuilder commonQuery = new StringBuilder();
		if (prodSelectionType.equals("products")) {
			commonQuery.append("product_id in (:productIdList) or ");
		}
		if (prodSelectionType.equals("categorys")) {
			commonQuery.append("category_id in (:categoryIdList) or ");
		}
		if (prodSelectionType.equals("subCategorys")) {
			commonQuery.append("sub_category_id in (:subCategoryIdList)  ");
		}
		commonQuery.append(") and (");
		if (allParamsMap.get("countrys") != null) {
			commonQuery.append("country_id in (:countryIdList) or ");
		}
		if (allParamsMap.get("regions") != null) {
			commonQuery.append("region_id in (:regionIdList) or ");
		}
		if (allParamsMap.get("states") != null) {
			commonQuery.append("state_id in (:stateIdList) or ");
		}
		if (allParamsMap.get("districts") != null) {
			commonQuery.append("district_id in (:districtId) or ");
		}
		if (allParamsMap.get("locations") != null) {
			commonQuery.append("location_id in (:locationIdList) or ");
		}
		if (allParamsMap.get("citys") != null) {
			commonQuery.append("city_id in (:cityIdList) ");
		}
		commonQuery.append(") and ");
		commonQuery
				.append("(year_week>=:fromYearWeek and year_week<=:toYearWeek) ");
		String lastQuery = commonQuery.toString();
		String xy = "or ) and";
		String yz = ") and";
		lastQuery = lastQuery.replace(xy,yz);
		return lastQuery;
	}
	
	public String getExternalCommonSqlQuery(
			HashMap<String, HashMap<String, String>> allParamsMap,String prodSelectionType) {
		StringBuilder commonQuery = new StringBuilder();
		
		if (prodSelectionType.equals("countrys")) {
			commonQuery.append("country_id in (:countryIdList) or ");
		}
		if (prodSelectionType.equals("regions")) {
			commonQuery.append("region_id in (:regionIdList) or ");
		}
		if (prodSelectionType.equals("states")) {
			commonQuery.append("state_id in (:stateIdList) or ");
		}
		if (prodSelectionType.equals("districts")) {
			commonQuery.append("district_id in (:districtId) or ");
		}
		if (prodSelectionType.equals("locations")) {
			commonQuery.append("location_id in (:locationIdList) or ");
		}
		if (prodSelectionType.equals("citys")) {
			commonQuery.append("city_id in (:cityIdList) ");
		}
		commonQuery.append(") and ");
		commonQuery
				.append("(year_week>=:fromYearWeek and year_week<=:toYearWeek) ");
		String lastQuery = commonQuery.toString();
		String xy = "or ) and";
		String yz = ") and";
		lastQuery = lastQuery.replace(xy,yz);
		System.out.println("last query:::::::::::::::::::::::::::::::::::"+lastQuery);
		return lastQuery;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Query setCommonQueryParams(
			HashMap<String, HashMap<String, String>> allParamsMap, Query query) {
		if (allParamsMap.get("products") != null) {
			query.setParameterList("productIdList", new ArrayList(allParamsMap
					.get("products").keySet()));
		}
		if (allParamsMap.get("categorys") != null) {
			query.setParameterList("categoryIdList", new ArrayList(allParamsMap
					.get("categorys").keySet()));
		}
		if (allParamsMap.get("subCategorys") != null) {
			query.setParameterList("subCategoryIdList", new ArrayList(
					allParamsMap.get("subCategorys").keySet()));
		}
		query = setLocationSelectionParameters(allParamsMap, query);
		String startDate = allParamsMap.get("dates").get("startDate");
		String endDate = allParamsMap.get("dates").get("endDate");
		query.setParameter("fromYearWeek", Integer.parseInt(startDate));
		query.setParameter("toYearWeek", Integer.parseInt(endDate));
		return query;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Query setSQLCommonParameters(HashMap<String, HashMap<String, String>> allParamsMap, Query query,String prodSelectionType){
		if (prodSelectionType.equals("products")) {
			query.setParameterList("productIdList", new ArrayList(allParamsMap.get("products").keySet()));
		}
		if (prodSelectionType.equals("categorys")) {
			query.setParameterList("categoryIdList", new ArrayList(allParamsMap.get("categorys").keySet()));		
		}
		if (prodSelectionType.equals("subCategorys")) {
			query.setParameterList("subCategoryIdList", new ArrayList(
					allParamsMap.get("subCategorys").keySet()));
		}
		if(prodSelectionType.equals("countrys") || prodSelectionType.equals("regions") || prodSelectionType.equals("states") || prodSelectionType.equals("districts") || prodSelectionType.equals("citys") || prodSelectionType.equals("locations")){
			query = setLocationSelectionParamsForExternal(allParamsMap, query,prodSelectionType);
		}else{
			query = setLocationSelectionParameters(allParamsMap, query);
		}
		return query;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Query setLocationSelectionParamsForExternal(HashMap<String, HashMap<String, String>> allParamsMap, Query query,String prodSelectionType){
		if (prodSelectionType.equals("countrys")) {
			query.setParameterList("countryIdList", new ArrayList(allParamsMap
					.get("countrys").keySet()));
		}
		if (prodSelectionType.equals("regions")) {
			query.setParameterList("regionIdList", new ArrayList(allParamsMap
					.get("regions").keySet()));
		}
		if (prodSelectionType.equals("states")) {
			query.setParameterList("stateIdList", new ArrayList(allParamsMap
					.get("states").keySet()));
		}
		if (prodSelectionType.equals("districts")) {
			query.setParameterList("districtId", new ArrayList(allParamsMap
					.get("districts").keySet()));
		}
		if (prodSelectionType.equals("locations")) {
			query.setParameterList("locationIdList", new ArrayList(allParamsMap
					.get("locations").keySet()));
		}
		if (prodSelectionType.equals("citys")) {
			query.setParameterList("cityIdList", new ArrayList(allParamsMap
					.get("citys").keySet()));
		}
		return query;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Query setLocationSelectionParameters(HashMap<String, HashMap<String, String>> allParamsMap, Query query){
		if (allParamsMap.get("countrys") != null) {
			query.setParameterList("countryIdList", new ArrayList(allParamsMap
					.get("countrys").keySet()));
		}
		if (allParamsMap.get("regions") != null) {
			query.setParameterList("regionIdList", new ArrayList(allParamsMap
					.get("regions").keySet()));
		}
		if (allParamsMap.get("states") != null) {
			query.setParameterList("stateIdList", new ArrayList(allParamsMap
					.get("states").keySet()));
		}
		if (allParamsMap.get("districts") != null) {
			query.setParameterList("districtId", new ArrayList(allParamsMap
					.get("districts").keySet()));
		}
		if (allParamsMap.get("locations") != null) {
			query.setParameterList("locationIdList", new ArrayList(allParamsMap
					.get("locations").keySet()));
		}
		if (allParamsMap.get("citys") != null) {
			query.setParameterList("cityIdList", new ArrayList(allParamsMap
					.get("citys").keySet()));
		}
		return query;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getNonAggregateCommonQuery(Session session,HashMap<String, HashMap<String, String>> allParamsMap,String selectionId,String querySelectionId){
		StringBuilder sqlQuery = new StringBuilder();
		//getSchema();
		sqlQuery.append("select res."+querySelectionId+" as prodId,res.year_week as yearWeek,sum(res.quantity) as actual,sum(res.final_qty) as forecast,res.cal_day as calDay from ");
		sqlQuery.append(" (select "+querySelectionId+",year_week,quantity,final_qty,cal_day from "+UserController.userSchemaName+"."+"rplus_ds_output_f_day where (");
		sqlQuery.append(getCommonSqlQuery(allParamsMap,selectionId));
		sqlQuery.append("order by year_week,cal_day) as res ");
		sqlQuery.append(" group by res."+querySelectionId+",res.year_week,res.cal_day order by res."+querySelectionId+",res.year_week,res.cal_day");

		Query query = session.createSQLQuery(sqlQuery.toString());

		String startDate = allParamsMap.get("dates").get("startDate");
		String endDate = allParamsMap.get("dates").get("endDate");
		query.setParameter("fromYearWeek", Integer.parseInt(startDate));
		query.setParameter("toYearWeek", Integer.parseInt(endDate));
		query = setSQLCommonParameters(allParamsMap, query,selectionId);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getNonAggregateSocialCommonQuery(Session session,HashMap<String, HashMap<String, String>> allParamsMap,String selectionId,String querySelectionId){
		StringBuilder sqlQuery = new StringBuilder();
	
		//getSchema();
		sqlQuery.append("select res."+querySelectionId+" as prodId,res.year_week as yearWeek,sum(res.sentiment_index) as actual,max(res.srs) as forecast,res.cal_day as calDay from ");
		sqlQuery.append(" (select "+querySelectionId+",year_week,sentiment_index,srs,cal_day from "+UserController.userSchemaName+"."+"rplus_prod_loc_time_day where (");
		sqlQuery.append(getCommonSqlQuery(allParamsMap,selectionId));
		sqlQuery.append("order by year_week,cal_day) as res ");
		sqlQuery.append(" group by res."+querySelectionId+",res.year_week,res.cal_day order by res."+querySelectionId+",res.year_week,res.cal_day");

		Query query = session.createSQLQuery(sqlQuery.toString());

		String startDate = allParamsMap.get("dates").get("startDate");
		String endDate = allParamsMap.get("dates").get("endDate");
		query.setParameter("fromYearWeek", Integer.parseInt(startDate));
		query.setParameter("toYearWeek", Integer.parseInt(endDate));
		query = setSQLCommonParameters(allParamsMap, query,selectionId);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getNonAggregateExternalCommonQuery(Session session,HashMap<String, HashMap<String, String>> allParamsMap,String selectionId,String querySelectionId){
		StringBuilder sqlQuery = new StringBuilder();
		//getSchema();
		//select res.yearWeek,avg(res.maxTemp),avg(res.minTemp),avg(res.precipitation),avg(res.feellikeTemp),avg(res.solar),res.calDay from ProdLocTimeByDay res  where
		
		sqlQuery.append("select res."+querySelectionId+",res.year_week,res.max_temp,res.min_temp,res.avg_temp,res.cal_day from ");
		sqlQuery.append(" (select "+querySelectionId+",year_week,max_temp,min_temp,avg_temp,cal_day from "+UserController.userSchemaName+"."+"rplus_prod_loc_time_day where (");
		sqlQuery.append(getExternalCommonSqlQuery(allParamsMap,selectionId));
		sqlQuery.append("order by "+querySelectionId+",year_week,cal_day) as res ");
		sqlQuery.append(" group by res."+querySelectionId+",res.year_week,res.max_temp,res.min_temp,res.avg_temp,res.cal_day order by res."+querySelectionId+",res.year_week,res.cal_day");

		Query query = session.createSQLQuery(sqlQuery.toString());

		String startDate = allParamsMap.get("dates").get("startDate");
		String endDate = allParamsMap.get("dates").get("endDate");
		query.setParameter("fromYearWeek", Integer.parseInt(startDate));
		query.setParameter("toYearWeek", Integer.parseInt(endDate));
		query = setSQLCommonParameters(allParamsMap, query,selectionId);
		return query.list();
	}
		
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getNonAggregateInternalQuery(Session session,HashMap<String, HashMap<String, String>> allParamsMap,String selectionId,String querySelectionId){
		StringBuilder sqlQuery = new StringBuilder();
		//getSchema();
		sqlQuery.append("select res."+querySelectionId+" as prodId,res.year_week as yearWeek,sum(res.price) as price,res.sales_executive_count as promotion,res.cal_day,res.location_count as calDay from ");
		sqlQuery.append(" (select "+querySelectionId+",year_week,price,sales_executive_count,cal_day,location_count from "+UserController.userSchemaName+"."+"rplus_prod_loc_time_day where (");
		sqlQuery.append(getCommonSqlQuery(allParamsMap,selectionId));
		sqlQuery.append("order by year_week,cal_day) as res ");
		sqlQuery.append(" group by res."+querySelectionId+",res.year_week,res.sales_executive_count,res.cal_day,res.location_count order by res."+querySelectionId+",res.year_week,res.cal_day");

		Query query = session.createSQLQuery(sqlQuery.toString());
		System.out.println("sqlQuery ################"+sqlQuery);

		String startDate = allParamsMap.get("dates").get("startDate");
		String endDate = allParamsMap.get("dates").get("endDate");
		query.setParameter("fromYearWeek", Integer.parseInt(startDate));
		query.setParameter("toYearWeek", Integer.parseInt(endDate));
		query = setSQLCommonParameters(allParamsMap, query,selectionId);
		return query.list();
	}
		
	@SuppressWarnings("unchecked")
	public List<RplusDsOutputFDay> getRplusOutputFdata(Map<String,String> allParams) {
		System.out.println("In getRplusOutputFdata()::::::::::::::");
		 Session session =HibernateUtility.getSession().openSession();
		 HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		 StringBuilder hqlQuery = new StringBuilder();
		 hqlQuery.append("from RplusDsOutputFDay res where (");
         //List<RplusDsOutputSFDay> rplusDsOutputSFList = session.createQuery("from RplusDsOutputSFDay").list();
 		hqlQuery.append(getCommonHqlQuery(allParamsMap));
 		Query query = session.createQuery(hqlQuery.toString());
		query = setCommonQueryParams(allParamsMap, query);
		List<RplusDsOutputFDay> rplusDsOutputFList = query.list();
		return rplusDsOutputFList;
	}
	
	
	/**
	 * This method used to get the filtered accuracy data.
	 * Mukesh
	 */
	
	@SuppressWarnings("unchecked")
	public List<RplusDsOutputADay> getRplusOutputAdata(Map<String,String> allParams) {
		System.out.println("In getRplusOutputAdata()::::::::::::::");
		Session session=HibernateUtility.getSession().openSession();
		HashMap<String,HashMap<String,String>> allParamsMap=predictiveService.getAllInputParamsMap(allParams);
		StringBuilder hqlQuery=new StringBuilder();
		hqlQuery.append("from RplusDsOutputADay res where(");
		hqlQuery.append(getCommonHqlQueryForSFandATable(allParamsMap));
		Query query = session.createQuery(hqlQuery.toString());
		query=setCommonQueryParamsForSFandATable(allParamsMap,query);
		//Query query = session.createQuery("from RplusDsOutputADay");
		List<RplusDsOutputADay> rplusDsOutputAList =query.list();
		System.out.println("Result ::::::::::::::::"+query.list());
		session.close();
		return rplusDsOutputAList;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<RplusDsOutputSFDay> getRplusOutputSFdata(Map<String,String> allParams) {
		System.out.println("In getRplusOutpuSFdata()::::::::::::::");
		Session session=HibernateUtility.getSession().openSession();
		HashMap<String,HashMap<String,String>> allParamsMap=predictiveService.getAllInputParamsMap(allParams);
		StringBuilder hqlQuery=new StringBuilder();
		hqlQuery.append("from RplusDsOutputSFDay res where(");
		hqlQuery.append(getCommonHqlQueryForSFandATable(allParamsMap));
		Query query = session.createQuery(hqlQuery.toString());
		query=setCommonQueryParamsForSFandATable(allParamsMap,query);
		//Query query = session.createQuery("from RplusDsOutputSFDay");
		List<RplusDsOutputSFDay> rplusDsOutputSFList =query.list();
		System.out.println("Result ::::::::::::::::"+query.list());
		session.close();
		return rplusDsOutputSFList;
		
	}
	

	public String getCommonHqlQueryForSFandATable(
			HashMap<String, HashMap<String, String>> allParamsMap) {
		StringBuilder commonQuery = new StringBuilder();
		if (allParamsMap.get("products") != null) {
			commonQuery.append("res.productId in (:productIdList) or ");
		}
		if (allParamsMap.get("categorys") != null) {
			commonQuery.append("res.categoryId in (:categoryIdList) or ");
		}
		if (allParamsMap.get("subCategorys") != null) {
			commonQuery
					.append("res.subCategoryId in (:subCategoryIdList) ");
		}
		commonQuery.append(") and (");
		if (allParamsMap.get("countrys") != null) {
			commonQuery.append("res.countryId in (:countryIdList) or ");
		}
		if (allParamsMap.get("regions") != null) {
			commonQuery.append("res.regionId in (:regionIdList) or ");
		}
		if (allParamsMap.get("states") != null) {
			commonQuery.append("res.stateId in (:stateIdList) or ");
		}
		if (allParamsMap.get("districts") != null) {
			commonQuery.append("res.districtId in (:districtId) or ");
		}
		if (allParamsMap.get("locations") != null) {
			commonQuery.append("res.locationId in (:locationIdList) or ");
		}
		if (allParamsMap.get("citys") != null) {
			commonQuery.append("res.cityId in (:cityIdList) ");
		}
		commonQuery.append(")");
		//commonQuery.append("(res.yearWeek>=:fromYearWeek and res.yearWeek<=:toYearWeek) ");
		String lastQuery = commonQuery.toString();
		String xy = "or )";
		String yz = ")";
		lastQuery = lastQuery.replace(xy,yz);
		System.out.println("last query:::::::::::::::::::::::::::::::::::"+lastQuery);
		return lastQuery;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Query setCommonQueryParamsForSFandATable(
			HashMap<String, HashMap<String, String>> allParamsMap, Query query) {
		if (allParamsMap.get("products") != null) {
			query.setParameterList("productIdList", new ArrayList(allParamsMap
					.get("products").keySet()));
		}
		if (allParamsMap.get("categorys") != null) {
			query.setParameterList("categoryIdList", new ArrayList(allParamsMap
					.get("categorys").keySet()));
		}
		if (allParamsMap.get("subCategorys") != null) {
			query.setParameterList("subCategoryIdList", new ArrayList(
					allParamsMap.get("subCategorys").keySet()));
		}
		query = setLocationSelectionParameters(allParamsMap, query);
		//String startDate = allParamsMap.get("dates").get("startDate");
		//String endDate = allParamsMap.get("dates").get("endDate");
		//query.setParameter("fromYearWeek", Integer.parseInt(startDate));
		//query.setParameter("toYearWeek", Integer.parseInt(endDate));
		return query;
	}
	

	
}
