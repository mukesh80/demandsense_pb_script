package com.rplus.ds.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
/**
 * 
 * @author Jerald Joseph J
 *
 */
public interface DataExploreDAO {
	
	@Autowired(required=true)
	public List<Object[]> getForecastData(Map<String,String> allParams);	
	
	@Autowired(required=true)
	public List<Object[]> getForecastNonAggregageData(Map<String,String> allParams);
	
	@Autowired(required=true)
	public List<Object[]> getForecastData200(String locId);
	
	@Autowired(required=true)
	public List<Object[]> getForecastDataAllParams(String hqlStr, HashMap<String, HashMap<String, String>> allParamsMap);
	
	@Autowired(required=true)
	public List<Object[]> getSignificantData200();
	
	@Autowired(required=true)
	public List<Object[]> getSignificantDataAllParams(String hqlStr, HashMap<String, HashMap<String, String>> allParamsMap);
	
	@Autowired(required=true)
	public List<Object[]> getAccuracyData200();
	
	@Autowired(required=true)
	public List<Object[]> getAccuracyDataAllParams(String hqlStr, HashMap<String, HashMap<String, String>> allParamsMap);

}
