package com.rplus.ds.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.model.SQLJobGroup;
import com.rplus.ds.model.TaskTrack;
import com.rplus.ds.model.User;

@Repository
@Component
public class SQLScriptDaoImpl implements SQLScriptDao {

	    private static final Log log = LogFactory.getLog(DimProductDAOImpl.class);

	    @Autowired
	    private SessionFactory sessionFactory;
	    
	    public void setSessionFactory(SessionFactory sessionFactory) {
	        this.sessionFactory = sessionFactory;
	    }
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SQLJobGroup> getSQLGroupdata() {
		 log.debug("getting data from SQLJOBGroup");
		 try{
			 Session session=sessionFactory.openSession();
			 List<SQLJobGroup> result=session.createQuery("from SQLJobGroup").list();
			 return result;
		 }catch(Exception e){
			 e.printStackTrace();
			 return null;
			 
		}
		 
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUsers() {
		 log.debug("getting data from User");
		 try{
			 Session session=sessionFactory.openSession();
			 List<User> result=session.createQuery("from User").list();
			 return result;
		 }catch(Exception e){
			 e.printStackTrace();
			 return null;
			 
		}
		 
		
	}

	@Override
	public boolean saveTaskTrack(TaskTrack tasktrack) {
		Session session=sessionFactory.openSession();
		try{
			session.saveOrUpdate(tasktrack);
			return true;
		}catch(Exception e){
			e.printStackTrace();
				return false;
			}
		
		
	}
	
	

}
