package com.rplus.ds.dao.upload;

import java.util.List;

import org.springframework.stereotype.Component;

import com.rplus.ds.model.upload.DimCustomer;
import com.rplus.ds.model.upload.DimLocation;
import com.rplus.ds.model.upload.DimProduct;
import com.rplus.ds.model.upload.DimSocial;
import com.rplus.ds.model.upload.DimTime;
import com.rplus.ds.model.upload.FactEvent;
import com.rplus.ds.model.upload.FactExternal;
import com.rplus.ds.model.upload.FactInventory;
import com.rplus.ds.model.upload.FactPrice;
import com.rplus.ds.model.upload.FactPromotion;
import com.rplus.ds.model.upload.FactSales;
import com.rplus.ds.model.upload.FactSocial;
import com.rplus.ds.model.upload.InvoiceSales;

@Component
public interface ViewFileDAO {

	
	public List<DimProduct> viewProductFile();
	
	public List<DimLocation> viewLocationFile();
	
	public List<DimTime> viewTimeFile();
	
	public List<FactExternal> viewFactExternalFile();
	
	/*public List<FactSales> viewFactSalesFile();*/
	
	public List<InvoiceSales> viewFactSalesFile();
	
	public List<FactSocial> viewFactSocialFile();
	
	public List<DimCustomer> viewDimCustomerFile();
	
	public List<DimSocial> viewDimSocialFile();
	
	public List<FactEvent> viewFactEventFile();
	
	public List<FactInventory> viewFactInventoryFile();
	
	public List<FactPrice> viewFactPriceFile();
	
	public List<FactPromotion> viewFactPromotionFile();
	
}
