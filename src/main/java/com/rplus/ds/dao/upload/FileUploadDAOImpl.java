package com.rplus.ds.dao.upload;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rplus.ds.model.upload.DimCustomer;
import com.rplus.ds.model.upload.DimLocation;
import com.rplus.ds.model.upload.DimProduct;
import com.rplus.ds.model.upload.DimSocial;
import com.rplus.ds.model.upload.DimTime;
import com.rplus.ds.model.upload.FactEvent;
import com.rplus.ds.model.upload.FactExternal;
import com.rplus.ds.model.upload.FactInventory;
import com.rplus.ds.model.upload.FactPrice;
import com.rplus.ds.model.upload.FactPromotion;
import com.rplus.ds.model.upload.FactSales;
import com.rplus.ds.model.upload.FactSocial;
import com.rplus.ds.model.upload.InvoiceSales;



@Repository
@Component
public class FileUploadDAOImpl implements FileUploadDAO{
	
	@Autowired
    private SessionFactory sessionFactory;
     
    public FileUploadDAOImpl() {
    }
 
    public FileUploadDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 
    @Override
    @Transactional
    public void save(DimProduct dimProduct) {
        sessionFactory.getCurrentSession().saveOrUpdate(dimProduct);
    }
   

    @Override
    @Transactional
	public void save(DimLocation dimLocation) {
		sessionFactory.getCurrentSession().saveOrUpdate(dimLocation);
		
	}
    
    @Override
    @Transactional
    public void save(DimCustomer dimCustomer) {
        sessionFactory.getCurrentSession().saveOrUpdate(dimCustomer);
    }
    
    @Override
    @Transactional
    public void save(DimSocial dimSocial) {
        sessionFactory.getCurrentSession().saveOrUpdate(dimSocial);
    }
	
	@Override
	@Transactional
	public void save(DimTime time) {
		sessionFactory.getCurrentSession().saveOrUpdate(time);
		
	}
	@Override
	@Transactional
	public void save(FactEvent event) {
		sessionFactory.getCurrentSession().saveOrUpdate(event);
		
	}
	@Override
	@Transactional
	public void save(FactInventory inventory) {
		sessionFactory.getCurrentSession().saveOrUpdate(inventory);
		
	}
	@Override
	@Transactional
	public void save(FactSales sales) {
		sessionFactory.getCurrentSession().saveOrUpdate(sales);
		
	}
	@Override
	@Transactional
	public void save(FactPrice price) {
		sessionFactory.getCurrentSession().saveOrUpdate(price);
		
	}
	@Override
	@Transactional
	public void save(FactPromotion promotion) {
		sessionFactory.getCurrentSession().saveOrUpdate(promotion);
		
	}

	
	
	@Override
	@Transactional
	public void save(FactExternal factExternal) {
		sessionFactory.getCurrentSession().saveOrUpdate(factExternal);
		
	}
	
	@Override
	@Transactional
	public void save(FactSocial factSocial) {
		sessionFactory.getCurrentSession().saveOrUpdate(factSocial);
		
	}

	@Override
	@Transactional
	public void save(InvoiceSales invoiceSales) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(invoiceSales);
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		
	}
}
