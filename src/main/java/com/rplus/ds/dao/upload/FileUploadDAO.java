package com.rplus.ds.dao.upload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rplus.ds.model.upload.DimCustomer;
import com.rplus.ds.model.upload.DimLocation;
import com.rplus.ds.model.upload.DimProduct;
import com.rplus.ds.model.upload.DimSocial;
import com.rplus.ds.model.upload.DimTime;
import com.rplus.ds.model.upload.FactEvent;
import com.rplus.ds.model.upload.FactExternal;
import com.rplus.ds.model.upload.FactInventory;
import com.rplus.ds.model.upload.FactPrice;
import com.rplus.ds.model.upload.FactPromotion;
import com.rplus.ds.model.upload.FactSales;
import com.rplus.ds.model.upload.FactSocial;
import com.rplus.ds.model.upload.InvoiceSales;

@Component
public interface FileUploadDAO {
	
	

	@Autowired(required=true)
    void save(DimProduct dimProduct);
	
	@Autowired(required=true)
    void save(InvoiceSales invoiceSales);
	
	@Autowired(required=true)
    void save(DimLocation dimLocation);
	
	@Autowired(required=true)
    void save(DimCustomer dimCustomer);
	
	@Autowired(required=true)
    void save(DimSocial dimSocial);
	
	@Autowired(required=true)
	void save(DimTime time);
	
	@Autowired(required=true)
	void save(FactEvent sales);
	
	@Autowired(required=true)
	void save(FactInventory sales);
	
	@Autowired(required=true)
	void save(FactPrice sales);
	
	@Autowired(required=true)
	void save(FactPromotion sales);
	

	@Autowired(required=true)
	void save(FactSales sales);

	
	@Autowired(required=true)
	void save(FactExternal factExternal);
	
	@Autowired(required=true)
	void save(FactSocial factSocial);
}
