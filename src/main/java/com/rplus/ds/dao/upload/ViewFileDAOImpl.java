package com.rplus.ds.dao.upload;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rplus.ds.model.upload.DimCustomer;
import com.rplus.ds.model.upload.DimLocation;
import com.rplus.ds.model.upload.DimProduct;
import com.rplus.ds.model.upload.DimSocial;
import com.rplus.ds.model.upload.DimTime;
import com.rplus.ds.model.upload.FactEvent;
import com.rplus.ds.model.upload.FactExternal;
import com.rplus.ds.model.upload.FactInventory;
import com.rplus.ds.model.upload.FactPrice;
import com.rplus.ds.model.upload.FactPromotion;
import com.rplus.ds.model.upload.FactSales;
import com.rplus.ds.model.upload.FactSocial;
import com.rplus.ds.model.upload.InvoiceSales;

@Component
public class ViewFileDAOImpl implements ViewFileDAO{

	@Autowired
    private SessionFactory sessionFactory;
	
	public ViewFileDAOImpl(){}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DimProduct> viewProductFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from DimProduct");
		List<DimProduct> productlist=query.list();
		return productlist;
	}

	@Override
	public List<DimLocation> viewLocationFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from DimLocation");
		List<DimLocation> locationlist=query.list();
		return locationlist;	
		
	}

	@Override
	public List<DimCustomer> viewDimCustomerFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from DimCustomer");
		List<DimCustomer> dimCustomerlist=query.list();
		return dimCustomerlist;	
	}

	@Override
	public List<DimSocial> viewDimSocialFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from DimSocial");
		List<DimSocial> dimSociallist=query.list();
		return dimSociallist;	
	}

	@Override
	public List<DimTime> viewTimeFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from DimTime");
		@SuppressWarnings("unchecked")
		List<DimTime> dimTimelist=query.list();
		return dimTimelist;	
	}

	@Override
	public List<FactEvent> viewFactEventFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from FactEvent");
		List<FactEvent> factEventlist=query.list();
		return factEventlist;
	}

	@Override
	public List<FactInventory> viewFactInventoryFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from FactInventory");
		List<FactInventory> factInventorylist=query.list();
		return factInventorylist;
	}

	@Override
	public List<FactExternal> viewFactExternalFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from FactExternal");
		List<FactExternal> factExternallist=query.list();
		return factExternallist;	
	}

	@Override
	public List<FactPrice> viewFactPriceFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from FactPrice");
		List<FactPrice> FactPricelist=query.list();
		return FactPricelist;
	}

	@Override
	public List<FactPromotion> viewFactPromotionFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from FactPromotion");
		List<FactPromotion> factPromotionlist=query.list();
		return factPromotionlist;
	}

	/*@Override
	public List<FactSales> viewFactSalesFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from FactSales");
		List<FactSales> factSaleslist=query.list();
		return factSaleslist;	
	}*/
	
	@Override
	public List<InvoiceSales> viewFactSalesFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from InvoiceSales");
		List<InvoiceSales> factSaleslist=query.list();
		return factSaleslist;	
	}

	@Override
	public List<FactSocial> viewFactSocialFile() {
		Session session=sessionFactory.openSession();
		Query query= session.createQuery("from FactSocial");
		List<FactSocial> factSociallist=query.list();
		return factSociallist;	
	}

}
