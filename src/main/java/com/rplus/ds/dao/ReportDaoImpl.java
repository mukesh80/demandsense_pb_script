package com.rplus.ds.dao;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.config.HibernateUtility;
import com.rplus.ds.controller.UserController;


@Repository
@Component
public class ReportDaoImpl implements ReportDao {
	
	/*Properties prop = new Properties();
	InputStream input = null;
	final static String resourceName = "application.properties";
	ClassLoader loader = Thread.currentThread().getContextClassLoader();
	
	
	// non-static initialization block
	{
		 try(InputStream resourceStream = loader.getResourceAsStream(resourceName)) {

				
				prop.load(resourceStream);
				UserController.userSchemaName+"."=prop.getProperty("hibernate.default_schema")+".";
				System.out.println(prop.getProperty("hibernate.default_schema"));

			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		 
	 }
	 
*/	

	



	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object> getMonthReport(Map<String, String> allParams) {

		Integer fromDate = Integer.parseInt(allParams.get("fromdate"));
		Integer toDate = Integer.parseInt(allParams.get("todate"));
		String[] citys = allParams.get("cityid").split(",");
		System.out.println("Hibernate query:::>>>>>>>> "+fromDate+"----"+toDate+"----"+citys);
		List<Object> result = null;
		
		Query query;
		Session session =HibernateUtility.getSession().openSession();
		
		for(int i=0;i<citys.length;i++){
			List<Object> uniqueQureyList = null;

			String sqlQuery1="select fday.region_id,fday.region_id_desc,fday.state_id,fday.state_id_desc,fday.district_id,fday.district_id_desc,fday.product_id,fday.product_id_desc,fday.year_month as yearweek ,fday.actual_value,fday.predicted_mean_value from "+UserController.userSchemaName+"."+"rplus_ds_report_month fday ";
			String sqlQuery2="where fday.district_id IN (:cityid) and (fday.year_month>=:fromYearWeek and fday.year_month<=:toYearWeek) order by fday.district_id, fday.year_month,fday.product_id"; 

			String hqlQuery=sqlQuery1.concat(sqlQuery2);
			System.out.println("Hibernate query::: "+hqlQuery);
			query=session.createSQLQuery(hqlQuery);
			query.setParameter("cityid", citys[i]);
			query.setInteger("fromYearWeek", fromDate); 
			query.setInteger("toYearWeek", toDate);

			uniqueQureyList=query.list();
			System.out.println("size of result >>>>>>> "+uniqueQureyList.size());
			
			if(result == null){
				result = uniqueQureyList;
			}else{

				result.addAll(uniqueQureyList);
			}
			
			
		}
		System.out.println("size of final result===== >>>>>>> "+result.size());
		return result;
}


@SuppressWarnings("unchecked")
public List<Object> getWeekReport(Map<String, String> allParams) {
	
	Integer fromDate = Integer.parseInt(allParams.get("fromdate"));
	Integer toDate = Integer.parseInt(allParams.get("todate"));
	String[] citys = allParams.get("cityid").split(",");
	System.out.println("Hibernate query:::>>>>>>>> "+fromDate+"----"+toDate+"----"+citys);
	List<Object> result = null;
	
	Query query;
	Session session =HibernateUtility.getSession().openSession();
	
	for(int i=0;i<citys.length;i++){
		List<Object> uniqueQureyList = null;

		String sqlQuery1="select fday.region_id,fday.region_id_desc,fday.state_id,fday.state_id_desc,fday.city_id,fday.city_id_desc,fday.product_id,fday.product_id_desc,fday.year_week as yearweek ,fday.actual_value,fday.predicted_mean_value from "+UserController.userSchemaName+"."+"rplus_ds_report_week fday ";
		String sqlQuery2="where fday.city_id IN (:cityid) and (fday.year_week>=:fromYearWeek and fday.year_week<=:toYearWeek) order by fday.city_id, fday.year_week,fday.product_id"; 

		String hqlQuery=sqlQuery1.concat(sqlQuery2);
		System.out.println("Hibernate query::: "+hqlQuery);
		query=session.createSQLQuery(hqlQuery);
		query.setParameter("cityid", citys[i]);
		query.setInteger("fromYearWeek", fromDate); 
		query.setInteger("toYearWeek", toDate);

		uniqueQureyList=query.list();
		System.out.println("size of result >>>>>>> "+uniqueQureyList.size());
		
		if(result == null){
			result = uniqueQureyList;
		}else{

			result.addAll(uniqueQureyList);
		}
		
		
	}
	System.out.println("size of final result===== >>>>>>> "+result.size());
	return result;
	
	}



@SuppressWarnings("unchecked")
@Override
public List<Object> getDayReport(Map<String, String> allParams) throws ParseException {
	String fromDate = allParams.get("fromdate");
	String toDate = allParams.get("todate");
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	Date fDate = sdf.parse(fromDate);
	Date tDate = sdf.parse(toDate);
	
	String[] citys = allParams.get("cityid").split(",");
	System.out.println("Hibernate query:::>>>>>>>> "+fDate+"----"+tDate+"----"+citys);
	List<Object> result = null;
	
	Query query;
	Session session =HibernateUtility.getSession().openSession();
	
	for(int i=0;i<citys.length;i++){
		List<Object> uniqueQureyList = null;

		String sqlQuery1="select fday.region_id,fday.region_id_desc,fday.state_id,fday.state_id_desc,fday.city_id,fday.city_id_desc,fday.product_id,fday.product_id_desc,fday.cal_day as yearweek ,fday.actual_value,fday.predicted_mean_value from "+UserController.userSchemaName+"."+"rplus_ds_report_day fday ";
		String sqlQuery2="where fday.city_id IN (:cityid) and (fday.cal_day>=:fromYearWeek and fday.cal_day<=:toYearWeek) order by fday.city_id, fday.cal_day,fday.product_id"; 

		String hqlQuery=sqlQuery1.concat(sqlQuery2);
		System.out.println("Hibernate query::: "+hqlQuery);
		query=session.createSQLQuery(hqlQuery);
		query.setParameter("cityid", citys[i]);
		query.setDate("fromYearWeek", fDate); 
		query.setDate("toYearWeek", tDate);

		uniqueQureyList=query.list();
		System.out.println("size of result >>>>>>> "+uniqueQureyList.size());
		
		if(result == null){
			result = uniqueQureyList;
		}else{

			result.addAll(uniqueQureyList);
		}
		
		
	}
	System.out.println("size of final result===== >>>>>>> "+result.size());
	return result;

}



@SuppressWarnings("unchecked")
@Override
public List<Object> getDriveAnalysisReport(Map<String, String> allParams) {
	String fromYear = allParams.get("fromYear");
	String toYear = allParams.get("toYear");
		
	String[] regions = allParams.get("regionId").split(",");
	String[] subCat = allParams.get("subCatId").split(",");
	System.out.println("Hibernate query:::>>>>>>>> "+fromYear+"----"+allParams.get("subCatId")+"----"+subCat);
	List<Object> result = null;
	
	Query query;
	Session session =HibernateUtility.getSession().openSession();
	
		String sqlQuery1="select (select region_id_desc from "+UserController.userSchemaName+"."+"dim_location  where a.region_id=region_id limit 1) regionDesc "+
				",a.year as year "+
				",(select sub_category_id_desc from "+UserController.userSchemaName+"."+"dim_product where a.subcategory_id=sub_category_id limit 1) subcatDesc  "+
				", a.significant_factors as significant ,a.correlation as corelation from "+UserController.userSchemaName+"."+"rplus_ds_routput_significant_factors_week1_region_subcat a "+
				"where  a.correlation in (select e.correlation "+
				"from   "+UserController.userSchemaName+"."+"rplus_ds_routput_significant_factors_week1_region_subcat  e "+
				"where a.region_id  = e.region_id  "+
				"and a.subcategory_id  = e.subcategory_id  "+
				"and a.year = e.year "+
				"order by e.correlation desc limit 5)";
		String sqlQuery2="and a.region_id IN (:region) and a.subcategory_id IN (:subcat) and (a.year>=:fromYear and a.year<=:toYear) order by year,regiondesc,subcatdesc"; 

		String hqlQuery=sqlQuery1.concat(sqlQuery2);
		System.out.println("Hibernate query::: "+hqlQuery);
		query=session.createSQLQuery(hqlQuery);
		query.setParameterList("subcat", subCat);
		query.setParameterList("region", regions);
		query.setString("fromYear", fromYear); 
		query.setString("toYear", toYear);

		result=(List<Object>) query.list();
	System.out.println("size of final result===== >>>>>>> "+result.size());
	return result;
}



	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getAllRegions() {
		Session session =HibernateUtility.getSession().openSession();		
		String hql = "select a.region_id, (select region_id_desc from "+UserController.userSchemaName+"."+"dim_location  where a.region_id=region_id limit 1) from "+UserController.userSchemaName+"."+"rplus_ds_routput_significant_factors_week1_region_subcat a ";
		Query query = session.createSQLQuery(hql);
		List<Object[]> resultList = query.list();
		HashMap<String,String> regionIdsandDescMap = new HashMap<String,String>();
		for(Object[] regionIdAndDesc:resultList){
			regionIdsandDescMap.put((String)regionIdAndDesc[0],(String)regionIdAndDesc[1]);
		}
		return regionIdsandDescMap;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getAllSubCategeorys() {
		Session session =HibernateUtility.getSession().openSession();		
		String hql = "select a.subcategory_id, (select sub_category_id_desc from "+UserController.userSchemaName+"."+"dim_product where a.subcategory_id=sub_category_id limit 1) from "+UserController.userSchemaName+"."+"rplus_ds_routput_significant_factors_week1_region_subcat a ";
		Query query = session.createSQLQuery(hql);
		List<Object[]> resultList = query.list();
		HashMap<String,String> regionIdsandDescMap = new HashMap<String,String>();
		for(Object[] regionIdAndDesc:resultList){
			regionIdsandDescMap.put((String)regionIdAndDesc[0],(String)regionIdAndDesc[1]);
		}
		return regionIdsandDescMap;
	}



	@SuppressWarnings({ "unchecked"})
	@Override
	public List<Object> getDriveDetails(Map<String, String> allParams) {
		String fromYear = allParams.get("fromYear");
		String toYear = allParams.get("toYear");
		String significant = allParams.get("significant");
		String[] regions = allParams.get("regionId").split(",");
		String[] subCat = allParams.get("subCatId").split(",");
		
		System.out.println("Hibernate query:::>>>>>>>> "+fromYear+"----"+allParams.get("regionId")+"----"+regions);
		List<HashMap<String, Object>> result = null;
		List<Object> finalResult = new ArrayList<>();
		Query query;
		Session session =HibernateUtility.getSession().openSession();
		/*(select region_id_desc from "+UserController.userSchemaName+"."+"dim_location  where a.region_id=region_id limit 1) regionDesc "+
				",a.year as year "+
				",(select sub_category_id_desc from "+UserController.userSchemaName+"."+"dim_product where a.subcategory_id=sub_category_id limit 1) subcatDesc  "+
				",a.sales as sales ,:significant as factorvalue 
				*/
		String sqlQuery1="select * from "+UserController.userSchemaName+"."+"rplus_ds_ui_input_sales_region_subcat1 a "+
				"where ";
		String sqlQuery2="a.region_id IN (:region) and a.subcategory_id IN (:subcat) and (CAST(a.year AS text)>=:fromYear and CAST(a.year AS text)<=:toYear)"; 

		String hqlQuery=sqlQuery1.concat(sqlQuery2);
		System.out.println("Hibernate query::: "+hqlQuery);
		query=session.createSQLQuery(hqlQuery);
		//query.setString("significant", significant);
		query.setParameterList("subcat", subCat);
		query.setParameterList("region", regions);
		query.setString("fromYear", fromYear); 
		query.setString("toYear", toYear);

		result=(List<HashMap<String, Object>>) query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
		
		System.out.println("size of final result ===== >>>>>>>"+result.size());
		for(int i = 0; i< result.size(); i++){
			HashMap<String, Object> obj =  result.get(i);
			
			List<String> ob = new ArrayList<>();
			ob.add((String) obj.get("region_id").toString());
			ob.add((String) obj.get("year").toString());
			ob.add((String) obj.get("subcategory_id").toString());
			ob.add((String) obj.get("sales").toString());
			
			if(obj.containsKey(significant)){
				ob.add((String) obj.get(significant).toString());
			}else{
				ob.add(null);
			}
			finalResult.add(ob);
			
		}
		return finalResult;
	}
}
