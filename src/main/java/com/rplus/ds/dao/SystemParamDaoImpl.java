/**
 * 
 */
package com.rplus.ds.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.config.HibernateUtility;
import com.rplus.ds.model.SystemParam;

/**
 * @author Rafeeq
 * 
 */
@Repository
@Component
public class SystemParamDaoImpl implements SystemParamDao {	

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getSysParam() {
		System.out.println("Inside getSysParamList method() ::SystemParamDaoImpl ");
		Session session =HibernateUtility.getSession().openSession();
		List<SystemParam> paramList = session.createQuery("from SystemParam").list();
		System.out.println("LIST::::::::::::::::::::::::::;;"+paramList);
		session.close();		

		Map<String, String> paramMap = new HashMap<String, String>();		
		
		for (SystemParam sysParam : paramList) {					
			String startDate = sysParam.getMinSaleDay().toString();
			String endDate = sysParam.getCurrentTime().toString();		
			System.out.println("START DATE::::::::::::::::::::::::::::"+startDate);
			System.out.println("END DATE::::::::::::::::::::::::::::::"+endDate);
			paramMap.put("startDate",startDate);
			paramMap.put("endDate",endDate);			
		}				
		return paramMap;
	}
	
	
	/*@SuppressWarnings("unchecked")
	@Override
	public List<String> getSysParam() {
		System.out.println("Inside getSysParamList method() ::SystemParamDaoImpl ");
		Session session =HibernateUtility.getSession().openSession();
		List<SystemParam> paramList = session.createQuery("from SystemParam").list();	
	
		session.close();		

		List<String> systemParams = new ArrayList<String>();		

		for (SystemParam sysParam : paramList) {			
			String endDate = sysParam.getCurrentTime().toString();	
			String startDate = sysParam.getMinSaleDay().toString();
			systemParams.add(startDate);
			systemParams.add(endDate);
			
		}
		return systemParams;
	}*/


}
