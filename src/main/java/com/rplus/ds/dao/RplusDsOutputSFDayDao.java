package com.rplus.ds.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.rplus.ds.model.RplusDsOutputSFDay;

/**
 * @author Rafeeq
 *
 */
@Component
public interface RplusDsOutputSFDayDao {	
	@Autowired(required=true)
	public List<RplusDsOutputSFDay> getRplusOutputSFdata();
	

}
