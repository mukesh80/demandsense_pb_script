/**
 * 
 */
package com.rplus.ds.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Rafeeq
 *
 */
@Component
public interface SystemParamDao {
	
	/*@Autowired(required = true)
	public List<String> getSysParam();*/
	
	@Autowired(required = true)
	public Map<String,String> getSysParam();

}

	