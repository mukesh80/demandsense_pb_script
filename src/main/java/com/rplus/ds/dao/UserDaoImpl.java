package com.rplus.ds.dao;


import java.util.List;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.model.User;
@Repository
@Component
public class UserDaoImpl implements UserDao{

	public String dbUserName="";
	public String dbPassword="";
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	HttpSession userSession;

	 public void setSessionFactory(SessionFactory sessionFactory) {
	        this.sessionFactory = sessionFactory;
	    }
	 
	 @SuppressWarnings("unused")
	private Session getCurrentSession() {
	        return sessionFactory.getCurrentSession();
	    }
	
	@Override
	//@Transactional
	public void save(User user) {
		System.out.println("persisting User instance");
	
		
		Session session = sessionFactory.openSession();
		Transaction tx = (Transaction) session.beginTransaction();
		System.out.println("in save methoddd:::::::::::::::::::::::"+user.getUsername());
		session.save(user);
		
		tx.commit();
		

		/*Session session = sessionFactory.openSession();
		session.save(user);*/
		System.out.println("in UserDaoImpl class save() method");
	}

	@SuppressWarnings("unused")
	@Override
	public void getAllUser() {
		
		System.out.println("geting all the user  in method");
		Session session = sessionFactory.openSession();
		Query query=session.createQuery("from User");
		Object resultSet=query.uniqueResult();
		User user=(User) resultSet;
		System.out.println("geting all the user  in method");
		//System.out.println(user.getEmailId()+","+user.getFirsName());
		
	}

	@Override
	@Transactional
	public void insertRow(User user) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		System.out.println("in insertrow() ::::::::::::::::::::::: In UserDaoImpl :::"+user.getUsername());
		session.saveOrUpdate(user);
		tx.commit();
		System.out.println("UserDaoImpl : data is saved:::: In UserDaoImpl");
	}

	
		@SuppressWarnings("unchecked")
		public List<User> getList() {
			
			Session session = sessionFactory.openSession();
			Criteria cr= session.createCriteria(User.class);
			ProjectionList p1=Projections.projectionList();
			p1.add(Projections.property("authProdText"));
			p1.add(Projections.property("auth_loc_text"));
			cr.setProjection(p1);
			List<User> result=cr.list();
			
			return result;
			
		}

		@Override
		public boolean validateUser(String username,String password) {
			/*String dbUserName="";
			String dbPassword="";*/
			boolean status=false;
			Session session=sessionFactory.openSession();
			
			@SuppressWarnings("unchecked")
			List<User> Lst = session.createQuery("from User where username=\'" + username + "\' and password=\'"+password+"\'").list();
			if(Lst.size()>0)
			{	
				status = true;
			}
			
			System.out.println("User status is : "+status);
			return status;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public boolean findUserByUserName(String username)
		{
			
			boolean usernamestatus=true;
			Session session=sessionFactory.openSession();
			String hql = "From User u where u.username = :username";
			List<User> list = session.createQuery(hql)
					.setParameter("username", username)
					.list();
			
			if(list.size()>0)
			{	
				usernamestatus = false;
			}
			//from User where username=\'" + username
			return usernamestatus;
			
		}
		

		@Override
		public User getUserInfo(String username) {
			/*return dbUserName;*/
			Session session=sessionFactory.openSession();
			Query query=session.createQuery("from User where username=\'" + username + "\' ");
			Object resultSet=query.uniqueResult();
			User user=(User) resultSet;
			return user;
		}

		@Override
		public String updateUser(User user) {
			Session session=sessionFactory.openSession();
			 Query q = session.createQuery("from User where username = :username ");
			   q.setParameter("username", user.getUsername());
			   User user1 = (User)q.list().get(0);
			      
			   user1.setFirstName(user.getFirstName());
			   user1.setLastName(user.getLastName());
			   user1.setEmailId(user.getEmailId());
			   user1.setMobilePhone(user.getMobilePhone());
			   
			   Transaction tx = session.beginTransaction();
			   tx.commit();
			   session.update(user1);
			
			return "sucess";
		}
		
		@Override
		public String updatePassword(String password) {
			Session session=sessionFactory.openSession();
			
			 Query q = session.createQuery("from User where username = :username ");
			   q.setParameter("username", userSession.getAttribute("loggedUser1"));
			   User user1 = (User)q.list().get(0);
			      
			   user1.setPassword(password);
			   
			   Transaction tx = session.beginTransaction();
			   tx.commit();
			   session.update(user1);
			
			return "sucess";
		}
		
		
		
}
