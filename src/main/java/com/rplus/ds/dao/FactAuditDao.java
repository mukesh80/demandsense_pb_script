package com.rplus.ds.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.rplus.ds.model.FactAuditReport;

/**
 * @author Rafeeq
 *
 */
@Component
public interface FactAuditDao {
	
	@Autowired(required=true)
	public List<FactAuditReport> getAuditreport();

}
