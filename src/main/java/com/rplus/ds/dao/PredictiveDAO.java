package com.rplus.ds.dao;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Mallikarjuna
 *
 */
@Component
@Lazy
public interface PredictiveDAO {

	@Autowired(required=true)
	public List<Object[]> getForecastData(Map<String,String> allParams);	
	
	@Autowired(required=true)
	public List<Object[]> getSocialFactorData(Map<String,String> allParams);
	
	@Autowired(required=true)
	public List<Object[]> getForecastNonAggregageData(Map<String,String> allParams);
	
	@Autowired(required=true)
	public List<Object[]> getNASocialFactorData(Map<String, String> allParams);
	
	@Autowired(required=true)
	public LinkedHashMap<String, List<HashMap<String,String>>> getInternalFactorData(Map<String,String> allParams);
	
	@Autowired(required=true)
	public LinkedHashMap<String, List<HashMap<String,String>>> getInternalNonAggregageData(Map<String,String> allParams);
	
	@Autowired(required=true)
	public List<Object[]> getExternalAggregateData(Map<String,String> allParams);

	@Autowired(required=true)
	List<Object[]> getNAExternalData(Map<String, String> allParams);
		
		
}
