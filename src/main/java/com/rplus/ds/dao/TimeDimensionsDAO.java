package com.rplus.ds.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rplus.ds.model.TimeDimensions;

/**
 * 
 * @author Mallikarjuna
 *
 */
@Component
public interface TimeDimensionsDAO {
	
	@Autowired(required=true)
	public void save(TimeDimensions timeDimensions);
	@Autowired(required=true)
    public List<TimeDimensions> list();
	
}
