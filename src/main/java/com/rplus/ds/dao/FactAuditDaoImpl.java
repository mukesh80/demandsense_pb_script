/**
 * 
 */
package com.rplus.ds.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.rplus.ds.config.HibernateUtility;
import com.rplus.ds.model.FactAuditReport;

/**
 * @author Rafeeq
 *
 */
//@Repository
@Component
public class FactAuditDaoImpl implements FactAuditDao {

	
	@SuppressWarnings("unchecked")
	@Override	
	public List<FactAuditReport> getAuditreport() {
		System.out.println("Inside getSysParamList method() ::SystemParamDaoImpl ");
		Session session =HibernateUtility.getSession().openSession();
		List<FactAuditReport> reportList = session.createQuery("from FactAuditReport far order by far.tableName asc,far.rownames").list();	
	
		session.close();		

		/*List<String> reports = new ArrayList<String>();	

		for (FactAuditReport factAuditReport : reportList) {			
		}*/
		
		return reportList;
	}

}
