/**
 * 
 */
package com.rplus.ds.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.config.HibernateUtility;
import com.rplus.ds.model.RplusDsOutputADay;

/**
 * @author Rafeeq-PC
 * currently not in use instead of this we are using prdective DAO for filteration
 *
 */
@Repository
@Component
public class RplusOutputADayDaoImpl implements RplusOutputADayDao {
private static final Logger logger = Logger.getLogger(RplusOutputADayDaoImpl.class);

   
	@Override
	@SuppressWarnings("unchecked")
	public List<RplusDsOutputADay> getRplusOutputAdata() {
		logger.info("Inside getRplusOutputAdata :RplusOutputADayDaoImpl ");
		 Session session =HibernateUtility.getSession().openSession();
         List<RplusDsOutputADay> rplusDsOutputAList = session.createQuery("from RplusDsOutputADay").list();
         session.close();
		return rplusDsOutputAList;
	}
	
	
}
