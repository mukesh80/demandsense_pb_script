package com.rplus.ds.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rplus.ds.config.HibernateUtility;
import com.rplus.ds.model.Location;
import com.rplus.ds.model.LocationDimensions;
import com.rplus.ds.service.MailService;
/**
 * 
 * @author Rafeeq
 * 
 */
@Repository
@Component
public class LocationDaoImpl implements LocationDao {
	
	private static final Logger logger = Logger.getLogger(LocationDaoImpl.class);

	@Autowired
	MailService mailService;



	@SuppressWarnings("unchecked")
	@Override
	public List<Location> getLocList() {
		System.out.println("Inside getLocList method() ::LocationDaoImpl ");
		Session session =HibernateUtility.getSession().openSession();
				//HibernateUtility.getSession();
		List<LocationDimensions> locationlist = session.createQuery(
				"from LocationDimensions").list();
		session.close();

		List<Location> locations = new ArrayList<Location>();

		for (LocationDimensions locDimension : locationlist) {
			Location location = new Location();
			//location.setLocationId(locDimension.getLocationId());
			location.setStateId(locDimension.getStateId());
			location.setRegionId(locDimension.getRegionId());
			location.setDistrictId(locDimension.getDistrictId());
			location.setCityId(locDimension.getCityId());

			locations.add(location);

		}

		return locations;
	}

	/**
	 * This methos is used to get all country ids and descriptions
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, String> getCountries() {
		logger.info(" Inside getCountries ");
		Session session =HibernateUtility.getSession().openSession();
		String hql = "select ld.countryId,ld.countryDesc from LocationDimensions ld group by ld.countryId,ld.countryDesc";
		Query query = session.createQuery(hql);
		List<Object[]> resultList = query.list();
		HashMap<String, String> countryIdAndDescMap = new HashMap<String, String>();
		for (Object[] countryIdAndDesc : resultList) {
			countryIdAndDescMap.put((String) countryIdAndDesc[0],
					(String) countryIdAndDesc[1]);
		}
		countryIdAndDescMap.put("count",((Integer) resultList.size()).toString());
		logger.info(" Countres count :  "+resultList.size());
		return countryIdAndDescMap;
	}

	/**
	 * This method is used to get region info based on countryID
	 * 
	 * @param countryId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, String> getRegionsByCountryId(String countryId) {
		logger.info(" Inside getRegionsByCountry ");
		Session session =HibernateUtility.getSession().openSession();
		String hql = "select ld.regionId,ld.regionIdDesc from LocationDimensions ld where ld.countryId=:countryId group by ld.regionId,ld.regionIdDesc";
		Query query = session.createQuery(hql);
		query.setParameter("countryId", countryId);
		List<Object[]> resultList = query.list();
		HashMap<String, String> regionIdAndDescMap = new HashMap<String, String>();
		for (Object[] regionIdAndDesc : resultList) {
			regionIdAndDescMap.put((String) regionIdAndDesc[0],
					(String) regionIdAndDesc[1]);
		}
		regionIdAndDescMap.put("count",((Integer) resultList.size()).toString());
		logger.info(" Regions count :  "+resultList.size());
		return regionIdAndDescMap;
	}

	/**
	 * This method is used to get state info based on regionID
	 * 
	 * @param regionId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getStatesByRegionId(String regionId) {
		logger.info(" Inside getStatesByRegion ");		
		Session session =HibernateUtility.getSession().openSession();
		String hql = "select ld.stateId,ld.stateIdDesc from LocationDimensions ld where ld.regionId=:regionId group by ld.stateId,ld.stateIdDesc";
		Query query = session.createQuery(hql);	
		query.setParameter("regionId", regionId);
		List<Object[]> resultList = query.list();
		HashMap<String, String> stateIdAndDescMap = new HashMap<String, String>();
		for (Object[] stateIdAndDesc : resultList) {
			stateIdAndDescMap.put((String) stateIdAndDesc[0],
					(String) stateIdAndDesc[1]);
		}
		stateIdAndDescMap.put("count", ((Integer) resultList.size()).toString());		
		logger.info(" States count :  "+resultList.size());
		return stateIdAndDescMap;
	}

	/**
	 * This method is used to get region info based on districtId
	 * 
	 * @param districtId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getDistrictsByStateId(String stateId) {
		logger.info(" Inside getDistrictsByState ");
		Session session =HibernateUtility.getSession().openSession();
		String hql = "select ldim.districtId,ldim.districtIdDesc from LocationDimensions ldim where ldim.stateId=:stateId group by ldim.districtId,ldim.districtIdDesc";
		Query query = session.createQuery(hql);
		query.setParameter("stateId", stateId);

		List<Object[]> resultList = query.list();
		HashMap<String, String> districtIdAndDescMap = new HashMap<String, String>();
		for (Object[] districtIdAndDesc : resultList) {
			districtIdAndDescMap.put((String) districtIdAndDesc[0],
					(String) districtIdAndDesc[1]);
		}
		districtIdAndDescMap.put("count",((Integer) resultList.size()).toString());
		logger.info(" Districts count :  "+resultList.size());
		return districtIdAndDescMap;

	}

	/**
	 * This method is used to get region info based on cityId
	 * 
	 * @param cityId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getCitiesByDistrictId(String districtId) {
		logger.info(" Inside getCitiesByDistrict ");
		Session session =HibernateUtility.getSession().openSession();
		String hql = "select ldim.cityId,ldim.cityIdDesc from LocationDimensions ldim where ldim.districtId=:districtId group by ldim.cityId,ldim.cityIdDesc";
		Query query = session.createQuery(hql);
		query.setParameter("districtId", districtId);

		List<Object[]> resultList = query.list();
		HashMap<String, String> cityIDAndDescMap = new HashMap<String, String>();
		for (Object[] cityIdAndDesc : resultList) {
			cityIDAndDescMap.put((String) cityIdAndDesc[0],
					(String) cityIdAndDesc[1]);
		}
		cityIDAndDescMap.put("count", ((Integer) resultList.size()).toString());
		logger.info(" Cities count :  "+resultList.size());
		return cityIDAndDescMap;

	}

	/**
	 * This method is used to get region info based on locationId
	 * 
	 * @param locationId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getLocationIdsByCityId(String cityId) {
		logger.info(" Inside getLocationIdsByCity ");
		Session session =HibernateUtility.getSession().openSession();
		String hql = "select ldim.locationId,ldim.locationDesc from LocationDimensions ldim where ldim.cityId=:cityId group by ldim.locationId,ldim.locationDesc";
		Query query = session.createQuery(hql);
		query.setParameter("cityId", cityId);

		List<Object[]> resultList = query.list();
		HashMap<String, String> locationIdAndDescMap = new HashMap<String, String>();
		for (Object[] locationIdAndDesc : resultList) {
			locationIdAndDescMap.put((String) locationIdAndDesc[0],
					(String) locationIdAndDesc[1]);
		}
		locationIdAndDescMap.put("count",
				((Integer) resultList.size()).toString());
		logger.info(" LocationIds count :  "+resultList.size());
		return locationIdAndDescMap;

	}
	
	/**
	 * This method is used to all available regions
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getAllRegions() {
		Session session =HibernateUtility.getSession().openSession();		
		String hql = "select ld.regionId,ld.regionIdDesc from LocationDimensions ld group by ld.regionId,ld.regionIdDesc";
		Query query = session.createQuery(hql);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> regionIdsandDescMap = new HashMap<String,String>();
    	for(Object[] regionIdAndDesc:resultList){
    		regionIdsandDescMap.put((String)regionIdAndDesc[0],(String)regionIdAndDesc[1]);
    	}
    	regionIdsandDescMap.put("count", ((Integer)resultList.size()).toString());
    	logger.info(" All Regions count :  "+resultList.size());
    	return regionIdsandDescMap;
	}

	/**
	 * This method is used to get all available states
	 */
    @SuppressWarnings("unchecked")
    @Override
	public HashMap<String, String> getAllStates() {	
	Session session =HibernateUtility.getSession().openSession();
	String hql = "select ld.stateId,ld.stateIdDesc from LocationDimensions ld group by ld.stateId,ld.stateIdDesc";
	Query query = session.createQuery(hql);		
	List<Object[]> resultList = query.list();
	HashMap<String, String> stateIdAndDescMap = new HashMap<String, String>();
	for (Object[] stateIdAndDesc : resultList) {
		stateIdAndDescMap.put((String) stateIdAndDesc[0],
				(String) stateIdAndDesc[1]);
	}
	stateIdAndDescMap.put("count", ((Integer) resultList.size()).toString());		
	logger.info(" All States count :  "+resultList.size());
	return stateIdAndDescMap;
	}

    /**
     * This method used to get all available districts
     */
    @SuppressWarnings("unchecked")
    @Override
	public HashMap<String, String> getAllDistricts() {
	Session session =HibernateUtility.getSession().openSession();
	String hql = "select ldim.districtId,ldim.districtIdDesc from LocationDimensions ldim group by ldim.districtId,ldim.districtIdDesc";
	Query query = session.createQuery(hql);
	
	List<Object[]> resultList = query.list();
	HashMap<String, String> districtIdAndDescMap = new HashMap<String, String>();
	for (Object[] districtIdAndDesc : resultList) {
		districtIdAndDescMap.put((String) districtIdAndDesc[0],
				(String) districtIdAndDesc[1]);
	}
	districtIdAndDescMap.put("count",((Integer) resultList.size()).toString());
	logger.info(" All Districts count :  "+resultList.size());
	return districtIdAndDescMap;
	}

    /**
     * This method used to get all available citys
     */
    @SuppressWarnings("unchecked")
    @Override
	public HashMap<String, String> getAllCities() {
    	
    	
	Session session =HibernateUtility.getSession().openSession();
	String hql = "select ldim.cityId,ldim.cityIdDesc from LocationDimensions ldim group by ldim.cityId,ldim.cityIdDesc";
	Query query = session.createQuery(hql);
	
	List<Object[]> resultList = query.list();
	HashMap<String, String> cityIDAndDescMap = new HashMap<String, String>();
	for (Object[] cityIdAndDesc : resultList) {
		cityIDAndDescMap.put((String) cityIdAndDesc[0],
				(String) cityIdAndDesc[1]);
	}
	cityIDAndDescMap.put("count", ((Integer) resultList.size()).toString());
	logger.info(" All Cities count :  "+resultList.size());
	return cityIDAndDescMap;
	}

    /**
     * This method used to get all available locations
     */
    @SuppressWarnings("unchecked")
   	@Override
	public HashMap<String, String> getAllLocationIds() {
    	Session session =HibernateUtility.getSession().openSession();
		String hql = "select ldim.locationId,ldim.locationIdDesc from LocationDimensions ldim group by ldim.locationId,ldim.locationIdDesc";
		Query query = session.createQuery(hql);		

		List<Object[]> resultList = query.list();
		HashMap<String, String> locationIdAndDescMap = new HashMap<String, String>();
		for (Object[] locationIdAndDesc : resultList) {
			locationIdAndDescMap.put((String) locationIdAndDesc[0],
					(String) locationIdAndDesc[1]);
		}
		locationIdAndDescMap.put("count",
				((Integer) resultList.size()).toString());
		logger.info(" All LocationIds count :  "+resultList.size());
		return locationIdAndDescMap;
	}

    /**
     * This method used to get region id an name map by list country ids
     */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getRegionsByCountryList(
			List<String> countryList) {
		Session session =HibernateUtility.getSession().openSession();
    	String hql = "select ldim.regionId,ldim.regionIdDesc from LocationDimensions ldim where ldim.countryId in (:countryIdList) group by ldim.regionId,ldim.regionIdDesc";
    	Query query = session.createQuery(hql);
    	query.setParameterList("countryIdList", countryList);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> regionIdAndDescMap = new HashMap<String,String>();
    	for(Object[] regionIdAndDesc:resultList){
    		regionIdAndDescMap.put((String)regionIdAndDesc[0],(String)regionIdAndDesc[1]);
    	}
    	return regionIdAndDescMap;
	}

	/**
	 * This method used to get state id and name map by list of region ids
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getStatesByRegionList(List<String> regionList) {
		Session session =HibernateUtility.getSession().openSession();
    	String hql = "select ldim.stateId,ldim.stateIdDesc from LocationDimensions ldim where ldim.regionId in (:regionIdList) group by ldim.stateId,ldim.stateIdDesc";
    	Query query = session.createQuery(hql);
    	query.setParameterList("regionIdList", regionList);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> stateIdAndDescMap = new HashMap<String,String>();
    	for(Object[] stateIdAndDesc:resultList){
    		stateIdAndDescMap.put((String)stateIdAndDesc[0],(String)stateIdAndDesc[1]);
    	}
    	return stateIdAndDescMap;
	}

	/**
	 * This method used to get district id and name map by list of state ids
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getDistrictsByStateList(
			List<String> stateList) {
		Session session =HibernateUtility.getSession().openSession();
    	String hql = "select ldim.districtId,ldim.districtIdDesc from LocationDimensions ldim where ldim.stateId in (:stateIdList) group by ldim.districtId,ldim.districtIdDesc";
    	Query query = session.createQuery(hql);
    	query.setParameterList("stateIdList", stateList);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> districtAndDescMap = new HashMap<String,String>();
    	for(Object[] districtIdAndDesc:resultList){
    		districtAndDescMap.put((String)districtIdAndDesc[0],(String)districtIdAndDesc[1]);
    	}
    	return districtAndDescMap;
	}

	/**
	 * This method used to get city id and name map by list of district ids
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getCitiesByDistrictList(
			List<String> districtList) {
		Session session =HibernateUtility.getSession().openSession();
    	String hql = "select ldim.cityId,ldim.cityIdDesc from LocationDimensions ldim where ldim.districtId in (:districtIdList) group by ldim.cityId,ldim.cityIdDesc";
    	Query query = session.createQuery(hql);
    	query.setParameterList("districtIdList", districtList);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> cityAndDescMap = new HashMap<String,String>();
    	for(Object[] cityIdAndDesc:resultList){
    		cityAndDescMap.put((String)cityIdAndDesc[0],(String)cityIdAndDesc[1]);
    	}
    	return cityAndDescMap;
	}

	/**
	 * This method used to get location id and name map by list of city ids
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getLocationsByCityList(List<String> cityList) {
		Session session =HibernateUtility.getSession().openSession();
    	String hql = "select ldim.locationId,ldim.locationIdDesc from LocationDimensions ldim where ldim.cityId in (:cityIdList) group by ldim.locationId,ldim.locationIdDesc";
    	Query query = session.createQuery(hql);
    	query.setParameterList("cityIdList", cityList);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> locationIdAndDescMap = new HashMap<String,String>();
    	for(Object[] locationIdAndDesc:resultList){
    		locationIdAndDescMap.put((String)locationIdAndDesc[0],(String)locationIdAndDesc[1]);
    	}
    	return locationIdAndDescMap;
	}

	/**
	 * This method used to get location id and name map by list of location ids
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getLocationsByLocationList(
			List<String> locationList) {
		Session session =HibernateUtility.getSession().openSession();
    	String hql = "select ldim.locationId,ldim.locationIdDesc from LocationDimensions ldim where ldim.locationId in (:locationIdList) group by ldim.locationId,ldim.locationIdDesc";
    	Query query = session.createQuery(hql);
    	query.setParameterList("locationIdList", locationList);
    	List<Object[]> resultList = query.list();
    	HashMap<String,String> locationIdAndDescMap = new HashMap<String,String>();
    	for(Object[] locationIdAndDesc:resultList){
    		locationIdAndDescMap.put((String)locationIdAndDesc[0],(String)locationIdAndDesc[1]);
    	}
    	return locationIdAndDescMap;
	}
	
	

}
