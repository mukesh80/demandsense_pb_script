package com.rplus.ds.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Service Interface for Data Explorer
 * @version 1.0.0	10 Sep 2015
 * @author Jerald Joseph
 */

@Component
public interface DataExplorerService {
	@Autowired(required=true)
	public String getCommonParameterHQL(Map<String,String> allParams);
	
	@Autowired(required=true)
	public List<Object[]> getForecostData(Map<String,String> allParams);
	
	@Autowired(required=true)
	public List<Object[]> getSignificantData(Map<String,String> allParams);
	
	@Autowired(required=true)
	public List<Object[]> getAccuracyData(Map<String,String> allParams);
}
