package com.rplus.ds.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public interface PredictiveService {
	
	@Autowired(required=true)
	public HashMap<String,String> prodIdAndNameMapBySubCategory(List<String> subCategoryIds);
	
	@Autowired(required=true)
	public HashMap<String,String> subCategoryIdAndNameMapByCategory(List<String> categoryIds);
	
	@Autowired(required=true)
	public HashMap<String,String> categoryIdAndNameMap(List<String> categoryIds);
	
	@Autowired(required=true)
	public HashMap<String,String> prodIdAndNameMap(List<String> productIds);
	
	@Autowired(required=true)
	public HashMap<String,String> regionIdAndNameMapByCountry(List<String> countryIds);
	
	@Autowired(required=true)
	public HashMap<String,String> stateIdAndNameMapByRegion(List<String> regionIds);
	
	@Autowired(required=true)
	public HashMap<String,String> districtIdAndNameByState(List<String> stateIds);
	
	@Autowired(required=true)
	public HashMap<String,String> cityIdAndNameByDistrict(List<String> districtIds);
	
	@Autowired(required=true)
	public HashMap<String,String> locationIdAndNameByCity(List<String> cityIds);
	
	@Autowired(required=true)
	public HashMap<String,String> locationIdAndNameBylocation(List<String> locationIds);
	
	@Autowired(required=true)
	public HashMap<String, HashMap<String, String>> getAllInputParamsMap(
			Map<String, String> allParams);
	
	@Autowired(required=true)
	public HashMap<String, String> getInputParamMap(
			Map<String, String> allParams, String paramName);
}
