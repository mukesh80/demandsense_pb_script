package com.rplus.ds.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rplus.ds.dao.DimProductDAO;
import com.rplus.ds.dao.LocationDao;
import com.rplus.ds.dao.PredictiveDAO;

/**
 * 
 * @author Mallikarjuna
 *
 */
@Service("predictiveService")
@Transactional
@Repository
@Component
public class PredictiveServiceImpl implements PredictiveService{
	@Autowired
	DimProductDAO dimProductDAO;
	
	@Autowired
	PredictiveDAO predictiveDAO;
	
	@Autowired
	LocationDao locationDAO;

	
	public static Integer noOfProduct=0;
	public static Integer noOfLocation=0;
	
	public static Integer tempProduct=0;
	public static Integer tempLocation=0;
	
	
	
	
	/**
	 * This method is used to get the product id and name map by list of sub category ids
	 */
	@Override
	public HashMap<String,String> prodIdAndNameMapBySubCategory(List<String> subCategoryIds){
		return dimProductDAO.getProductsBySubCategoryList(subCategoryIds);
	}

	/**
	 * This method used to get sub category id and name map by list of category ids
	 */
	@Override
	public HashMap<String, String> subCategoryIdAndNameMapByCategory(
			List<String> categoryIds) {
		return dimProductDAO.getSubCategorysByCategoryList(categoryIds);
	}

	/**
	 * This method used to get category id and name map by list of category ids
	 */
	@Override
	public HashMap<String, String> categoryIdAndNameMap(List<String> categoryIds) {
		return dimProductDAO.getCategorysByCategoryList(categoryIds);
	}

	/**
	 * This method is used to get product id and name map by list of product ids
	 */
	@Override
	public HashMap<String, String> prodIdAndNameMap(List<String> productIds) {
		return dimProductDAO.getProductsByProductList(productIds);
	}
	
	/**
	 * This method is used to prepare seperate maps for each input parameters from all selected input parameters.
	 * ex : {products:{2001:FOOD,2002:BEVERAGES}, {categorys:{200101:'category1,200202:category2'} etc....
	 */
	
	@Override
	public HashMap<String, HashMap<String, String>> getAllInputParamsMap(
			
			Map<String, String> allParams) {
		HashMap<String, HashMap<String, String>> allParamsMap = new HashMap<String, HashMap<String, String>>();
		
		//HttpServletRequest request="adc";

		noOfLocation=0;
		noOfProduct=0;
		
		HashMap<String, String> prodParamsMap = getInputParamMap(allParams, "product");
	
		if(prodParamsMap!=null)
		{
			noOfProduct=noOfProduct+prodParamsMap.size(); // 1
		}
		
		allParamsMap.put("products", prodParamsMap);
		
		HashMap<String, String> categoryParamsMap = getInputParamMap(allParams,
				"cat");
		if(categoryParamsMap!=null)
		{
			noOfProduct=noOfProduct+categoryParamsMap.size(); // 1+2=3
		}
		
		allParamsMap.put("categorys", categoryParamsMap);
		
		
		HashMap<String, String> subCategoryParamsMap = getInputParamMap(
				allParams, "subcatid");
		if(subCategoryParamsMap!=null)
		{
			noOfProduct=noOfProduct+subCategoryParamsMap.size(); // 3+0=3
		}
		allParamsMap.put("subCategorys", subCategoryParamsMap);
		
		
		HashMap<String, String> countryParamsMap = getInputParamMap(allParams,
				"con_id");
		if(countryParamsMap!=null)
		{
			noOfLocation=noOfLocation+countryParamsMap.size();
		}
		
		allParamsMap.put("countrys", countryParamsMap);
		
		
		HashMap<String, String> regionParamsMap = getInputParamMap(allParams,
				"regionId");
		if(regionParamsMap!=null)
		{
			noOfLocation=noOfLocation+regionParamsMap.size();
		}
		
		allParamsMap.put("regions", regionParamsMap);
		
		
		HashMap<String, String> stateParamsMap = getInputParamMap(allParams,
				"state");
		if(stateParamsMap!=null)
		{
			noOfLocation=noOfLocation+stateParamsMap.size();
		}
		
		allParamsMap.put("states", stateParamsMap);
		
		
		HashMap<String, String> cityParamsMap = getInputParamMap(allParams,
				"city");
		if(cityParamsMap!=null)
		{
			noOfLocation=noOfLocation+cityParamsMap.size();
		}
		
		allParamsMap.put("citys", cityParamsMap);
		
		HashMap<String, String> districtParamsMap = getInputParamMap(allParams,
				"district");
		if(districtParamsMap!=null)
		{
			noOfLocation=noOfLocation+districtParamsMap.size();
		}
		allParamsMap.put("districts", districtParamsMap);
		
		HashMap<String, String> locationParamsMap = getInputParamMap(allParams,
				"location");
		if(locationParamsMap!=null)
		{
			noOfLocation=noOfLocation+locationParamsMap.size();
		}
		allParamsMap.put("locations", locationParamsMap);
		
		
		
		tempLocation=noOfLocation;
		tempProduct=noOfProduct;
		
		
		System.out.println("Total no of location::::::::::InService:::"+noOfLocation);
		System.out.println("Total no of products:::::::InService:::"+noOfProduct);
		
		/*if(request!=null)
		{
			 request.getSession().setAttribute("noOflocation", 0);
			 request.getSession().setAttribute("noOfProduct", 0);
		}
		 request.getSession().setAttribute("noOflocation", noOfLocation);
		 request.getSession().setAttribute("noOfProduct", noOfProduct);
		
		 
		
		 noOfLocation=0;
		 noOfProduct=0;*/
		
		HashMap<String,String> dateParamsMap = new HashMap<String,String>();
		dateParamsMap.put("startDate", allParams.get("date_start"));
		dateParamsMap.put("endDate", allParams.get("date_end"));
		allParamsMap.put("dates", dateParamsMap);
		
		
		
		return allParamsMap;
	}
	
	/**
	 * This method will take the inputs as all input params and param name And
	 * from that we will generate id and name map. Suppose user is selecting two
	 * products called FOOD and BEVERAGES, then will send the result as id name
	 * map {2001:FOOD,2002:BEVERAGES}
	 */
	
	@Override
	public HashMap<String, String> getInputParamMap(
			Map<String, String> allParams, String paramName) {
		HashMap<String, String> idAndNameMap = null;
		String[] params={};
		
		if (allParams.get(paramName) != null
				&& !allParams.get(paramName).equals("")) {

			 params = allParams.get(paramName).split(",");
			
			//request.getSession().setAttribute("noOfParameter", noOfParameter);
			idAndNameMap = new HashMap<String, String>();
			for (int i = 0; i < params.length; i++) {
				
				if (!(params[i].equals("date_start") || params[i]
						.equals("date_end"))) {
					
					//System.out.println("In predective service impl:::::::::"+noOfParameter);
					String[] idNameArray = params[i].split(":");
					idAndNameMap.put(idNameArray[0], idNameArray[1]);
				}
			}
		}
		
		return idAndNameMap;
	}

	/**
	 * This method used to get region id and name map by list of country ids
	 */
	@Override
	public HashMap<String, String> regionIdAndNameMapByCountry(
			List<String> countryIds) {
		return locationDAO.getRegionsByCountryList(countryIds);
	}

	/**
	 * This method is used to get state id and name map by list of region ids
	 */
	@Override
	public HashMap<String, String> stateIdAndNameMapByRegion(
			List<String> regionIds) {
		return locationDAO.getStatesByRegionList(regionIds);
	}

	/**
	 * This method is used to get district id and name map by list of state ids
	 */
	@Override
	public HashMap<String, String> districtIdAndNameByState(
			List<String> stateIds) {
		return locationDAO.getDistrictsByStateList(stateIds);
	}

	/**
	 * This method is used to get city id and name map by list of district ids
	 */
	@Override
	public HashMap<String, String> cityIdAndNameByDistrict(
			List<String> districtIds) {
		return locationDAO.getCitiesByDistrictList(districtIds);
	}

	/**
	 * This method is used to get location id and name map by list of city ids
	 */
	@Override
	public HashMap<String, String> locationIdAndNameByCity(List<String> cityIds) {
		return locationDAO.getLocationsByCityList(cityIds);
	}

	/**
	 * This method is used to get location id and name map by list of location ids
	 */
	@Override
	public HashMap<String, String> locationIdAndNameBylocation(
			List<String> locationIds) {
		return locationDAO.getLocationsByLocationList(locationIds);
	}
}
