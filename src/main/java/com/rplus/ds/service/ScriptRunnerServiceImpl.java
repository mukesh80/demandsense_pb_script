package com.rplus.ds.service;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

@Repository
@Component
@PropertySource(value = { "classpath:application.properties" })
public class ScriptRunnerServiceImpl implements ScriptRunnerService {

	 @Autowired
	    private Environment environment;
	
	//final String DB_URL = "jdbc:postgresql://localhost:5432/Demand_Sense_PB?autoReconnect=true&relaxAutoCommit=false";

	@Override
	public  Connection getConnection() {
		Connection conn=null;
		try {
			Class.forName(environment.getRequiredProperty("jdbc.driverClassName"));
			conn = DriverManager.getConnection(
					environment.getRequiredProperty("jdbc.url")+"?autoReconnect=true&relaxAutoCommit=false",
					environment.getRequiredProperty("jdbc.username"),
					environment.getRequiredProperty("jdbc.password"));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    System.out.println("Connecting to database...");
	      
		return conn;
	}
	
	public boolean saveSQLFileToDirectory(MultipartFile file)
 	{
		boolean status=false;
 		System.out.println("Entry in saveFileToDirectory()");
 		if(!file.isEmpty())
 		{
 			try{
 				
 				Date date = new Date();
 				SimpleDateFormat sdate=new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
 				
 				String fullDate=sdate.format(date)+"_"+file.getOriginalFilename();
 				
 				String realPathtoUploads = "C:/Office_project/SQLFileUpload/"; 
 				if(! new File(realPathtoUploads).exists())
                {
 					System.out.println("Making new directory");
                    new File(realPathtoUploads).mkdir();
                }
 				String filePath = realPathtoUploads +fullDate.trim();
 	
 				File dest = new File(filePath);
                file.transferTo(dest);
                status=true;
                
 			}catch(Exception e)
 			{
 				e.printStackTrace();
 				System.out.println("MailServiceImpl:::: Error occord during saving excel file. "+e.getMessage());
 			}
 		}
		return status;
 		
 		
 	}
	

}
