package com.rplus.ds.service;

import java.sql.Connection;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public interface ScriptRunnerService {

	public boolean saveSQLFileToDirectory(MultipartFile file);
	public Connection getConnection();
}
