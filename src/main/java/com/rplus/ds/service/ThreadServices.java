package com.rplus.ds.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

/**
 * Service Interface for Thread admin
 * @version 1.0.0	27 Aug 2015
 * @author Jerald Joseph
 */

@Component
public interface ThreadServices {
	public boolean uploadNewRFile(HttpServletRequest Req, MultipartFile file);
	public boolean saveThreadSettings(HttpServletRequest Req);
	public boolean stopThreadOperation(HttpServletRequest Req);
	public boolean readThreadSettings(HttpServletRequest Req, Model map);
}
