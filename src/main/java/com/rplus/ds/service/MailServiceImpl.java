package com.rplus.ds.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.rplus.ds.config.MailConfig;


@Service("crunchifyEmail")
public class MailServiceImpl implements MailService {

	@Autowired
	MailSender mailSender;
	
	MailConfig sender;
	
	@Autowired
    private HttpServletRequest request;
	
	@Autowired
    private Environment environment;
	
	@Autowired
	HttpSession userSession;
	
	static String subject ;
	static String msgBody ;
	
	
	private Properties getMailProperties() {
	       Properties properties = new Properties();
	       properties.setProperty("mail.transport.protocol", "smtp");
	       properties.setProperty("mail.smtp.auth", "true");
	       properties.setProperty("mail.smtp.starttls.enable", "true");
	       properties.setProperty("mail.debug", "true");
	       properties.setProperty("mail.imap.ssl.trust", environment.getRequiredProperty("mail.imap.ssl.trust"));
	       
	       return properties;
	   }
	
	private JavaMailSenderImpl getJavaMail(){
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

        javaMailSender.setHost(environment.getRequiredProperty("spring.mail.host"));
        javaMailSender.setPort(Integer.parseInt(environment.getRequiredProperty("spring.mail.port")));
        javaMailSender.setUsername("support@rplusanalytics.com");
        javaMailSender.setPassword("r+support");
        javaMailSender.setJavaMailProperties(getMailProperties());
		return javaMailSender;

	}
	
	/*
	 * 
	 */
	@Override
	public void mailToCustomerFileLoad(boolean uploadStatus,String fileName,String filePath) {
		
		
		String toAddr= (String) userSession.getAttribute("userEmail1");  //"praveen.reddivari@gmail.com"; //userSession.getAttribute("userEmail1");
		//String toAddr[] =  {"praveen.kumar@rplusanalytics.com","saravanan@rplusanalytics.com"};
		
		String username = (String) userSession.getAttribute("loggedUser1");
		System.out.println("username=======>>>>>>>>> "+username);
		String fromAddr = "info@rplusanalytics.com";
		String cc = "support@rplusanalytics.com";
		
		if(uploadStatus)
		{
			 subject = "Paperboat � Data files loaded successfully at "+new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format( new Date());
			 msgBody = "Dear Mr. "+username+",<br><br><br>";
			 
			 msgBody += "You have successfully uploaded the following files into Paperboat�s cloud instance of DemandSense Application.<br><br>";
			 msgBody +=  "1. "+fileName+"<br><br>";

			  
			 msgBody += "You will be receiving a mail confirmation after the data is pre-processed and analysed and verified by the Customer Support team. ";
			 msgBody += "After that you can see the impact of uploaded data in the reports and analysis sections of DemandSense product. Thanks.<br><br><br>";
			 
			 msgBody += "Regards,<br> ";
			  
			 msgBody += "Support Team";
      
			 
		}
		else
		{
			subject = "Paperboat � Data files loading problem at "+new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format( new Date());
			msgBody = "Hi Customer,<br><br> Your data upload is unsucessfull, please try again.";
			 
			msgBody +=  "1. "+fileName+"<br><br><br>";
			msgBody += "Regards,<br> ";
			  
			msgBody += "Support Team";
		}
		
        JavaMailSenderImpl javaMailSender = getJavaMail();
		
		MimeMessage message = javaMailSender.createMimeMessage();
		
		MimeMessageHelper mailMessage;
		try {
			mailMessage = new MimeMessageHelper(message, true, "UTF-8");
			mailMessage.setFrom(fromAddr);
			mailMessage.setTo(toAddr);
			mailMessage.addCc(cc);
			mailMessage.setSubject(subject);
			mailMessage.setText(msgBody, true);
			
		} catch (MessagingException e) {
			
			e.printStackTrace();
		}
		
		
		javaMailSender.send(message);
		
		
		System.out.println("MailServiceImpl: mailToCustomerFileLoad(): end");
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see com.rplus.ds.service.MailService#rplusTeam(boolean, java.lang.String, java.lang.String)
	 */
	@Override
	public void rplusTeam(boolean uploadStatus,String fileName, String filePath) {
		
		/*String toAddr="praveen.reddivari@gmail.com";*/
		String toAddr[] =  {"praveen.kumar@rplusanalytics.com","saravanan@rplusanalytics.com",
				"raghavendra@rplusanalytics.com","murali.krishna@rplusanalytics.com","sandeep.babu@rplusanalytics.com",
				"adarsa.s@rplusanalytics.com","mukesh.kumar@rplusanalytics.com","vinesh.kumar@rplusanalytics.com"};

		String fromAddr = "info@rplusanalytics.com";
		
		if(uploadStatus)
		{
			 subject = "Paperboat � Data files loaded successfully at "+new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format( new Date());
			 msgBody = "Hi Team,<br><br> Our customer M/S.Paperboat has uploaded following file in to DemandSense and they are stored in the cloud file server link "+filePath;
			 
			 msgBody += "Please process the data and run the analysis with the fresh/ delta data.<br><br>";         
			 msgBody +=  "1. "+fileName+"<br><br><br>";
			 msgBody += "Regards, <br> DemandSense.";
		}
		else
		{
			subject = "Paperboat � Data files loading problem at "+new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format( new Date());
			msgBody = "Hi Team,<br><br> Our customer M/S.Paperboat is trying to load file in to DemandSense and failed.";
			 
			msgBody +=  "1. "+fileName+"<br><br><br>";
			msgBody += "Regards, <br> DemandSense.";
		}
		
        JavaMailSenderImpl javaMailSender = getJavaMail();
		
		MimeMessage message = javaMailSender.createMimeMessage();
		
		MimeMessageHelper mailMessage;
		try {
			mailMessage = new MimeMessageHelper(message, true, "UTF-8");
			mailMessage.setFrom(fromAddr);
			mailMessage.setTo(toAddr);
			mailMessage.setSubject(subject);
			mailMessage.setText(msgBody, true);
			
		} catch (MessagingException e) {
			
			e.printStackTrace();
		}
		
		
		javaMailSender.send(message);
		
		
		System.out.println("MailServiceImpl: rplusTeam(): end");
	}
	
	public void saveFileToDirectory(MultipartFile file)
 	{
 		System.out.println("Entry in saveFileToDirectory()");
 		if(!file.isEmpty())
 		{
 			try{
 				
 				Date date = new Date();
 				SimpleDateFormat sdate=new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
 				
 				String fullDate=sdate.format(date)+"_"+file.getOriginalFilename();
 				
 				String realPathtoUploads = "C:/Office_project/UploadedExcelFile/paperboat/"; 
 				if(! new File(realPathtoUploads).exists())
                {
 					System.out.println("Making new directory");
                    new File(realPathtoUploads).mkdir();
                }
 				String filePath = realPathtoUploads +fullDate.trim();
 	
 				File dest = new File(filePath);
                file.transferTo(dest);
                
                //Sending mail after upload
                rplusTeam(true,file.getOriginalFilename(),filePath);
                mailToCustomerFileLoad(true,file.getOriginalFilename(),filePath);
                
 			}catch(Exception e)
 			{
 				e.printStackTrace();
 				System.out.println("MailServiceImpl:::: Error occord during saving excel file. "+e.getMessage());
 			}
 		}
 		
 		
 	}

	@Override
	public String mailSupport(String name, String mobile, String subject,
			String body) {
		
		String mailTo = "support@rplusanalytics.com";
		String fromAddr = (String) userSession.getAttribute("userEmail1");  //"info@rplusanalytics.com";
		
		msgBody = body;
		msgBody += "<br><br> Posted by: "+name+" (Mobile: "+mobile+")";
		
		
        JavaMailSenderImpl javaMailSender = getJavaMail();
		
		MimeMessage message = javaMailSender.createMimeMessage();
		
		MimeMessageHelper mailMessage;
		try {
			mailMessage = new MimeMessageHelper(message, true, "UTF-8");
			mailMessage.setFrom(new InternetAddress(fromAddr));
			mailMessage.setReplyTo(new InternetAddress(fromAddr));
			mailMessage.setTo(mailTo);
			mailMessage.setSubject(subject);
			mailMessage.setText(msgBody, true);
			javaMailSender.send(message);
			mailSupportAcknowledge(name);
			return "sucess";
		} catch (MessagingException e) {
			
			e.printStackTrace();
			return "fail";
		}
		
		
		
	}
	
	public String mailSupportAcknowledge(String name) {
		
		String mailTo = (String) userSession.getAttribute("userEmail1"); 
		String fromAddr = "support@rplusanalytics.com"; //"info@rplusanalytics.com";
		
		subject = "Your Request/Feedback Received";
		msgBody = "Hi "+name+",<br><br> We have received your Request/Feedback. We are looking into it and we would revert to you soon. Thanks for your cooperation.<br><br><br>";
		msgBody += "Regards,<br> ";
		  
		msgBody += "R+ Support Team<br><br>";
		msgBody = "<strong>Note:</strong> This is an auto-generated mail.";
		
		
        JavaMailSenderImpl javaMailSender = getJavaMail();
		
		MimeMessage message = javaMailSender.createMimeMessage();
		
		MimeMessageHelper mailMessage;
		try {
			mailMessage = new MimeMessageHelper(message, true, "UTF-8");
			mailMessage.setFrom(new InternetAddress(fromAddr));
			mailMessage.setTo(mailTo);
			mailMessage.setSubject(subject);
			mailMessage.setText(msgBody, true);
			javaMailSender.send(message);
			return "sucess";
		} catch (MessagingException e) {
			
			e.printStackTrace();
			return "fail";
		}
		
		
		
	}


}


/*System.out.println("===============================::: mail");
try {
	mailService.rplusTeam(true, "testname");
} catch (MessagingException e) {
	// 
	e.printStackTrace();
}*/
