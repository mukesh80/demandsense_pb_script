package com.rplus.ds.service;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public interface MailService {

	public void saveFileToDirectory(MultipartFile file);

	void rplusTeam(boolean uploadStatus, String fileName, String filePath);

	void mailToCustomerFileLoad(boolean uploadStatus, String fileName,
			String filePath);

	public String mailSupport(String name, String mobile, String subject,
			String body);
}
