package com.rplus.ds.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.SAXException;

import com.rplus.ds.rJava.RJavaMainThread;

/**
 * Service Class for Thread admin
 * @version 1.0.0	27 Aug 2015
 * @author Jerald Joseph
 */

@Service("threadService")
@Repository
@Component
public class ThreadServiceImpl implements ThreadServices {

	private static final String UPLOAD_DIRECTORY = "upload";
    private static final String R_DIRECTORY = "R_Files";
    
    /**
	 * This method is used to Read Thread Settings
	 */
    public boolean readThreadSettings(HttpServletRequest req, Model map) {
    	boolean retVal = false; 
    	
    	Document dom;

    	// Make an  instance of the DocumentBuilderFactory
    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    	
    	String rWorkingFld = "c:\\rJava_Workspace";
    	File directory = new File(rWorkingFld);
    	String[] myFiles;
    	FilenameFilter filter = new FilenameFilter() {
    		public boolean accept(File directory, String fileName) {
        		return fileName.endsWith(".r");
    			//return fileName.endsWith(FilterStr);
    		}
    	};

    	myFiles = directory.list(filter);
    	System.out.println("c:\\rJava_Workspace folder r files count is: " + myFiles.length);
    	
    	try {
            // use the factory to take an instance of the document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
            // parse using the builder to get the DOM mapping of the    

            String xmlStr = "c:\\rJava_Workspace\\ThreadSetting.xml";
            dom = db.parse(xmlStr);
            
            Element doc = dom.getDocumentElement();
            
            String threadCountStr = getTextValue(null, doc, "thread-count");
            if (threadCountStr != null) {
                if (!threadCountStr.isEmpty())
                	map.addAttribute("thread-count", threadCountStr);
            }
            
            String executeRFileStr = getTextValue(null, doc, "executed-r-file");
            String rFileList = "\r\n<option value=\"Select\">-Select-</option>";
            for(String str: myFiles) {
        		if(str.equalsIgnoreCase(executeRFileStr) == true) 
        			rFileList += "\r\n<option value=\"" + str + "\" selected>" + str + "</option>";
        		else
        			rFileList += "\r\n<option value=\"" + str + "\">" + str + "</option>"; 
        	}
            map.addAttribute("existing-r-files", rFileList);
            
            String tableNameStr = getTextValue(null, doc, "table-name");
            if (tableNameStr != null) {
                if (!tableNameStr.isEmpty())
                	map.addAttribute("table-name", tableNameStr);
            }
            
            String schemaNameStr = getTextValue(null, doc, "schema-name");
            if (schemaNameStr != null) {
                if (!schemaNameStr.isEmpty())
                	map.addAttribute("schema-name", schemaNameStr);
            }
            
            String jvmRamSizeStr = getTextValue(null, doc, "jvm-ram-size");
            if (jvmRamSizeStr != null) {
                if (!jvmRamSizeStr.isEmpty())
                	map.addAttribute("jvm-ram-size", jvmRamSizeStr);
            }
            
            String jvmHeapSizeStr = getTextValue(null, doc, "jvm-heap-size");
            if (jvmHeapSizeStr != null) {
                if (!jvmHeapSizeStr.isEmpty())
                	map.addAttribute("jvm-heap-size", jvmHeapSizeStr);
            }
            
            retVal = true;
            
    	} catch(ParserConfigurationException pCE) {
    		System.out.println("ParserConfigurationException: " + pCE.getMessage());
    	} catch(SAXException sEx) {
    		System.out.println("SAXException: " + sEx.getMessage());
    	} catch(IOException ioEx) {
    		System.out.println("IOException: " + ioEx.getMessage());
    	}
    	return retVal;
    }
    
    /**
	 * This method is used to Save Thread Settings
	 */
    public boolean saveThreadSettings(HttpServletRequest req) {
		boolean retVal = false; 
		Document dom;
		Element e = null;
		
		System.out.println("ThreadService - saveThreadSettings");
		// instance of a DocumentBuilderFactory
	    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	    try {
	    	
	    	// use factory to get an instance of document builder
	        DocumentBuilder db = dbf.newDocumentBuilder();
	        
	        // create instance of DOM
	        dom = db.newDocument();
	        
	        // create the root element
	        Element rootEle = dom.createElement("thread-setting");
	        
	        // create data elements and place them under root
	        e = dom.createElement("thread-count");
	        String threadCntStr = req.getParameter("threadCount");
	        if(null != threadCntStr) {
	        	e.appendChild(dom.createTextNode(threadCntStr));
	        }
	        else {
	        	e.appendChild(dom.createTextNode("1"));
	        }
	        rootEle.appendChild(e);
	        
	        e = dom.createElement("executed-r-file");
	        String execRFileStr = "", newRFileStr = "";
	        if(null != req.getParameter("existRFiles"))
	        	execRFileStr = req.getParameter("existRFiles");
	        if(null != req.getParameter("rFile"))
	        	newRFileStr = req.getParameter("rFile");
	        if(null != newRFileStr) {
	        	if(newRFileStr.trim().length() > 0) {
	        		execRFileStr = newRFileStr;
	        	}
	        }
	        	
	        e.appendChild(dom.createTextNode(execRFileStr));
	        rootEle.appendChild(e);
	        
	        e = dom.createElement("table-name");
	        e.appendChild(dom.createTextNode(req.getParameter("dbTableName")));
	        rootEle.appendChild(e);
	        
	        e = dom.createElement("schema-name");
	        e.appendChild(dom.createTextNode(req.getParameter("dbSchemaName")));
	        rootEle.appendChild(e);
	        
	        e = dom.createElement("jvm-ram-size");
	        e.appendChild(dom.createTextNode(req.getParameter("jvmRamSize")));
	        rootEle.appendChild(e);
	        
	        e = dom.createElement("jvm-heap-size");
	        e.appendChild(dom.createTextNode(req.getParameter("jvmHeapSize")));
	        rootEle.appendChild(e);
	        
	        dom.appendChild(rootEle);

	        try {
	            Transformer tr = TransformerFactory.newInstance().newTransformer();
	            tr.setOutputProperty(OutputKeys.INDENT, "yes");
	            tr.setOutputProperty(OutputKeys.METHOD, "xml");
	            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
//	            tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "roles.dtd");
	            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

	            String xmlStr = "c:\\rJava_Workspace\\ThreadSetting.xml";
	            // send DOM to file
	            tr.transform(new DOMSource(dom), 
	                                 new StreamResult(new FileOutputStream(xmlStr)));
	            retVal = true;
	        } catch (TransformerException te) {
	            System.out.println("TransformerException: " + te.getMessage());
	        } catch (IOException ioe) {
	            System.out.println("IOException: " + ioe.getMessage());
	        }
	    } catch(ParserConfigurationException pce) {
	    	System.out.println("ParserConfigurationException: " + pce.getMessage());
	    }
		
		return retVal;
	}
    
    /**
	 * This method is used to stop the Thread Operations
	 */
	@SuppressWarnings("static-access")
	@Override
    public boolean stopThreadOperation(HttpServletRequest Req) {
    	boolean retVal = false;
    	RJavaMainThread thrdObj = new RJavaMainThread();
    	System.out.println("Service - stopThreadOperation");
		int procState = thrdObj.getThreadOptState();
    	System.out.println("Service - Thread Operation State: " + procState);
    	if(procState == 1) {
    		thrdObj.setThreadOptState(2);
    		retVal = true;
    	}
    	return retVal;
    }
    
	/**
	 * This method is used to upload new R File
	 */
	@Override
	public boolean uploadNewRFile(HttpServletRequest req, MultipartFile file) {
		boolean retVal = false;
		System.out.println("Inside uploadNewRFile (Service)");
		System.out.println("Property \"java.io.tmpdir\" value is: " + System.getProperty("java.io.tmpdir"));
	
		String fileNameStr = req.getParameter("hrFile");

		if(null != fileNameStr) {
			System.out.println("Upload file name is(1): " + fileNameStr);
			fileNameStr = fileNameStr.substring(fileNameStr.lastIndexOf(File.separator)+1);
			System.out.println("Upload file name is(2): " + fileNameStr);
		}
		else {
			return false;
		}

		String uploadPath = req.getServletContext().getRealPath("")
		                + File.separator + UPLOAD_DIRECTORY;

		System.out.println("File Upload location: " + uploadPath); 

		File uploadDir = new File(uploadPath);
		if (!uploadDir.exists()) {
			uploadDir.mkdir();
		}

		try {
			if (!file.isEmpty()) {
				byte[] bytes = file.getBytes();
				String servFileNameStr = uploadDir.getAbsolutePath() + File.separator + fileNameStr;
				File serverFile = new File(servFileNameStr);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();
                FileUtils.copyFile(serverFile, new File("C:\\rJava_Workspace\\" + fileNameStr));
                //System.out.println("before thread Object create");
                RJavaMainThread thrdObj = new RJavaMainThread();
                thrdObj.setrScriptStr("C:\\rJava_Workspace\\" + fileNameStr);
                //System.out.println("after thread Object create");
                String schemaStr = "", thrdCntStr = "", tblStr = "";
                thrdObj.setPropFileLoc(req.getServletContext().getRealPath("") + File.separator + "WEB-INF" + File.separator + "classes"  + File.separator);
                //C:\Tomcat7\apache-tomcat-7\apache-tomcat-7.0.59\wtpwebapps\DemandSense\WEB-INF\classes\com\rplus\ds\rJava
                if(null != req.getParameter("dbTableName")) {
                	tblStr = req.getParameter("dbTableName");
                }
                if(null != req.getParameter("dbSchemaName")) {
                	schemaStr = req.getParameter("dbSchemaName"); 
                }
                System.out.println("DB schema name is: " + schemaStr);
                if(null != req.getParameter("threadCount")) {
                	thrdCntStr = req.getParameter("threadCount");
                	try {
                		int thrdCnt = Integer.parseInt(thrdCntStr);
                		if(schemaStr.length()>1) {
                			thrdObj.setSchemaTblStr(schemaStr, tblStr);
                			thrdObj.setThreadCnt(thrdCnt);
                			thrdObj.setrScriptStr("C:\\rJava_Workspace\\" + fileNameStr);
                			System.out.println("Thread proc begin to start");
                			thrdObj.mainProcess();
                		}
                	}
                	catch(NumberFormatException nFEx) {
                		System.out.println("NumberFormatException: " + nFEx.getMessage());
                	}
                	catch(Exception ex) {
                		System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
                	}
                }
                int thrdCnt;
                retVal = true;
			}
		}
		catch (Exception ex) {
			System.out.println("failed to upload " + fileNameStr + " => " + ex.getMessage());
        }
		return retVal;
	}
	
	private void showAllReqParams(HttpServletRequest req) {
		Enumeration<String> enuParam = req.getAttributeNames();
		while(enuParam.hasMoreElements()) {
			
		}
	}
	
	private String getTextValue(String def, Element doc, String tag) {
	    String value = def;
	    NodeList nl;
	    nl = doc.getElementsByTagName(tag);
	    if (nl.getLength() > 0 && nl.item(0).hasChildNodes()) {
	        value = nl.item(0).getFirstChild().getNodeValue();
	    }
	    return value;
	}
}
