package com.rplus.ds.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rplus.ds.dao.DataExploreDAO;
import com.rplus.ds.dao.PredictiveDAO;
import com.rplus.ds.dao.PredictiveDAOImpl;

@Service("deService")
@Transactional
@Repository
@Component
public class DataExplorerServiceImpl implements DataExplorerService {
	/**
	 * Autowired PredictiveService Interface instance 
	 */
	@Autowired
	PredictiveService predictiveService;
	
	/**
	 * Autowired PredictiveDAOImpl Class instance 
	 */
	@Autowired
	PredictiveDAOImpl predictiveDAO; 
	
	@Autowired
	DataExploreDAO deDAO;
	
	@Override
	public String getCommonParameterHQL(Map<String,String> allParams) {
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		StringBuilder hqlQuery = new StringBuilder();
		//String sqlStr = "select city_id, category_id, location_id, final_qty, product_id, sub_category_id, best_model, best_mean_value, best_max_value, best_min_value from rplus_ds_customer.rplus_ds_output_f_day where quantity>2 limit 20";
		hqlQuery.append("select res.cityId, res.categoryId, res.locationId, res.finalQty, res.productId, res.subCategoryId, res.bestModel,");
		hqlQuery.append(" res.bestMeanValue, res.bestMaxValue, res.bestMinValue from ProdLocTimeByDay res where (");
		hqlQuery.append(predictiveDAO.getCommonHqlQuery(allParamsMap));
		return hqlQuery.toString();
	}
	
	@Override
	public List<Object[]> getForecostData(Map<String,String> allParams) {
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		StringBuilder hqlQuery = new StringBuilder();
		//String sqlStr = "select city_id, category_id, location_id, final_qty, product_id, sub_category_id, best_model, best_mean_value, best_max_value, best_min_value from rplus_ds_customer.rplus_ds_output_f_day where quantity>2 limit 20";
		hqlQuery.append("select res.cityId, res.categoryId, res.locationId, res.finalQty, res.productId, res.subCategoryId, res.bestModel,");
		hqlQuery.append(" res.bestMeanValue, res.bestMaxValue, res.bestMinValue from RplusDsOutput res where (");
		hqlQuery.append(predictiveDAO.getCommonHqlQuery(allParamsMap));
		return deDAO.getForecastDataAllParams(hqlQuery.toString(), allParamsMap);
	}
	
	@Override
	public List<Object[]> getSignificantData(Map<String,String> allParams) {
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		StringBuilder hqlQuery = new StringBuilder();
		//String sqlStr = "select country_id, location_id, region_id, state_id, district_id, city_id, product_id, category_id, sub_category_id, significantfactors, correlation from rplus_ds_customer.rplus_ds_output_sf_day limit 20";
		hqlQuery.append("select res.countryId, res.locationId, res.regionId, res.stateId, res.districtId, res.cityId, res.productId,");
		hqlQuery.append(" res.categoryId, res.subCategoryId, res.significantFactors, res.correlation from RplusDsOutputSFDay res where (");
		hqlQuery.append(predictiveDAO.getCommonHqlQuery(allParamsMap));
		return deDAO.getSignificantDataAllParams(hqlQuery.toString(), allParamsMap);
	}
	
	@Override
	public List<Object[]> getAccuracyData(Map<String,String> allParams) {
		HashMap<String, HashMap<String, String>> allParamsMap = predictiveService.getAllInputParamsMap(allParams);
		StringBuilder hqlQuery = new StringBuilder();
		//String sqlStr = "select country_id, location_id, region_id, state_id, district_id, city_id, product_id, category_id, sub_category_id, model, rmse from rplus_ds_customer.rplus_ds_output_a_day limit 20";
		hqlQuery.append("select res.countryId, res.locationId, res.regionId, res.stateId, res.districtId, res.cityId, res.productId,");
		hqlQuery.append(" res.categoryId, res.subCategoryId, res.model, res.rmse from RplusDsOutputADay res where (");
		hqlQuery.append(predictiveDAO.getCommonHqlQuery(allParamsMap));
		return deDAO.getAccuracyDataAllParams(hqlQuery.toString(), allParamsMap);
	}
}
