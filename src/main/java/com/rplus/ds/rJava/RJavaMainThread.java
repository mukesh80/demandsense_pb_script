package com.rplus.ds.rJava;

import java.sql.*;
import java.util.*;
import java.io.*;

import org.springframework.core.env.Environment;
import com.rplus.ds.rJava.RJavaNoThread;

public class RJavaMainThread extends Thread {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "org.postgresql.Driver"; // MySql: "com.mysql.jdbc.Driver";  
    static final String DB_URL = "jdbc:postgresql://192.168.1.2:5432/Demand_Sense";  // "jdbc:mysql://localhost/EMP";
    //static final String DB_URL = "jdbc:postgresql://207.182.147.154:5432/Demand_Sense";  // Cloud Server dt: 26-08-2015

    //  Database credentials
    static final String USER = "postgres";
    //static final String PASS = "postgres";
    static final String PASS = "product@123";

    private String locIdStr = "", schemaTblStr = "", rScriptStr = "", propFileLoc = "", classPathStr = "";
    private boolean threadFlag = false;
    private int threadCnt = 1;
    
    private static int threadOptState = 0; //{0 - idle| 1 - running| 2 - Stop}
    private static String threadStateStr  = ""; //{"Process Started"|"Process Completed"|"Process Stopped"}
    
	public RJavaMainThread() {
		super();
		threadCnt = 1;
    }

    public RJavaMainThread(String Str) {
    	super();
    	locIdStr = Str;
    }

    public void setThreadName1(String Str) {
    	locIdStr = Str;
    }

    public void setThreadFlag(boolean flg) {
    	threadFlag = flg;
    }

    public boolean getThreadFlag() {
    	return threadFlag;

    }
    
	public String getLocIdStr() {
		return locIdStr;
	}

	public void setLocIdStr(String locIdStr) {
		this.locIdStr = locIdStr;
	}

	public String getSchemaTblStr() {
		return schemaTblStr;
	}

	public void setSchemaTblStr(String schemaTblStr) {
		this.schemaTblStr = schemaTblStr;
	}
	
	public void setSchemaTblStr(String schemaStr, String tblStr) {
		this.schemaTblStr = schemaStr + "." + tblStr;
	}

	public String getrScriptStr() {
		return rScriptStr;
	}

	public void setrScriptStr(String rScriptStr) {
		this.rScriptStr = rScriptStr;
	}

	public String getPropFileLoc() {
		return propFileLoc;
	}

	public void setPropFileLoc(String propFileLoc) {
		this.propFileLoc = propFileLoc;
	}

	public String getClassPathStr() {
		return classPathStr;
	}

	public void setClassPathStr(String classPathStr) {
		this.classPathStr = classPathStr;
	}

	public int getThreadCnt() {
		return threadCnt;
	}

	public void setThreadCnt(int threadCnt) {
		this.threadCnt = threadCnt;
	}

	public static int getThreadOptState() {
		return threadOptState;
	}

	public static void setThreadOptState(int threadOptState) {
		RJavaMainThread.threadOptState = threadOptState;
	}

	public static String getThreadStateStr() {
		return threadStateStr;
	}

	public static void setThreadStateStr(String threadStateStr) {
		RJavaMainThread.threadStateStr = threadStateStr;
	}

	public void run() {
		try{
	        //System.out.println("Hello from a thread: " + m_LocIdStr + "\r\nRunning RScript is: " + m_RScriptStr + "\r\nUser Dir: " + System.getProperty("user.dir"));
			System.out.println("Hello from a thread: " + locIdStr + "\r\nRunning RScript is: " + rScriptStr + "\r\nUser Dir: " + System.getProperty("user.dir"));
			Runtime rt = Runtime.getRuntime();
			int procCnt = rt.availableProcessors();
			Process pro1 = null;
			System.out.println("availableProcessors: " + procCnt);
			
			if(procCnt>0) {
			    //Pro1 = Rt.exec("java -cp JRI.jar;JRIEngine.jar;REngine.jar;. RJavaNoThread " + m_LocIdStr);  // server VM // m_RScriptStr
				//pro1 = rt.exec("c:\\rJava_Workspace\\rJava.bat " + m_PropFileLoc + " " + m_LocIdStr + " " + m_RScriptStr);  // server VM // m_RScriptStr
				pro1 = rt.exec("c:\\rJava_Workspace\\rJava.bat " + propFileLoc + " " + locIdStr + " " + rScriptStr);  // server VM // rScriptStr
			    int completed = pro1.waitFor();
			    System.out.println("Rscript exit value is: " + pro1.exitValue() + "\r\nThe Process waitFor status is: " + completed);
			}
		}
		catch(Exception Ex) {
		    System.out.println(Ex.getClass().getName() + ": " + Ex.getMessage());
		}
		finally {
		    setThreadFlag(false);
		}
    }

	public void mainProcess() {
		try
		{
			RJavaMainThread thrd[] = new RJavaMainThread[10];
			Connection conn = null;
			Statement stmt = null;
			
			Properties prop = new Properties();
			FileInputStream fis = new FileInputStream(new File(propFileLoc +  "application.properties"));
			prop.load(fis);
			fis.close();
			String drvCls = prop.getProperty("jdbc.driverClassName"); 
			String dbURL = prop.getProperty("jdbc.url");
			String dbUsr = prop.getProperty("jdbc.username");
			String dbPwd = prop.getProperty("jdbc.password");
			Class.forName(drvCls);
			System.out.println("DB driver is: " + drvCls);
			System.out.println("Connecting to database...\r\nUsing args (URL, User, Pwd): " + dbURL + ", " + dbUsr + ", " + dbPwd);
		    //conn = DriverManager.getConnection(DB_URL,USER,PASS);
			conn = DriverManager.getConnection(dbURL,dbUsr,dbPwd);
			System.out.println("Creating statement...");
	        stmt = conn.createStatement(
	                           ResultSet.TYPE_SCROLL_INSENSITIVE,
	                           ResultSet.CONCUR_READ_ONLY);
	        String sql = "select distinct(location_id) from " + schemaTblStr; 
	        System.out.println("The SQL Str: select distinct(location_id) from " + schemaTblStr);
	        //sql = "select distinct(location_id) from rplus_ds_demo.rplus_ds_rinput_day"; //  // Cloud Server dt: 26-08-2015
	
	        ResultSet rs = stmt.executeQuery(sql);
			int cnt = 0, procCnt = 0;
			List<String> lst = new ArrayList();
			while(rs.next()) {
			    String locIdStr = rs.getString("location_id");
			    System.out.println("location_id: " + locIdStr);
			    lst.add(locIdStr);
			    cnt++;
			}
			rs.close();
	        stmt.close();
	        conn.close();
	
	        for(int i = 0; i<threadCnt; i++) {
			    thrd[i] = new RJavaMainThread();
			    thrd[i].setThreadFlag(true);
			    thrd[i].setThreadName1(lst.get(procCnt++));
			    thrd[i].propFileLoc = this.propFileLoc;
			    thrd[i].rScriptStr = this.rScriptStr;
			    thrd[i].start();
			}
		
	        threadOptState = 1;
	        threadStateStr  = "Process Started";
	        
			boolean whileFlg = false;
			int rScriptCnt = 0;
			while(true){
				if(threadOptState == 2) { // For manually stop the process
					threadStateStr = "Process Stopped";
					break;
				}
			    for(int i = 0; i<threadCnt; i++){
					if(thrd[i].getThreadFlag() == false) { 
					    rScriptCnt = checkForRScript();
					    if(rScriptCnt < threadCnt)
					    {
							System.out.println("Assign Thread Name as: " + lst.get(procCnt));
							thrd[i] = new RJavaMainThread();
							thrd[i].setThreadFlag(true);
							thrd[i].setThreadName1(lst.get(procCnt++));
						    thrd[i].propFileLoc = this.propFileLoc;
						    thrd[i].rScriptStr = this.rScriptStr;
							thrd[i].start();
							sleep(20 * 1000);
					    }
					}
					if(procCnt >= cnt){
					    System.out.println("(For Loop - Exit) Location Processing Count is: " + procCnt);
					    whileFlg = true;
					    break;
					}
			    } // End of For
			    if(whileFlg == true){
					System.out.println("(While Loop - Exit) Location Processing Count is: " + procCnt + "\r\nThe Process is Completed");
					threadOptState = 0;
					threadStateStr  = "Process Completed";
					break;
			    }
			    //System.out.println("Active Thread Count is: " + Activ_Thrd_Cnt);
			    sleep(20 * 1000);
			} // End of While
		} // Try Block Ends
		catch(Exception Ex){
		    System.out.println(Ex.getClass().getName() + ": " + Ex.getMessage());
		}
    }
    public int checkForRScript()
    {
		int Cnt = 0;
		try
		{
			Runtime runtime = Runtime.getRuntime();
			String cmds[] = {"cmd", "/c", "tasklist"};
			Process proc = runtime.exec(cmds);
			InputStream inputstream = proc.getInputStream();
			InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
			BufferedReader bufferedreader = new BufferedReader(inputstreamreader);
			String line = "", line1 = "";
		
			List<String> lst = new ArrayList<String>();
			while ((line = bufferedreader.readLine()) != null) {
			    line1 = line.toUpperCase();
			    lst.add(line1);
			    if(line1.indexOf("RSCRIPT") != -1)
				Cnt++;
	        }
		}
		catch(IOException IOEx)
		{
		    Cnt = 10;
		    System.out.println("IOException: " + IOEx.getMessage());
		}
		return Cnt;
    }
}
