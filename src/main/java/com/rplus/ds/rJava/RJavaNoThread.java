package com.rplus.ds.rJava;

import org.rosuda.JRI.Rengine;

public class RJavaNoThread {
    
    private String locIdStr = "";
    private String rFileStr = "";

    public RJavaNoThread(String Str) {
    	locIdStr = Str;
    }
    
    public RJavaNoThread(String locStr, String rFileStr) {
    	locIdStr = locStr;
    	this.rFileStr = rFileStr;
    }

    public void noThreadRun() {
		try {
	        System.out.println("Hello from Java To Location: " + locIdStr);
			Runtime Rt = Runtime.getRuntime();
			int Proc_Cnt = Rt.availableProcessors();
			Process Pro1 = null;
			
			if(Proc_Cnt>0) {
				Pro1 = Rt.exec("Rscript " + rFileStr + " " + locIdStr);  // Cloud server // Dt: 26-08-2015
			    //Pro1 = Rt.exec("Rscript C:\\rJava_Workspace\\r_code_final_daily.r " + m_LocIdStr);  // Cloud server // Dt: 26-08-2015
//			    Pro1 = Rt.exec("Rscript C:\\Jerald\\Java_Workspace\\Thread\\5_Thread_RJava\\Threading_finalR_311_32.R " + m_LocIdStr);  // local system - JJ
//			    Rengine re = new Rengine (new String [] {"--vanilla"}, false, null);
			    Rengine re = new Rengine (new String [] {"--Rplus", "--rJava"}, false, null);			    
		
			    if (!re.waitForR()) {
			         System.out.println ("Cannot load R");
			         return;
			    }
			    int Completed = Pro1.waitFor();
			    re.end();
			    System.out.println("Rscript exit value is: " + Pro1.exitValue() + "\r\nThe Process waitFor status is: " + Completed);
			    Rt.gc();
			}
			else {
			    System.out.println("Not able to RUN due to un AVAILABILITY of Processor Count: " + Proc_Cnt);
			}
		}
		catch(Exception Ex) {
		    System.out.println(Ex.getClass().getName() + " (In noThreadRun): " + Ex.getMessage());
		}
    }

    public static void main(String args[]) {
		try {
		    if(args.length > 0) {
				String ArrStr[] = {"MyThreadSuspend.java", "MyThreadYield.java"};
				//RJavaNoThread Obj = new RJavaNoThread(args[0]);
				RJavaNoThread Obj = new RJavaNoThread(args[0], args[1]);
				Obj.noThreadRun();
				System.out.println("Process Completed for: \"" + args[0] +"\"");
		    }
		    else {
		    	System.out.println("Use AppName \"arg\"");
		    }
		}
		catch(Exception Ex) {
		    System.out.println(Ex.getClass().getName() + " (In main): " + Ex.getMessage());
		}
    }
}
