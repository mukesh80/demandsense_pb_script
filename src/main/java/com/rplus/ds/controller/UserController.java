package com.rplus.ds.controller;



import java.net.UnknownHostException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rplus.ds.dao.UserDao;
import com.rplus.ds.dao.UserLogDao;
import com.rplus.ds.dao.UserLogDaoImpl;
import com.rplus.ds.model.User;

@RestController
@RequestMapping("/user")
public class UserController {
	
	private static final Logger logger = Logger.getLogger(UserController.class);
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	UserLogDao useLogDao;
	
	
	@Autowired
	UserLogDaoImpl userLogDaoImpl;
	
	@Autowired
	HttpSession userSession;
	
	public static String userSchemaName;
	
	
	
	@RequestMapping("registercontroller")
	public ModelAndView registerUser(@ModelAttribute User user,final RedirectAttributes redirectAttributes) {
		logger.info("UserController : registerUser()");
	
		boolean status=userDao.findUserByUserName(user.getUsername());
		
		if(status)
		{
			System.out.println("new user");
			userDao.save(user);
			redirectAttributes.addFlashAttribute("success", "User name with "+user.getUsername()+" successfully registered");
			return new ModelAndView("redirect:/loginpage");
			
		}
		else
		{
			System.out.println("ecsiting user");
			redirectAttributes.addFlashAttribute("existinguser", "User name with "+user.getUsername()+" already exits");
			return new ModelAndView("redirect:/registerPage");
		}
	}	
	
	@RequestMapping("success")
	public ModelAndView getSuccessPage()
	{
		logger.info(" UserController: getting success page");
		return new ModelAndView("success");
	}
	

	@RequestMapping(value="loginCheck1",method = RequestMethod.POST)
	public ModelAndView loginUser(final RedirectAttributes redirectAttributes,@RequestParam String username,
			@RequestParam String password ,HttpServletRequest request,HttpServletResponse response,Model model) throws Exception
	{   logger.info(" UserController: loginUser() ");
		
	
		List<User> result =userDao.getList();
		request.getSession().setAttribute("authentication", result);
		
		boolean status=userDao.validateUser(username,password);
		System.out.println("After validation ::::::::::in UserController");
		if(status){
			User userInfo=userDao.getUserInfo(username);
			String firstNm=userInfo.getFirstName();
			String lastNm=userInfo.getLastName();
			
			if(firstNm == null){
				firstNm = userInfo.getUsername();
			}
			
			if(lastNm==null)
			{
				lastNm=" ";
			}
			String fullName=(firstNm.substring(0, 1).toUpperCase() + firstNm.substring(1))+" "
					+(lastNm.substring(0, 1).toUpperCase() + lastNm.substring(1));
			System.out.println("First and last name Full name :   "+fullName);
			request.getSession().setAttribute("loggedUserName", fullName);
			
			
			userSession.setAttribute("loggedUser", userInfo.getUsername());
			userSession.setAttribute("userType", userInfo.getUserType());
			
			redirectAttributes.addFlashAttribute("loginFirst", "please Login first");
			userSession.setAttribute("userSchemaName", userInfo.getSchemaName());
			System.out.println("Schema name in session :::UserController" +userSession.getAttribute("userSchemaName"));
			userSchemaName=(String) userSession.getAttribute("userSchemaName");
			userSession.setAttribute("userEmail1", userInfo.getEmailId());
			userSession.setAttribute("loggedUser1", userInfo.getUsername());
			useLogDao.saveUserLog("login");
			
			//Configuration con = new Configuration(); 
			//con.setProperties(prop);
			return new ModelAndView("redirect:/index");
		}
		else {
			request.setAttribute("Error","Sorry! Username or Password Error. plz Enter Correct Detail or Register");
			 String message = "Invalid username or password!";
			 redirectAttributes.addFlashAttribute("invaliduser", message);
			 request.getSession().setAttribute("Loginmsg","plz sign in first");
			return new ModelAndView("redirect:/");
		}

	}
	

	
	@RequestMapping("logout")
	public ModelAndView logout(HttpServletRequest request,final RedirectAttributes redirectAttributes) throws UnknownHostException{
		logger.info(" UserController: logout() ");
		

		
		useLogDao.saveUserLog("logout");
		
		
		request.getSession().invalidate();
		redirectAttributes.addFlashAttribute("succusslogout","You have logged out succesfully");
		logger.info(" Page Logout successfully ");
		return new ModelAndView("redirect:/");
	}
	

	@RequestMapping(value="authentication",method = RequestMethod.GET)
	public  List<User> getUserDetails(HttpServletRequest request)
	{
	
		List<User> result =userDao.getList();
		return result;
	}
	
	@RequestMapping(value="userupdate",method = RequestMethod.POST)
	public String getUserUpdates(
			HttpServletRequest httpServletRequest,@ModelAttribute User user) throws UnknownHostException
	{
		System.out.println("Update controller Start..");
		
		useLogDao.saveUserLog("Profile Update");
		
		user.setUsername((String)userSession.getAttribute("loggedUser1"));
		String result =userDao.updateUser(user);
		System.out.println("Update controller END.."+result);
		return result;
	}
	
	@RequestMapping(value="passupdate",method = RequestMethod.POST)
	
	public String getUserPassword(
			HttpServletRequest httpServletRequest,@RequestParam String pass) throws UnknownHostException
	{
		//ModelAndView res = new ModelAndView("/profile");
		System.out.println("Update password controller Start..");
		
		useLogDao.saveUserLog("Password Update");
		
		String result =userDao.updatePassword(pass);
		System.out.println("Update password controller END.."+result);
		//res.addObject("result", result);
		return result;
	}

	

}



