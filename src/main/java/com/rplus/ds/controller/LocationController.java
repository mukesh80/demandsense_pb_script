package com.rplus.ds.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.LocationDao;
import com.rplus.ds.model.Location;

/**
 * 
 * @author Rafeeq
 * 
 */
@RestController
@RequestMapping("/location")
public class LocationController {
	
	private static final Logger logger = Logger.getLogger(LocationController.class);
	
	@Autowired
	LocationDao locationDao;
	
	
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<Location> getLocList() {
		
		logger.info("LocationController:: getLocList() START");
		List<Location> locationList = locationDao.getLocList();
		
		logger.info("LocationController:: getLocList() END");
		return locationList;
	}

	/**
	 * This method is used to get the all the countries
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "countryList", method = RequestMethod.GET)
	public HashMap<String, String> getCountryList(HttpServletRequest request)

	{
		logger.info("LocationController:: getCountryList() START");
		HashMap<String, String> CountriesMap = locationDao.getCountries();
		logger.info("LocationController:: getCountryList() END");
		return CountriesMap;
	}

	/**
	 * This mapping is to get all the regions based on countryID
	 * 
	 * @param httpServletRequest
	 * @param countryId
	 * @return
	 */
	@RequestMapping(value = "regions", method = RequestMethod.GET)
	public HashMap<String, String> getRegionList(
			HttpServletRequest httpServletRequest,@RequestParam String countryId)

	{
		logger.info("Invoke getRegionList()- START ");
		HashMap<String, String> regionsMap = locationDao
				.getRegionsByCountryId(countryId);
		logger.info("Invoke getRegionList()- END ");
		return regionsMap;
	}

	/**
	 * This mapping is to get all the states based on regionID
	 * 
	 * @param httpServletRequest
	 * @param regionId
	 * @return
	 */
	@RequestMapping(value = "states", method = RequestMethod.GET)
	public HashMap<String, String> getStatesList(
			HttpServletRequest httpServletRequest, @RequestParam String regionId)
	{
		logger.info(" Invoke getStatesList()START ");
		HashMap<String, String> statesMap = locationDao
				.getStatesByRegionId(regionId);	
		logger.info(" Invoke getStatesList()END ");
		return statesMap;
	}

	/**
	 * This mapping is to get all districts based on StateId
	 * 
	 * @param httpServletRequest
	 * @param stateId
	 * @return
	 */
	@RequestMapping(value = "districts", method = RequestMethod.GET)
	public HashMap<String, String> getDistrictsList(
			HttpServletRequest httpServletRequest, @RequestParam String stateId)

	{
		logger.info("Invoke getDistrictsList() START ");
		HashMap<String, String> districtsMap = locationDao
				.getDistrictsByStateId(stateId);
		logger.info("Invoke getDistrictsList() END ");
		return districtsMap;
	}

	/**
	 * This mapping is used to get all cities based on districtId
	 * 
	 * @param httpServletRequest
	 * @param districtId
	 * @return
	 */
	@RequestMapping(value = "cities", method = RequestMethod.GET)
	public HashMap<String, String> getCitiesList(
			HttpServletRequest httpServletRequest, @RequestParam String districtId)

	{
		logger.info("Invoke getCitiesList() START ");
		HashMap<String, String> citiesMap = locationDao
				.getCitiesByDistrictId(districtId);
		logger.info("Invoke getCitiesList() END ");
		return citiesMap;
	}

	/**
	 * This method is used to get all locations based on cityID
	 * 
	 * @param httpServletRequest
	 * @param cityId
	 * @return
	 */
	@RequestMapping(value = "locids", method = RequestMethod.GET)
	public HashMap<String, String> getLocationsList(
			HttpServletRequest httpServletRequest, @RequestParam String cityId)

	{
		logger.info("Invoke getLocationsList() START");
		HashMap<String, String> locationsMap = locationDao
				.getLocationIdsByCityId(cityId);
		logger.info("Invoke getLocationsList() END ");
		return locationsMap;
	}
	
	 /**
     * This method is used to get all the available regions
     *  @param httpServletRequest
     * @return
     */
    @RequestMapping(value="allregions", method= RequestMethod.GET)
    public HashMap<String,String> getAllRegions(HttpServletRequest request){
    	logger.info("Invoke getAllRegions() START ");
    	HashMap<String,String> regionsMap = locationDao.getAllRegions();
    	logger.info("Invoke getAllRegions() END ");
        return regionsMap;
    }
    
    /**
     * This method is used to get all the available states
     *  @param httpServletRequest
     * @return
     */
    @RequestMapping(value="allstates", method= RequestMethod.GET)
    public HashMap<String,String> getAllStates(HttpServletRequest request){
    	logger.info("Invoke getAllStates() START ");
    	HashMap<String,String> statesMap = locationDao.getAllStates();
    	logger.info("Invoke getAllStates() END ");
        return statesMap;
    }
    
    /**
     * This method is used to get all the available districts
     *  @param httpServletRequest
     * @return
     */
    @RequestMapping(value="alldistricts", method= RequestMethod.GET)
    public HashMap<String,String> getAllDistricts(HttpServletRequest request){
    	logger.info("Invoke getAllDistricts() START ");
    	HashMap<String,String> districtsMap = locationDao.getAllDistricts();
    	logger.info("Invoke getAllDistricts() END ");
        return districtsMap;
    }
    
    /**
     * This method is used to get all the available cities
     *  @param httpServletRequest
     * @return
     */
    @RequestMapping(value="allcities", method= RequestMethod.GET)
    public HashMap<String,String> getAllCities(HttpServletRequest request){
    	logger.info("Invoke getAllCities() START ");
    	HashMap<String,String> citiesMap = locationDao.getAllCities();
    	logger.info("Invoke getAllCities() END ");
        return citiesMap;
    }
    
    /**
     * This method is used to get all the available LocationIds
     *  @param httpServletRequest
     * @return
     */
    @RequestMapping(value="alllocationIds", method= RequestMethod.GET)
    public HashMap<String,String> getAllLocationIds(HttpServletRequest request){
    	logger.info("Invoke getAllLocationIds() START ");
    	HashMap<String,String> locationidsMap = locationDao.getAllLocationIds();
    	logger.info("Invoke getAllLocationIds() END ");
        return locationidsMap;
    }
	
	
	
}
