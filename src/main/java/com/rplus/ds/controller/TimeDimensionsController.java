package com.rplus.ds.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.TimeDimensionsDAO;
import com.rplus.ds.model.TimeDimensions;



/**
 * 
 * @author Mallikarjuna
 *
 */
@RestController
@RequestMapping("/timeDimensions")
public class TimeDimensionsController {
	
	private static final Logger logger = Logger.getLogger(TimeDimensionsController.class);
	@Autowired
    private TimeDimensionsDAO timeDimensionsDao;
	
		
	/**
	 * This method is used to get the time dimensions
	 * @return
	 */
	@RequestMapping("getTimeDimensions")
	public List<TimeDimensions> getTimeDimensionsData(){
		logger.info(" TimeDimensionsController : getTimeDimensionsData() START ");
		List<TimeDimensions> timeDimensionsList = timeDimensionsDao.list();
		System.out.println(" TimeDimensionsList "+timeDimensionsList);
		return timeDimensionsList;
	}

}
