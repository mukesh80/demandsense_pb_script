package com.rplus.ds.controller;


import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rplus.ds.dao.UserDao;
import com.rplus.ds.dao.UserLogDao;
import com.rplus.ds.model.User;
import com.rplus.ds.model.UserLog;

/**
 * 
 * @author Mallikarjuna
 *
 */
@Controller
@RequestMapping("/")
public class HomeController {
	
	private static final Logger logger = Logger.getLogger(HomeController.class);
	
	@Autowired
	UserLogDao useLogDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	HttpSession httpSession;
	
	
	
	
	UserLog userLog  = new UserLog();

	@RequestMapping(value = "/NewProductLaunch", method = RequestMethod.GET)
	public String getNewProductLaunch(final RedirectAttributes redirectAttributes) throws UnknownHostException {
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			System.out.println("Home Controller: correct user");
			logger.info(" Invoked getNewProductLaunch");

			
			//useLogDao.saveUserLog("New Product Launch");
			
			
			return "/newproductlaunch";
			
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return "redirect:/";
		}
		
	}
	
	@RequestMapping(value = "/Setting", method = RequestMethod.GET)
	public String getSetting(final RedirectAttributes redirectAttributes) throws UnknownHostException {
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			logger.info(" Invoked getSetting");
			//useLogDao.saveUserLog("Setting");
			return "/setting";
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return "redirect:/";
		}
		
	}
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public String getReport(final RedirectAttributes redirectAttributes) throws UnknownHostException {
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			logger.info(" Invoked getReport");
			useLogDao.saveUserLog("Reports Tab");
			return "/reports";
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return "redirect:/";
		}
		
	}
	
	@RequestMapping(value = "/logs", method = RequestMethod.GET)
	public String getLogs(final RedirectAttributes redirectAttributes) throws UnknownHostException {
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			logger.info(" Invoked getLogs");
			useLogDao.saveUserLog("Usage Log");
			return "/logs";
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return "redirect:/";
		}
		
	}
	
	@RequestMapping(value = "/support", method = RequestMethod.GET)
	public String getSupport(final RedirectAttributes redirectAttributes) throws UnknownHostException {
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			logger.info(" Invoked getSupport");
			useLogDao.saveUserLog("Support Tab");
			return "/support";
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return "redirect:/";
		}
		
	}
	
	@RequestMapping(value = "/DataExplorer", method = RequestMethod.GET)
	public String getDataExplorer(final RedirectAttributes redirectAttributes) throws UnknownHostException {
		
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			System.out.println("Home Controller: correct user");
			logger.info(" Invoked DataExplorer");
			useLogDao.saveUserLog("Data Explorer Tab");
			logger.info(" Invoked getDataExplorer");
			return "/dataexplorer";
			
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return "redirect:/";
		}
		
	}

	@RequestMapping(value = "/ExtractionApp", method = RequestMethod.GET) // Upload page
	public String getExtractionapp(final RedirectAttributes redirectAttributes) throws UnknownHostException {
		
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			useLogDao.saveUserLog("Data Upload Tab");
			
			System.out.println("Home Controller: correct user");
			logger.info(" Invoked getExtractionapp");
			return "/extractionapp";
			
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return "redirect:/";
		}
		
	}

	@RequestMapping(value = "/DataAudit", method = RequestMethod.GET)
	public String getDataAudit(final RedirectAttributes redirectAttributes) throws UnknownHostException {
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			System.out.println("Home Controller: correct user");
			logger.info(" Invoked getDataAudit");
			useLogDao.saveUserLog("Data Audit Tab");
			return "/dataaudit";
			
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return "redirect:/";
		}

		
	}

	@RequestMapping(value = "/monthsBar", method = RequestMethod.GET)
	public String getMonthsBar() throws UnknownHostException {
		logger.info(" Invoked getMonthsBar");
		//useLogDao.saveUserLog("newproductlaunch");
		return "/Charts/monthsBar";
	}

	@RequestMapping(value = "/weeksBar", method = RequestMethod.GET)
	public String getWeeksBar() throws UnknownHostException {
		logger.info(" Invoked getWeeksBar");
		//useLogDao.saveUserLog("newproductlaunch");
		return "/Charts/weeksBar";
	}

	// googleLineGraph
	@RequestMapping(value = "/rainfallChart", method = RequestMethod.GET)
	public String getRainfallChart() {
		logger.info(" Invoked getRainfallChart");
		return "/Charts/rainfallChart";
	}

	@RequestMapping(value = "/multiLineChart", method = RequestMethod.GET)
	public String getMultiLineChart() {
		logger.info(" Invoked getMultiLineChart");
		return "/Charts/multiLineChart";
	}

	@RequestMapping(value = "/stockChart", method = RequestMethod.GET)
	public String getStockChart() {
		logger.info(" Invoked getStockChart");
		return "/Charts/stockChart";
	}

	@RequestMapping(value = "/googleLineGraph", method = RequestMethod.GET)
	public String getGoogleLineGraph() {
		logger.info(" Invoked getGoogleLineGraph");
		return "/Charts/googleLineGraph";
	}

	@RequestMapping(value = "/External_Factor", method = RequestMethod.GET)
	public String getExternalFactors() throws UnknownHostException {
		logger.info(" Invoked getExternalFactors");
		useLogDao.saveUserLog("newproductlaunch");
		return "/External_Factor";
	}

	@RequestMapping(value = "/loginCheck", method = RequestMethod.GET)
	public String getLoginCheck(HttpServletRequest Req, ModelMap map) {		
		logger.info(" Invoked getLoginCheck");
		return "/loginCheck";
	}

	@RequestMapping(value = "/loginCheck", method = RequestMethod.POST)
	public String postLoginCheck(HttpServletRequest Req, ModelMap map) {
		logger.info(" Invoked postLoginCheck");
		return "/loginCheck";
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String getIndex( ModelMap map,final RedirectAttributes redirectAttributes) throws UnknownHostException {
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			System.out.println("Home Controller: correct user");
			useLogDao.saveUserLog("Analysis Tab");
			logger.info(" Invoked getIndex");
			return "/index";
			
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return "redirect:/";
		}
		
	}

	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public String postIndex(HttpServletRequest Req, ModelMap map) throws UnknownHostException {
		logger.info(" Invoked postIndex");
		useLogDao.saveUserLog("index");
		return "/index";
	}

	@RequestMapping(value = "/charts", method = RequestMethod.GET)
	public String get_charts(HttpServletRequest Req, ModelMap map) throws UnknownHostException {
		logger.info(" Invoked get_charts");
		//useLogDao.saveUserLog("newproductlaunch");
		return "/charts";
	}

	@RequestMapping(value = "/charts", method = RequestMethod.POST)
	public String post_charts(HttpServletRequest Req, ModelMap map) {
		logger.info(" Invoked post_charts");
		return "/charts";
	}

	@RequestMapping(value = "/predective", method = RequestMethod.GET)
	public String get_predective(HttpServletRequest Req, ModelMap map) throws UnknownHostException {
		logger.info(" Invoked get_predective");
		//useLogDao.saveUserLog("predective");
		return "/predective";
	}

	@RequestMapping(value = "/predective", method = RequestMethod.POST)
	public String post_predective(HttpServletRequest Req, ModelMap map) throws UnknownHostException {
		logger.info(" Invoked post_predective");
		//useLogDao.saveUserLog("predective");
		return "/predective";
	}
	
	@RequestMapping(value="/registerPage")
	public String saveUser() throws UnknownHostException
	{
		logger.info(" Invoked saveUser");
		//useLogDao.saveUserLog("newproductlaunch");
		return "/signup";
	}

	@RequestMapping(value="/loginpage")
	public String getLoginPage(HttpSession httpSession)
	{
		
			System.out.println("Home Controller: notLoggedIN user");
			logger.info(" Invoked getLoginPage");;
			return "/login";
		
		
	}
	
	@RequestMapping(value="/")
	public String getWelcomePage(HttpServletRequest Req, HttpSession httpSession) throws UnknownHostException
	{
		System.out.println("In getWelcomepage() ::::::::::::HomeController");
		logger.info(" Invoked getLoginPage");
		
		
		String ipAddress = Req.getHeader("X-FORWARDED-FOR");
		       if (ipAddress == null) {
		            ipAddress = Req.getRemoteAddr();
		       }
		    
		httpSession.setAttribute("ip", ipAddress);
		
			System.out.println("Home Controller: notLoggedIN user");
			return "/login";
		
		
	}
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView getProfile(final RedirectAttributes redirectAttributes) throws UnknownHostException {
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			logger.info(" Invoked getProfile");
			String username = (String) httpSession.getAttribute("loggedUser1");
			User userInfo=userDao.getUserInfo(username);
			useLogDao.saveUserLog("Profile");
			return new ModelAndView("/profile","userDetails", userInfo);
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return new ModelAndView("redirect:/");
		}
		
	}
	
	@RequestMapping(value = "/scheduler", method = RequestMethod.GET)
	public ModelAndView getScheduler(final RedirectAttributes redirectAttributes) throws UnknownHostException {
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			logger.info(" Invoked getScheduler");
			String username = (String) httpSession.getAttribute("loggedUser1");
			User userInfo=userDao.getUserInfo(username);
			useLogDao.saveUserLog("Scheduler");
			return new ModelAndView("/scheduler","userDetails", userInfo);
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return new ModelAndView("redirect:/");
		}
		
	}
	
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public ModelAndView getAdmin(final RedirectAttributes redirectAttributes) throws UnknownHostException {
		if(httpSession.getAttribute("loggedUser")!=null)
		{
			logger.info(" Invoked getAdmin");
			String username = (String) httpSession.getAttribute("loggedUser1");
			User userInfo=userDao.getUserInfo(username);
			useLogDao.saveUserLog("Admin");
			return new ModelAndView("/admin","userDetails", userInfo);
		}
		else
		{
			System.out.println("Home Controller: notLoggedIN user");
			redirectAttributes.addFlashAttribute("notLoggedIN","Please login first");
			return new ModelAndView("redirect:/");
		}
		
	}
	
	@RequestMapping(value = "/script-page", method = RequestMethod.GET)
	public ModelAndView getScriptPage() throws UnknownHostException {
		
			System.out.println("Home Controller: notLoggedIN user");
			return new ModelAndView("scriptrun");
		
		
	}
	
	@RequestMapping(value = "/error-page", method = RequestMethod.GET)
	public ModelAndView getError() throws UnknownHostException {
		
			System.out.println("Some exception occured. Callong error page");
			return new ModelAndView("error");
		
		
	}
	
	

}
