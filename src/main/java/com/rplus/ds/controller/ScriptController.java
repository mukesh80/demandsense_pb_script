package com.rplus.ds.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.rplus.ds.dao.SQLScriptDao;
import com.rplus.ds.dao.UserDao;
import com.rplus.ds.model.SQLJobGroup;
import com.rplus.ds.model.TaskTrack;
import com.rplus.ds.model.User;
import com.rplus.ds.service.ScriptRunner;
import com.rplus.ds.service.ScriptRunnerService;

/**
 * @author Mukesh
 *
 */

@RestController
@RequestMapping(value="script")
public class ScriptController {
	
	@Autowired
	ScriptRunnerService scriptRunnerService;
	
	@Autowired
	SQLScriptDao sqlScriptDao;
	
	@Autowired
	UserDao userDao;;
	
	@Autowired
	HttpSession httpSession;
	

	public static String inputSchema;
	   
	
	@RequestMapping(value="/runsqlscript",method = RequestMethod.POST)
	public String runSQLScriptFile() { //@RequestParam MultipartFile prodMaster,Model model
		
		if(httpSession.getAttribute("userType").equals("super_admin")){
			
			/*boolean status=scriptRunnerService.saveSQLFileToDirectory(prodMaster);
			if(status)
			{
				model.addAttribute("uploadStatus","Succesfully uploded");
			}*/
		    Connection  conn = scriptRunnerService.getConnection();
		      
		      try {
					ScriptRunner sr = new ScriptRunner(conn, false, false);
					Reader reader = new BufferedReader(
		                            new FileReader("C:\\Office_project\\SqlAutomations\\CreateSchema.sql"));

					sr.runScript(reader);

				 } catch (Exception e) {
					 e.printStackTrace();
					System.err.println("Failed to Execute C:\\Office_project\\SqlAutomations\\CreateSchema.sql The error is " + e.getMessage());
				}
		return "index";
		}
		else{
			System.out.println("Sorry ! you dont have right to execute file");
			return null;
		}
		
	   
	}
	
	@RequestMapping(value="/tablecleaner",method = RequestMethod.POST)
	public String sqlTableCleaner(@RequestParam MultipartFile prodMaster,Model model) {
		System.out.println("ScriptController : sqlTableCleaner() ");
		if(httpSession.getAttribute("userType").equals("super_admin")){
			
			System.out.println("Run script controller");
			boolean status=scriptRunnerService.saveSQLFileToDirectory(prodMaster);
			if(status)
			{
				model.addAttribute("uploadStatus","Succesfully uploded");
			}
		    Connection  conn = scriptRunnerService.getConnection();
		      
		      try {
					ScriptRunner sr = new ScriptRunner(conn, false, false);
					Reader reader = new BufferedReader(
		                            new FileReader("C:\\Office_project\\SqlAutomations\\CreateSchema.sql"));

					//sr.runScript(reader);

				 } catch (Exception e) {
					System.err.println("Failed to Execute C:\\Office_project\\SqlAutomations\\CreateSchema.sql The error is " + e.getMessage());
				}
		return "/index";
		}
		else{
			System.out.println("Sorry ! you dont have right to execute file");
			model.addAttribute("permission_message", 
					"Sorry ! you dont have right to execute file");
			return null;
		}
		
	   
	}
	
	@RequestMapping(value="/datatransform",method = RequestMethod.POST)
	public String dataTransform(@RequestParam MultipartFile prodMaster,Model model) {
		System.out.println("ScriptController : dataTransform() ");
		if(httpSession.getAttribute("userType").equals("super_admin")){
			
			System.out.println("Run script controller");
			boolean status=scriptRunnerService.saveSQLFileToDirectory(prodMaster);
			if(status)
			{
				model.addAttribute("uploadStatus","Succesfully uploded");
			}
		    Connection  conn = scriptRunnerService.getConnection();
		      
		      try {
					ScriptRunner sr = new ScriptRunner(conn, false, false);
					Reader reader = new BufferedReader(
		                            new FileReader("C:\\Office_project\\SqlAutomations\\CreateSchema.sql"));

					//sr.runScript(reader);

				 } catch (Exception e) {
					System.err.println("Failed to Execute C:\\Office_project\\SqlAutomations\\CreateSchema.sql The error is " + e.getMessage());
				}
		return "/index";
		}
		else{
			System.out.println("Sorry ! you dont have right to execute file");
			model.addAttribute("permission_message", 
					"Sorry ! you dont have right to execute file");
			return null;
		}
		
	   
	}
	
	
	@RequestMapping(value="/dataload",method = RequestMethod.POST)
	public String dataLoading(@RequestParam MultipartFile prodMaster,Model model) {
		System.out.println("ScriptController : dataLoading() ");
		if(httpSession.getAttribute("userType").equals("super_admin")){
			
			System.out.println("Run script controller");
			boolean status=scriptRunnerService.saveSQLFileToDirectory(prodMaster);
			if(status)
			{
				model.addAttribute("uploadStatus","Succesfully uploded");
			}
		    Connection  conn = scriptRunnerService.getConnection();
		      
		      try {
					ScriptRunner sr = new ScriptRunner(conn, false, false);
					Reader reader = new BufferedReader(
		                            new FileReader("C:\\Office_project\\SqlAutomations\\CreateSchema.sql"));

					//sr.runScript(reader);

				 } catch (Exception e) {
					System.err.println("Failed to Execute C:\\Office_project\\SqlAutomations\\CreateSchema.sql The error is " + e.getMessage());
				}
		return "/index";
		}
		else{
			System.out.println("Sorry ! you dont have right to execute file");
			model.addAttribute("permission_message", 
					"Sorry ! you dont have right to execute file");
			return null;
		}
		
	   
	}
	
	@RequestMapping(value="/runrscript",method = RequestMethod.POST)
	public String runRScript(@RequestParam MultipartFile prodMaster,Model model) {
		System.out.println("ScriptController : runRScript() ");
		if(httpSession.getAttribute("userType").equals("super_admin")){
			
			System.out.println("Run script controller");
			boolean status=scriptRunnerService.saveSQLFileToDirectory(prodMaster);
			if(status)
			{
				model.addAttribute("uploadStatus","Succesfully uploded");
			}
		    Connection  conn = scriptRunnerService.getConnection();
		      
		      try {
					ScriptRunner sr = new ScriptRunner(conn, false, false);
					Reader reader = new BufferedReader(
		                            new FileReader("C:\\Office_project\\SqlAutomations\\CreateSchema.sql"));

					//sr.runScript(reader);

				 } catch (Exception e) {
					System.err.println("Failed to Execute C:\\Office_project\\SqlAutomations\\CreateSchema.sql The error is " + e.getMessage());
				}
		return "/index";
		}
		else{
			System.out.println("Sorry ! you dont have right to execute file");
			model.addAttribute("permission_message", 
					"Sorry ! you dont have right to execute file");
			return null;
		}
		
	   
	}

	
	@RequestMapping(value="/runScripts",method = RequestMethod.POST)
	public String getRunScript(@RequestParam String userSchema,String jobGroup, String schedule) {
		System.out.println("ScriptController : getRunScript() ");
		if(httpSession.getAttribute("userType").equals("super_admin")){
			
			System.out.println("Run script controller");
			inputSchema = userSchema;
		    Connection  conn = scriptRunnerService.getConnection();
		    String strArray[] = jobGroup.split(",");
			for(int i = 0; i < strArray.length;i++){
				String str = strArray[i].toString().replaceAll("\\b\\\b", "\\");
				System.out.println(str);
				try {
					ScriptRunner sr = new ScriptRunner(conn, false, false);
					
					Reader reader = new BufferedReader(
		                            new FileReader("C:\\Office_project"+str));

					sr.runScript(reader);
				
		      } catch (Exception e) {
					System.err.println("Failed to Execute C:\\Office_project"+str+" The error is " + e.getMessage());
		      }
			}
		      
		return "sucess";
		}
		else{
			System.out.println("Sorry ! you dont have right to execute file");
			
			return "fail";
		}
		
	   
	}
	
	
	/**
	 * This method is used to get the data from SQLJobGroup object  
	 * @param null
	 * @return List<SQLJobGroup>
	 * @url http://localhost:8080/DemandSense/script/sqljobgroup 
	 */
	
	@RequestMapping(value="sqljobgroup", method=RequestMethod.GET)
	public List<SQLJobGroup> getSQLJobGroupData(){
		
		List<SQLJobGroup> sqljobgroup= sqlScriptDao.getSQLGroupdata();
		
		return sqljobgroup;
	}
	
	/**
	 * This method is used to get the data from User object  
	 * @param null
	 * @return List<User>
	 * @url http://localhost:8080/DemandSense/script/usersList 
	 */
	
	@RequestMapping(value="usersList", method=RequestMethod.GET)
	public List<User> getUserList(){
		
		List<User> User= sqlScriptDao.getUsers();
		
		return User;
	}
	
	/**
	 * This method is used to save  TaskTrack data into database
	 * @param null
	 * @return String
	 * @url http://localhost:8080/DemandSense/script/saveorupdatetasktrack 
	 */
	
	@RequestMapping(value="saveorupdatetasktrack", method=RequestMethod.POST)
	public String saveOrUpdateTaskTrack(@ModelAttribute TaskTrack tasktrack
			,BindingResult bindingResults){
		String savedtasktracker=null;
		if(bindingResults.hasErrors())
		{
			int noOfError=bindingResults.getErrorCount();
			FieldError filedError=bindingResults.getFieldError();
			System.out.println("No of errors :"+noOfError
					+" "+filedError.getField());
			return null;
			 
		}
		
		boolean status=sqlScriptDao.saveTaskTrack(tasktrack);
			
			if(status){
				 savedtasktracker = "Saved successfully ";
			}
			else{
				savedtasktracker = "Not Saved successfully ";
			}
				
			
			return savedtasktracker;
		
	}
	
	/**
	 * This method is used to save  user data
	 * @param User object
	 * @return String
	 * @url http://localhost:8080/DemandSense/script/saveuserdata 
	 */
	@RequestMapping(value="saveuserdata", method=RequestMethod.POST)
	public String saveUserdata(@ModelAttribute User user){
		String status = "fail";
		System.out.println(user.getEmailId());
		if(httpSession.getAttribute("userType").equals("super_admin")){
			
			user.setSchemaName("rplus_ds_"+user.getSchemaName());
			userDao.save(user);
			//httpSession.setAttribute("InputSchema", user.getSchemaName());
			 inputSchema=user.getSchemaName();
			runSQLScriptFile();
		}else{
			status = "Login as Admin";
		}
		
		return status;
	}
	
	/**
	 * This method is for testing purpose
	 * @param null
	 * @return String
	 * @url http://localhost:8080/DemandSense/script/responsebodytest 
	 */
	
	@RequestMapping(value="responsebodytest", method=RequestMethod.POST)
	public String testing(){
		
		return "hello";
	}

}
