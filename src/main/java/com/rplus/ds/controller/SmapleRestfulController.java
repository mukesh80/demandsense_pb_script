package com.rplus.ds.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Mallikarjuna
 *
 */
@RestController
@RequestMapping("/sampleRestful")
public class SmapleRestfulController {
	private static final Logger logger = Logger.getLogger(SmapleRestfulController.class);
	
	@RequestMapping("/sample")
    public String sampleJson() {
		logger.info(" SmapleRestfulController: sampleJson() ");
		return "Demand Sense";
    }

}
