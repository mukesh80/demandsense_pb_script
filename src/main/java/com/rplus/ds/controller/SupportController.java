package com.rplus.ds.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.service.MailService;


@RestController
@RequestMapping("/support")
public class SupportController {
	
	@Autowired
	MailService mailService;
	
	private static final Logger logger = Logger.getLogger(LocationController.class);
	
	
	@RequestMapping(value = "mail", method = RequestMethod.GET)
	public @ResponseBody String getMail(@RequestParam String name, String mobile, String subject, String body) {
		logger.info("Invoke getMail() START ");
		
		mailService.mailSupport(name, mobile, subject, body);
		
		return "sucess";
    	
	}

}
