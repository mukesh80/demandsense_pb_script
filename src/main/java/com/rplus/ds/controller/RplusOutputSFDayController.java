package com.rplus.ds.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.PredictiveDAOImpl;
import com.rplus.ds.model.RplusDsOutputSFDay;


/**
 * @author Rafeeq
 *
 */
@RestController
@RequestMapping("/outputSFdata")
public class RplusOutputSFDayController {	
	private static final Logger logger = Logger.getLogger(RplusDsOutputFDayController.class);
/*
	@Autowired
	RplusDsOutputSFDayDao outputSFDayDao;*/

	@Autowired
    PredictiveDAOImpl predictiveDAOImpl;
		
	@RequestMapping(value = "getSFlist", method = RequestMethod.GET)
	 public List<RplusDsOutputSFDay> getOutputSFData(HttpServletRequest request,@RequestParam Map<String,String> allParams) // 
	    {   logger.info(" RplusOutputSFDayController: getOutputSFData START");
	    
	    HttpSession httpSession=request.getSession(); 
	 		httpSession.setAttribute("forcastData", allParams);
	 	    	
	    
	        List<RplusDsOutputSFDay> outputSFList = predictiveDAOImpl.getRplusOutputSFdata(allParams);
	        logger.info(" RplusOutputSFDayController: getOutputSFData END");
	        return outputSFList;
	    }

}
