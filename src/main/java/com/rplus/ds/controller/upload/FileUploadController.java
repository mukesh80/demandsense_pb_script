package com.rplus.ds.controller.upload;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataFormatter;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rplus.ds.dao.upload.FileUploadDAO;
import com.rplus.ds.model.upload.DimCustomer;
import com.rplus.ds.model.upload.DimLocation;
import com.rplus.ds.model.upload.DimProduct;
import com.rplus.ds.model.upload.DimSocial;
import com.rplus.ds.model.upload.DimTime;
import com.rplus.ds.model.upload.FactEvent;
import com.rplus.ds.model.upload.FactExternal;
import com.rplus.ds.model.upload.FactInventory;
import com.rplus.ds.model.upload.FactPrice;
import com.rplus.ds.model.upload.FactPromotion;
import com.rplus.ds.model.upload.FactSocial;
import com.rplus.ds.model.upload.InvoiceSales;
import com.rplus.ds.service.MailService;

@RestController
@RequestMapping("/fileUpload")
public class FileUploadController {
	
	private static final Logger logger = Logger.getLogger(FileUploadController.class);
	@Autowired
    private FileUploadDAO fileUploadDAO;
	private static Integer totalRowCount = 0;
	private static Integer currentRowNumber = 0;
	private static boolean uploadStatus;
	
	@Autowired
	MailService mailService;
	
	// method to upload product master file
	@RequestMapping(value = "/loadProductMasterFileToDB", method = RequestMethod.POST)
	    public String loadProductFileToDB(HttpServletRequest request,@RequestParam MultipartFile prodMaster) throws Exception {
		 System.out.println("Product master file is started  uploading in FileUPloadController java page ::::::::::::::::::::::");
		 logger.info("FileUploadController : loadFileToDB START");
		 
		 	mailService.saveFileToDirectory(prodMaster);
	    	try{
	    		
	            InputStream input = prodMaster.getInputStream();  
	                POIFSFileSystem fs = new POIFSFileSystem( input );  
	                HSSFWorkbook wb = new HSSFWorkbook(fs);  
	                HSSFSheet sheet = wb.getSheetAt(0);  
	                HSSFRow row;  
	                
	                totalRowCount = sheet.getLastRowNum();

	                for(int i=1; i<=sheet.getLastRowNum(); i++)
	                {  
	                	currentRowNumber = i;
	                	logger.info("currentRowNumber :"+currentRowNumber);
	                	
	                    DimProduct product=new DimProduct();
	                    row = sheet.getRow(i);  
	                    product.setProductId(row.getCell(0)!=null?String.valueOf(row.getCell(0).getStringCellValue()):null);
	                    product.setProductIdDesc(row.getCell(1)!=null?String.valueOf(row.getCell(1).getRichStringCellValue()):null);
	                    product.setCategoryId(row.getCell(2)!=null?String.valueOf(row.getCell(2).getStringCellValue()):null);
	                    product.setCategoryIdDesc(row.getCell(3)!=null?String.valueOf(row.getCell(3).getRichStringCellValue()):null);
	                    product.setSubcategoryId(row.getCell(4)!=null?String.valueOf(row.getCell(4).getStringCellValue()):null);
	                    product.setSubcategoryIdDescr(row.getCell(5)!=null?String.valueOf(row.getCell(5).getRichStringCellValue()):null);
	                    product.setProductType(row.getCell(6)!=null?String.valueOf(row.getCell(6).getStringCellValue()):null);
	                    product.setProductTypeDesc(row.getCell(7)!=null?String.valueOf(row.getCell(7).getRichStringCellValue()):null);
	                    product.setNoType(row.getCell(8)!=null?String.valueOf(row.getCell(8).getStringCellValue()):null);
	                    product.setRefProd(row.getCell(9)!=null?String.valueOf(row.getCell(9).getRichStringCellValue()):null);
	                    product.setClusterId(row.getCell(10)!=null?String.valueOf(row.getCell(10).getRichStringCellValue()):null);
	                    product.setProdAttr1(row.getCell(11)!=null?String.valueOf(row.getCell(11).getRichStringCellValue()):null);
	                    product.setProdAttr2(row.getCell(12)!=null?String.valueOf(row.getCell(12).getRichStringCellValue()):null);
	                    product.setProdAttr3(row.getCell(13)!=null?String.valueOf(row.getCell(13).getRichStringCellValue()):null);
	                    product.setProdAttr4(row.getCell(14)!=null?String.valueOf(row.getCell(14).getRichStringCellValue()):null);
	                    product.setProdAttr5(row.getCell(15)!=null?String.valueOf(row.getCell(15).getRichStringCellValue()):null);
	                    product.setProductStartDate(row.getCell(15)!=null?row.getCell(16).getDateCellValue():null);
	                    fileUploadDAO.save(product);
	                    
	                }
	                request.getSession().removeAttribute("uploadError");
	                uploadStatus=true;
	                
	    } catch (Exception ec) {
	    	uploadStatus=false;
	    	System.out.println("Promotion file exception1");
	    	request.getSession().setAttribute("uploadError","Exception");
	    	System.out.println("Promotion file exception2");
	        ec.printStackTrace();
	    }
	    	
	    	logger.info("FileUploadController : loadFileToDB success");
	        return "success";
	    }  

	// method to upload Location master file
	 @SuppressWarnings("static-access")
	@RequestMapping(value = "/loadLocationMasterFileToDB", method = RequestMethod.POST)
	    public String loadLocationFileToDB(HttpServletRequest request,@RequestParam MultipartFile locationMaster) throws Exception { 
		 logger.info("FileUploadController : loadLocationFileToDB START");
		 System.out.println("Location master file is started  uploading in FileUPloadController java page ::::::::::::::::::::::");
	    
		 mailService.saveFileToDirectory(locationMaster);
		 try{
	    		InputStream input = locationMaster.getInputStream();  
	                POIFSFileSystem fs = new POIFSFileSystem( input );  
	                HSSFWorkbook wb = new HSSFWorkbook(fs);  
	                HSSFSheet sheet = wb.getSheetAt(0);  
	                HSSFRow row;  
	                
	                totalRowCount = sheet.getLastRowNum();

	                for(int i=1; i<=sheet.getLastRowNum(); i++)
	                {  
	                	currentRowNumber = i;
	                	
	                	DimLocation location=new DimLocation();
	                    row = sheet.getRow(i);

	                    location.setLocationId(row.getCell(0)!=null?String.valueOf((long)row.getCell(0).getNumericCellValue()):null);
	                    location.setLocationIdDesc(row.getCell(1)!=null?String.valueOf(row.getCell(1).getRichStringCellValue()):null);
	                    location.setLocationPostCode(row.getCell(2)!=null?String.valueOf(row.getCell(2).getNumericCellValue()):null);;
	                    location.setStateId(row.getCell(2)!=null?String.valueOf(row.getCell(3).getRichStringCellValue()):null);
	                    location.setStateIdDesc(row.getCell(4)!=null?String.valueOf(row.getCell(4).getRichStringCellValue()):null);
	                    location.setRegionId(row.getCell(5)!=null?String.valueOf(row.getCell(5).getRichStringCellValue()):null);
	                    location.setRegionIdDesc(row.getCell(6)!=null?String.valueOf(row.getCell(6).getRichStringCellValue()):null);
	                    location.setDistrictId(row.getCell(7)!=null?String.valueOf(row.getCell(7).getRichStringCellValue()):null);
	                    location.setDistrictIdDesc(row.getCell(8)!=null?String.valueOf(row.getCell(8).getRichStringCellValue()):null);
	                    location.setCityId(String.valueOf((row.getCell(9).CELL_TYPE_NUMERIC==0 ? row.getCell(9).getNumericCellValue():row.getCell(9).getStringCellValue())));
	                    location.setCityIdDesc(row.getCell(10)!=null?String.valueOf(row.getCell(10).getRichStringCellValue()):null);
	                    location.setCountryId(row.getCell(11)!=null?String.valueOf(row.getCell(11).getRichStringCellValue()):null);
	                    location.setCountryDesc(row.getCell(12)!=null?String.valueOf(row.getCell(12).getRichStringCellValue()):null);
	                    location.setLocationArea(row.getCell(13)!=null?Integer.valueOf((int) row.getCell(13).getNumericCellValue()):null);
	                    location.setAreaUnit(row.getCell(14)!=null?String.valueOf(row.getCell(14).getRichStringCellValue()):null);
	                    location.setLocationCategory(row.getCell(15)!=null?String.valueOf(row.getCell(15).getRichStringCellValue()):null);
	                    location.setLocationAmbience(row.getCell(16)!=null?String.valueOf(row.getCell(16).getRichStringCellValue()):null);
	                    location.setLocationType(row.getCell(16)!=null?row.getCell(17).getStringCellValue():null);
	                    location.setLongtitude(row.getCell(18)!=null?String.valueOf(row.getCell(18).getNumericCellValue()):null);
	                    location.setLatitude(row.getCell(19)!=null?String.valueOf(row.getCell(18).getNumericCellValue()):null);
	                    location.setTableCount(row.getCell(20)!=null?Integer.valueOf((int) row.getCell(18).getNumericCellValue()):null);
	                    location.setPeopleMax(row.getCell(21)!=null?Integer.valueOf((int) row.getCell(19).getNumericCellValue()):null);
	                    location.setCustomField1(row.getCell(21)!=null?row.getCell(22).getStringCellValue():null);
	                    location.setCustomField2(row.getCell(21)!=null?String.valueOf(row.getCell(23).getStringCellValue()):null);
	                    location.setStartDate(row.getCell(24)!=null?row.getCell(24).getDateCellValue():null);
	                    location.setCustomMeasure1(row.getCell(25)!=null?Integer.valueOf((int) row.getCell(25).getNumericCellValue()):null);
	                    location.setCustomMeasure2(row.getCell(26)!=null?Integer.valueOf((int) row.getCell(26).getNumericCellValue()):null);
	                    
	                    fileUploadDAO.save(location);
	                }  
	                uploadStatus=true;
	                request.getSession().removeAttribute("uploadError");
	    } catch (Exception ec) {
	    	uploadStatus=false;
	    	System.out.println("Location file exception1");
	    	request.getSession().setAttribute("uploadError","Exception");
	    	System.out.println("Location file exception2");
	        ec.printStackTrace();
	    } 
	    	System.out.println("Location file is uploaded Successfully  ::::::::::::::::::::::");
	    	logger.info("Location master file uploaded successfully::::::::::::::::::::::::::::::::");
	        return "success";
	    }  

	
	 
	// method to upload customer master file
	 @RequestMapping(value = "/loadCustomerMasterFileToDB", method = RequestMethod.POST)
	 	public String loadCustomerFileToDB(HttpServletRequest request,@RequestParam MultipartFile customerMaster) throws Exception 
	 	{
		 logger.info("FileUploadController : loadCustomerFileToDB START");
		 System.out.println("Uploading customer master data::::::::::::::::::::::::::::");
	 		
		 try{
	    		InputStream input = customerMaster.getInputStream();  
	                POIFSFileSystem fs = new POIFSFileSystem( input );  
	                HSSFWorkbook wb = new HSSFWorkbook(fs);  
	                HSSFSheet sheet = wb.getSheetAt(0);  
	                HSSFRow row;  

	                totalRowCount = sheet.getLastRowNum();
	                
	                for(int i=1; i<=sheet.getLastRowNum(); i++)
	                {  
	                	currentRowNumber = i;
	                	
	                	DimCustomer customer=new DimCustomer();
	                    row = sheet.getRow(i);
	                    
	                    customer.setCustomeId(row.getCell(0)!=null?String.valueOf(row.getCell(0).getRichStringCellValue()):null);
	                    customer.setCustomerIdDesc(row.getCell(1)!=null?String.valueOf(row.getCell(1).getRichStringCellValue()):null);
	                    customer.setCustomerIdDesc(row.getCell(2)!=null?String.valueOf(row.getCell(2).getRichStringCellValue()):null);
	                    customer.setCustomerIdDesc(row.getCell(3)!=null?String.valueOf(row.getCell(3).getRichStringCellValue()):null);
	                    customer.setCustomerIdDesc(row.getCell(4)!=null?String.valueOf(row.getCell(4).getRichStringCellValue()):null);
	                    //customer.setCustomerIdDesc(String.valueOf(row.getCell(1).getRichStringCellValue()));
	                    customer.setDob(row.getCell(5)!=null?row.getCell(5).getDateCellValue():null);//date
	                    //customer.setAge(Integer.valueOf((int) row.getCell(6).getNumericCellValue()));
	                    //factSocial.setSoicialId(row.getCell(0)!=null?String.valueOf(row.getCell(0).getNumericCellValue()):null);
	                    //factSocial.setPositiveSentiment(row.getCell(2)!=null?Integer.valueOf((int) row.getCell(2).getNumericCellValue()):null);
	                    customer.setAge(row.getCell(6)!=null?Integer.valueOf((int) row.getCell(6).getNumericCellValue()):null);
	                    customer.setContactNo(row.getCell(7)!=null?Integer.valueOf((int) row.getCell(7).getNumericCellValue()):null);
	                    customer.setCustomeId(row.getCell(8)!=null?String.valueOf(row.getCell(8).getRichStringCellValue()):null);
	                    customer.setFavPd1(row.getCell(9)!=null?String.valueOf(row.getCell(9).getRichStringCellValue()):null);
	                    customer.setFavPd2(row.getCell(10)!=null?String.valueOf(row.getCell(10).getRichStringCellValue()):null);
	                    customer.setCustChar1(row.getCell(11)!=null?String.valueOf(row.getCell(11).getRichStringCellValue()):null);
	                    customer.setCustChar2(row.getCell(12)!=null?String.valueOf(row.getCell(12).getRichStringCellValue()):null);
	                    customer.setCustChar3(row.getCell(13)!=null?String.valueOf(row.getCell(13).getRichStringCellValue()):null);
	                    customer.setCustKf1(row.getCell(14)!=null?Integer.valueOf((int) row.getCell(14).getNumericCellValue()):null);
	                    customer.setCustKf2(row.getCell(15)!=null?Integer.valueOf((int) row.getCell(15).getNumericCellValue()):null);
	                    customer.setCustKf3(row.getCell(16)!=null?Integer.valueOf((int) row.getCell(16).getNumericCellValue()):null);
	                    fileUploadDAO.save(customer);
	                }  
	                request.getSession().removeAttribute("uploadError");
	    } catch (Exception ec) {
	    	System.out.println("Customer file exception1");
	    	request.getSession().setAttribute("uploadError","Exception");
	    	System.out.println("Customer file exception2");
	        ec.printStackTrace();
	    } 
	  
	    	System.out.println("Customer master file is uploaded Successfully:::::::::::::::::::::::");
	    	logger.info("Customer file uploaded successfully:::::");
	        return "success";
	    }  

	// method to upload Social master file
	 @RequestMapping(value = "/loadSocialMasterFileToDB", method = RequestMethod.POST)

	    public String loadSocialMasterFileToDB(HttpServletRequest request,@RequestParam MultipartFile socialMaster) throws Exception { 
	 		logger.info("FileUploadController : loadSocialMasterFileToDB START");
	 		System.out.println("SocialMaster data  started uploading::::::::::::::::::::::");
	    	try{
	    			InputStream input = socialMaster.getInputStream();  
	                POIFSFileSystem fs = new POIFSFileSystem( input );  
	                HSSFWorkbook wb = new HSSFWorkbook(fs);  
	                HSSFSheet sheet = wb.getSheetAt(0);  
	                HSSFRow row;  
	                
	                totalRowCount = sheet.getLastRowNum();

	                for(int i=1; i<=sheet.getLastRowNum(); i++)
	                {  
	                	
	                	 currentRowNumber = i;
	                	
	                	DimSocial dimSocial=new DimSocial();
	                    row = sheet.getRow(i); 
	                    dimSocial.setSocial_mediatype(row.getCell(0)!=null?String.valueOf(row.getCell(0).getRichStringCellValue()):null);
	                    dimSocial.setSocial_topic(row.getCell(1)!=null?String.valueOf(row.getCell(1).getRichStringCellValue()):null);
	                    dimSocial.setSocial_product(row.getCell(2)!=null?String.valueOf(row.getCell(2).getRichStringCellValue()):null);
	                    dimSocial.setSocial_location(row.getCell(3)!=null?String.valueOf(row.getCell(3).getRichStringCellValue()):null);
	                    dimSocial.setSocial_id(row.getCell(4)!=null?String.valueOf(row.getCell(4).getRichStringCellValue()):null);
	                    //dimSocial.setSocial_id(String.valueOf(row.getCell(4).getRichStringCellValue()));
	                   
	                   fileUploadDAO.save(dimSocial);
	                }  
	                request.getSession().removeAttribute("uploadError");
	    } catch (Exception ec) {
	    	System.out.println("DimLocation file exception1");
	    	request.getSession().setAttribute("uploadError","Exception");
	    	System.out.println("DimLocation file exception2");
	        ec.printStackTrace();
	    } 
	  
	    	logger.info("dimsocial file uploaded successfully:::::::::");
	    	System.out.println("dimsocial(social master) file uploaded successfully:::::::::::::::::");
	        return "success";
	    }
	
 
	
	                /*
	                	
	                	FactSocial factSocial=new FactSocial();
	                    row = sheet.getRow(i);  
	                    factSocial.setSoicialId(row.getCell(0)!=null?String.valueOf(row.getCell(0).getNumericCellValue()):null);
	                    factSocial.setCalDay(row.getCell(1)!=null?row.getCell(1).getDateCellValue():null);
	                    factSocial.setPositiveSentiment(row.getCell(2)!=null?Integer.valueOf((int) row.getCell(2).getNumericCellValue()):null);
	                    factSocial.setNegativeSentiment(row.getCell(3)!=null?Integer.valueOf((int) row.getCell(3).getNumericCellValue()):null);
	                    factSocial.setNeutralSentiment(row.getCell(4)!=null?Integer.valueOf((int) row.getCell(4).getNumericCellValue()):null);
	                    factSocial.setInfentualScore(row.getCell(5)!=null?row.getCell(5).getNumericCellValue():null);
	                    factSocial.setSentimentalIndex(row.getCell(6)!=null?row.getCell(6).getNumericCellValue():null);
	                    factSocial.setSrs(row.getCell(7)!=null?row.getCell(7).getNumericCellValue():null);
	                    fileUploadDAO.save(factSocial);
	                }  
	    } catch (FileNotFoundException ec) {
	        ec.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	  
	    	logger.info("Factsocial file uploaded successfully:::::::::");
	    	System.out.println("Factsocial(social master) file uploaded successfully:::::::::");
	        return "success";
	    }
	 
	*/ 
	// method to upload time master file
	 
	@RequestMapping(value = "/loadTimeMasterFileToDB", method = RequestMethod.POST)
	 @ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	    public ModelAndView loadTimeFileToDB(HttpServletRequest request,
	    		@RequestParam MultipartFile timeMaster,RedirectAttributes redirectAttributes) throws Exception { 
		 logger.info("FileUploadController : loadTimeFileToDB START");
		 System.out.println("Time  master data is started uploading:::::::::::::::::::::::::::::");
		  
	    	try{
	    		InputStream input = timeMaster.getInputStream();  
	                POIFSFileSystem fs = new POIFSFileSystem( input );  
	                HSSFWorkbook wb = new HSSFWorkbook(fs);  
	                HSSFSheet sheet = wb.getSheetAt(0);  
	                HSSFRow row;  

	                totalRowCount = sheet.getLastRowNum();
	                
	                for(int i=1; i<=sheet.getLastRowNum(); i++)
	                {  
	                	 currentRowNumber = i;
	                	
	                	DimTime time=new DimTime();
	                    row = sheet.getRow(i);  
	                    time.setCalDay(row.getCell(0).getDateCellValue());
	                    time.setCalWeek(Integer.valueOf((int) row.getCell(1).getNumericCellValue()));
	                    time.setCalMonth(Integer.valueOf((int) row.getCell(2).getNumericCellValue()));
	                    time.setCalMonthDesc(String.valueOf(row.getCell(3).getRichStringCellValue()));
	                    time.setCalQuarter(Integer.valueOf((int) row.getCell(4).getNumericCellValue()));
	                    time.setCalHalfYear(Integer.valueOf((int) row.getCell(5).getNumericCellValue()));
	                    time.setCalYear(Integer.valueOf((int) row.getCell(6).getNumericCellValue()));
	                    time.setFiscalVar(String.valueOf(row.getCell(7).getRichStringCellValue()));
	                    time.setFisDay(row.getCell(8).getDateCellValue());
	                    time.setFiscalWeek(Integer.valueOf((int) row.getCell(9).getNumericCellValue()));
	                    time.setFiscalMonth(Integer.valueOf((int) row.getCell(10).getNumericCellValue()));
	                    time.setFiscalMonthDesc(String.valueOf(row.getCell(11).getRichStringCellValue()));
	                    time.setFiscalQuarter(Integer.valueOf((int) row.getCell(12).getNumericCellValue()));
	                    time.setCalHalfYear(Integer.valueOf((int) row.getCell(13).getNumericCellValue()));
	                    time.setFiscalYear(Integer.valueOf((int) row.getCell(14).getNumericCellValue()));
	                    time.setYearWeek(Integer.valueOf((int) row.getCell(15).getNumericCellValue()));
	                    time.setYearMonth(row.getCell(16)!=null?Integer.valueOf((int) row.getCell(16).getNumericCellValue()):null);
	                    
	                    	 fileUploadDAO.save(time);
	                    
	                   
	                }  
	                request.getSession().removeAttribute("uploadError");
	    } catch (Exception ec) {
	    	System.out.println("Time file exception1");
	    	request.getSession().setAttribute("uploadError","Exception");
	    	System.out.println("Time file exception2");
	    	 //redirectAttributes.addFlashAttribute("uploadError", "Some error occured while uploading");
	    
	        
	    } 
	    	logger.info("Time file uploaded successfully:::::");
	    	System.out.println("Time file uploaded successfully:::::::::::::::::::::::");
	        return new ModelAndView("success");
	    }  


	// method to upload Event data file
		 @RequestMapping(value = "/loadEventDataFileToDB", method = RequestMethod.POST)
		 	public String loadEventDataFileToDB(HttpServletRequest request,@RequestParam MultipartFile eventData) throws Exception 
		 	{
			 logger.info("FileUploadController : loadEventFileToDB START");
			 System.out.println("Event Data file is started uploading:::::::::::::::::::::::::::::");
		    	try{
		    		InputStream input = eventData.getInputStream();  
		                POIFSFileSystem fs = new POIFSFileSystem( input );  
		                HSSFWorkbook wb = new HSSFWorkbook(fs);  
		                HSSFSheet sheet = wb.getSheetAt(0);  
		                HSSFRow row;  
		                
		                totalRowCount = sheet.getLastRowNum();

		                for(int i=1; i<=sheet.getLastRowNum(); i++)
		                {  
		                	 currentRowNumber = i;
		                	
		                	FactEvent event=new FactEvent();
		                    row = sheet.getRow(i);  
		                    event.setCal_day(row.getCell(0).getDateCellValue());
		                    //factExternal.setLongitude(row.getCell(0)!=null?row.getCell(0).getNumericCellValue():null);//double
		                    event.setLongitude(row.getCell(1)!=null?row.getCell(1).getNumericCellValue():null);//double
		                    event.setLatitude(row.getCell(2)!=null?row.getCell(2).getNumericCellValue():null);
		                    //customer.setCustomeId(row.getCell(8)!=null?String.valueOf(row.getCell(8).getRichStringCellValue()):null);
		                    event.setPostcode(row.getCell(3)!=null? String.valueOf(row.getCell(3).getRichStringCellValue()):null);
		                    event.setEvent_type(row.getCell(4)!=null?String.valueOf(row.getCell(4).getRichStringCellValue()):null);
		                    event.setLocation_id(row.getCell(5)!=null?String.valueOf(row.getCell(5).getRichStringCellValue()):null);
		                    event.setEvent_indicator(row.createCell(6)!=null?Boolean.valueOf(row.getCell(6).getBooleanCellValue()):null);
		                    event.setHoliday_indicator(row.createCell(7)!=null?Boolean.valueOf(row.getCell(7).getBooleanCellValue()):null);
		                    event.setSpecial_indicator(row.createCell(8)!=null?Boolean.valueOf(row.getCell(8).getBooleanCellValue()):null);
		                    event.setDay_type(row.getCell(9)!=null?String.valueOf(row.getCell(9).getRichStringCellValue()):null);
		                    event.setCal_day_text(row.getCell(10)!=null?String.valueOf(row.getCell(10).getRichStringCellValue()):null);
		                    event.setCal_day_id(row.getCell(11)!=null?Integer.valueOf((int) row.getCell(11).getNumericCellValue()):null);
		                    event.setWeek_day(row.getCell(12)!=null?Integer.valueOf((int) row.getCell(12).getNumericCellValue()):null);//ternary operator
		     
		                    fileUploadDAO.save(event);
		                }  
		                request.getSession().removeAttribute("uploadError");
		    } catch (Exception ec) {
		    	System.out.println("Event file exception1");
		    	request.getSession().setAttribute("uploadError","Exception");
		    	System.out.println("Event file exception2");
		        ec.printStackTrace();
		    } 
		  
		    	logger.info("Event Data file uploaded successfully:::::");
		    	System.out.println("Event Data file uploaded successfully:::::::::::::::::::::::");
		        return "success";
		    }  
		 
		// method to Inventory Event data file
				 @RequestMapping(value = "/loadInventoryDataFileToDB", method = RequestMethod.POST)
				 	public String loadInventoryDataFileToDB(HttpServletRequest request,@RequestParam MultipartFile inventoryData) throws Exception 
				 	{
						 logger.info("FileUploadController : loadInventoryDataFileToDB START");
						 System.out.println("Inventory Data file is started uploading:::::::::::::::::::::::::::::");
					    	try{
					    		InputStream input = inventoryData.getInputStream();  
					                POIFSFileSystem fs = new POIFSFileSystem( input );  
					                HSSFWorkbook wb = new HSSFWorkbook(fs);  
					                HSSFSheet sheet = wb.getSheetAt(0);  
					                HSSFRow row;  
					                
					                totalRowCount = sheet.getLastRowNum();

					                for(int i=1; i<=sheet.getLastRowNum(); i++)
					                {  
					                	 currentRowNumber = i;
					                	
					                	FactInventory inventory=new FactInventory();
					                    row = sheet.getRow(i); 
					                    inventory.setProduct_id(row.getCell(0)!=null?String.valueOf(row.getCell(0).getRichStringCellValue()):null);
					                    inventory.setLocation_id(row.getCell(1)!=null?String.valueOf(row.getCell(1).getRichStringCellValue()):null);
					                    inventory.setCal_day(row.getCell(2).getDateCellValue());
					                    inventory.setStock_type(row.getCell(3)!=null?String.valueOf(row.getCell(3).getRichStringCellValue()):null);
					                    inventory.setStock_category(row.getCell(4)!=null?String.valueOf(row.getCell(4).getRichStringCellValue()):null);
					                    inventory.setTotal_stock(row.getCell(5)!=null?Integer.valueOf((int)row.getCell(5).getNumericCellValue()):null);
					                    inventory.setTs_unit(row.getCell(6)!=null?String.valueOf(row.getCell(6).getRichStringCellValue()):null);
					                
					                    fileUploadDAO.save(inventory);
					                }  
					                request.getSession().removeAttribute("uploadError");
					    } catch (Exception ec) {
					    	System.out.println("Inventory file exception1");
					    	request.getSession().setAttribute("uploadError","Exception");
					    	System.out.println("Inventory file exception2");
					        ec.printStackTrace();
					    } 
					  
					    	logger.info("Inventory Data file uploaded successfully:::::");
					    	System.out.println("Inventory Data file uploaded successfully:::::::::::::::::::::::");
					        return "success";
					    }  
					 

	// method to  External data file			 
	 @RequestMapping(value = "/loadExternalDataFileToDB", method = RequestMethod.POST)
	    public String loadFactExternalFileToDB(HttpServletRequest request,@RequestParam MultipartFile externalData) throws Exception { 
	 		logger.info("FileUploadController : loadFactExternalFileToDB START");
	 		System.out.println("FactExtenal  data is start uploading::::::::::::::::::::::::");
	    	try{
	    		InputStream input = externalData.getInputStream();  
	                POIFSFileSystem fs = new POIFSFileSystem( input );  
	                HSSFWorkbook wb = new HSSFWorkbook(fs);  
	                HSSFSheet sheet = wb.getSheetAt(0);  
	                HSSFRow row;  
	                
	                totalRowCount = sheet.getLastRowNum();

	                for(int i=1; i<=sheet.getLastRowNum(); i++)
	                { 
	                	 currentRowNumber = i;
	                	
	                	FactExternal factExternal=new FactExternal();
	                    row = sheet.getRow(i);  
	                    factExternal.setLongitude(row.getCell(0)!=null?row.getCell(0).getNumericCellValue():null);//double
	                     factExternal.setLatitude(row.getCell(1)!=null?row.getCell(1).getNumericCellValue():null);//double
	                     factExternal.setPostcode(row.getCell(2)!=null?String.valueOf(row.getCell(2).getNumericCellValue()):null);
	                     factExternal.setCalDay(row.getCell(3)!=null?row.getCell(3).getDateCellValue():null);
	                     factExternal.setSeason(row.getCell(4)!=null?String.valueOf(row.getCell(4).getRichStringCellValue()):null);
	                     factExternal.setMaxTemp(row.getCell(5)!=null?row.getCell(5).getNumericCellValue():null);
	                     factExternal.setMaxTempUnit(row.getCell(6)!=null?String.valueOf(row.getCell(6).getRichStringCellValue()):null);
	                     factExternal.setMinTemp(row.getCell(7)!=null?row.getCell(7).getNumericCellValue():null);
	                     factExternal.setMinTempUnit(row.getCell(8)!=null?String.valueOf(row.getCell(8).getRichStringCellValue()):null);
	                     factExternal.setAvgTemp(row.getCell(9)!=null?row.getCell(9).getNumericCellValue():null);
	                     factExternal.setAvgTempUnit(row.getCell(10)!=null?String.valueOf(row.getCell(10).getRichStringCellValue()):null);
	                     factExternal.setRainfall(row.getCell(11)!=null?row.getCell(11).getNumericCellValue():null);
	                     factExternal.setRainfallUnit(row.getCell(12)!=null?String.valueOf(row.getCell(12).getRichStringCellValue()):null);
	                     factExternal.setWind(row.getCell(13)!=null?row.getCell(13).getNumericCellValue():null);
	                     factExternal.setWindUnit(row.getCell(14)!=null?String.valueOf(row.getCell(14).getRichStringCellValue()):null);
	                     factExternal.setFeellikeTemp(row.getCell(15)!=null?row.getCell(15).getNumericCellValue():null);
	                     factExternal.setFeellikeTempUnit(row.getCell(16)!=null?String.valueOf(row.getCell(16).getRichStringCellValue()):null);
	                     factExternal.setDensity(row.getCell(17)!=null?row.getCell(17).getNumericCellValue():null);
	                     factExternal.setDensityUnit(row.getCell(18)!=null?String.valueOf(row.getCell(18).getRichStringCellValue()):null);
	                     factExternal.setGdp(row.getCell(19)!=null?row.getCell(19).getNumericCellValue():null);
	                     factExternal.setGdpUnit(row.getCell(20)!=null?String.valueOf(row.getCell(20).getRichStringCellValue()):null);
	                     factExternal.setIncomeLevel(row.getCell(21)!=null?row.getCell(21).getNumericCellValue():null);
	                     factExternal.setIncomeLevelUnit(row.getCell(22)!=null?String.valueOf(row.getCell(22).getRichStringCellValue()):null);
	                     factExternal.setElevation(row.getCell(23)!=null?row.getCell(23).getNumericCellValue():null);
	                     factExternal.setElevationUnit(row.getCell(24)!=null?String.valueOf(row.getCell(24).getRichStringCellValue()):null);
	                     factExternal.setPrecipitation(row.getCell(25)!=null?row.getCell(25).getNumericCellValue():null);
	                     factExternal.setPrecipitationUnit(row.getCell(26)!=null?String.valueOf(row.getCell(26).getRichStringCellValue()):null);
	                     factExternal.setRelative_humidity(row.getCell(27)!=null?row.getCell(27).getNumericCellValue():null);
	                     factExternal.setRelativeHumidityUnit(row.getCell(28)!=null?String.valueOf(row.getCell(28).getRichStringCellValue()):null);
	                     factExternal.setSolar(row.getCell(29)!=null?row.getCell(29).getNumericCellValue():null);
	                     factExternal.setSolarUnit(row.getCell(30)!=null?String.valueOf(row.getCell(30).getRichStringCellValue()):null);
	                    
	                    fileUploadDAO.save(factExternal);
	                }  
	                request.getSession().removeAttribute("uploadError");
	    } catch (Exception ec) {
	        ec.printStackTrace();
	    } 
	  System.out.println("Factexternal file uploaded successfully:::::::::::");
	    	logger.info("Factexternal file uploaded successfully:::::::::::");
	        return "success";
	    }

	
	// method to Price data file
	 @RequestMapping(value = "/loadPriceDataFileToDB", method = RequestMethod.POST)
	 	public String loadPriceDataFileToDB(HttpServletRequest request,@RequestParam MultipartFile priceData) throws Exception 
	 	{
		 logger.info("FileUploadController : loadPriceFileToDB START");
		 System.out.println("Price data is started uploading::::::::::::::::");
	    	try{
	    		InputStream input = priceData.getInputStream();  
	                POIFSFileSystem fs = new POIFSFileSystem( input );  
	                HSSFWorkbook wb = new HSSFWorkbook(fs);  
	                HSSFSheet sheet = wb.getSheetAt(0);  
	                HSSFRow row;  
	                
	                totalRowCount = sheet.getLastRowNum();

	                for(int i=1; i<=sheet.getLastRowNum(); i++)
	                {  
	                	
	                	 currentRowNumber = i;
	                	
	                	FactPrice price=new FactPrice();
	                    row = sheet.getRow(i);  
	                    
	                    price.setProduct_id(String.valueOf(row.getCell(0).getRichStringCellValue()));
	                    price.setLocation_id(row.getCell(1)!=null?String.valueOf(row.getCell(1).getRichStringCellValue()):null);
	                    price.setCal_day(row.getCell(2)!=null?row.getCell(2).getDateCellValue():null); // date
	                    price.setPrice(row.getCell(3)!=null?row.getCell(3).getNumericCellValue():null);
	                    price.setLocation_id(row.getCell(4)!=null?String.valueOf(row.getCell(4).getRichStringCellValue()):null);
	                    fileUploadDAO.save(price);
	                }  
	                request.getSession().removeAttribute("uploadError");
	    } catch (FileNotFoundException ec) {
	        ec.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    	System.out.println("Factprice file uploaded successfully");
	    	logger.info(" Factprice file uploaded successfully::::::::::::::::::: ");	    	
	        return "success";
	    } 
		 
		 
	 
	// method to Promotion data file
		 @RequestMapping(value = "/loadPromotionDataFileToDB", method = RequestMethod.POST)
		 	public String loadPromotionDataFileToDB(HttpServletRequest request,@RequestParam MultipartFile promotionData) throws Exception 
		 	{
				 logger.info("FileUploadController : loadPromotionFileToDB START");
				 System.out.println("Promotion data is started uploading::::::::::::::::");
			    	try{
			    		InputStream input = promotionData.getInputStream();  
			                POIFSFileSystem fs = new POIFSFileSystem( input );  
			                HSSFWorkbook wb = new HSSFWorkbook(fs);  
			                HSSFSheet sheet = wb.getSheetAt(0);  
			                HSSFRow row;  
			                
			                totalRowCount = sheet.getLastRowNum();

			                for(int i=1; i<=sheet.getLastRowNum(); i++)
			                {  
			                	
			                	 currentRowNumber = i;
			                	
			                	FactPromotion promotion=new FactPromotion();
			                    row = sheet.getRow(i);  
			                    
			                    promotion.setProduct_id(row.getCell(0)!=null?String.valueOf(row.getCell(0).getRichStringCellValue()):null);
			                    promotion.setLocation_id(row.getCell(1)!=null?String.valueOf(row.getCell(1).getRichStringCellValue()):null);
			                    promotion.setPromo_id(row.getCell(2)!=null?String.valueOf(row.getCell(2).getRichStringCellValue()):null);
			                    promotion.setValid_from(row.getCell(3)!=null?row.getCell(3).getDateCellValue():null); // date
			                    promotion.setValid_to(row.getCell(4)!=null?row.getCell(4).getDateCellValue():null); // date
			                   // promotion.setPr(Integer.valueOf((int) row.getCell(3).getNumericCellValue()));
			                    promotion.setPromo_type(row.getCell(5)!=null?String.valueOf(row.getCell(5).getRichStringCellValue()):null);
			                    promotion.setPromo_category(row.getCell(6)!=null?String.valueOf(row.getCell(6).getRichStringCellValue()):null);
			                    promotion.setDiscount(row.getCell(7)!=null?row.getCell(7).getNumericCellValue():null);
			                    promotion.setPromo_desc(row.getCell(8)!=null?String.valueOf(row.getCell(8).getRichStringCellValue()):null);
			                    promotion.setEstimated_sales_qty(row.getCell(9)!=null?Integer.valueOf((int) row.getCell(9).getNumericCellValue()):null);
			                    //promotion.setLocation_id(row.getCell(4)!=null?String.valueOf(row.getCell(4).getRichStringCellValue()):null);
			                    fileUploadDAO.save(promotion);
			                }  
			                request.getSession().removeAttribute("uploadError");
			    } catch (Exception ec) {
			    	System.out.println("Promotion file exception1");
			    	request.getSession().setAttribute("uploadError","Exception");
			    	System.out.println("Promotion file exception2");
			        ec.printStackTrace();
			    }
			    	System.out.println("Factpromotion file uploaded successfully");
			    	logger.info(" Factpromotion file uploaded successfully::::::::::::::: ");	    	
			        return "success";
			    } 
				 
				 
		// method to sales data file
		/*@RequestMapping(value = "/loadSalesDataFileToDB", method = RequestMethod.POST)
	    public String loadSalesFileToDB(HttpServletRequest request,@RequestParam MultipartFile salesData) throws Exception { 
		 logger.info("FileUploadController : loadSalesDataFileToDB START");
		 System.out.println("FactSalesData data is start uploading::::::::::::::::");
		 mailService.saveFileToDirectory(salesData);
	    	try{
	    		InputStream input = salesData.getInputStream();  
	                POIFSFileSystem fs = new POIFSFileSystem( input );  
	                HSSFWorkbook wb = new HSSFWorkbook(fs);  
	                HSSFSheet sheet = wb.getSheetAt(0);  
	                HSSFRow row;  
	                
	                totalRowCount = sheet.getLastRowNum();

	                for(int i=1; i<=sheet.getLastRowNum(); i++)
	                {  
	                	
	                	
	                	 currentRowNumber = i;
	                	
	                	FactSales sales=new FactSales();
	                    row = sheet.getRow(i);  
	                    
	                    //sales.setProductId(String.valueOf(row.getCell(0).getNumericCellValue()));
	                    //sales.setLocationId(String.valueOf((long)row.getCell(1).getNumericCellValue()));
	                    sales.setCalDay(row.getCell(0).getDateCellValue()); // date
	                   // sales.setCategoryId(String.valueOf(row.getCell(1).getNumericCellValue()));
	                    //sales.setCustomerId(row.getCell(2)!=null?String.valueOf(row.getCell(2).getRichStringCellValue()):null);
	                    sales.setCust1(row.getCell(3)!=null?String.valueOf(row.getCell(3).getRichStringCellValue()):null);
	                    sales.setCust2(row.getCell(4)!=null?String.valueOf(row.getCell(4).getRichStringCellValue()):null);
	                    sales.setCust3(row.getCell(5)!=null?String.valueOf(row.getCell(5).getNumericCellValue()):null);
	                    sales.setPrice(row.getCell(6)!=null?row.getCell(6).getNumericCellValue():null); // double
	                    sales.setPriceCurrency(row.getCell(7)!=null?String.valueOf(row.getCell(7).getRichStringCellValue()):null);  // string
	                    sales.setQuantity(row.getCell(8)!=null?row.getCell(8).getNumericCellValue():null);
	                    sales.setQtyUnit(row.getCell(9)!=null?String.valueOf(row.getCell(9).getRichStringCellValue()):null);
	                    sales.setAmount(row.getCell(10)!=null?row.getCell(10).getNumericCellValue():null); // double
	                    sales.setAmtCurrency(row.getCell(11)!=null?String.valueOf(row.getCell(11).getRichStringCellValue()):null);
	                    sales.setTax(row.getCell(12)!=null?row.getCell(12).getNumericCellValue():null); // double
	                    sales.setTaxCurrency(row.getCell(13)!=null?String.valueOf(row.getCell(13).getRichStringCellValue()):null);
	                    sales.setServiceCharge(row.getCell(14)!=null?row.getCell(14).getNumericCellValue():null);
	                    sales.setScCurrency(row.getCell(15)!=null?String.valueOf(row.getCell(15).getRichStringCellValue()):null);
	                    sales.setServiceTax(row.getCell(16)!=null?row.getCell(16).getNumericCellValue():null);
	                    sales.setStCurrency(row.getCell(17)!=null?String.valueOf(row.getCell(17).getRichStringCellValue()):null);
	                    sales.setTotalSales(row.getCell(18)!=null?row.getCell(18).getNumericCellValue():null);
	                    sales.setTsCurrency(row.getCell(19)!=null?String.valueOf(row.getCell(19).getRichStringCellValue()):null);
	                    sales.setDiscount(row.getCell(20)!=null?row.getCell(20).getNumericCellValue():null);
	                    sales.setDiscountCurrency(row.getCell(21)!=null?String.valueOf(row.getCell(21).getRichStringCellValue()):null);
	                    sales.setBillValue(row.getCell(22)!=null?row.getCell(22).getNumericCellValue():null);
	                    sales.setBvCurrency(row.getCell(23)!=null?String.valueOf(row.getCell(23).getRichStringCellValue()):null);
	                    sales.setMaxTdate(row.getCell(24)!=null?row.getCell(24).getDateCellValue():null);
	                    sales.setAvgtimespend((Time) (row.getCell(25)!=null?((Cell) row.getCell(25)).getDateCellValue():null));
	                    sales.setKotno(row.getCell(26)!=null?Integer.valueOf((int) row.getCell(26).getNumericCellValue()):null);
	                    sales.setIsno(row.getCell(27)!=null?Integer.valueOf((int) row.getCell(27).getNumericCellValue()):null);
	                    sales.setCustKf1(row.getCell(28)!=null?Integer.valueOf((int) row.getCell(28).getNumericCellValue()):null);
	                    sales.setCustKf2(row.getCell(29)!=null?Integer.valueOf((int) row.getCell(29).getNumericCellValue()):null);
	                    sales.setCustKf3(row.getCell(30)!=null?Integer.valueOf((int) row.getCell(30).getNumericCellValue()):null);
	                    sales.setPromoId(row.getCell(31)!=null?String.valueOf(row.getCell(31).getNumericCellValue()):null);
	                    
	                    fileUploadDAO.save(sales);
	                } 
	                uploadStatus=true;
	                request.getSession().removeAttribute("uploadError"); 
	    } catch (Exception ec) {
	    	uploadStatus=false;
	    	System.out.println("Sales file exception1");
	    	request.getSession().setAttribute("uploadError","Exception");
	    	System.out.println("Sales file exception2");
	        ec.printStackTrace();
	    } 
	    	finally
		    {
		    	System.out.println("finally block sending mail...");
		    	mailService.sendMail(uploadStatus, totalRowCount, salesData.getOriginalFilename());
		    	System.out.println("finally block mail sent.");
		    }
	    	
	    	System.out.println("FactsalesData file uploaded successfully");
	    	logger.info(" FactsalesData file uploaded successfully::::::::::::::: ");	    	
	        return "success";
	    }  

*/	 
	// method to socialData data file
			 @RequestMapping(value = "/loadSocialDataFileToDB", method = RequestMethod.POST)
			 	public String loadSocialDataFileToDB(HttpServletRequest request,@RequestParam MultipartFile socialData) throws Exception 
			 	{
				
				   
				 		logger.info("FileUploadController : loadFactSocialFileToDB START");
				 		System.out.println("Factsocial data  started uploading");
				    	try{
				    			InputStream input = socialData.getInputStream();  
				                POIFSFileSystem fs = new POIFSFileSystem( input );  
				                HSSFWorkbook wb = new HSSFWorkbook(fs);  
				                HSSFSheet sheet = wb.getSheetAt(0);  
				                HSSFRow row;  
				                
				                totalRowCount = sheet.getLastRowNum();

				                for(int i=1; i<=sheet.getLastRowNum(); i++)
				                {  
				                	 currentRowNumber = i;
				                	
				                	FactSocial factSocial=new FactSocial();
				                    row = sheet.getRow(i);  
				                    factSocial.setSoicialId(row.getCell(0)!=null?String.valueOf(row.getCell(0).getNumericCellValue()):null);
				                    factSocial.setCalDay(row.getCell(1)!=null?row.getCell(1).getDateCellValue():null);
				                    factSocial.setPositiveSentiment(row.getCell(2)!=null?Integer.valueOf((int) row.getCell(2).getNumericCellValue()):null);
				                    factSocial.setNegativeSentiment(row.getCell(3)!=null?Integer.valueOf((int) row.getCell(3).getNumericCellValue()):null);
				                    factSocial.setNeutralSentiment(row.getCell(4)!=null?Integer.valueOf((int) row.getCell(4).getNumericCellValue()):null);
				                    factSocial.setInfentualScore(row.getCell(5)!=null?row.getCell(5).getNumericCellValue():null);
				                    factSocial.setSentimentalIndex(row.getCell(6)!=null?row.getCell(6).getNumericCellValue():null);
				                    factSocial.setSrs(row.getCell(7)!=null?row.getCell(7).getNumericCellValue():null);
				                    fileUploadDAO.save(factSocial);
				                }  
				                request.getSession().removeAttribute("uploadError");
				    } catch (Exception ec) {
				    	System.out.println("Promotion file exception1");
				    	request.getSession().setAttribute("uploadError","Exception");
				    	System.out.println("Promotion file exception2");
				        ec.printStackTrace();
				    }
				  
				    	logger.info("FactsocialData file uploaded successfully:::::::::");
				    	System.out.println("FactsocialData file uploaded successfully:::::::::");
				        return "success";
				    }
			 
			 
			 	@RequestMapping(value = "/progressBarValues", method = RequestMethod.POST)
			 	public Map<String,String> ProgressBarValues(HttpServletRequest request) throws Exception 
			 	{
				 
				 Map<String,String> returnMap = new HashMap<String,String>();
				 returnMap.put("totalRowCount",totalRowCount.toString());
				 returnMap.put("currentRowNumber", currentRowNumber.toString());
				 return returnMap;
			 	}	
			 	
			 	
			 	
			 	@RequestMapping(value = "/loadSalesDataFileToDB", method = RequestMethod.POST)
			    public String loadInviceSalesFileToDB(HttpServletRequest request,@RequestParam MultipartFile salesData) throws Exception { 
				 logger.info("FileUploadController : loadSalesDataFileToDB START");
				 System.out.println("FactSalesData data is start uploading::::::::::::::::");
				 mailService.saveFileToDirectory(salesData);
			    	try{
			    		InputStream input = salesData.getInputStream();  
			                POIFSFileSystem fs = new POIFSFileSystem( input );  
			                HSSFWorkbook wb = new HSSFWorkbook(fs);  
			                HSSFSheet sheet = wb.getSheetAt(0);  
			                HSSFRow row;  
			                
			                totalRowCount = sheet.getLastRowNum();

			                for(int i=1; i<=sheet.getLastRowNum(); i++)
			                {  
			                	
			                	
			                	 currentRowNumber = i;
			                	
			                	InvoiceSales sales=new InvoiceSales();
			                    row = sheet.getRow(i);  
			                    
			                    sales.setSlNo(row.getCell(0)!=null?Integer.valueOf((int) row.getCell(0).getNumericCellValue()):null);
			                    sales.setInvoiceId(row.getCell(1)!=null?String.valueOf(row.getCell(1).getNumericCellValue()):null);
			                    sales.setBillNo(row.getCell(2)!=null?Integer.valueOf((int) row.getCell(2).getNumericCellValue()):null);
			                    sales.setOrderId(row.getCell(3)!=null?Integer.valueOf((int) row.getCell(3).getNumericCellValue()):null);
			                    sales.setPjpOutlet(row.getCell(4)!=null?Integer.valueOf((int) row.getCell(4).getNumericCellValue()):null);
			                    sales.setSalesDate(row.getCell(5)!=null?row.getCell(5).getDateCellValue():null);
			                    sales.setSalesTime(row.getCell(6)!=null?String.valueOf(row.getCell(6).getNumericCellValue()):null);
			                    sales.setOutletId(row.getCell(7)!=null?String.valueOf(row.getCell(7).getNumericCellValue()):null);
			                    sales.setOutlet(row.getCell(8)!=null?String.valueOf(row.getCell(8).getRichStringCellValue()):null);
			                    sales.setProductId(row.getCell(9)!=null?String.valueOf(row.getCell(9).getRichStringCellValue()):null);
			                    sales.setAddress(row.getCell(10)!=null?String.valueOf(row.getCell(10).getRichStringCellValue()):null);
			                    sales.setType(row.getCell(11)!=null?String.valueOf(row.getCell(11).getRichStringCellValue()):null);
			                    sales.setLatitude(row.getCell(12)!=null?row.getCell(12).getNumericCellValue():null);
			                    sales.setLongitude(row.getCell(13)!=null?row.getCell(13).getNumericCellValue():null);
			                    sales.setPhoneNo(row.getCell(14)!=null?String.valueOf(row.getCell(14).getNumericCellValue()):null);
			                    sales.setEmail(row.getCell(15)!=null?String.valueOf(row.getCell(15).getRichStringCellValue()):null);
			                    sales.setOwnerName(row.getCell(16)!=null?String.valueOf(row.getCell(16).getRichStringCellValue()):null);
			                    sales.setMobileNo(row.getCell(17)!=null?String.valueOf(row.getCell(17).getNumericCellValue()):null);
			                    sales.setCity(row.getCell(18)!=null?String.valueOf(row.getCell(18).getRichStringCellValue()):null);
			                    sales.setState(row.getCell(19)!=null?String.valueOf(row.getCell(19).getRichStringCellValue()):null);
			                    sales.setPin(row.getCell(20)!=null?Integer.valueOf((int) row.getCell(20).getNumericCellValue()):null);
			                    sales.setArea(row.getCell(21)!=null?String.valueOf(row.getCell(21).getRichStringCellValue()):null);
			                    sales.setWarehouse(row.getCell(22)!=null?String.valueOf(row.getCell(22).getRichStringCellValue()):null);
			                    sales.setUser(row.getCell(23)!=null?String.valueOf(row.getCell(23).getRichStringCellValue()):null);
			                    sales.setEmpId(row.getCell(24)!=null?String.valueOf(row.getCell(24).getRichStringCellValue()):null);
			                    sales.setFordate(row.getCell(25)!=null?convertDate(row.getCell(25)):null);
			                    sales.setSkuPlaced(row.getCell(26)!=null?String.valueOf(row.getCell(26).getRichStringCellValue()):null);
			                    sales.setSkuCode(row.getCell(27)!=null?String.valueOf(row.getCell(27).getRichStringCellValue()):null);
			                    sales.setCategory(row.getCell(28)!=null?String.valueOf(row.getCell(28).getRichStringCellValue()):null);
			                    sales.setQuantity(row.getCell(29)!=null?row.getCell(29).getNumericCellValue():null);
			                    sales.setSchemeRate(row.getCell(30)!=null?row.getCell(30).getNumericCellValue():null);
			                    sales.setQpsScheme(row.getCell(31)!=null?Integer.valueOf((int) row.getCell(31).getNumericCellValue()):null);
			                    sales.setDiscountScheme(row.getCell(32)!=null?row.getCell(32).getNumericCellValue():null);
			                    sales.setUnitPrice(row.getCell(33)!=null?row.getCell(33).getNumericCellValue():null);
			                    sales.setAmount(row.getCell(34)!=null?row.getCell(34).getNumericCellValue():null);
			                    sales.setBeat(row.getCell(35)!=null?String.valueOf(row.getCell(35).getRichStringCellValue()):null);
			                    sales.setNoOfPicture(row.getCell(36)!=null?Integer.valueOf((int) row.getCell(36).getNumericCellValue()):null);
			                    sales.setApprovedDisplayForQpds(row.getCell(37)!=null?String.valueOf(row.getCell(37).getRichStringCellValue()):null);
			                    sales.setRejectDisplayForQpds(row.getCell(38)!=null?String.valueOf(row.getCell(38).getRichStringCellValue()):null);
			                    sales.setExistingOutletcode(row.getCell(39)!=null?String.valueOf(row.getCell(39).getRichStringCellValue()):null);
			                    sales.setIsHector(row.getCell(40)!=null?String.valueOf(row.getCell(40).getRichStringCellValue()):null);
			                     			                    			                   
			                    fileUploadDAO.save(sales);
			                } 
			                uploadStatus=true;
			                request.getSession().removeAttribute("uploadError"); 
			    } catch (Exception ec) {
			    	uploadStatus=false;
			    	System.out.println("Sales file exception1");
			    	request.getSession().setAttribute("uploadError","Exception");
			    	System.out.println("Sales file exception2");
			        ec.printStackTrace();
			    } 

			    	
			    	System.out.println("FactsalesData file uploaded successfully");
			    	logger.info(" FactsalesData file uploaded successfully::::::::::::::: ");	    	
			        return "success";
			    }  

			 	
			 private String convertDate(HSSFCell hssfCell) throws ParseException{
				 HSSFDataFormatter hdf = new HSSFDataFormatter();
				 System.out.println ("formatted "+ hdf.formatCellValue(hssfCell));
				 return hdf.formatCellValue(hssfCell);
				}
	 	
			 
				 
				          
}
