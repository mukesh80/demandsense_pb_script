package com.rplus.ds.controller.upload;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.upload.ViewFileDAO;
import com.rplus.ds.model.upload.DimCustomer;
import com.rplus.ds.model.upload.DimLocation;
import com.rplus.ds.model.upload.DimProduct;
import com.rplus.ds.model.upload.DimSocial;
import com.rplus.ds.model.upload.DimTime;
import com.rplus.ds.model.upload.FactEvent;
import com.rplus.ds.model.upload.FactExternal;
import com.rplus.ds.model.upload.FactInventory;
import com.rplus.ds.model.upload.FactPrice;
import com.rplus.ds.model.upload.FactPromotion;
import com.rplus.ds.model.upload.FactSales;
import com.rplus.ds.model.upload.FactSocial;
import com.rplus.ds.model.upload.InvoiceSales;

@RestController
@RequestMapping(value="/viewdata")
public class ViewDataController {

	@Autowired
    private ViewFileDAO viewFileDAO;
	
	@RequestMapping(value="product")
	public List<DimProduct> viewProductFile()
	{
		List<DimProduct> productFile=viewFileDAO.viewProductFile();
		return productFile;
	}
	
	@RequestMapping(value="location")
	public List<DimLocation> viewLocationFile()
	{
		List<DimLocation> locationFile=viewFileDAO.viewLocationFile();
		return locationFile;
	}
	
	@RequestMapping(value="customer")
	public List<DimCustomer> viewCustomer()
	{
		List<DimCustomer> dimCustomerFile=viewFileDAO.viewDimCustomerFile();
		return dimCustomerFile;
	}
	
	@RequestMapping(value="dimSocial")
	public List<DimSocial> viewDimSocialFile()
	{
		List<DimSocial> dimSocialFile=viewFileDAO.viewDimSocialFile();
		return dimSocialFile;
	}
	
	@RequestMapping(value="time")
	public List<DimTime> viewTimeFile()
	{
		List<DimTime> timeFile=viewFileDAO.viewTimeFile();
		return timeFile;
	}
	
	@RequestMapping(value="event")
	public List<FactEvent> viewEventFile()
	{
		List<FactEvent> eventFile=viewFileDAO.viewFactEventFile();
		return eventFile;
	}
	
	@RequestMapping(value="inventory")
	public List<FactInventory> viewInventoryFile()
	{
		List<FactInventory> inventoryFile=viewFileDAO.viewFactInventoryFile();
		return inventoryFile;
	}
	
	@RequestMapping(value="external")
	public List<FactExternal> viewExternalFile()
	{
		List<FactExternal> factExternalFile=viewFileDAO.viewFactExternalFile();
		return factExternalFile;
	}
	
	@RequestMapping(value="price")
	public List<FactPrice> viewPriceFile()
	{
		List<FactPrice> factPriceFile=viewFileDAO.viewFactPriceFile();
		return factPriceFile;
	}
	
	@RequestMapping(value="promotion")
	public List<FactPromotion> viewPromotionFile()
	{
		List<FactPromotion> promotionFile=viewFileDAO.viewFactPromotionFile();
		return promotionFile;
	}
	
	@RequestMapping(value="sales")
	public List<InvoiceSales> viewSalesFile()
	{
		List<InvoiceSales> salesFile=viewFileDAO.viewFactSalesFile();
		return salesFile;
	}
	
	@RequestMapping(value="factSocial")
	public List<FactSocial> viewFactSocialFile()
	{
		List<FactSocial> factSocialFile=viewFileDAO.viewFactSocialFile();
		return factSocialFile;
	}
}
