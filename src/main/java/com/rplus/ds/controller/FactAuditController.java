package com.rplus.ds.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.FactAuditDao;
import com.rplus.ds.model.FactAuditReport;

/**
 * @author Rafeeq
 *
 */
@RestController
@RequestMapping("/factaudit")
public class FactAuditController {
	
private static final Logger logger = Logger.getLogger(FactAuditController.class);
	
	@Autowired
	FactAuditDao factAuditDao;

	@RequestMapping(value = "getreport", method = RequestMethod.GET)
	public HashMap<String,List<FactAuditReport>> getAuditreport() {
		logger.info(" FactAuditController :getAuditreport() START");
		
		List<FactAuditReport> reportList = factAuditDao.getAuditreport();
		HashMap<String,List<FactAuditReport>> resultMap = new HashMap<String,List<FactAuditReport>>();
		List<FactAuditReport> auditReportList = null;;
		String tableName = null;
		for(FactAuditReport factAuditReport : reportList){
			if(tableName == null || !factAuditReport.getTableName().equals(tableName)){
				if(tableName !=null && !factAuditReport.getTableName().equals(tableName)){
					resultMap.put(tableName,auditReportList);		
				}
				tableName = factAuditReport.getTableName();
				logger.info(" FactAuditController :TableName :"+tableName);
				auditReportList = new ArrayList<FactAuditReport>();
			}
			auditReportList.add(factAuditReport);
		}
		logger.info(" FactAuditController :getAuditreport() END");
		return resultMap;
	}

}
