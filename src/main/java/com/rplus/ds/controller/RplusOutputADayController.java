package com.rplus.ds.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.PredictiveDAOImpl;
import com.rplus.ds.model.RplusDsOutputADay;

/**
 * @author Rafeeq
 *
 */
@RestController
@RequestMapping("/outputAdata")
public class RplusOutputADayController {	
	private static final Logger logger = Logger.getLogger(RplusOutputADayController.class);

	/*@Autowired
	RplusOutputADayDao rplusOutputADayDao;*/
	
	@Autowired
    PredictiveDAOImpl predictiveDAOImpl;

		
	@RequestMapping(value = "getAdata", method = RequestMethod.GET)
	 public List<RplusDsOutputADay> getRplusOutputAdata(HttpServletRequest request,@RequestParam Map<String,String> allParams)// 
	    {   logger.info(" RplusOutputADayController: getRplusOutputAdata START");
	    
	    HttpSession httpSession=request.getSession(); 
		httpSession.setAttribute("forcastData", allParams);
	    	
	        //List<RplusDsOutputADay> outputAList = predictiveDAOImpl.getRplusOutputAdata();
	    	List<RplusDsOutputADay> outputAList = predictiveDAOImpl.getRplusOutputAdata(allParams);
	        logger.info(" RplusOutputADayController: getRplusOutputAdata END");
	        return outputAList;
	    }

}
