package com.rplus.ds.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.SystemParamDao;

/**
 * @author Rafeeq
 *
 */
@RestController
@RequestMapping("/sysparams")
public class SystemParamController {
	
   private static final Logger logger = Logger.getLogger(SystemParamController.class);
	@Autowired
    private SystemParamDao systemParamDao;
	
		
	/**
	 * This method is used to get the System params
	 * @return
	 */
	@RequestMapping("getparam")
	public Map<String, String> getSystemParams(){
		logger.info("SystemParamController : getSystemParams");
		Map<String, String> systemParam = systemParamDao.getSysParam();
		System.out.println("systemParams Data : "+systemParam);
		return systemParam;
	}
	
	/*@RequestMapping("getparam")
	public List<String> getSystemParams(){
		System.out.println("enabled logger for getSystemParams");
		List<String> systemParam = systemParamDao.getSysParam();
		System.out.println("loaded data"+systemParam);
		return systemParam;
	}*/

}
