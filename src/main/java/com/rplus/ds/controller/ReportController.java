/**
 * 
 */
package com.rplus.ds.controller;

import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.DimProductDAO;
import com.rplus.ds.dao.LocationDao;
import com.rplus.ds.dao.ReportDao;
import com.rplus.ds.dao.ReportDaoImpl;
import com.rplus.ds.dao.UserLogDao;

/**
 * @author PRAVEEN REDDIVARI
 *
 */
@RestController
@RequestMapping("/report")
public class ReportController {
	
	private static final Logger logger = Logger.getLogger(LocationController.class);
	
	@Autowired
	LocationDao locationDao;
	
	@Autowired
	DimProductDAO productDao;
	
	@Autowired
	ReportDao reportDao;
	
	@Autowired
	UserLogDao useLogDao;
	
	@Autowired
	ReportDaoImpl reportDaoImpl;
	
	@RequestMapping(value = "citys", method = RequestMethod.GET)
	public HashMap<String,String> getWarehouseList() {
		logger.info("Invoke getAllCities() START ");
    	HashMap<String,String> citiesMap = locationDao.getAllCities();
    	logger.info("Invoke getAllCities() END ");
        return citiesMap;
	}
	
	

	@RequestMapping(value = "districts", method = RequestMethod.GET)
	public HashMap<String,String> getDistrictList() {
		logger.info("Invoke getDistrictList() START ");
    	HashMap<String,String> citiesMap = locationDao.getAllDistricts();
    	logger.info("Invoke getDistrictList() END ");
        return citiesMap;
	}
	
	@RequestMapping(value = "region", method = RequestMethod.GET)
	public HashMap<String,String> getRegionList() {
		logger.info("Invoke getRegionList() START ");
    	HashMap<String,String> citiesMap = reportDao.getAllRegions();
    	logger.info("Invoke getDistrictList() END ");
        return citiesMap;
	}
	
	@RequestMapping(value = "subcat", method = RequestMethod.GET)
	public HashMap<String,String> getSubCategoryList() {
		logger.info("Invoke getSubCategoryList() START ");
    	HashMap<String,String> citiesMap = reportDao.getAllSubCategeorys();
    	logger.info("Invoke getSubCategoryList() END ");
        return citiesMap;
	}
	
	
	
	@RequestMapping(value = "week", method = RequestMethod.GET)
	public List<Object> getWeeklyList1(@RequestParam Map<String,String> allParams) throws UnknownHostException {

		logger.info("Invoke getAllCities() START ");
		useLogDao.saveUserLog("Reports Tab SCM Week");
		List<Object> result=reportDao.getWeekReport(allParams);
    	logger.info("Invoke getAllCities() END ");
        return result;
	}
	
	@RequestMapping(value = "date", method = RequestMethod.GET)
	public List<Object> getDailyList(@RequestParam Map<String,String> allParams) throws UnknownHostException, ParseException {
		useLogDao.saveUserLog("Reports Tab SCM Day");
		List<Object> result=reportDao.getDayReport(allParams);
        return result;
	}
	
	@RequestMapping(value = "month", method = RequestMethod.GET)
	public List<Object> getMonthlyList1(@RequestParam Map<String,String> allParams) throws UnknownHostException {
		logger.info("Invoke getAllCities() START ");
		useLogDao.saveUserLog("Reports Tab Sales Month");
		List<Object> result=reportDao.getMonthReport(allParams);
    	logger.info("Invoke getAllCities() END ");
        return result;
	}
	
	@RequestMapping(value = "drive", method = RequestMethod.GET)
	public List<Object> getDriveAnalysisList(@RequestParam Map<String,String> allParams) throws UnknownHostException {
		logger.info("Invoke getDriveAnalysisList() START ");
		useLogDao.saveUserLog("Reports Tab Sales Drive Analysis");
		List<Object> result=reportDao.getDriveAnalysisReport(allParams);
    	logger.info("Invoke getDriveAnalysisList() END ");
        return result;
	}
	
	@RequestMapping(value = "details", method = RequestMethod.GET)
	public List<Object> getDriveDetails(@RequestParam Map<String,String> allParams) throws UnknownHostException {
		logger.info("Invoke getDriveAnalysisList() START ");
		List<Object> result=reportDao.getDriveDetails(allParams);
    	logger.info("Invoke getDriveAnalysisList() END ");
        return result;
	}
}
