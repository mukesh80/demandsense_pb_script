
package com.rplus.ds.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.PredictiveDAOImpl;
import com.rplus.ds.model.RplusDsOutputFDay;

/**
 * @author Rafeeq
 * 
 */
@RestController
@RequestMapping("/outputdata")
public class RplusDsOutputFDayController {	
	private static final Logger logger = Logger.getLogger(RplusDsOutputFDayController.class);

	@Autowired
    PredictiveDAOImpl predictiveDAOImpl;

		
	@RequestMapping(value = "getlist", method = RequestMethod.GET)
	 public List<RplusDsOutputFDay> getRplusOutputdata(HttpServletRequest request,@RequestParam Map<String,String> allParams) //
	    {   logger.info(" RplusDsOutputController: getRplusOutputdata START");
	    
	  	    	

	    HttpSession httpSession=request.getSession(); 
		httpSession.setAttribute("forcastData", allParams);
	    	
	    	
	    
	        List<RplusDsOutputFDay> outputList = predictiveDAOImpl.getRplusOutputFdata(allParams);
	        logger.info(" RplusDsOutputController: getRplusOutputdataFDay END");
	        return outputList;
	    }

}
