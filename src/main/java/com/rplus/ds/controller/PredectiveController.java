package com.rplus.ds.controller;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.PredictiveDAOImpl;
import com.rplus.ds.dao.UserLogDao;
import com.rplus.ds.service.PredictiveService;
import com.rplus.ds.service.PredictiveServiceImpl;

/**
 * 
 * @author Mallikarjuna
 *
 */
@RestController
@RequestMapping("/predictions")
public class PredectiveController {
	
	int forecastlooopcount;
	
	private static final Logger logger = Logger.getLogger(PredectiveController.class);
	@Autowired
	@Lazy
    PredictiveDAOImpl predictiveDAOImpl;
	
	@Autowired
	@Lazy
	PredictiveService predictiveService;
	
	@Autowired
	@Lazy
	UserLogDao useLogDao;
	
	
	
	

	/**
	 * This method is used to get the predecitve data for the forcast graph
	 * @param request
	 * @return
	 * @throws UnknownHostException 
	 */
	//@RequestMapping(value="forecastMap", method= RequestMethod.GET)
    /*public LinkedHashMap<String, List<HashMap<String, String>>> getForecastGraphData(HttpServletRequest request,@RequestParam Map<String,String> allParams){
		logger.info("Invoke getForecastGraphData()- START ");
		
		HttpSession httpSession=request.getSession(); 
		httpSession.setAttribute("forcastSession", "hello from session");
		httpSession.setAttribute("forcastData", allParams);
		System.out.println("session id::::::::::::::::::::::"+httpSession.getId());
		
		List<Object[]> resultList = predictiveDAOImpl.getForecastData(allParams); // Model class :RplusDsOutputByDay : rplus_ds_output_f_day
		
		
		LinkedHashMap<String, List<HashMap<String, String>>> forecastMap = new LinkedHashMap<String, List<HashMap<String, String>>>();
		List<HashMap<String, String>> forecastInfoList = new ArrayList<HashMap<String, String>>();
		
		for (Object[] resultListObject : resultList) {
			
			LinkedHashMap<String, String> qtyAndForecastQtyMap = new LinkedHashMap<String, String>();
			
			for(int i=0;i<resultListObject.length;i++)
			{
				f
			}
			
			List<Object> newarr=new ArrayList<Object>();
			
			for(Object resultListObject1: resultListObject)
			{
				newarr.add(resultListObject1);
			}
			
			
			
			qtyAndForecastQtyMap.put("yearWeek", resultListObject[0].toString());
			qtyAndForecastQtyMap.put("actual", resultListObject[1].toString());
			qtyAndForecastQtyMap.put("forecasted",resultListObject[2].toString());
			qtyAndForecastQtyMap.put("date", resultListObject[3].toString());
			
			forecastInfoList.add(qtyAndForecastQtyMap);
			
			forecastMap.put("Agg Data", forecastInfoList);
			
		}
		
		

		logger.info("Invoke getForecastGraphData()- END ");
		return forecastMap;
    }*/
	
	@RequestMapping(value="forecastMap", method= RequestMethod.GET)
	public LinkedHashMap<String, List<HashMap<String, String>>>  getForecastGraphData(HttpServletRequest request,@RequestParam Map<String,String> allParams) 
			throws UnknownHostException
	{
		useLogDao.saveUserLog("Analysis Tab Aggrigation");
		
		HttpSession httpSession=request.getSession(); 
		httpSession.setAttribute("forcastSession", "hello from session");
		httpSession.setAttribute("forcastData", allParams);
		System.out.println("session id::::::::::::::::::::::"+httpSession.getId());
		
		List<Object[]> resultList = predictiveDAOImpl.getForecastData(allParams);
	
		int sizeofresultlist=resultList.size();
		
		int totalLocation=PredictiveServiceImpl.tempLocation;
		int totalProduct=PredictiveServiceImpl.tempProduct;
		
		System.out.println("No of Locations::::::::::::::Forecast"+totalLocation);
		
		System.out.println("No of Products::::::::::::::Forecast"+totalProduct);
		
		
		int x_length=sizeofresultlist/totalProduct;
				//(int) request.getAttribute("noOfProduct");
				//totalProduct;
				//;
		
		List<Object[]> l1=resultList.subList(0, x_length);
		List<Object[]> l2=resultList.subList(x_length, sizeofresultlist);
		LinkedHashMap<String, List<HashMap<String, String>>> forecastMap = new LinkedHashMap<String, List<HashMap<String, String>>>();
		List<HashMap<String, String>> forecastInfoList = new ArrayList<HashMap<String, String>>();
		
		int j=0;
		
		for(int i=0;i<l2.size();i++){
			
			
			if(j==x_length)
			{
				j=0;
			}
			
			
			if(l2.get(i)[0].equals(l1.get(j)[0]))
			{
				l1.get(j)[1] = Double.parseDouble(l1.get(j)[1] == null ? "0" : l1.get(j)[1].toString()) + Double.parseDouble(l2.get(i)[1] == null ? "0" : l2.get(i)[1].toString());
				l1.get(j)[2] = Double.parseDouble(l1.get(j)[2] == null ? "0" : l1.get(j)[2].toString()) + Double.parseDouble(l2.get(i)[2] == null ? "0" : l2.get(i)[2].toString());
			}
			
			
			j++;
		}
		
		
		
		
		for (Object[] resultListObject : l1) {
			
			LinkedHashMap<String, String> qtyAndForecastQtyMap = new LinkedHashMap<String, String>();
					
			qtyAndForecastQtyMap.put("yearWeek", resultListObject[0].toString());
			//qtyAndForecastQtyMap.put("actual", resultListObject[1].toString());
			//qtyAndForecastQtyMap.put("forecasted",resultListObject[2].toString());
			qtyAndForecastQtyMap.put("actual",resultListObject[1] == null ? "0" : resultListObject[1].toString());
			qtyAndForecastQtyMap.put("forecasted",resultListObject[2] == null ? "0" : resultListObject[2].toString());
			qtyAndForecastQtyMap.put("date", resultListObject[3].toString());
			
			forecastInfoList.add(qtyAndForecastQtyMap);
			
			forecastMap.put("Agg Data", forecastInfoList);
			
		}
		
		return forecastMap;
	}
	
	
	/**
	 * This method used to get non aggregate forecast data for forecast type non aggregate map
	 * @param request
	 * @return
	 * @throws UnknownHostException 
	 */
	@SuppressWarnings({ })
	@RequestMapping(value="forecastMapNA", method= RequestMethod.GET)
    public LinkedHashMap<String, List<HashMap<String,String>>> getNAForecastGraphData(HttpServletRequest request,@RequestParam Map<String,String> allParams) throws UnknownHostException{
		
		HttpSession httpSession=request.getSession(); 
		httpSession.setAttribute("forcastSession", "my name is mukesh");
		httpSession.setAttribute("forcastData", allParams);
		System.out.println("session id::::::::::::::::::::::"+httpSession.getId());
		
		
		
		useLogDao.saveUserLog("Analysis Tab NonAggrigation");
		
		
		logger.info("Invoke getNAForecastGraphData()- START ");
		List<Object[]> totalResultsList = predictiveDAOImpl.getForecastNonAggregageData(allParams);
		
		LinkedHashMap<String, List<HashMap<String, String>>> forecastNonAggregateMap = new LinkedHashMap<String, List<HashMap<String, String>>>();
		List<HashMap<String, String>> forecastInfoList = new ArrayList<HashMap<String, String>>();
		
		String productId = null;
		int count = 0;
		HashMap<String, String> productIdAndNameMap = null;
		HashMap<String, String> subCategoryIdAndNameMap = null;
		
		if (allParams.get("product") != null && !(allParams.get("product").equals("") || allParams.get("product").equals(" "))) {
			HashMap<String, String> productParamMap = predictiveService.getInputParamMap(
					allParams, "product");
			productIdAndNameMap = predictiveService.prodIdAndNameMap(new ArrayList<String>(productParamMap.keySet()));
		}
		if (allParams.get("cat") != null && !(allParams.get("cat").equals("") || allParams.get("cat").equals(" "))) {
			HashMap<String, String> categoryParamMap = predictiveService.getInputParamMap(
					allParams, "cat");
			subCategoryIdAndNameMap = predictiveService.subCategoryIdAndNameMapByCategory(new ArrayList<String>(categoryParamMap.keySet()));
			
		}
		if (allParams.get("subcatid") != null && !(allParams.get("subcatid").equals("") || allParams.get("subcatid").equals(" "))) {
			HashMap<String, String> subCategoryParamMap = predictiveService.getInputParamMap(
					allParams, "subcatid");
			productIdAndNameMap = predictiveService.prodIdAndNameMapBySubCategory(new ArrayList<String>(subCategoryParamMap.keySet()));
		}
		
		
		/*
		 * productIdAndNameMap.put("20711", "BEVERAGES");
		 * productIdAndNameMap.put("40780", "COLD COFFEE");
		 */
		for (Object[] resultListObject : totalResultsList) {
			count++;
			if (productId == null
					|| (productId != null && !productId.equals(resultListObject[0].toString()))
					|| count == totalResultsList.size()) {
				if (productId != null) {
					String prodName = "";
					if (productIdAndNameMap != null && productIdAndNameMap.get(productId) != null) {
						prodName = productIdAndNameMap.get(productId);
					} else if (subCategoryIdAndNameMap!=null && subCategoryIdAndNameMap.get(productId) != null) {
						prodName = subCategoryIdAndNameMap.get(productId);
					} 
					forecastNonAggregateMap.put(prodName, forecastInfoList);
				}
				productId = resultListObject[0].toString();
				forecastInfoList = new ArrayList<HashMap<String, String>>();
			}
			LinkedHashMap<String, String> qtyAndForecastQtyMap = new LinkedHashMap<String, String>();
			qtyAndForecastQtyMap.put("yearWeek", resultListObject[1].toString());
			qtyAndForecastQtyMap.put("date", resultListObject[4].toString());
			//qtyAndForecastQtyMap.put("actual", resultListObject[2].toString());
			//qtyAndForecastQtyMap.put("forecasted",resultListObject[3].toString());
			
			qtyAndForecastQtyMap.put("actual",resultListObject[2] == null ? "0" : resultListObject[2].toString());
			qtyAndForecastQtyMap.put("forecasted",resultListObject[3] == null ? "0" : resultListObject[3].toString());

			
			forecastInfoList.add(qtyAndForecastQtyMap);

		}
		logger.info("Invoke getNAForecastGraphData()- END ");
		return forecastNonAggregateMap;
    }
	
	/**
	 * This method is used to get socialfactor data 
	 * @param request
	 * @return socialFactorGraphMap
	 */
	
	@RequestMapping(value = "socialfactor", method = RequestMethod.GET)
	public LinkedHashMap<String, List<HashMap<String, String>>> getSocialFactorGraph(HttpServletRequest request,@RequestParam Map<String,String> allParams)
	{
		
		logger.info("Invoke getSocialFactorGraph()- START ");
		List<Object[]> socialResultList = predictiveDAOImpl.getSocialFactorData(allParams); // ProdLocTimeByDay : rplus_prod_loc_time_day
		
		int sizeofresultlist=socialResultList.size();
		
		int totalLocation=PredictiveServiceImpl.tempLocation;
		int totalProduct=PredictiveServiceImpl.tempProduct;
		System.out.println("No of Locations::::::::::::::Social factor::::"+totalLocation);
		System.out.println("No of Products::::::::::::::Social factor::::"+totalProduct);
		
		
		int x_length=sizeofresultlist/totalProduct;

		List<Object[]> l1=socialResultList.subList(0, x_length);
		List<Object[]> l2=socialResultList.subList(x_length, sizeofresultlist);
		LinkedHashMap<String, List<HashMap<String, String>>> socialFactorsData = new LinkedHashMap<String, List<HashMap<String, String>>>();
		List<HashMap<String, String>> socialFactorList = new ArrayList<HashMap<String, String>>();
		
		int j=0;
		
		for(int i=0;i<l2.size();i++){
			
			
			if(j==x_length)
			{
				j=0;
			}
			
			
			if(l2.get(i)[0].equals(l1.get(j)[0]))
			{
				//System.out.println("In if condition::::::::::::::total product:::"+totalProduct+"\n total location:::"+totalLocation);
				
				l1.get(j)[1] =(Double.parseDouble(l1.get(j)[1].toString()) + Double.parseDouble(l2.get(i)[1].toString()))/2;
				
				l1.get(j)[2]=(Double.parseDouble(l1.get(j)[2].toString()) + Double.parseDouble(l2.get(i)[2].toString()))/2;
			}
			
			
			
			j++;
		}
		
		
		/*LinkedHashMap<String, List<HashMap<String, String>>> socialFactorsData = new LinkedHashMap<String, List<HashMap<String, String>>>();
		List<HashMap<String, String>> socialFactorList = new ArrayList<HashMap<String, String>>();
		*/
		
		for (Object[] resultListObject : l1) {

			LinkedHashMap<String, String> sentimentalAndSrsMap = new LinkedHashMap<String, String>();
			sentimentalAndSrsMap.put("yearWeek", resultListObject[0].toString());
			sentimentalAndSrsMap.put("date", resultListObject[3].toString());
			sentimentalAndSrsMap.put("sentimental",resultListObject[1] != null?resultListObject[1].toString():"0");
			sentimentalAndSrsMap.put("srs", resultListObject[2] != null?resultListObject[2].toString():"0");
			socialFactorList.add(sentimentalAndSrsMap);
			socialFactorsData.put("Agg Data", socialFactorList);
		}
		logger.info("Invoke getSocialFactorGraph()- END ");
		return socialFactorsData;
	}
	
	/**
	 * This method is used to get socialfactor data 
	 * @param request
	 * @return socialFactorGraphMap
	 */
	
	@RequestMapping(value = "socialfactorNA", method = RequestMethod.GET)
	public LinkedHashMap<String, List<HashMap<String,String>>> getNASocialFactorGraph(
			HttpServletRequest request, @RequestParam Map<String,String> allParams) {
		logger.info("Invoke getNASocialFactorGraph()- START ");
		List<Object[]> resultList = predictiveDAOImpl.getNASocialFactorData(allParams);			
		
		LinkedHashMap<String, List<HashMap<String, String>>> socialFactorGraphMap = new LinkedHashMap<String, List<HashMap<String, String>>>();
		List<HashMap<String, String>> forecastInfoList = new ArrayList<HashMap<String, String>>();
		String productId = null;
		int count = 0;
		HashMap<String, String> productIdAndNameMap = null;
		HashMap<String, String> subCategoryIdAndNameMap = null;
		if (allParams.get("product") != null && !(allParams.get("product").equals("") || allParams.get("product").equals(" "))) {
			HashMap<String, String> productParamMap = predictiveService.getInputParamMap(
					allParams, "product");
			productIdAndNameMap = predictiveService.prodIdAndNameMap(new ArrayList<String>(productParamMap.keySet()));
		}
		if (allParams.get("cat") != null && !(allParams.get("cat").equals("") || allParams.get("cat").equals(" "))) {
			HashMap<String, String> categoryParamMap = predictiveService.getInputParamMap(
					allParams, "cat");
			subCategoryIdAndNameMap = predictiveService.subCategoryIdAndNameMapByCategory(new ArrayList<String>(categoryParamMap.keySet()));
			
		}
		if (allParams.get("subcatid") != null && !(allParams.get("subcatid").equals("") || allParams.get("subcatid").equals(" "))) {
			HashMap<String, String> subCategoryParamMap = predictiveService.getInputParamMap(
					allParams, "subcatid");
			productIdAndNameMap = predictiveService.prodIdAndNameMapBySubCategory(new ArrayList<String>(subCategoryParamMap.keySet()));
		}
		/*HashMap<String, String> productIdAndNameMap = new HashMap<String, String>();
		productIdAndNameMap.put("20711", "CAKE");
		productIdAndNameMap.put("40780", "SHOTS");*/
		for (Object[] resultListObject : resultList) {
			count++;
			if (productId == null
					|| (productId != null && !productId.equals(resultListObject[0].toString()))
					|| count == resultList.size()) {
				if (productId != null){
					String prodName = "";
					if (productIdAndNameMap != null && productIdAndNameMap.get(productId) != null) {
						prodName = productIdAndNameMap.get(productId);
					} else if (subCategoryIdAndNameMap!=null && subCategoryIdAndNameMap.get(productId) != null) {
						prodName = subCategoryIdAndNameMap.get(productId);
					} 
					socialFactorGraphMap.put(prodName,forecastInfoList);
				}
				productId = resultListObject[0].toString();
				forecastInfoList = new ArrayList<HashMap<String, String>>();
			}
			LinkedHashMap<String, String> qtyAndForecastQtyMap = new LinkedHashMap<String, String>();
			qtyAndForecastQtyMap.put("yearWeek",
					resultListObject[1] == null ? "0" : resultListObject[1].toString());
			qtyAndForecastQtyMap.put("date", resultListObject[4] == null ? "0"
					: resultListObject[4].toString());
			qtyAndForecastQtyMap.put("sentimental",
					resultListObject[2] == null ? "0" : resultListObject[2].toString());
			qtyAndForecastQtyMap.put("srs", resultListObject[3] == null ? "0"
					: resultListObject[3].toString());
			forecastInfoList.add(qtyAndForecastQtyMap);

		}
		logger.info("Invoke getNASocialFactorGraph()- END ");
		return socialFactorGraphMap;
	}	
	
	
	@RequestMapping(value="internalfactor", method= RequestMethod.GET)
    public LinkedHashMap<String, List<HashMap<String, String>>> getInternalGraphData(HttpServletRequest request, @RequestParam Map<String,String> allParams){
		
		logger.info("Invoke getNAInternalGraphData()- START ");
		LinkedHashMap<String, List<HashMap<String,String>>> internalNAMap = predictiveDAOImpl.getInternalNonAggregageData(allParams);
		logger.info("Invoke getNAInternalGraphData()- END ");
        return internalNAMap;
		
		/*Mallikarjuna work
		 * 
		 * logger.info("Invoke getInternalGraphData()- START ");
		LinkedHashMap<String, List<HashMap<String, String>>> internalMap = predictiveDAOImpl.getInternalFactorData(allParams); //ProdLocTimeByDay : rplus_prod_loc_time_day
		logger.info("Invoke getInternalGraphData()- END ");
        return internalMap;*/
    }
	
	@RequestMapping(value="internalfactorNA", method= RequestMethod.GET)
    public LinkedHashMap<String, List<HashMap<String,String>>> getNAInternalGraphData(HttpServletRequest request, @RequestParam Map<String,String> allParams){
		logger.info("Invoke getNAInternalGraphData()- START ");
		LinkedHashMap<String, List<HashMap<String,String>>> internalNAMap = predictiveDAOImpl.getInternalNonAggregageData(allParams);
		logger.info("Invoke getNAInternalGraphData()- END ");
        return internalNAMap;
    }
	
	@RequestMapping(value="external", method= RequestMethod.GET)
    public LinkedHashMap<String, List<HashMap<String, String>>> getExternalAggregateMap(HttpServletRequest request,@RequestParam Map<String,String> allParams){
		logger.info("Invoke getExternalAggregateMap()- START ");
		List<Object[]> resultList = predictiveDAOImpl.getExternalAggregateData(allParams); //ProdLocTimeByDay : rplus_prod_loc_time_day
		
		int totalLocation=PredictiveServiceImpl.tempLocation;
		System.out.println("No of Locations::::::::::::::External"+totalLocation);
		int totalProduct=PredictiveServiceImpl.tempProduct;
		System.out.println("No of Products::::::::::::::External"+totalProduct);
		
		LinkedHashMap<String, List<HashMap<String, String>>> externalMap = new LinkedHashMap<String, List<HashMap<String, String>>>();
		List<HashMap<String, String>> forecastInfoList = new ArrayList<HashMap<String, String>>();
		for (Object[] resultListObject : resultList) {
			// [0]res.yearWeek,[1]avg(res.maxTemp),[2]avg(res.minTemp),[3]avg(res.precipitation),[4]avg(res.feellikeTemp),[5]avg(res.solar),[6]res.calDay
			LinkedHashMap<String, String> externalInfoMap = new LinkedHashMap<String, String>();
			externalInfoMap.put("yearWeek", resultListObject[0].toString());
			externalInfoMap.put("maxTemp", resultListObject[1]!=null?resultListObject[1].toString():"0");
			externalInfoMap.put("minTemp", resultListObject[2]!=null?resultListObject[2].toString():"0");
			externalInfoMap.put("avgTemp",
					resultListObject[3]!=null?resultListObject[3].toString():"0");
			externalInfoMap.put("calDay",
					resultListObject[4].toString());
			/*
			externalInfoMap.put("precipitation",
					resultListObject[3]!=null?resultListObject[3].toString():"0");
			externalInfoMap.put("feelLikeTemp",
					resultListObject[4]!=null?resultListObject[4].toString():"0");
			externalInfoMap.put("solar",
					resultListObject[5]!=null?resultListObject[5].toString():"0");
			externalInfoMap.put("calDay",
					resultListObject[6].toString());
			externalInfoMap.put("wind",
					resultListObject[7].toString());
			externalInfoMap.put("humidity",
					resultListObject[8].toString());
			*/
			forecastInfoList.add(externalInfoMap);
			externalMap.put("Agg Data", forecastInfoList);
		}
		logger.info("Invoke getExternalAggregateMap()- END ");
		
		/*PredictiveServiceImpl.tempLocation=0;
		PredictiveServiceImpl.tempProduct=0;*/
		
		return externalMap;
    }
	
	@RequestMapping(value = "externalNA", method = RequestMethod.GET)
	public LinkedHashMap<String, List<HashMap<String,String>>> getNAExternalGraph(
			HttpServletRequest request, @RequestParam Map<String,String> allParams) {
		logger.info("Invoke getNAExternalGraph()- START ");
		List<Object[]> resultList = predictiveDAOImpl.getNAExternalData(allParams);			
		
		LinkedHashMap<String, List<HashMap<String, String>>> externalNAMap = new LinkedHashMap<String, List<HashMap<String, String>>>();
		List<HashMap<String, String>> externalInfoList = new ArrayList<HashMap<String, String>>();
		String locationTypeId = null;
		int count = 0;
		HashMap<String, String> regionIdAndNameMap = null;
		HashMap<String, String> stateIdAndNameMap = null;
		HashMap<String, String> districtIdAndNameMap = null;
		HashMap<String, String> cityIdAndNameMap = null;
		HashMap<String, String> locationIdAndNameMap = null;
		//HashMap<String, String> stateIdAndNameMap = null;
		
		if (allParams.get("con_id") != null && !(allParams.get("con_id").equals("") || allParams.get("con_id").equals(" "))) {
			HashMap<String, String> countryParamMap = predictiveService.getInputParamMap(
					allParams, "con_id");
			regionIdAndNameMap = predictiveService.regionIdAndNameMapByCountry(new ArrayList<String>(countryParamMap.keySet()));
		}
		if (allParams.get("regionId") != null && !(allParams.get("regionId").equals("") || allParams.get("regionId").equals(" "))) {
			HashMap<String, String> regionParamMap = predictiveService.getInputParamMap(
					allParams, "regionId");
			stateIdAndNameMap = predictiveService.stateIdAndNameMapByRegion(new ArrayList<String>(regionParamMap.keySet()));
		}
		if (allParams.get("state") != null && !(allParams.get("state").equals("") || allParams.get("state").equals(" "))) {
			HashMap<String, String> stateParamMap = predictiveService.getInputParamMap(
					allParams, "state");
			districtIdAndNameMap = predictiveService.districtIdAndNameByState(new ArrayList<String>(stateParamMap.keySet()));
		}
		if (allParams.get("district") != null && !(allParams.get("district").equals("") || allParams.get("district").equals(" "))) {
			HashMap<String, String> districtParamMap = predictiveService.getInputParamMap(
					allParams, "district");
			cityIdAndNameMap = predictiveService.cityIdAndNameByDistrict(new ArrayList<String>(districtParamMap.keySet()));
		}
		if (allParams.get("city") != null && !(allParams.get("city").equals("") || allParams.get("city").equals(" "))) {
			HashMap<String, String> cityParamMap = predictiveService.getInputParamMap(
					allParams, "city");
			locationIdAndNameMap = predictiveService.locationIdAndNameByCity(new ArrayList<String>(cityParamMap.keySet()));
		}
		if (allParams.get("location") != null && !(allParams.get("location").equals("") || allParams.get("location").equals(" "))) {
			HashMap<String, String> districtParamMap = predictiveService.getInputParamMap(
					allParams, "location");
			locationIdAndNameMap = predictiveService.locationIdAndNameBylocation(new ArrayList<String>(districtParamMap.keySet()));
		}
		/*HashMap<String, String> productIdAndNameMap = new HashMap<String, String>();
		productIdAndNameMap.put("20711", "CAKE");
		productIdAndNameMap.put("40780", "SHOTS");*/
		for (Object[] resultListObject : resultList) {
			count++;
			if (locationTypeId == null
					|| (locationTypeId != null && !locationTypeId.equals(resultListObject[0].toString()))
					|| count == resultList.size()) {
				if (locationTypeId != null){
					String locationTypeName = "";
					if (regionIdAndNameMap != null && regionIdAndNameMap.get(locationTypeId) != null) {
						locationTypeName = regionIdAndNameMap.get(locationTypeId);
					} else if (stateIdAndNameMap!=null && stateIdAndNameMap.get(locationTypeId) != null) {
						locationTypeName = stateIdAndNameMap.get(locationTypeId);
					} else if (districtIdAndNameMap!=null && districtIdAndNameMap.get(locationTypeId) != null) {
						locationTypeName = districtIdAndNameMap.get(locationTypeId);
					} else if (cityIdAndNameMap!=null && cityIdAndNameMap.get(locationTypeId) != null) {
						locationTypeName = cityIdAndNameMap.get(locationTypeId);
					} else if (locationIdAndNameMap!=null && locationIdAndNameMap.get(locationTypeId) != null) {
						locationTypeName = locationIdAndNameMap.get(locationTypeId);
					} 
					externalNAMap.put(locationTypeName,externalInfoList);
				}
				locationTypeId = resultListObject[0].toString();
				externalInfoList = new ArrayList<HashMap<String, String>>();
			}
			LinkedHashMap<String, String> infoMap = new LinkedHashMap<String, String>();
			// [0]res."+querySelectionId+",[1]res.year_week,[2]avg(res.max_temp),[3]avg(res.min_temp),[4]avg(res.precipitation),[5]avg(res.feellike_temp),[6]avg(res.solar),[7]res.calDay
			infoMap.put("yearWeek",
					resultListObject[1] == null ? "0" : resultListObject[1].toString());
			infoMap.put("maxTemp", resultListObject[2] == null ? "0" : resultListObject[2].toString());
			infoMap.put("minTemp", resultListObject[3] == null ? "0" : resultListObject[3].toString());
			infoMap.put("avgTemp", resultListObject[4] == null ? "0" : resultListObject[4].toString());
			infoMap.put("calDay", resultListObject[5] == null ? "0"  : resultListObject[5].toString());
			/*
			infoMap.put("precipitation", resultListObject[4] == null ? "0" : resultListObject[4].toString());
			infoMap.put("feelLikeTemp", resultListObject[5] == null ? "0" : resultListObject[5].toString());
			infoMap.put("solar", resultListObject[6] == null ? "0" : resultListObject[6].toString());
			infoMap.put("calDay", resultListObject[7] == null ? "0"
					: resultListObject[7].toString());
			infoMap.put("wind", resultListObject[8] == null ? "0"
					: resultListObject[8].toString());
			infoMap.put("humidity", resultListObject[9] == null ? "0"
					: resultListObject[9].toString());
			*/
			externalInfoList.add(infoMap);

		}
		logger.info("Invoke getNAExternalGraph()- END ");
		return externalNAMap;
	}
	
	/**
	  * This method will send all input params back 
	  * @param request
	  * @param allParams
	  * @return
	  */
	@RequestMapping(value = "/inputParams", method = RequestMethod.GET)
	public HashMap<String, HashMap<String, String>> getInputParams(
			HttpServletRequest request,@RequestParam Map<String, String> allParams) {
		logger.info("Invoke getInputParams()- START ");
		HashMap<String, HashMap<String, String>> inpurParamsMap = predictiveService
				.getAllInputParamsMap(allParams);
		logger.info("Invoke getInputParams()- END ");
		return inpurParamsMap;
	}
	
	/**
	  * This method will remove the selected session
	  * @param request
	  * @return void
	*/
	@RequestMapping(value = "/resetSelection", method = RequestMethod.GET)
	public void reset(HttpServletRequest request) {
		request.getSession().removeAttribute("forcastData");
		
	}
	
}
