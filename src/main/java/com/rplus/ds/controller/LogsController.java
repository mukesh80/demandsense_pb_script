package com.rplus.ds.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.UserLogDao;



/**
 * @author PRAVEEN REDDIVARI
 *
 */

@RestController
@RequestMapping("/logs")
public class LogsController {
	
	
	@Autowired
	UserLogDao LogDao;
	
	@Autowired
	HttpSession userSession;
	
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping(value = "/init", method = RequestMethod.POST)
	public HashMap<String, List> getLogsUsers(HttpServletRequest request){
		System.out.println("Init method in logs controller");
		List userlist;
		HashMap<String, List> result = new HashMap<String, List>();
		if(request.getSession().getAttribute("userType").equals("super_admin")){
			userlist = LogDao.getUserList();
			result.put("admin", userlist);
		}else{
			userlist = LogDao.getUserLogs((String) userSession.getAttribute("loggedUser"));
			
			result.put("user", userlist);
		}
		
		
		return result;
		
	}
	
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping(value = "/userlog", method = RequestMethod.POST)
	public HashMap<String, List> getUserLogs(@RequestParam String userName, HttpServletRequest request){
		System.out.println("Init method in logs controller");
		List userlist;
		HashMap<String, List> result = new HashMap<String, List>();
		if(userName != null){
			userlist = LogDao.getUserLogs(userName);
			
			result.put("user", userlist);
		}
		
		
		return result;
		
	}

}
