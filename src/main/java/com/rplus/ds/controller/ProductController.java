package com.rplus.ds.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rplus.ds.dao.DimProductDAOImpl;
import com.rplus.ds.model.DimProduct;

/**
 * Created by freakster on 24/7/15.
 */

@RestController
@RequestMapping("/Products")
public class ProductController {
	private static final Logger logger = Logger.getLogger(ProductController.class);

    @Autowired
    DimProductDAOImpl dimProductDAO;

    @RequestMapping(value="products", method= RequestMethod.GET)
    public List<DimProduct> getProducts()
    {   logger.info(" ProductController: getProducts START");
        List<DimProduct> productList = dimProductDAO.listOfProducts();
        logger.info(" ProductController: getProducts END");
        return productList;
    }

    /**
     * This mapping is used to get categorys list based on product
     * @param request
     * @param productId
     * @return
     */
    @RequestMapping(value="categorys", method= RequestMethod.GET)
    public  HashMap<String,String> getCategorys(HttpServletRequest request)
            
    {   logger.info(" ProductController: getCategorys START");
        HashMap<String,String> categoryMap = dimProductDAO.getCategorysList();
        logger.info(" ProductController: getCategorys END");
        return categoryMap;
    }
    
    /**
     * This mapping is used to get subcategorys list based on category
     * @param request
     * @param categoryId
     * @return
     */
    @RequestMapping(value="subCategorysByCategory", method= RequestMethod.GET)
    public HashMap<String,String> getSubCategorysByCategory(HttpServletRequest request,
            @RequestParam String categoryId)
    {   logger.info(" ProductController: getSubCategorysByCategory START");
    	HashMap<String,String> subCategeryMap= dimProductDAO.getSubCategorysByCategory(categoryId);
    	logger.info(" ProductController: getSubCategorysByCategory END");
        return subCategeryMap;
    }
    
    /**
     * This mapping is used to get the product list based on subcategory
     * @param request
     * @param subCategoryId
     * @return
     */
    @RequestMapping(value="productsBySubCategory", method= RequestMethod.GET)
    public HashMap<String,String> getProductsBySubCategory(HttpServletRequest request,
            @RequestParam String subCategoryId)
    {   logger.info(" ProductController: getProductsBySubCategory START");
    	HashMap<String,String> productsMap= dimProductDAO.getProductsBySubCategory(subCategoryId);
    	logger.info(" ProductController: getProductsBySubCategory END");
        return productsMap;
    }
    
    /**
     * This method is used to get all the available products
     * @return
     */
    @RequestMapping(value="allProducts", method= RequestMethod.GET)
    public HashMap<String,String> getAllProducts(){
    	logger.info(" ProductController: getAllProducts START");
    	HashMap<String,String> productsMap= dimProductDAO.getAllProducts();
    	logger.info(" ProductController: getAllProducts END");
        return productsMap;
    }
    
    /**
     * This method is used to get all the available sub categorys
     * @return
     */
    @RequestMapping(value="allSubCategorys", method= RequestMethod.GET)
    public HashMap<String,String> getAllSubCategorys(){
    	logger.info(" ProductController: getAllSubCategorys START");
    	HashMap<String,String> productsMap= dimProductDAO.getAllSubCategeorys();
    	logger.info(" ProductController: getAllSubCategorys END");
        return productsMap;
    }

}
