package com.rplus.ds.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.rplus.ds.dao.DataExploreDAO;
import com.rplus.ds.service.DataExplorerService;
import com.rplus.ds.service.ThreadServices;

/**
 * 
 * @author Jerald Joseph. J
 *
 */
@Controller
@RequestMapping("/rAdmin")
public class RJavaController {
	private static final Logger logger = Logger.getLogger(RJavaController.class);

	/**
	 * Autowired ThreadService Interface instance 
	 */
	@Autowired
	ThreadServices threadService;
	
	/**
	 * Autowired DataExploreDAO Interface instance 
	 */
	@Autowired
	DataExploreDAO deDAO;
	
	/**
	 * Autowired DataExploreService Interface instance 
	 */
	@Autowired
	DataExplorerService deService;
	/**
	 * This method is used to get the "rJava Admin page"   
	 * @param 
	 * @return String
	 * @url http://localhost:8080/DemandSense/rAdmin/rJavaAdmin 
	 */
	@RequestMapping(value = "/rJavaAdmin", method = RequestMethod.GET)
	public String getrJavaAdmin(HttpServletRequest req, Model map) {
		logger.info("RJavaController : getrJavaAdmin() ");
		System.out.println("rJavaController : getrJavaAdmin() ");
		//String retVal = "index"; // rJavaThread
		String retVal = "rJavaThread";  
		if(threadService.readThreadSettings(req, map)) 
			retVal = "rJavaThread";
		return retVal;
	}
	
	/**
	 * This method is used to do the "rJava MultiThread Process" for weeks|day  
	 * @param request, MultipartFile
	 * @return boolean String {"true"|"false"}
	 * @url http://localhost:8080/DemandSense/rAdmin/rJavaMultiThreadProc 
	 */
	@RequestMapping(value = "/rJavaMultiThreadProc", method = RequestMethod.POST)
	@ResponseBody
	public String rJavaMultiThreadProc(HttpServletRequest req,  @RequestParam("rFile") MultipartFile file) {
		String retVal = "false";
		logger.info("rJava - rJavaMultiThreadProc() ");
		System.out.println("rJava - rJavaMultiThreadProc() ");
		if(threadService.uploadNewRFile(req, file)==true) 
			retVal = "true";
		return retVal;
	}
	
	/**
	 * This method is used to do the "rJava Admin Save" for weeks|day  
	 * @param request
	 * @return boolean String
	 * @url http://localhost:8080/DemandSense/rAdmin/rJavaMultiThreadSave 
	 */
	@RequestMapping(value = "/rJavaMultiThreadSave", method = RequestMethod.POST)
	@ResponseBody
	public String rJavaMultiThreadSave(HttpServletRequest req) {
		String retVal = "false";
		System.out.println("rJavaController - rJavaMultiThreadSave()");
		logger.info("rJavaController - rJavaMultiThreadSave() ");
		if(threadService.saveThreadSettings(req)==true) 
			retVal = "true";
		return retVal;
	}
	
	/**
	 * This method is used to do the "rJava Admin Thread Stop" for weeks|day  
	 * @param request
	 * @return boolean String
	 * @url http://localhost:8080/DemandSense/rAdmin/rJavaThreadStop
	 */
	@RequestMapping(value = "/rJavaThreadStop", method = RequestMethod.POST)
	@ResponseBody
	public String rJavaThreadStop(HttpServletRequest req) {
		String retVal = "false";
		System.out.println("rJavaController - rJavaThreadStop()");
		logger.info("rJavaController - rJavaThreadStop() ");
		if(threadService.stopThreadOperation(req)==true) 
			retVal = "true";
		return retVal;
	}
	
	@RequestMapping(value = "/forecostDataExplore", method = RequestMethod.GET)
	public void forecostDataExplore(HttpServletRequest req, HttpServletResponse res, @RequestParam Map<String,String> allParams) {
		String retVal = "false";
		logger.info("Controller - forecostDataExplore() ");
		
		List<Object[]> lst = deService.getForecostData(allParams);
		
		try	{
			PrintWriter pw = res.getWriter();
			pw.write("<table id=\"example\" class=\"display\" cellspacing=\"0\" width=\"100%\" class=\"table-responsive\" border=\"1\">");
			pw.write("<thead><tr><th>cityId</th><th>categoryId</th><th>locationId</th><th>finalQty</th><th>productId</th>");
			pw.write("<th>subCategoryId</th><th>bestModel</th><th>bestMeanValue</th><th>bestMaxValue</th><th>bestMinValue</th></tr></thead>");
			for(Object[] obj: lst) {
				float flt = Float.parseFloat(obj[3].toString());
				pw.write("\r\n<tr><td>" + obj[0].toString() + "</td><td>" + obj[1].toString() + "</td><td>" + obj[2].toString() + "</td><td>" + String.format("%.02f", flt) + "</td>");
				flt = Float.parseFloat(obj[7].toString());
				pw.write("<td>" + obj[4].toString() + "</td><td>" + obj[5].toString() + "</td><td>" + obj[6].toString() + "</td><td>" + String.format("%.02f", flt) + "</td>");
				flt = Float.parseFloat(obj[8].toString());
				float flt1 = Float.parseFloat(obj[9].toString());
				pw.write("<td>" + String.format("%.02f", flt) + "</td><td>" + String.format("%.02f", flt1) + "</td><td></tr>");
			}
			pw.write("</table>");
			pw.close();
		}
		catch(IOException ioEx) {
			System.out.println(ioEx.getMessage());
		}
	}
	
	@RequestMapping(value = "/significantDataExplore", method = RequestMethod.GET)
	public void significantDataExplore(HttpServletRequest req, HttpServletResponse res, @RequestParam Map<String,String> allParams) {
		String retVal = "false";
		logger.info("Controller - significantDataExplore() ");
		System.out.println("Controller - significantDataExploreDataExplore() ");
		//List<Object[]> lst = deDAO.getSignificantData200();
		List<Object[]> lst = deService.getSignificantData(allParams);
		try	{
			PrintWriter pw = res.getWriter();
			pw.write("<table id=\"example1\" class=\"display\" cellspacing=\"0\" width=\"100%\" class=\"table-responsive\" border=\"1\">");
			pw.write("<thead><tr><th>Country ID</th><th>Location ID</th><th>Region ID</th><th>State ID</th><th>District ID</th>");
			pw.write("<th>City ID</th><th>Product ID</th><th>Category ID</th><th>Sub category ID</th><th>Significant Factors</th><th>Correlation</th></tr></thead>");
			for(Object[] obj: lst) {
				pw.write("\r\n<tr><td>" + obj[0].toString() + "</td><td>" + obj[1].toString() + "</td><td>" + obj[2].toString() + "</td><td>" + obj[3].toString() + "</td>");
				pw.write("<td>" + obj[4].toString() + "</td><td>" + obj[5].toString() + "</td><td>" + obj[6].toString() + "</td><td>" + obj[7].toString() + "</td>");
				pw.write("<td>" + obj[8].toString() + "</td><td>" + obj[9].toString() + "</td><td>" + obj[10].toString() + "</td></tr>");
				//pw.write("<td>" + obj[12].toString() + "</td><td>" + obj[13].toString() + "</td><td>" + obj[14].toString() + "</td><td></tr>" );
			}
			pw.write("</table>");
			pw.close();
		}
		catch(IOException ioEx) {
			System.out.println(ioEx.getMessage());
		}
	}
	
	@RequestMapping(value = "/accuracyDataExplore", method = RequestMethod.GET)
	public void accuracyDataExplore(HttpServletRequest req, HttpServletResponse res, @RequestParam Map<String,String> allParams) {
		String retVal = "false";
		logger.info("Controller - accuracyDataExplore() ");
		System.out.println("Controller - accuracyDataExplore() ");
		//List<Object[]> lst = deDAO.getAccuracyData200();
		List<Object[]> lst = deService.getAccuracyData(allParams);
		try	{
			PrintWriter pw = res.getWriter();
			pw.write("<table id=\"example2\" class=\"display\" cellspacing=\"0\" width=\"100%\" class=\"table-responsive\" border=\"1\">");
			pw.write("<thead><tr><th>Country ID</th><th>Location ID</th><th>Region ID</th><th>State ID</th><th>District ID</th>");
			pw.write("<th>City ID</th><th>Product ID</th><th>Category ID</th><th>Sub category ID</th><th>Model</th><th>RMSE</th></tr></thead>");
			for(Object[] obj: lst) {
				pw.write("\r\n<tr><td>" + obj[0].toString() + "</td><td>" + obj[1].toString() + "</td><td>" + obj[2].toString() + "</td><td>" + obj[3].toString() + "</td>");
				pw.write("<td>" + obj[4].toString() + "</td><td>" + obj[5].toString() + "</td><td>" + obj[6].toString() + "</td><td>" + obj[7].toString() + "</td>");
				pw.write("<td>" + obj[8].toString() + "</td><td>" + obj[9].toString() + "</td><td>" + obj[10].toString() + "</td></tr>");
				//pw.write("<td>" + obj[12].toString() + "</td><td>" + obj[13].toString() + "</td><td>" + obj[14].toString() + "</td><td></tr>" );
			}
			pw.write("</table>");
			pw.close();
		}
		catch(IOException ioEx) {
			System.out.println(ioEx.getMessage());
		}
	}
}
